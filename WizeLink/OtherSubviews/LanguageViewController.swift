//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit

struct LanguageType{
    var buttonChecked: UIImageView!
    var language_type: String = ""
    var version_type: String = ""
    var isSelected: Bool = false
}
/**
 語言頁
 - Date: 08/25 Yang
*/
class LanguageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        //        let location = sender.location(in: nil)
        //        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    @IBOutlet weak var recognizer: UISwipeGestureRecognizer!
    @IBAction func swipe(_ sender: UISwipeGestureRecognizer) {
        //        let point = self.view.center
        //        if recognizer.direction == .right {
        //            if point.x <= fullScreenSize.width - 150 {
        //                self.view.center = CGPoint(
        //                    x: self.view.center.x + 100,
        //                    y: self.view.center.y)
        //            } else {
        //                self.view.center = CGPoint(
        //                    x: fullScreenSize.width - 50,
        //                    y: self.view.center.y)
        //            }
        //        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        ///第一列與第二列移除分隔線 其他列顯示分隔線
        if (indexPath.row == 0 || indexPath.row == 1) {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        }else{
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = LanguageTableView.indexPathForSelectedRow{
            if indexPath.row == 0 || indexPath.row == 1{
//                LanguageTableView.deselectRow(at: indexPath, animated: true)
            }else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 {
                ///點選到的語言
                types[indexPath.row].isSelected = true
                selectedIndexPath = indexPath
                LanguageTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "LanguageTypeCell")
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].language_type
        cell.textLabel?.textAlignment = .left
        switch currentDevice.deviceType {
        case .iPad:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 22)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        default:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        }
        if indexPath.row == 0 || indexPath.row == 1 {
            if indexPath.row == 0{
                ///更新後的語言顯示
                if let languageType = UserDefaults.standard.object(forKey:"languageType") as? String {
                    cell.detailTextLabel?.text = languageType
                    cell.detailTextLabel?.textAlignment = NSTextAlignment.center
                    cell.detailTextLabel?.textColor = PrimaryColor2
                }
                cell.textLabel?.textColor = .black
            }else if indexPath.row == 1{
                cell.textLabel?.textColor = .gray
                cell.backgroundColor = TextColor2
            }
//            LanguageTableView.rowHeight = fullScreenSize.height*0.065
        }else{ //indexPath.row == 2....8 語言選擇列
            if types[indexPath.row].isSelected == false{
                cell.textLabel?.textColor = .black
                types[indexPath.row].buttonChecked = UIImageView(image: UIImage(named: "button_unchecked")?.withRenderingMode(.alwaysTemplate))
                cell.imageView?.image = types[indexPath.row].buttonChecked.image?.maskWithColor(color: .gray)
            }else{
                cell.textLabel?.textColor = PrimaryColor2
                types[indexPath.row].buttonChecked = UIImageView(image: UIImage(named: "button_checked")?.withRenderingMode(.alwaysTemplate))
                cell.imageView?.image = types[indexPath.row].buttonChecked.image?.maskWithColor(color: PrimaryColor2)
            }
            ///壓縮cell左側icon大小
            let itemSize = CGSize.init(width: 20, height: 20)
            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
            let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
            cell.imageView?.image!.draw(in: imageRect)
            cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
        }
        types[indexPath.row].isSelected = false
        return cell
    }
    
    func setTableView(){
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        LanguageTableView = UITableView()
        LanguageTableView.register(UITableViewCell.self, forCellReuseIdentifier: "LanguageTypeCell")
        LanguageTableView.delegate = self
        LanguageTableView.dataSource = self
        LanguageTableView.bounces = false
        LanguageTableView.allowsSelection = true
        LanguageTableView.backgroundColor = .white
        LanguageTableView.rowHeight = fullScreenSize.height*0.065
        self.view.addSubview(LanguageTableView)
        LanguageTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(navigationBarHeight)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(LanguageTableView.rowHeight*9)
            make.centerX.equalToSuperview()
        }
    }
    
    var types = [
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "")), language_type: LocalizeUtils.shared.localized(withKey: "CURRENT_LANGUAGE", withTableName: localTable.dataTitle.rawValue), version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "")), language_type: LocalizeUtils.shared.localized(withKey: "LANGUAGE_CHOOSE", withTableName: localTable.headerTitle.rawValue), version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "English", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Español", version_type: "",isSelected: false),
        LanguageType(buttonChecked:UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Português", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Deutsch", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "日本語", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "繁體中文", version_type: "",isSelected: false),
        LanguageType(buttonChecked:UIImageView(image: UIImage(named: "button_unchecked")), language_type: "简体中文", version_type: "",isSelected: false)
    ]
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkView = UIImageView()
    let backButton = UIButton()
    let changeButton = UIButton()
    private var LanguageTableView: UITableView!
    var selectedIndexPath: IndexPath! = nil
    weak var LanguageDelegate: LanguageDelegate?
    /**
     讀取上次選到的語言檔案 並自動讓字體顯示綠色
     若從未選取 就依照裝置預設的語系去顯示當前語言
     - Date: 08/26 Yang
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            if let dataValue = UserDefaults.standard.object(forKey: "selectedIndexPath") as? Data , let indexPath = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(dataValue) as? IndexPath {
                print("@viewDidLoad dataValue:\(dataValue), indexPath:\(indexPath)")
                types[indexPath.row].isSelected = true
                selectedIndexPath = indexPath
            }else{
                var r : Int = 2
                switch bundleLocalizeFileName {
                case "en":
                    r = 2
                case "es":
                    r = 3
                case "pt":
                    r = 4
                case "de":
                    r = 5
                case "ja":
                    r = 6
                case "zh-Hant":
                    r = 7
                case "zh-Hans":
                    r = 8
                default:
                    r = 2
                }
                types[r].isSelected = true
            }
        }
        catch let error as NSError {
            print(error)
        }
        self.setViews()
    }
    
    func setViews(){
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        navBarLabel.backgroundColor = .clear
        navBarLabel.attributedText = NSAttributedString(string: LocalizeUtils.shared.localized(withKey: "LANGUAGE", withTableName: localTable.pageTitle.rawValue), attributes:
        nil)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1))
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        setTableView()
        
        changeButton.backgroundColor = SecondaryColor3
        changeButton.layer.cornerRadius = 21
        changeButton.setTitle(LocalizeUtils.shared.localized(withKey: "APPLY", withTableName: localTable.button.rawValue), for: [])
        changeButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        changeButton.titleLabel?.textColor = .white
        changeButton.isEnabled = true
        changeButton.addTarget(self,action: #selector(self.clickChangeButton),for: .touchUpInside)
        self.view.addSubview(changeButton)
        changeButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(LanguageTableView.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width*0.48)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        switch currentDevice.deviceType {
        case .iPhone35:
            changeButton.layer.cornerRadius = 17
        case .iPhone40:
            changeButton.layer.cornerRadius = 17
        case .iPhone47:
            changeButton.layer.cornerRadius = 20
        case .iPhone55:
            changeButton.layer.cornerRadius = 22
        case .iPhone60:
            changeButton.layer.cornerRadius = 24
        case .iPad:
            changeButton.layer.cornerRadius = 30
        default:
            changeButton.layer.cornerRadius = 24
        }
    }
    
    //For Test
    func reloadString(){
        self.types[0].language_type = LocalizeUtils.shared.localized(withKey: "CURRENT_LANGUAGE", withTableName: localTable.dataTitle.rawValue)
        self.types[1].language_type = LocalizeUtils.shared.localized(withKey: "LANGUAGE_CHOOSE", withTableName: localTable.headerTitle.rawValue)
        self.navBarLabel.attributedText = NSAttributedString(string: LocalizeUtils.shared.localized(withKey: "LANGUAGE", withTableName: localTable.pageTitle.rawValue), attributes:
            nil)
        self.changeButton.setTitle(LocalizeUtils.shared.localized(withKey: "APPLY", withTableName: localTable.button.rawValue), for: [])
    }
    /**
     將點選到的語言存檔 下次進來此頁面直接顯示
     - Date: 08/26 Yang
    */
    @objc func clickChangeButton(_ sender: UIButton){
        
        if let indexPath = selectedIndexPath {
            print(selectedIndexPath.row)
            // 2-8
            switch indexPath.row {
            case 2:
                bundleLocalizeFileName = "en"
            case 3:
                bundleLocalizeFileName = "es"
            case 4:
                bundleLocalizeFileName = "pt"
            case 5:
                bundleLocalizeFileName = "de"
            case 6:
                bundleLocalizeFileName = "ja"
            case 7:
                bundleLocalizeFileName = "zh-Hant"
            case 8:
                bundleLocalizeFileName = "zh-Hans"
            default:
                print("clickChangeButton error")
                bundleLocalizeFileName = "en"
            }
            //            print("bundleLocalizeFileName now :\(bundleLocalizeFileName)")
            //TODO
            //設計一個方法來 reload All app's String
            //TEST
            self.reloadString()
            LanguageDelegate?.notiftNewLanguage()
            if LanguageDelegate == nil{
                print("no LanguageDelegate")
            }
            
            let selectedCell:UITableViewCell = LanguageTableView.cellForRow(at: indexPath)!
            selectedCell.textLabel?.textColor = PrimaryColor2
            types[indexPath.row].buttonChecked = UIImageView(image: UIImage(named: "button_checked")?.withRenderingMode(.alwaysTemplate))
            selectedCell.imageView?.image = types[indexPath.row].buttonChecked.image?.maskWithColor(color: PrimaryColor2)
            let itemSize = CGSize.init(width: 20, height: 20)
            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
            let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
            selectedCell.imageView?.image!.draw(in: imageRect)
            selectedCell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            
            languageType = types[indexPath.row].language_type
            UserDefaults.standard.set(languageType, forKey:"languageType")
            let indexPath2 = IndexPath(item: 0, section: 0)
            let indexPath3 = IndexPath(item: 1, section: 0)
            LanguageTableView.reloadRows(at: [indexPath2,indexPath3], with: .automatic)
            
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: indexPath , requiringSecureCoding: false)
                UserDefaults.standard.set(data, forKey: "selectedIndexPath")
            }
            catch let error as NSError {
                print(error)
            }
        }
    }
    
    @objc func backVC(){
        selectedIndexPath = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
