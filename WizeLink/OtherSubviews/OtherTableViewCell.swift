////
////  OtherTableViewCell.swift
////  WizeLink
////
////  Created by 洋洋 on 2020/6/5.
////  Copyright © 2020 YangYang. All rights reserved.
////
//
//import UIKit
//import SnapKit
//
//class OtherTableViewCell: UITableViewCell {
//
//    var detailButton = UIButton()
//
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//
//        detailButton.backgroundColor = .clear
//        contentView.addSubview(detailButton)
//        detailButton.snp.makeConstraints { (makes) in
//            makes.edges.equalToSuperview()
//        }
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//
//}
