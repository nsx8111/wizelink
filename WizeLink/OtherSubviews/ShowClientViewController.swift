////
////  AdvancedSetViewController.swift
////  WizeLink
////
////  Created by 洋洋 on 2020/4/7.
////  Copyright © 2020 YangYang. All rights reserved.
////


import UIKit
import WebKit
import SnapKit
/**
 顯示用戶webView
 - Date: 08/25 Yang
*/
class ShowClientViewController: UIViewController,WKUIDelegate,WKNavigationDelegate{
    let webView = WKWebView()
    let navigationBarView = UIImageView()
    let backButton = UIButton()
    let goBackBtn = UIButton()
    let goForwardBtn = UIButton()
    

    func configureView() {
        let url = URL(string: "\(machineUrl)dhcptbl.htm")
        let request = URLRequest(url: url!)
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        webView.contentMode = .center
        view.addSubview(webView)
        webView.snp.makeConstraints { (makes) in
            makes.top.left.right.equalToSuperview()
            makes.bottom.equalTo(self.navigationBarView.snp.top)
        }
        webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
        challenge.sender?.use(credential, for: challenge)
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        configureView()
//        getData{ self.searchInfo() }
//        self.searchInfo()
    }
    /**
     判斷是否離線 離線則彈出alert提示
     - Date: 08/25 Yang
    */
    func searchInfo(){
        let IpValue = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
        print("IpValue \(IpValue)")
        if IpValue == "" || IpValue == "0.0.0.0" {
            alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
        }
    }
    
    func setViews(){
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = TextColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.bottom.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight/1.5)
            makes.left.equalToSuperview().offset(0)
        }

        backButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+3)
        backButton.setTitle("back", for: .normal)
        backButton.setTitleColor(SecondaryColor4, for: .normal)
        backButton.backgroundColor = .clear
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.equalTo(navigationBarHeight/1.6)
            makes.height.equalTo(navigationBarHeight/4)
            makes.left.equalToSuperview().offset(spacing2+2)
        }

        goForwardBtn.setImage(UIImage(named: "S__33603593"), for: .normal)
        goForwardBtn.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(goForwardBtn)
        goForwardBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.equalTo(navigationBarHeight/3.2)
            makes.height.equalTo(navigationBarHeight/3.5)
            makes.right.equalToSuperview().offset(-spacing3)
        }

        goBackBtn.setImage(UIImage(named: "S__33603591"), for: .normal)
        goBackBtn.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(goBackBtn)
        goBackBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.height.equalTo(goForwardBtn)
            makes.right.equalTo(goForwardBtn.snp.left).offset(-spacing2*2)
        }
        
    }
    /**
     底下navigationBar箭頭可控制webView的上下頁切換瀏覽
     - Date: 08/25 Yang
    */
    @objc func onBackButton_Clicked(sender: UIButton) {
        user = "admin"
        password = "admin"

        if sender == backButton{
            self.webView.reload()
            self.navigationController?.popViewController(animated: true)
        }else if sender == goBackBtn{
            if(webView.canGoBack) {
                webView.goBack()
            }
        }else{
            if(webView.canGoForward) {
                webView.goForward()
            }
            
        }

    }

}


//import UIKit
//import WebKit
//import SnapKit
//
//class ShowClientViewController: UIViewController,WKUIDelegate {
//    let navigationBarView = UIImageView()
//    let backButton = UIButton()
//    var webView: WKWebView!
//
//    override func loadView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webView = WKWebView(frame: .zero, configuration: webConfiguration)
//        webView.uiDelegate = self
//        view = webView
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setViews()
//        let myURL = URL(string: "http://192.168.10.1/dhcptbl.htm")
//        let myRequest = URLRequest(url: myURL!)
//        self.webView.load(myRequest)
//
//    }
//
//    func setViews(){
//          let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
//              self.navigationController!.navigationBar.frame.height
//          navigationBarView.backgroundColor = TextColor1
//          self.view.addSubview(navigationBarView)
//          navigationBarView.snp.makeConstraints { (makes) in
//              makes.bottom.equalToSuperview().offset(0)
//              makes.width.equalTo(fullScreenSize.width)
//              makes.height.equalTo(navigationBarHeight/1.5)
//              makes.left.equalToSuperview().offset(0)
//          }
//
//          backButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+3)
//          backButton.setTitle("back", for: .normal)
//          backButton.setTitleColor(SecondaryColor4, for: .normal)
//          backButton.backgroundColor = .clear
//          backButton.isEnabled = true
//          backButton.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
//          self.view.addSubview(backButton)
//          backButton.snp.makeConstraints { (makes) in
//              makes.centerY.equalTo(navigationBarView)
//              makes.width.equalTo(navigationBarHeight/1.6)
//              makes.height.equalTo(navigationBarHeight/4)
//              makes.left.equalToSuperview().offset(10+2)
//          }
//
//      }
//
//    @objc func onBackButton_Clicked(sender: UIButton)
//    {
//        if(webView.canGoBack) {
//            webView.goBack()
//        } else {
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
//
//
//    /*
//     // MARK: - Navigation
//
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destination.
//     // Pass the selected object to the new view controller.
//     }
//     */
//
//}
