//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
/**
 註冊雲端帳號頁
 - Date: 08/25 Yang
*/
class SignUpCloudViewController: UIViewController,UITextFieldDelegate {
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView = UIImageView()
    let backButton = UIButton()
    let passwordLabel = UILabel()
    let passwordTextField = CustomTextField3()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = CustomTextField3()
    let emailLabel = UILabel()
    let emailTextField = UITextField()
    let signUpButton = UIButton()
    let rightViewBtn = UIButton(type: .custom)
    let rightViewBtn2 = UIButton(type: .custom)
    let bkView = UIView()
    let bkView2 = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        hideKeyboardWhenTappedAround()
    }
    
    func setViews(){
        bkgImageView.backgroundColor = .white
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        navBarLabel.backgroundColor = .clear
        navBarLabel.text = localString(key: "REGISTER_CLOUD", table: .pageTitle)
        navBarLabel.numberOfLines = 0
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.centerY.equalTo(navigationBarView).offset(statusBarHeight/2.5)
            makes.left.greaterThanOrEqualTo(backButton.snp.right).offset(spacing1/1)
        }
        
        bkView.backgroundColor = .lightGray
        bkView.layer.cornerRadius = 8
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview().inset(spacing3)
            makes.top.equalTo(navigationBarView.snp.bottom).offset(spacing3)
        }
        
        bkView2.backgroundColor = .white
        bkView2.layer.cornerRadius = 8
        self.view.addSubview(bkView2)
        bkView2.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView).inset(0.5)
        }
        
        emailLabel.backgroundColor = .clear
        emailLabel.text = localString(key: "EMAIL_ADDRESS", table: .inputTitle)
        emailLabel.font = UIFont.systemFont(ofSize: labelFontSize+1)
        emailLabel.textColor = PrimaryColor2
        emailLabel.textAlignment = .left
        self.view.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(bkView2).offset(spacing3)
            makes.left.right.equalTo(bkView2).inset(spacing3)
        }
        
        emailTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        emailTextField.textAlignment = .left
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .black
        emailTextField.backgroundColor = TextFieldColor
        emailTextField.layer.cornerRadius = 5.0
        emailTextField.keyboardType = .emailAddress
        emailTextField.delegate = self
        self.view.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailLabel.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = localString(key: "PASSWORD", table: .inputTitle)
        passwordLabel.font = UIFont.systemFont(ofSize: labelFontSize+1)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(spacing3)
            makes.width.height.left.equalTo(emailLabel)
        }
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = TextFieldColor
        passwordTextField.layer.cornerRadius = 5.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
        passwordLabel2.text = localString(key: "ENTER_PASSWORD_AGAIN", table: .inputTitle)
        passwordLabel2.font = UIFont.systemFont(ofSize: labelFontSize+1)
        passwordLabel2.textColor = PrimaryColor2
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing3)
            makes.width.height.left.equalTo(emailLabel)
        }
        
        passwordTextField2.isSecureTextEntry = true
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .black
        passwordTextField2.backgroundColor = TextFieldColor
        passwordTextField2.layer.cornerRadius = 5.0
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
            makes.bottom.equalTo(bkView2.snp.bottom).offset(-spacing1)
        }
        
        signUpButton.backgroundColor = SecondaryColor3
        signUpButton.layer.cornerRadius = 21
        signUpButton.setTitle(localString(key: "REGISTER", table: .button), for: [])
        signUpButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        signUpButton.titleLabel?.textColor = .white
        signUpButton.isEnabled = true
        signUpButton.addTarget(self,action: #selector(self.clickButton),for: .touchUpInside)
        self.view.addSubview(signUpButton)
        signUpButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(bkView.snp.bottom).offset(spacing3*2)
            makes.width.equalTo(fullScreenSize.width*0.45)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            signUpButton.layer.cornerRadius = 17
        case .iPhone40:
            signUpButton.layer.cornerRadius = 17
        case .iPhone47:
            signUpButton.layer.cornerRadius = 20
        case .iPhone55:
            signUpButton.layer.cornerRadius = 22
        case .iPhone60:
            signUpButton.layer.cornerRadius = 24
        case .iPad:
            signUpButton.layer.cornerRadius = 30
        default:
            signUpButton.layer.cornerRadius = 24
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
            rightViewBtn2.addTarget(self,action: #selector(self.Security_Click2),for: .touchUpInside)
            rightViewBtn2.setImage(smallImage, for: .normal)
            passwordTextField2.rightView = rightViewBtn2
            passwordTextField2.rightViewMode = .always
        }
    }
    
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    @objc func Security_Click2() {
        if passwordTextField2.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = true
        }
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
    }
    
    @objc func clickButton(){
        checkDataFormat()
        emailStr = emailTextField.text!
        passStr = passwordTextField.text!
        cloudRegister {
            print("cloudRegister done")
            if cloudRegResult{
                print("reg ok")
                let controller = UIAlertController(title: localString(key: "ACCOUNT_REGISTRATION", table: .alertTitle), message: localString(key: "ACTIVATE_ACCOUNT", table: .alertMsg), preferredStyle: .alert)
                let okAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default) { (_) in
                    print("啟用此帳號")
                    self.tryActiveAccount()
                }
                controller.addAction(okAction)
                let cancelAction = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
                controller.addAction(cancelAction)
                self.present(controller, animated: true, completion: nil)
            }else{
                print("reg error")
                self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: cloundErrorMsg)
            }
        }
        //        print("emailTextField.text:\(emailTextField.text)")
        //        print("passwordTextField.text:\(passwordTextField.text)")
        //        print("passwordTextField2.text:\(passwordTextField2.text)")
    }
    
    func tryActiveAccount(){
        print("tryActiveAccount")
        cloudActiveAccount {
            print("cloudActiveAccount done")
            if cloudRegResult{
                print("ActiveAccount ok")
                
                let controller = UIAlertController(title: localString(key: "ACCOUNT_ACTIVATION", table: .alertTitle), message: localString(key: "DEVICE_BINDING", table: .alertMsg), preferredStyle: .alert)
                let okAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default) { (_) in
                    print("嘗試綁定此設備")
                    self.tryCreateDevices()
                }
                controller.addAction(okAction)
                let cancelAction = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
                controller.addAction(cancelAction)
                self.present(controller, animated: true, completion: nil)
            }else{
                print("ActiveAccount error")
                self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: cloundErrorMsg)
            }
        }
    }
    
    func tryCreateDevices(){
        print("tryActiveAccount")
        cloudCreateDevices {
            print("cloudCreateDevices done")
            if cloudRegResult{
                print("cloudCreateDevices ok")
                
                let controller = UIAlertController(title: localString(key: "DEVICE_BINDING", table: .alertTitle), message: localString(key: "SUCCESS", table: .alertMsg), preferredStyle: .alert)
                let okAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default) { (_) in
                    print("設備綁定成功")
                }
                controller.addAction(okAction)
                self.present(controller, animated: true, completion: nil)
            }else{
                print("cloudCreateDevices error")
                //                self.alertMsg(title: "啟用失敗", msg: cloundErrorMsg)
                self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: cloundErrorMsg)
            }
        }
    }
    
    func checkDataFormat(){
        if emailTextField.text == "" || passwordTextField.text == "" || passwordTextField2.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "CLOUD_PASSWORD_EMPTY", table: .alertMsg))
        }
        // TODO 可以用正規式檢查 e-mail 格式
        if passwordTextField.text != passwordTextField2.text{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "CLOUD_PASSWORD_DIFFERENT", table: .alertMsg))
        }
        
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
