//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
/**
 設備綁定頁
 - Date: 08/25 Yang
*/
class DeviceBindViewController: UIViewController,UITextFieldDelegate {
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView = UIImageView()
    let backButton = UIButton()
    let passwordLabel = UILabel()
    let passwordTextField = CustomTextField3()
    let titleLabel = UILabel()
    let emailLabel = UILabel()
    let emailTextField = UITextField()
    let bindingButton = UIButton()
    let rightViewBtn = UIButton(type: .custom)
    let bkView = UIView()
    let bkView2 = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    /**
     某些較小型的裝置在鍵盤出現時需要將屏幕上移 避免鍵盤遮住輸入框影響打字
     - Date: 08/26 Yang
    */
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            let info = sender.userInfo!
            let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
            let kbSize = rect.size
            let activeField: UITextField? = [passwordTextField].first { $0.isFirstResponder }
            if activeField != nil {
                self.view.frame.origin.y = -kbSize.height/3
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func setViews(){
        bkgImageView.backgroundColor = .white
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        navBarLabel.backgroundColor = .clear
        navBarLabel.text = localString(key: "DEVICE_BINDING", table: .pageTitle)
        navBarLabel.numberOfLines = 0
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.centerY.equalTo(navigationBarView).offset(statusBarHeight/2.5)
            makes.left.greaterThanOrEqualTo(backButton.snp.right).offset(spacing1/1)
        }
        
        bkView.backgroundColor = .lightGray
        bkView.layer.cornerRadius = 8
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview().inset(spacing3)
            makes.top.equalTo(navigationBarView.snp.bottom).offset(spacing3)
        }
        
        bkView2.backgroundColor = .white
        bkView2.layer.cornerRadius = 8
        self.view.addSubview(bkView2)
        bkView2.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView).inset(0.5)
        }
        
        titleLabel.backgroundColor = .clear
        titleLabel.text = localString(key: "CLOUD_BINDING_INFO", table: .otherText)
        titleLabel.font = UIFont.boldSystemFont(ofSize: labelFontSize+2)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = PrimaryColor2
        titleLabel.textAlignment = .left
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(bkView2).offset(loginLabelHeight2)
            makes.left.right.equalTo(bkView2).inset(spacing3)
        }
        
        emailLabel.backgroundColor = .clear
        emailLabel.text = localString(key: "EMAIL_ADDRESS", table: .inputTitle)
        emailLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize+1)
        emailLabel.textColor = PrimaryColor2
        emailLabel.textAlignment = .left
        self.view.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(loginLabelHeight2)
            makes.width.left.equalTo(titleLabel)
        }
        
        emailTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        emailTextField.textAlignment = .left
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .black
        emailTextField.backgroundColor = TextFieldColor
        emailTextField.layer.cornerRadius = 5.0
        emailTextField.keyboardType = .emailAddress
        emailTextField.delegate = self
        self.view.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailLabel.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = localString(key: "PASSWORD", table: .inputTitle)
        passwordLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize+1)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(spacing3)
            makes.width.height.left.equalTo(emailLabel)
        }
        
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = TextFieldColor
        passwordTextField.layer.cornerRadius = 5.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
            makes.bottom.equalTo(bkView2.snp.bottom).offset(-spacing1)
        }
        
        bindingButton.backgroundColor = SecondaryColor3
        bindingButton.layer.cornerRadius = 21
        bindingButton.setTitle(localString(key: "BIND", table: .button), for: [])
        bindingButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        bindingButton.titleLabel?.textColor = .white
        bindingButton.isEnabled = true
        bindingButton.addTarget(self,action: #selector(self.clickButton),for: .touchUpInside)
        self.view.addSubview(bindingButton)
        bindingButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(bkView.snp.bottom).offset(spacing3*2)
            makes.width.equalTo(fullScreenSize.width*0.45)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        ///根據不同裝置改變按鈕圓角
        switch currentDevice.deviceType {
        case .iPhone35:
            bindingButton.layer.cornerRadius = 17
        case .iPhone40:
            bindingButton.layer.cornerRadius = 17
        case .iPhone47:
            bindingButton.layer.cornerRadius = 20
        case .iPhone55:
            bindingButton.layer.cornerRadius = 22
        case .iPhone60:
            bindingButton.layer.cornerRadius = 24
        case .iPad:
            bindingButton.layer.cornerRadius = 30
        default:
            bindingButton.layer.cornerRadius = 24
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
    }
    
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @objc func clickButton(){
        checkDataFormat()
        emailStr = emailTextField.text!
        passStr = passwordTextField.text!
        cloudLogin {
            cloudCreateDevices {
                print("cloudCreateDevices done")
                if cloudRegResult{
                    print("cloudCreateDevices ok")
                    
                    let controller = UIAlertController(title: localString(key: "DEVICE_BINDING", table: .alertTitle), message: localString(key: "SUCCESS", table: .alertMsg), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default) { (_) in
                        print("設備綁定成功")
                        //                    self.tryActiveAccount()
                    }
                    controller.addAction(okAction)
                    self.present(controller, animated: true, completion: nil)
                }else{
                    print("cloudCreateDevices error")
                    self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: cloundErrorMsg)
                }
            }
        }
    }
    /**
     檢查格式 密碼不可為空白
     - Date: 08/26 Yang
    */
    func checkDataFormat(){
        if emailTextField.text == "" || passwordTextField.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "CLOUD_PASSWORD_EMPTY", table: .alertMsg))
        }
        //        // TODO 可以用正規式檢查 e-mail 格式
        //
        //        if passwordTextField.text != passwordTextField2.text{
        //            alertMsg(title: "無效", msg: "密碼不相同")
        //        }
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
