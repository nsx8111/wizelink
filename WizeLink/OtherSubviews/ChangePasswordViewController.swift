//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Alamofire
/**
 變更登入密碼頁
 - Date: 08/25 Yang
*/
class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView = UIImageView()
    let backButton = UIButton()
    let passwordLabel = UILabel()
    let passwordTextField = CustomTextField3()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = CustomTextField3()
    let changeButton = UIButton()
    let rightViewBtn = UIButton(type: .custom)
    let rightViewBtn2 = UIButton(type: .custom)
    let bkView = UIView()
    let bkView2 = UIView()

    ///存放新密碼用
    var tempPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        hideKeyboardWhenTappedAround()
    }
    
    func setViews(){
        bkgImageView.backgroundColor = .white
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        navBarLabel.backgroundColor = .clear
        navBarLabel.text = localString(key: "CHANGE_LOGIN_PASSWORD", table: .pageTitle)
        navBarLabel.numberOfLines = 0
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
        }
        
        bkView.backgroundColor = .lightGray
        bkView.layer.cornerRadius = 8
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview().inset(spacing3)
            makes.top.equalTo(navigationBarView.snp.bottom).offset(spacing3)
        }
        
        bkView2.backgroundColor = .white
        bkView2.layer.cornerRadius = 8
        self.view.addSubview(bkView2)
        bkView2.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView).inset(0.5)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = localString(key: "NEW_LOGIN_PASSWORD", table: .inputTitle)
        passwordLabel.font = UIFont.systemFont(ofSize: labelFontSize+1)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(bkView2).offset(spacing3)
            makes.left.right.equalTo(bkView2).inset(spacing3)
        }
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = TextFieldColor
        passwordTextField.layer.cornerRadius = 5.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
        passwordLabel2.text = localString(key: "ENTER_PASSWORD_AGAIN", table: .inputTitle)
        passwordLabel2.font = UIFont.systemFont(ofSize: labelFontSize+1)
        passwordLabel2.textColor = PrimaryColor2
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing3)
            makes.width.height.left.equalTo(passwordLabel)
        }
        
        passwordTextField2.isSecureTextEntry = true
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .black
        passwordTextField2.backgroundColor = TextFieldColor
        passwordTextField2.layer.cornerRadius = 5.0
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(spacing1)
            makes.left.right.equalTo(bkView2).inset(spacing3)
            makes.height.equalTo(fullScreenSize.height*0.07)
            makes.bottom.equalTo(bkView2.snp.bottom).offset(-spacing1)
        }
        
        changeButton.backgroundColor = SecondaryColor3
        changeButton.layer.cornerRadius = 21
        changeButton.setTitle(localString(key: "APPLY", table: .button), for: [])
        changeButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        changeButton.titleLabel?.textColor = .white
        changeButton.isEnabled = true
        changeButton.addTarget(self,action: #selector(self.clickChangeButton),for: .touchUpInside)
        self.view.addSubview(changeButton)
        changeButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(bkView.snp.bottom).offset(spacing3*2)
            makes.width.equalTo(fullScreenSize.width*0.45)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        ///密碼輸入框右側切換是否隱藏密碼icon
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
            rightViewBtn2.addTarget(self,action: #selector(self.Security_Click2),for: .touchUpInside)
            rightViewBtn2.setImage(smallImage, for: .normal)
            passwordTextField2.rightView = rightViewBtn2
            passwordTextField2.rightViewMode = .always
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            changeButton.layer.cornerRadius = 17
        case .iPhone40:
            changeButton.layer.cornerRadius = 17
        case .iPhone47:
            changeButton.layer.cornerRadius = 20
        case .iPhone55:
            changeButton.layer.cornerRadius = 22
        case .iPhone60:
            changeButton.layer.cornerRadius = 24
        case .iPad:
            changeButton.layer.cornerRadius = 30
        default:
            changeButton.layer.cornerRadius = 24
        }
    }
    /**
    - 密碼輸入框切換隱藏密碼icon變換功能
    - Date: 08/24 Yang
     */
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    @objc func Security_Click2() {
        if passwordTextField2.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = true
        }
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
    }
    /**
     執行變更密碼
     - Date: 08/26 Yang
    */
    @objc func clickChangeButton(){
        //        if passwordTextField.text == nil{
        //            print("passwordTextField.text == nil")
        //        }else{
        //            print(passwordTextField.text)
        //        }
        self.checkFormat()
        self.sendData()
        //        alertMsg(title: "無效", msg: "登入密碼不可為空白")
        //            if passwordTextField.text = ""
    }
    
    func checkFormat(){
        //        var ans:Bool = false
        
        if passwordTextField.text == nil || passwordTextField.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_EMPTY", table: .alertMsg))
        }
        if passwordTextField2.text == nil || passwordTextField2.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_EMPTY", table: .alertMsg))
        }
        if passwordTextField.text != passwordTextField2.text{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_DIFFERENT", table: .alertMsg))
        }
        //        return ans
        // Mike
        password = self.passwordTextField2.text!
    }
    
    func sendData(){
        /**
        move checkFormat in
        - Date:07/22
         */
        if passwordTextField.text == nil || passwordTextField.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_EMPTY", table: .alertMsg))
            return
        }
        if passwordTextField2.text == nil || passwordTextField2.text == ""{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_EMPTY", table: .alertMsg))
            return
        }
        if passwordTextField.text != passwordTextField2.text{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_DIFFERENT", table: .alertMsg))
            return
        }
        tempPassword = self.passwordTextField2.text!
        
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        // password -> tempPassword
        let requestStr : String = "\(formPasswordSetupStr)?userName=\(user)&newPass=\(tempPassword)"
        
        let actView : UIView = UIView()
        self.setActView(actView: actView)
        
        AF.request(requestStr,headers: headers)
            .responseJSON { response in
                print("sent")
                print(requestStr)
                //                password = self.passwordTextField2.text!
                password = self.tempPassword
                actView.removeFromSuperview()
                
                //                let alertController = UIAlertController(title: "Done", message: "change password done!", preferredStyle: .alert)
                let alertController = UIAlertController(title: localString(key: "PASSWORD_CHANGED", table: .alertTitle), message: localString(key: "SUCCESS", table: .alertMsg), preferredStyle: .alert)
                //                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                let defaultAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                //                self.present(alertController, animated: true, completion: nil)
                self.present(alertController, animated: true, completion: self.backVC)
                
                //    //                self.backVC()
                //    //                self.alertMsgMike1(msg: "change password done!") {
                //    //                    self.backVC()
                //    //                }
                //                    self.backVC()
                //                    self.alertMsg(msg: "change password done!")
                //    //                self.backVC()
        }
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
