//
//  StatusInfoViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/6.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct OptionType{
    var iconImageView: UIImageView!
    var network_type: String = ""
    var version_type: String = ""
}

class OtherViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = OptionTableView.indexPathForSelectedRow{
            if indexPath.row == 0{
                let vc = getVC(storyboardName: "Main", identifier: "AdvancedSetViewController") as! AdvancedSetViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
                let vc = getVC(storyboardName: "Main", identifier: "ShowClientViewController") as! ShowClientViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 2{
                let vc = getVC(storyboardName: "Main", identifier: "ChangePasswordViewController") as! ChangePasswordViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 3{
                let vc = getVC(storyboardName: "Main", identifier: "SignUpCloudViewController") as! SignUpCloudViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 4{
                let vc = getVC(storyboardName: "Main", identifier: "DeviceBindingViewController") as! DeviceBindingViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 5{
                let vc = getVC(storyboardName: "Main", identifier: "LanguageViewController") as! LanguageViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 7{
                let controller = UIAlertController(title: "登出", message: "您確定要登出？", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "確認", style: .default, handler: nil)
                let cancelAction2 = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                controller.addAction(cancelAction2)
                controller.addAction(cancelAction)
                present(controller, animated: true, completion: nil)
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "OptionTypeCell")
        
        cell.imageView?.image = types[indexPath.row].iconImageView.image
        let itemSize = CGSize.init(width: 20, height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
        let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].network_type
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .black
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        if indexPath.row == 5{
            if let languageType = UserDefaults.standard.object(forKey:"languageType") as? String {
                cell.detailTextLabel?.text = languageType
            }
        }else{
            cell.detailTextLabel?.text = types[indexPath.row].version_type
        }
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
        cell.detailTextLabel?.textColor = .black
        
        return cell
    }
    
    func setTableView(){
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        OptionTableView = UITableView()
        OptionTableView.register(UITableViewCell.self, forCellReuseIdentifier: "OptionTypeCell")
        OptionTableView.delegate = self
        OptionTableView.dataSource = self
        OptionTableView.bounces = false
        OptionTableView.allowsSelection = true
        OptionTableView.backgroundColor = .white
        OptionTableView.rowHeight = fullScreenSize.height*0.075
        self.view.addSubview(OptionTableView)
        
        OptionTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(navigationBarHeight)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(OptionTableView.rowHeight*8)
            make.centerX.equalToSuperview()
        }
    }
    
    var types = [
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Advanced Setting-b")), network_type: "進階設定", version_type: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Show Client-b")), network_type: "顯示用戶", version_type: ""),
        OptionType(iconImageView:UIImageView(image: UIImage(named: "Parent Control")), network_type: "變更登入密碼", version_type: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Parent Control")), network_type: "註冊雲端帳號", version_type: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Parent Control")), network_type: "設備綁定", version_type: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Language-b")), network_type: "語言", version_type: languageType),
        OptionType(iconImageView:UIImageView(image: UIImage(named: "version-b")), network_type: "App版本", version_type: "1.1.2"),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "logout-b")), network_type: "登出", version_type: "")
    ]
    
    let infoLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView2 = UIImageView()
    private var OptionTableView: UITableView!
    var OptionTypes: [OptionType] = []
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        OptionTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = .white
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        infoLabel.backgroundColor = .clear
        infoLabel.attributedText = NSAttributedString(string: "其他", attributes:
            nil)
        infoLabel.font = UIFont.boldSystemFont(ofSize: 17)
        infoLabel.textColor = .white
        infoLabel.textAlignment = .center
        self.view.addSubview(infoLabel)
        
        infoLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        setTableView()
        
    }
    
    
}
