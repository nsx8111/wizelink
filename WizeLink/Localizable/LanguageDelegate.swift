//
//  LanguageDelegate.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/6/19.
//  Copyright © 2020 YangYang. All rights reserved.
//

import Foundation

protocol LanguageDelegate:class {
    func notiftNewLanguage()
    func modifySubsViews()
}
