//
//  LocalizeUtils.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/6/4.
//  Copyright © 2020 Natalie. All rights reserved.
//

import Foundation
import UIKit
//LocalizeUtils.shared.localized(withKey: "ROUTER_MODE")
//var bundleLocalizeFileName: String!
let preferredLanguages: String = Locale.preferredLanguages.first!
//判斷是否為 zh-hant or zh-hans
//若不是則 count = 2, ex: ja-JP, en-JP, ja-TW
//若是則 count = 3, ex: zh-Hant-TW, zh-Hant-JP
let forZHCount = preferredLanguages.split(separator: "-").count

enum localTable : String {
    case button = "BUTTON"
    case pageTitle = "PAGE_TITLE"
    case tabTitle = "TAB_TITLE"
    case headerTitle = "HEADER_TITLE"
    case inputTitle = "INPUT_TITLE"
    case dataTitle = "DATA_TITLE"
    case dataValue = "DATA_VALUE"
    case alertTitle = "ALERT_TITLE"
    case alertMsg = "ALERT_MSG"
    case loading = "LOADING"
    case otherText = "OTHER_TEXT"
    case srAndLoginInfo = "SubRouterAndLoginInfo"
}

var lt : localTable = .button

var bundleLocalizeFileName : String = {
    // 有讀取到 selectedIndexPath 的 value 就用其儲存的設定
    // 若是沒有讀取到，則用當前 IOS 系統的語言設定
    do {
        if let dataValue = UserDefaults.standard.object(forKey: "selectedIndexPath") as? Data , let indexPath = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(dataValue) as? IndexPath {
            print("@viewDidLoad dataValue:\(dataValue), indexPath:\(indexPath)")
            
            switch indexPath.row {
            case 2:
                return "en"
            case 3:
                return "es"
            case 4:
                return "pt"
            case 5:
                return "de"
            case 6:
                return "ja"
            case 7:
                return "zh-Hant"
            case 8:
                return "zh-Hans"
            default:
                //something error send en
                return "en"
            }
        }else{
            print("get dataValue error")
            print("forZHCount \(forZHCount)")
            if forZHCount > 2{
                if preferredLanguages.split(separator: "-")[1] == "Hant" {
                    // 繁體
                    print("Hant11")
                    return "zh-Hant"
                } else if preferredLanguages.split(separator: "-")[1] == "Hans" {
                    // 簡體
                    return "zh-Hans"
                }
            }else{
                print("Hant22")
                print(preferredLanguages.split(separator: "-")[0])
                return String(preferredLanguages.split(separator: "-")[0])
            }
        }
    }
    catch let error as NSError {
        print(error)
    }
    
//    if forZHCount > 2{
//        if preferredLanguages.split(separator: "-")[1] == "Hant" {
//            // 繁體
//            return "zh-Hant"
//        } else if preferredLanguages.split(separator: "-")[1] == "Hans" {
//            // 簡體
//            return "zh-Hans"
//        }
//    }else{
//        return String(preferredLanguages.split(separator: "-")[0])
//    }
    
    //something error send en
    return "en"
}()

class LocalizeUtils: NSObject {
    
    static let shared = LocalizeUtils()
    
    /// 依語系檔裡的 key 取得 value 值
    ///
    /// - Parameters:
    ///   - key: 多國語系檔的 key 值
    ///   - localizationFileNmae: 多國語系檔的檔名
    /// - Returns: String
    func localized(withKey key: String, withLocalizationFileNmae localizationFileNmae: String) -> String {
        // 取得 Bundle 下的的多國語系檔
        let path = Bundle.main.path(forResource: localizationFileNmae, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        // 依 key 值和 Bundle 的多國語系檔取得對應的 valu
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    func localized(withKey key: String) -> String {
        // 取得 Bundle 下的的多國語系檔
        let path = Bundle.main.path(forResource: bundleLocalizeFileName, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        // 依 key 值和 Bundle 的多國語系檔取得對應的 valu
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
   
    func localized(withKey key: String,withTableName tableName: String) -> String {
        /// 取得 Bundle 下的的多國語系檔
        let path = Bundle.main.path(forResource: bundleLocalizeFileName, ofType: "lproj")
        let bundle = Bundle(path: path!)
        /// 依 key 值和 Bundle 的多國語系檔取得對應的 value
        return NSLocalizedString(key, tableName: tableName, bundle: bundle!, value: "", comment: "")
    }
    
}

func localString(key:String,table:localTable) -> String{
    return LocalizeUtils.shared.localized(withKey: key, withTableName: table.rawValue)
}
