////
////  StatusViewController.swift
////  WizeLink
////
////  Created by 蒼月喵 on 2020/4/7.
////  Copyright © 2020 YangYang. All rights reserved.
////
//
//import UIKit
//import Foundation
//
//struct NetworkType{
//    var iconImageView: UIImageView!
//    var network_type: String = ""
//}
//
//class StatusViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
//
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            return types.count
//        }
//
//        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            if let indexPath = NetworkTypeTableView.indexPathForSelectedRow{
//                if indexPath.row == 0{
//                    networkNameLabel = types[indexPath.row].network_type
//                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }else if indexPath.row == 1{
//                    networkNameLabel = types[indexPath.row].network_type
//                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }else if indexPath.row == 2{
//                    networkNameLabel = types[indexPath.row].network_type
//                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetail2ViewController") as! NetworkDetail2ViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }else if indexPath.row == 3{
//                    networkNameLabel = types[indexPath.row].network_type
//                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetail3ViewController") as! NetworkDetail3ViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//
//            }
//
//        }
//
//    //    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    //
//    //    }
//
//        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "NetworkTypeCell")
//
//            cell.imageView?.image = types[indexPath.row].iconImageView.image
//            let itemSize = CGSize.init(width: 20, height: 20)
//            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
//            let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
//            cell.imageView?.image!.draw(in: imageRect)
//            cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
//            UIGraphicsEndImageContext();
//
//            cell.backgroundColor = .white
//            cell.selectionStyle = .none
//            cell.textLabel?.text = types[indexPath.row].network_type
//            cell.textLabel?.textAlignment = .left
//            cell.textLabel?.textColor = .black
//            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
//
//            return cell
//        }
//
//        func setTableView(){
//
//            let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
//                self.navigationController!.navigationBar.frame.height
//
//            NetworkTypeTableView = UITableView()
//            NetworkTypeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "NetworkTypeCell")
//            NetworkTypeTableView.delegate = self
//            NetworkTypeTableView.dataSource = self
//            NetworkTypeTableView.bounces = false
//            NetworkTypeTableView.allowsSelection = true
//            NetworkTypeTableView.backgroundColor = .white
//            NetworkTypeTableView.rowHeight = fullScreenSize.height*0.067
//            self.view.addSubview(NetworkTypeTableView)
//
//            NetworkTypeTableView.snp.makeConstraints { (make) in
//                make.top.equalToSuperview().offset(navigationBarHeight)
//                make.width.equalTo(fullScreenSize.width)
//                make.height.equalTo(NetworkTypeTableView.rowHeight*4)
//                make.centerX.equalToSuperview()
//            }
//
//        }
//
//        var types = [
//            NetworkType(iconImageView: UIImageView(image: UIImage(named: "wifi-b")), network_type: "無線網路2.4G配置"),
//            NetworkType(iconImageView: UIImageView(image: UIImage(named: "wifi-b")), network_type: "無線網路5G配置"),
//            NetworkType(iconImageView:UIImageView(image: UIImage(named: "LANConfig-b")), network_type: "區域網路配置"),
//            NetworkType(iconImageView: UIImageView(image: UIImage(named: "WAN-b")), network_type: "外部網路配置")
//        ]
//
//    let titleLabel = UILabel()
//    let navigationBarView = UIImageView()
//    let bkgImageView2 = UIImageView()
//    private var NetworkTypeTableView: UITableView!
//    var NetworkTypes: [NetworkType] = []
//
//    weak var ButtonDelegate: ButtonDelegate?
//    @IBOutlet var MenuButton : UIButton!
//
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        setMenuButtonView()
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
////        MenuButton.snp.makeConstraints { (makes) in
////            makes.height.width.equalTo(44)
////        }
//        setViews()
//        setTableView()
//
//    }
//
//    func setViews(){
//
//        bkgImageView2.backgroundColor = .white
//        self.view.addSubview(bkgImageView2)
//        bkgImageView2.snp.makeConstraints{ (makes) in
//            makes.edges.equalToSuperview()
//        }
//
//        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
//            self.navigationController!.navigationBar.frame.height
//
//        navigationBarView.backgroundColor = PrimaryColor1
//        self.view.addSubview(navigationBarView)
//        navigationBarView.snp.makeConstraints { (makes) in
//            makes.top.equalToSuperview().offset(0)
//            makes.width.equalTo(fullScreenSize.width)
//            makes.height.equalTo(navigationBarHeight)
//        }
//
//        titleLabel.backgroundColor = .clear
//        titleLabel.attributedText = NSAttributedString(string: "狀態資訊", attributes:
//            nil)
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
//        titleLabel.textColor = .white
//        titleLabel.textAlignment = .center
//        self.view.addSubview(titleLabel)
//
//        titleLabel.snp.makeConstraints { (makes) in
//            makes.centerX.equalToSuperview()
//            makes.top.equalToSuperview().offset(loginLabelHeight)
//            makes.width.equalTo(fullScreenSize.width*0.6)
//        }
//
//    }
//
//    func setMenuButtonView(){
//          MenuButton.setImage(UIImage(named: "Menu"), for: .normal)
//          MenuButton.isEnabled = true
//          MenuButton.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
//          self.view.addSubview(MenuButton)
//          MenuButton.snp.makeConstraints { (makes) in
//              makes.centerY.equalTo(titleLabel)
//              makes.width.height.equalTo(loginLabelHeight)
//              makes.left.equalToSuperview().offset(spacing)
//          }
//      }
//
//    @IBAction func menuClick(_ sender: Any) {
//        navigationController?.view.menu()
//        ButtonDelegate?.clickLeft()
//    }
//
//    @IBAction func handleTapGesture(_ sender: Any) {
//        if navigationController?.view.superview?.frame.origin.x != 0 {
//            navigationController?.view.menu()
//        }
//    }
//
//    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
//        let location = sender.location(in: nil)
//        navigationController?.view.slideByFinger(location: location, state: sender.state)
//        ButtonDelegate?.clickLeft()
//    }
//
//
//
//}
