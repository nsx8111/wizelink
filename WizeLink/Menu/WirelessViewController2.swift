////
////  WirelessViewController.swift
////  WizeLink
////
////  Created by 蒼月喵 on 2020/4/6.
////  Copyright © 2020 YangYang. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SnapKit
//import KMPageMenu
////Line 40 135
//class WirelessViewController: UIViewController {
//    
////    var connectNum : Int = 0
////    let titleLabel = UILabel()
////    let connectNumLabel = UILabel()
////    let titles : [String] = ["網際網路", "路由器"]
////    //    let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: self.titles)
////    /** 分頁標籤 */
////    lazy var menu: KMPageMenu = {
////        let m = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: titles)
////        //        m.style.titleFont = UIFont.systemFont(ofSize: 14)
////        m.isScrollEnable = false
////        m.backgroundColor = .white
////        view.addSubview(m)
////        return m
////    }()
////    /** 連結控制 */
////    let vc1: Wireless24GViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: Wireless24GViewController.self)) as! Wireless24GViewController
////    /** 連結控制 */
////    let vc2: Wireless5GViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: Wireless5GViewController.self)) as! Wireless5GViewController
////
////    /** 設定分頁 */
////    lazy var page: KMPagingViewController = {
////        var viewControllers: [UIViewController] = []
////
////        vc1.pageViewDelegate = self as! PageViewDelegate
////        vc2.pageViewDelegate = self as! PageViewDelegate
////
////        viewControllers.append(vc1)
////        viewControllers.append(vc2)
////
////        let p = KMPagingViewController(viewControllers: viewControllers)
////
////        //        p.view.snp.makeConstraints { (makes) in
////        //            makes.left.right.bottom.equalToSuperview()
////        //            makes.top.equalTo(self.menu.snp.bottom)
////        //        }
////
////        //        p.view.frame = CGRect(x: 0,
////        //                              y: menu.frame.maxY,
////        //                              width: width,
////        //                              height: view.frame.height - menu.frame.maxY)
////        p.view.frame = CGRect(x: 0,
////                              y: 134,
////                              width: UIScreen.main.bounds.size.width,
////                              height: UIScreen.main.bounds.size.height - 134)
////        self.addChild(p)
////        p.didMove(toParent: self)
////        p.isScrollEnable = false
////        self.view.addSubview(p.view)
////
////        return p
////    }()
//    
//    weak var ButtonDelegate: ButtonDelegate?
//    @IBOutlet var MenuButton : UIButton!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        MenuButton.snp.makeConstraints { (makes) in
//            makes.height.width.equalTo(44)
//        }
////        print("MenuButton.frame:\(MenuButton.frame)")
////        view.backgroundColor = .yellow
////        print("self.view.frame:\(self.view.frame)")
//
////        setViews()
//        
//        view.backgroundColor = .blue
//        
//        self.setViews()
////        self.setConnectSubviews()
//        
//    }
//    
//    func setViews(){
//        let bkView = UIView()
//        bkView.backgroundColor = .red
//        self.view.addSubview(bkView)
//        bkView.snp.makeConstraints { (makes) in
//            makes.edges.equalToSuperview()
//        }
//    }
//    
////    func setViews(){
////
////        titleLabel.text = "連線狀態"
////        titleLabel.textColor = .white
////        view.addSubview(titleLabel)
////        titleLabel.snp.makeConstraints { (makes) in
////            makes.centerX.equalToSuperview()
////            makes.top.equalToSuperview().offset(40)
////            makes.height.equalTo(20)
////        }
////
////        connectNumLabel.text = "連線裝置數：\(connectNum)"
////        connectNumLabel.textColor = .white
////        view.addSubview(connectNumLabel)
////        connectNumLabel.snp.makeConstraints { (makes) in
////            makes.centerX.equalToSuperview()
////            makes.top.equalTo(titleLabel.snp.bottom).offset(10)
////            makes.height.equalTo(20)
////        }
////
////    }
////
////    func setConnectSubviews(){
////        //        let titles : [String] = ["網際網路", "路由器", "裝置"]
////        //        let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: titles)
////        //        let menu = KMPageMenu(titles: titles)
////        //        m.style.titleFont = UIFont.systemFont(ofSize: 14)
////        menu.applyLabelWidthFixed()
////        menu.applyLinePositionBottom()
////        menu.applyIndicatorLineFixed()
////        menu.addTarget(self, action: #selector(menuValueChange(sender:)), for: .valueChanged)
////        menu.valueChange = { [weak self] index in
////            self?.page.pagingToViewController(at: index)
////        }
////
////        //        view.addSubview(menu)
////
////        page.delegate = self as! KMPagingViewControllerDelegate
////        page.didFinishPagingCallBack = { [weak self] (currentViewController, currentIndex)in
////            self?.menu.setSelectIndex(index: currentIndex, animated: true)
////        }
////
////        //        menu.snp.makeConstraints { (makes) in
////        //            makes.left.right.equalToSuperview()
////        //            makes.top.equalTo(connectNumLabel.snp.bottom).offset(10)
////        //            makes.height.equalTo(60)
////        //        }
////    }
////
////    /** 切換頁面時的動作 */
////    @objc func menuValueChange(sender: KMPageMenu) {
////        print("a)selectIndex == \(sender.selectIndex)")
////        //        if sender.selectIndex == 0 {
////        //            vc2.vlcPlayer.stop()
////        //        }else{
////        //            vc1.vlcPlayer.stop()
////        //        }
////    }
////
////    func callClick(){
////         navigationController?.view.menu()
////     }
//    
//    @IBAction func menuClick(_ sender: Any) {
//        navigationController?.view.menu()
//        ButtonDelegate?.clickLeft()
//    }
//    
//    @IBAction func handleTapGesture(_ sender: Any) {
//        if navigationController?.view.superview?.frame.origin.x != 0 {
//            navigationController?.view.menu()
//        }
//    }
//    
//    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
//        let location = sender.location(in: nil)
//        navigationController?.view.slideByFinger(location: location, state: sender.state)
//        ButtonDelegate?.clickLeft()
//    }
//
//}
