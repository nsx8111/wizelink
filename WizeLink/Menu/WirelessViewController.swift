//
//  WirelessViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/6.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import KMPageMenu
/**
 無線網路頁
 主要顯示的是navigationBar 其他用來控制container裡的2.4G跟5G頁面
 - Date: 08/25 Yang
*/
class WirelessViewController: UIViewController, UIScrollViewDelegate {
    /**
     無線網路2.4G跟5G頁面為嵌入此頁面的Container 刷新WirelessVC亦同步刷新Wireless24GVC與Wireless5GVC
     - Date: 08/25 Yang
    */
    let vc24G: Wireless24GViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Wireless24GViewController") as! Wireless24GViewController
    let vc5G: Wireless5GViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Wireless5GViewController") as! Wireless5GViewController
    
    let actView : UIView = UIView()
    let titleLabel = UILabel()
    let navigationBarView = UIImageView()
    let scrollView = UIScrollView()
    let contentView = UIView()
    var pageLanguage : String = ""
    
    @IBOutlet var MenuButton : UIButton!
    @IBOutlet weak var RefreshButton: UIButton!
    @IBOutlet weak var wirelessContainerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        ///獲取navigationBar的高度
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        UserDefaults.standard.set(String(Int(navBarHeight)),forKey: "navBarHeight")
        ///壓縮navigationItem的icon顯示比例
        if let image = UIImage(named: "reload-outline") {
            let smallImage = resizeImage(image: image, width: buttonSize)
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: smallImage.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(refreshClick(_:)))
            navigationItem.rightBarButtonItem?.tintColor = SecondaryColor3
        }
        if let image = UIImage(named: "menu-options") {
            let smallImage = resizeImage(image: image, width: buttonSize)
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: smallImage.withRenderingMode(.automatic), style: .plain, target: self, action: #selector(menuClick(_:)))
        }
        setViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ///變更navigationBar的樣式
        navigationController?.navigationBar.barTintColor = PrimaryColor1
        switch currentDevice.deviceType {
        case .iPad:
            let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 20)]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        default:
            let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 17)]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }
        ///變更狀態列背景色
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(named: "BarColor")
        print("originX1 \(String(describing: navigationController?.view.superview?.frame.origin.x))")
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        let name = NSNotification.Name("ContentView_Notification")
        NotificationCenter.default.addObserver(self, selector:
        #selector(updateContentViewNoti(noti:)), name: name, object: nil)
    }
    
    @objc func updateContentViewNoti(noti: Notification) {
        if navigationController?.view.superview?.frame.origin.x == 0 {
            removeSubview()
        }
        print("Notification")
    }
    /**
     從背景回到前景時呼叫的程式
     - Date: 08/26 Yang
    */
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        // Application is back in the foreground
        print("originX2 \(String(describing: navigationController?.view.superview?.frame.origin.x))")
        if navigationController?.view.superview?.frame.origin.x == 0{
            navigationController?.view.superview?.frame.origin.x = 0
            removeSubview()
        }else{
            showContentView()
        }
        print("active")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear @ WirelessVC")
        checkLanguage()
        
        let name = NSNotification.Name("ScrollViewRefresh")
        NotificationCenter.default.addObserver(self, selector:
            #selector(ScrollViewRefresh(noti:)), name: name, object: nil)
    }
    /**
     其他介面通知或下滑刷新無線網路頁面
     - Date: 08/26 Yang
    */
    @objc func ScrollViewRefresh(noti: Notification) {
        RefreshWirelessVC()
    }
    
    func RefreshWirelessVC(){
        isShowAlert = true
        self.setActView(actView: self.actView)
        getData{
            ///同時刷新ConnectVC
            let name4 = Notification.Name("Refresh_ConnectVC")
            NotificationCenter.default.post(name: name4, object: nil, userInfo:
                nil)
            ///同時刷新StatusVC searchInfo()
            let name = Notification.Name("Refresh_StatusVC")
            NotificationCenter.default.post(name: name, object: nil, userInfo:
                nil)
            self.searchInfo()
            let name2 = Notification.Name("channelNumNow")
            NotificationCenter.default.post(name: name2, object: nil, userInfo:
                nil)
            let name3 = Notification.Name("channelNumNow2")
            NotificationCenter.default.post(name: name3, object: nil, userInfo:
                nil)
            self.actView.removeFromSuperview()
        }
    }
    
    func checkLanguage(){
        print("checkLanguage @ WirelessVC")
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        print("reloadString @ WirelessVC")
        pageLanguage = bundleLocalizeFileName
        self.navigationItem.title = localString(key: "WIRELESS", table: .pageTitle)
    }
    
    func setViews(){
        if let navBarHeight =
            UserDefaults.standard.string(forKey: "navBarHeight") , let navBarH = (Int(navBarHeight)) {
            ///給原生navigationBar調色用 加深背景色
            navigationBarView.backgroundColor = PrimaryColor1
            self.view.addSubview(navigationBarView)
            navigationBarView.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(0)
                makes.width.equalTo(fullScreenSize.width)
                makes.height.equalTo(navBarH)
            }
        }
        self.navigationItem.title = localString(key: "WIRELESS", table: .pageTitle)
    }
    /**
     登入後先getData再撈資料 基本上若沒按重刷新 searchInfo就為舊資料
     - Date: 08/25 Yang
    */
    func searchInfo(){
        print("isShowAlert \(isShowAlert)")
        
        let regDomain : String = searchFunc(src: wizardHtmData, str1: "regDomain[1]=", str2: ";")
        vc24G.channelArray = getChannelArray2_4G(regDomain: regDomain)
        vc24G.channelNumNow = searchFunc(src: wizardHtmData, str1: "defaultChan[1]=", str2: ";")
        print("channelNumNow @ wireless2_4g.searchInfo() :\(vc24G.channelNumNow)")
        // Mike for AUTO
        if vc24G.channelNumNow == "0"{
            vc24G.channelCountButton.setTitle("AUTO", for: .normal)
        }else{
            vc24G.channelCountButton.setTitle(vc24G.channelNumNow, for: .normal)
        }
        channelNum = vc24G.channelNumNow
        
        let regDomain2 : String = searchFunc(src: wizardHtmData, str1: "regDomain[0]=", str2: ";")
        vc5G.channelArray = getChannelArray5G(regDomain: regDomain2)
        vc5G.channelNumNow = searchFunc(src: wizardHtmData, str1: "defaultChan[0]=", str2: ";")
        print("channelNumNow @ wireless5g.searchInfo() :\(vc5G.channelNumNow)")
        // Mike for AUTO
        if vc5G.channelNumNow == "0"{
            vc5G.channelCountButton.setTitle("AUTO", for: .normal)
        }else{
            vc5G.channelCountButton.setTitle(vc5G.channelNumNow, for: .normal)
        }
        channelNum2 = vc5G.channelNumNow
        
        var temp1 : [String] = []
        var temp2 : [String] = []
        
        temp1 = statusHtmData.components(separatedBy: "ssid_drv[1] ='")
        ///判斷是否離線 若離線就顯示alert
        if temp1.count < 2 {
            if isShowAlert == true{
                alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        
        temp1 = wizardHtmData.components(separatedBy: "pskValue[1]='")
        if temp1.count < 2 {
            if isShowAlert == true{
                alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        
        temp1 = wizardHtmData.components(separatedBy: "IEEE80211r_enable[1]=")
        if temp1.count < 2 {
            if isShowAlert == true{
                alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: ";")
        let isRoaminStr = temp2[0]
        if isRoaminStr == "1"{
        }else{
        }
        temp1 = statusHtmData.components(separatedBy: "mssid_ssid_drv[1][1] ='")
        if temp1.count < 2 {
            if isShowAlert == true{
                alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        print("temp2.count:\(temp2.count),temp2[0]:\(temp2[0])")
        if temp2[0] == ""{
            print("沒有開 GN")
        }else{
            temp1 = statusHtmData.components(separatedBy: "mssid_wep[1][1] ='")
            if temp1.count < 2 {
                if isShowAlert == true{
                    alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                }
                self.actView.removeFromSuperview()
                return
            }
            temp2 = temp1[1].components(separatedBy: "';")
            if temp2[0] == "Open"{
                print("無安全性")
            }else{
                temp1 = statusHtmData.components(separatedBy: "mssid_psk[1][1] ='")
                if temp1.count < 2 {
                    if isShowAlert == true{
                        alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                    }
                    self.actView.removeFromSuperview()
                    return
                }
                temp2 = temp1[1].components(separatedBy: "';")
            }
        }
    }
    
    @IBAction func refreshClick(_ sender: UIButton) {
        RefreshWirelessVC()
    }
    /**
     側邊欄顯示與否的收合動作
     - Date: 08/26 Yang
    */
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
        print("originX3 \(String(describing: navigationController?.view.superview?.frame.origin.x))")
        if navigationController?.view.superview?.frame.origin.x != 0 {
            showContentView()
        }else{
            removeSubview()
        }
    }
    /**
     contentView作用為按一下無線網路頁的屏幕就把側邊欄收起用的
     - Date: 08/26 Yang
    */
    @objc func showContentView(){
        contentView.tag = 100
        contentView.isUserInteractionEnabled = true
        contentView.backgroundColor = .clear
        wirelessContainerView.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(wirelessContainerView)
        }
    }
    /**
     側邊欄一旦收起後 就把contentView給移除掉 以便可以觸控無線網路的元件
     - Date: 08/26 Yang
    */
    @objc func removeSubview(){
        print("Start remove sibview")
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    /**
     點擊屏幕關閉側邊欄的手勢
     - Date: 08/26 Yang
    */
    @IBAction func handleTapGesture(_ sender: Any) {
        print("originX4 \(String(describing: navigationController?.view.superview?.frame.origin.x))")
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
            removeSubview()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }

}

//1）如果我们需要竖向的滑动效果，就把width设为和scrollview相同，如果需要横向的滑动 就把height设为和scrollview相同
//2）使用了SnapKit或者说AutoLayout,那么就不需要设置UIScrollView的ContentSize了，系统会根据constraints自己来确定ContentSize的大小
// 底部一定要make.bottom.equalToSuperview()，不然不能够确定contentSize。
