//
//  OtherViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct OptionType{
    var iconImageView: UIImageView!
    var leftTextLabel: String = ""
    var rightTextLabel: String = ""
}
/**
 其他頁
 - Date: 08/25 Yang
*/
class OtherViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "OptionTypeCell")
        cell.imageView?.image = types[indexPath.row].iconImageView.image
        let itemSize = CGSize.init(width: 20, height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
        let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();

        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].leftTextLabel
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .black
        ///語言確認變更後 在此頁同步刷新顯示
        if indexPath.row == 6{
            if let languageType = UserDefaults.standard.object(forKey:"languageType") as? String {
                cell.detailTextLabel?.text = languageType
            }
        }else{
            cell.detailTextLabel?.text = types[indexPath.row].rightTextLabel
        }
        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
        cell.detailTextLabel?.textColor = .black
        ///根據不同的裝置有不同的字體大小
        switch currentDevice.deviceType {
        case .iPhone40:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        case .iPhone47:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        case .iPad:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 22)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        default:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        }
        ///detailButton包覆整個TableViewCell 用來當作didSelectRow使用
        let detailButton = UIButton()
        detailButton.backgroundColor = .clear
        cell.contentView.addSubview(detailButton)
        detailButton.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        detailButton.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        return cell
    }
    /**
     用Button取代didSelectRow
     - Date: 08/26 Yang
    */
    @objc func clickToDetail(_ sender: UIButton){
        if navigationController?.view.superview?.frame.origin.x == 0 {
            let point = sender.convert(CGPoint.zero, to: OptionTableView)
            if let indexPath = OptionTableView.indexPathForRow(at: point) {
                print("cell_Detail \(indexPath.row)")
                if indexPath.row == 0{
                    let vc = getVC(storyboardName: "Main", identifier: "AdvancedSetViewController") as! AdvancedSetViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 1{
                    let vc = getVC(storyboardName: "Main", identifier: "ShowClientViewController") as! ShowClientViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 2{
                    ///新功能 待實作
                    self.setActView(actView: self.actView)
                    Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
                        self.alertMsg(title: localString(key: "CHECK_FIRMWARE_VERSION", table: .pageTitle), msg: "OTP伺服器錯誤")
                        self.actView.removeFromSuperview()
                    }
                }else if indexPath.row == 3{
                    let vc = getVC(storyboardName: "Main", identifier: "ChangePasswordViewController") as! ChangePasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 4{
                    let vc = getVC(storyboardName: "Main", identifier: "SignUpCloudViewController") as! SignUpCloudViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 5{
                    let vc = getVC(storyboardName: "Main", identifier: "DeviceBindViewController") as! DeviceBindViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 6{
                    let vc = getVC(storyboardName: "Main", identifier: "LanguageViewController") as! LanguageViewController
                    vc.LanguageDelegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 9{
                    ///登出回首頁
                    let controller = UIAlertController(title: localString(key: "LOGOUT", table: .alertTitle), message: localString(key: "LOGOUT", table: .alertMsg), preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default, handler: { cancelAction in
                        self.setActView(actView: self.actView)
                        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                            let VC = getVC(storyboardName: "Main", identifier: "rootNavigationController")
                            VC.modalPresentationStyle = .fullScreen
                            guard let window = UIApplication.shared.keyWindow else {
                                return
                            }
                            self.dismiss(animated: true, completion: nil)
                            window.rootViewController = VC
                            self.actView.removeFromSuperview()
                        }
                    })
                    let cancelAction2 = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
                    controller.addAction(cancelAction2)
                    controller.addAction(cancelAction)
                    present(controller, animated: true, completion: nil)
                }
            }
        }else{
            navigationController?.view.menu()
        }
    }
    
    func setTableView(){
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        OptionTableView = UITableView()
        OptionTableView.register(UITableViewCell.self, forCellReuseIdentifier: "OptionTypeCell")
        OptionTableView.delegate = self
        OptionTableView.dataSource = self
        OptionTableView.bounces = false
        OptionTableView.allowsSelection = true
        OptionTableView.backgroundColor = .white
        OptionTableView.rowHeight = fullScreenSize.height*0.075
        self.view.addSubview(OptionTableView)
        OptionTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(navigationBarHeight)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(OptionTableView.rowHeight*10)
            make.centerX.equalToSuperview()
        }
    }
    
    var types = [
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Advanced Setting-b")), leftTextLabel: localString(key: "ADVANCED_SETTINGS", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Show Client-b")), leftTextLabel: localString(key: "SHOW_CLIENTS", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "check-version")), leftTextLabel: localString(key: "CHECK_FIRMWARE_VERSION", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView:UIImageView(image: UIImage(named: "change-login-password")), leftTextLabel: localString(key: "CHANGE_LOGIN_PASSWORD", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "cloud")), leftTextLabel: localString(key: "REGISTER_CLOUD", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "link")), leftTextLabel: localString(key: "DEVICE_BINDING", table: .pageTitle), rightTextLabel: ""),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "Language-b")), leftTextLabel: localString(key: "LANGUAGE", table: .pageTitle), rightTextLabel: languageType),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "firmware-version")), leftTextLabel: localString(key: "VERSION_INFO", table: .pageTitle), rightTextLabel: "0.0.0.0"),
        OptionType(iconImageView:UIImageView(image: UIImage(named: "version-b")), leftTextLabel: localString(key: "APP_VERSION", table: .pageTitle), rightTextLabel: "1.0.9"),
        OptionType(iconImageView: UIImageView(image: UIImage(named: "logout-b")), leftTextLabel: localString(key: "LOGOUT", table: .pageTitle), rightTextLabel: "")
    ]
    
    let actView : UIView = UIView()
    let titleLabel = UILabel()
    let navigationBarView = UIImageView()
    private var OptionTableView: UITableView!
    var OptionTypes: [OptionType] = []
    var LanguageViewController : LanguageViewController?
    weak var LanguageDelegate: LanguageDelegate?
    @IBOutlet var MenuButton : UIButton!
    var MenuButton2 = UIButton(type: .custom)
    /**
     跟語言頁做資料傳遞溝通用
     - Date: 08/26 Yang
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc1 = segue.destination as? LanguageViewController {
            print("@prepare find LanguageViewController")
            LanguageViewController = vc1
            LanguageViewController?.LanguageDelegate = self
        }
    }
    
    func setDelegate(){
        print("setDelegate")
        let vc = getVC(storyboardName: "Main", identifier: "LanguageViewController") as! LanguageViewController
        print(vc)
        LanguageViewController = vc
        LanguageViewController?.LanguageDelegate = self
    }
    /**
     語言變更後需刷新頁面
     - Date: 08/26 Yang
    */
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        OptionTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        setTableView()
    }
    
    func setViews(){
        let bkView = UIView()
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(statusBarHeight)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        titleLabel.backgroundColor = .clear
        titleLabel.text = localString(key: "OTHERS", table: .pageTitle)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        MenuButton2.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
        MenuButton2.tintColor = SecondaryColor3
        MenuButton2.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
        self.view.addSubview(MenuButton2)
        MenuButton2.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel)
            makes.width.height.equalTo(buttonSize)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    
    func reloadString(){
        titleLabel.text = localString(key: "OTHERS", table: .pageTitle)
        types[0].leftTextLabel = localString(key: "ADVANCED_SETTINGS", table: .pageTitle)
        types[1].leftTextLabel = localString(key: "SHOW_CLIENTS", table: .pageTitle)
        types[2].leftTextLabel = localString(key: "CHECK_FIRMWARE_VERSION", table: .pageTitle)
        types[3].leftTextLabel = localString(key: "CHANGE_LOGIN_PASSWORD", table: .pageTitle)
        types[4].leftTextLabel = localString(key: "REGISTER_CLOUD", table: .pageTitle)
        types[5].leftTextLabel = localString(key: "DEVICE_BINDING", table: .pageTitle)
        types[6].leftTextLabel = localString(key: "LANGUAGE", table: .pageTitle)
        types[7].leftTextLabel = localString(key: "VERSION_INFO", table: .pageTitle)
        types[8].leftTextLabel = localString(key: "APP_VERSION", table: .pageTitle)
        types[9].leftTextLabel = localString(key: "LOGOUT", table: .pageTitle)
    }
}

extension OtherViewController: LanguageDelegate{
    func notiftNewLanguage() {
//        print("notiftNewLanguage @ OtherViewController")
        modifySubsViews()
        self.LanguageDelegate?.notiftNewLanguage()
    }
    func modifySubsViews() {
//        print("modifySubsViews @ OtherViewController")
//        self.ta
        reloadString()
        OptionTableView.reloadData()
    }
}
