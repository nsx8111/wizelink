//
//  ConnectViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/6.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import KMPageMenu
/**
 連線狀態頁
 - Date: 08/25 Yang
*/
class ConnectViewController: UIViewController {
    
    var LeftMenuViewController: LeftMenuViewController?
    
    let actView : UIView = UIView()
    let navigationBarView = UIImageView()
    let titleLabel = UILabel()
    let connectNumLabel = UILabel()
    let blueBKView = UIImageView()
    let bkView2 = UIImageView()
    let bkView3 = UIImageView()
    let WAPView = UIImageView()
    let bkView5 = UIImageView()
    let bkView6 = UIImageView()
    let bkView4 = UIImageView()
    let internetLabel = UILabel()
    let deviceLabel = UILabel()
    let showBKViewBtn = UIButton()
    let showBKViewBtn2 = UIButton()
    let WAPBtn = UIButton()
    let lineView1 = UIView()
    let lineView2 = UIView()
    var isClosed : Bool?
    var connectNum : Int = 0
    var titles : [String] = [localString(key: "INTERNET", table: .tabTitle),localString(key: "ROUTER", table: .tabTitle),localString(key: "DEVICES", table: .tabTitle)]
    
    //    let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: self.titles)
    /** 分頁標籤 */
    lazy var menu: KMPageMenu = {
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        let m = KMPageMenu(frame: CGRect(x: 0, y: navBarHeight+bkViewHeight, width: fullScreenSize.width, height: ButtonHeight), titles: titles)
        
        m.style.normalTitleColor = .lightGray
        m.style.selectedTitleColor = .black
        m.style.titleFont = UIFont.boldSystemFont(ofSize: 17)
        m.isScrollEnable = false
        m.layer.shadowOffset = CGSize(width: 0, height: 1)
        m.layer.shadowOpacity = 0.3
        m.layer.shadowRadius = 3
        m.backgroundColor = .white
        view.addSubview(m)
        return m
    }()
    
    /** 連結控制 CoInternetViewController */
    let vc1: CoInternetViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoInternetViewController.self)) as! CoInternetViewController
    /** 連結控制 CoRouterViewController */
    let vc2: CoRouterViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoRouterViewController.self)) as! CoRouterViewController
    /** 連結控制 CoDevicesTableViewController */
    let vc3: CoDevicesTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoDevicesTableViewController.self)) as! CoDevicesTableViewController
    /** 連結控制 CDDetailTableViewController */
    let vc4: CDDetailTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CDDetailTableViewController.self)) as! CDDetailTableViewController
    
    
    lazy var menuBkView = UIView()
    /** 設定分頁 */
    lazy var page: KMPagingViewController = {
        var viewControllers: [UIViewController] = []
        
        vc1.pageViewDelegate = self
        vc2.pageViewDelegate = self
        vc3.pageViewDelegate = self
        vc4.pageViewDelegate = self
        
        viewControllers.append(vc1)
        viewControllers.append(vc2)
        viewControllers.append(vc3)
        viewControllers.append(vc4)
        
        let p = KMPagingViewController(viewControllers: viewControllers)
        
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navBarH = navBarHeight
        
        p.view.frame = CGRect(x: 0,
                              y: navBarHeight+bkViewHeight+ButtonHeight+3,
                              width: fullScreenSize.width,
                              height: fullScreenSize.height-(navBarHeight+bkViewHeight+ButtonHeight+3))
        
        self.addChild(p)
        p.didMove(toParent: self)
        p.isScrollEnable = false
        self.view.addSubview(p.view)
        return p
    }()
    
    weak var MenuDelegate: MenuDelegate?
    @IBOutlet weak var RefreshButton: UIButton!
    @IBOutlet var MenuButton : UIButton!
    var MenuButton2 = UIButton(type: .custom)
    var RefreshButton2 = UIButton(type: .custom)
    
    var pageLanguage : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = PrimaryColor1
        print("MenuButton.frame:\(MenuButton.frame)")
        ///抓取當前裝置的IP位址
        let WiFiAddress : String = getWiFiAddress() ?? "0.0.0.0"
        print("current_WiFiAddress \(WiFiAddress)")
        currentWiFiAddress = WiFiAddress
        
        self.setViews()
        self.setConnectSubviews()
        self.vc1.searchInfo()
        self.vc2.searchInfo()
        self.vc3.searchInfo()
        self.vc3.tableView.reloadData()
        
//        self.getStatusHtm(){
//            // load temp data for server error
//            if serverStatus == nil {
//                print("serverStatus:\(serverStatus)")
//                statusHtmData = tempStatusHtmData
//                clientsHtmData = tempClientsHtmData
//            }
//            self.setViews()
//            self.setConnectSubviews()
//        }
    }
    /**
     刷新三個分頁資料
     - Date: 08/26 Yang
    */
    func RefreshConnectVC(){
        let WiFiAddress : String = getWiFiAddress() ?? "0.0.0.0"
        print("current_WiFiAddress \(WiFiAddress)")
        currentWiFiAddress = WiFiAddress
        
        self.vc3.searchInfo()
        self.vc2.searchInfo()
        self.vc1.searchInfo()
        if ipValue == "" || ipValue == "0.0.0.0" {
            self.vc2.connectFail()
            self.vc3.connectFail()
            ///icon變為離線狀態樣式
            self.WAPView.image = UIImage(named: "3514-off")
            self.bkView2.image = UIImage(named: "internet-Not-Connected")
        }else{
            self.WAPView.image = UIImage(named: "3514-on")
            self.bkView2.image = UIImage(named: "internet-Connected")
        }
        self.vc3.tableView.reloadData()
        self.actView.removeFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = NSNotification.Name("Refresh_ConnectVC")
        NotificationCenter.default.addObserver(self, selector:
            #selector(RefreshNoti(noti:)), name: name, object: nil)
        
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        ///隱藏原生的NavigationBar 顯示客製化navigationBarView
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setMenuButtonView()
        checkLanguage()
    }
    
    @objc func RefreshNoti(noti: Notification) {
        RefreshConnectVC()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        titles = [localString(key: "INTERNET", table: .tabTitle),localString(key: "ROUTER", table: .tabTitle),localString(key: "DEVICES", table: .tabTitle)]
        self.menu.titles = titles
        internetLabel.text = localString(key: "INTERNET", table: .tabTitle)
        deviceLabel.text = localString(key: "DEVICE", table: .tabTitle)
        titleLabel.text = localString(key: "CONNECTION", table: .pageTitle)
    }
    
    func setConnectSubviews(){
        //        let titles : [String] = ["網際網路", "路由器", "裝置"]
        //        let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: titles)
        //        let menu = KMPageMenu(titles: titles)
        //        m.style.titleFont = UIFont.systemFont(ofSize: 14)
        
        
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        menuBkView.frame = CGRect(x: 0, y: navBarHeight+bkViewHeight, width: fullScreenSize.width, height: ButtonHeight+3)
        menuBkView.backgroundColor = .white
        view.addSubview(menuBkView)
        ///陰影效果設定
        menu.layer.shadowOffset = CGSize(width: 0, height: 1)
        menu.layer.shadowOpacity = 0.43
        menu.layer.shadowRadius = 2
        
        menu.applyLabelWidthFixed()
        menu.applyLinePositionBottom()
        menu.applyIndicatorLineFixed()
        menu.addTarget(self, action: #selector(menuValueChange(sender:)), for: .valueChanged)
        menu.valueChange = { [weak self] index in
            self?.page.pagingToViewController(at: index)
        }
        
        page.delegate = self
        page.didFinishPagingCallBack = { [weak self] (currentViewController, currentIndex)in
            self?.menu.setSelectIndex(index: currentIndex, animated: true)
        }
        
        //        menu.snp.makeConstraints { (makes) in
        //            makes.left.right.equalToSuperview()
        //            makes.top.equalTo(connectNumLabel.snp.bottom).offset(10)
        //            makes.height.equalTo(60)
        //        }
    }
    /**
     點選按鈕讓畫面緩慢上升 並讓上面的WAP圖被隱藏或重新展開
     - Date: 08/26 Yang
    */
    @objc func clickShowBKView(){
        print("menu origin y \(self.menu.frame.origin.y)")
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.57, delay: 0, animations: {
            if self.isClosed == false {
                self.page.view.frame.origin.y = navBarHeight+bkViewHeight+ButtonHeight+3-bkViewHeight*3/4
                self.page.view.frame.size.height = fullScreenSize.height-(navBarHeight+bkViewHeight+ButtonHeight+3)+bkViewHeight*3/4
                self.menu.frame.origin.y = navBarHeight+bkViewHeight-bkViewHeight*3/4
                self.menuBkView.frame.origin.y = navBarHeight+bkViewHeight-bkViewHeight*3/4
                self.showBKViewBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (-180))
                self.showBKViewBtn.snp.updateConstraints { (makes) in
                    makes.top.equalToSuperview().offset(navBarHeight/1.5+bkViewHeight-spacing2-bkViewHeight*3/4)
                }
                self.connectNumLabel.alpha = 0
                self.bkView2.alpha = 0
                self.bkView3.alpha = 0
                self.WAPView.alpha = 0
                self.bkView5.alpha = 0
                self.bkView6.alpha = 0
                self.bkView4.alpha = 0
                self.internetLabel.alpha = 0
                self.deviceLabel.alpha = 0
                self.lineView1.alpha = 0
                self.lineView2.alpha = 0
                self.isClosed = true
            }else{
                self.page.view.frame.size.height = fullScreenSize.height-(navBarHeight+bkViewHeight+ButtonHeight+3)
                self.page.view.frame.origin.y = navBarHeight+bkViewHeight+ButtonHeight+3
                self.menu.frame.origin.y = navBarHeight+bkViewHeight
                self.menuBkView.frame.origin.y = navBarHeight+bkViewHeight
                self.showBKViewBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (0))
                self.showBKViewBtn.snp.updateConstraints { (makes) in
                    makes.top.equalToSuperview().offset(navBarHeight/1.5+bkViewHeight-spacing1/2-spacing3)
                }
                self.connectNumLabel.alpha = 1
                self.bkView2.alpha = 1
                self.bkView3.alpha = 1
                self.WAPView.alpha = 1
                self.bkView5.alpha = 1
                self.bkView6.alpha = 1
                self.bkView4.alpha = 1
                self.internetLabel.alpha = 1
                self.deviceLabel.alpha = 1
                self.lineView1.alpha = 1
                self.lineView2.alpha = 1
                self.isClosed = false
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    /**
     點選WAP裝置圖直接跳轉到Mesh頁 並改變側邊欄LeftMenu的cell的背景色
     - Date: 08/26 Yang
    */
    @objc func clickToMesh(sender:Any){
        MenuDelegate?.changeUserMenuView(6)
        
        if let indexPath = selectedIndexPath2{
            print("selectedIndexPath2 \(selectedIndexPath2.row)")
            if indexPath.row == 2{
                leftCell.contentView.backgroundColor = SecondaryColor3
                LeftMenuViewController?.MenuTableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        if let indexPath2 = selectedIndexPath3{
            if indexPath2.row == 1{
                leftCell2.contentView.backgroundColor = LightSecondaryColor5
                LeftMenuViewController?.MenuTableView.reloadRows(at: [indexPath2], with: .automatic)
            }
        }
    }
    /**
    初始畫面佈局
     - Date: 08/26 Yang
    */
    func setViews(){
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        blueBKView.backgroundColor = PrimaryColor1
        self.view.addSubview(blueBKView)
        blueBKView.snp.makeConstraints{ (makes) in
            makes.centerX.equalToSuperview()
            makes.width.equalTo(fullScreenSize.width)
            makes.top.equalToSuperview().offset(navBarHeight/1.65)
            makes.height.equalTo(bkViewHeight)
        }
        
        let bkView4Radius = loginLabelHeight2*2+spacing1
        bkView4.layer.cornerRadius = bkView4Radius
        bkView4.backgroundColor = LightSecondaryColor
        view.addSubview(bkView4)
        bkView4.snp.makeConstraints{ (makes) in
            makes.left.right.equalTo(blueBKView).inset((fullScreenSize.width-bkView4Radius*2)/2)
            makes.top.bottom.equalTo(blueBKView).inset((bkViewHeight-bkView4Radius*2)/2)
        }
        
        bkView3.layer.cornerRadius = loginLabelHeight2*2
        bkView3.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView3)
        bkView3.snp.makeConstraints{ (makes) in
            makes.left.right.equalTo(blueBKView).inset((fullScreenSize.width-loginLabelHeight2*4)/2)
            makes.top.bottom.equalTo(blueBKView).inset((bkViewHeight-loginLabelHeight2*4)/2)
        }
        
        WAPView.image = UIImage(named: "3514-on")
        view.addSubview(WAPView)
        WAPView.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView3).inset(spacing1)
        }
        
        WAPBtn.backgroundColor = .clear
        WAPBtn.addTarget(self,action: #selector(self.clickToMesh),for: .touchUpInside)
        self.view.addSubview(WAPBtn)
        WAPBtn.snp.makeConstraints { (makes) in
            makes.edges.equalTo(WAPView)
        }
        
        lineView1.backgroundColor = PrimaryColor2
        view.addSubview(lineView1)
        lineView1.snp.makeConstraints{ (makes) in
            makes.centerY.equalTo(WAPView)
            makes.right.equalTo(WAPView.snp.left).offset(6)
            makes.width.equalTo(spacing3*2-3)
            makes.height.equalTo(1)
        }
        
        lineView2.backgroundColor = PrimaryColor2
        view.addSubview(lineView2)
        lineView2.snp.makeConstraints{ (makes) in
            makes.centerY.equalTo(WAPView)
            makes.left.equalTo(WAPView.snp.right).offset(-6)
            makes.width.equalTo(spacing3*2-3)
            makes.height.equalTo(1)
        }
        
        bkView5.layer.cornerRadius = spacing3*3/2
        bkView5.backgroundColor = PrimaryColor2
        self.view.addSubview(bkView5)
        bkView5.snp.makeConstraints{ (makes) in
            makes.centerY.equalTo(blueBKView)
            makes.height.width.equalTo(spacing3*3)
            makes.left.equalTo(bkView3.snp.right).offset(spacing2*2)
        }
        
        bkView6.layer.cornerRadius = spacing3*3/2-2
        bkView6.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView6)
        bkView6.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView5).inset(2)
        }
        
        ///連線裝置數
        connectNumLabel.layer.cornerRadius = spacing3*3/2
        connectNumLabel.backgroundColor = .clear
        connectNumLabel.textAlignment = .center
        connectNumLabel.text = "\(connectNum)"
        connectNumLabel.textColor = .white
        connectNumLabel.font = UIFont.systemFont(ofSize: 32)
        view.addSubview(connectNumLabel)
        connectNumLabel.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView6).inset(4)
        }
        
        bkView2.image = UIImage(named: "internet-Connected")
        view.addSubview(bkView2)
        bkView2.snp.makeConstraints{ (makes) in
            makes.centerY.equalTo(blueBKView)
            makes.height.width.equalTo(spacing3*3)
            makes.right.equalTo(bkView3.snp.left).offset(-spacing2*2)
        }
        
        internetLabel.text = localString(key: "INTERNET", table: .tabTitle)
        internetLabel.textAlignment = .center
        internetLabel.textColor = PrimaryColor2
        internetLabel.font = UIFont.boldSystemFont(ofSize: 14)
        view.addSubview(internetLabel)
        internetLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView2)
            makes.top.equalTo(bkView2.snp.bottom).offset(spacing1/2)
        }
        
        deviceLabel.text = localString(key: "DEVICE", table: .tabTitle)
        deviceLabel.textAlignment = .center
        deviceLabel.textColor = PrimaryColor2
        deviceLabel.font = UIFont.boldSystemFont(ofSize: 14)
        view.addSubview(deviceLabel)
        deviceLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView5)
            makes.top.equalTo(bkView5.snp.bottom).offset(spacing1/2)
        }
        
        showBKViewBtn.setImage(UIImage(named: "down-w"), for: .normal)
        showBKViewBtn.addTarget(self,action: #selector(self.clickShowBKView),for: .touchUpInside)
        self.view.addSubview(showBKViewBtn)
        showBKViewBtn.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(blueBKView)
            makes.top.equalToSuperview().offset(navBarHeight/1.5+bkViewHeight-spacing1/2-spacing3)
            makes.width.height.equalTo(spacing3)
        }
        ///與showBKViewBtn同一個功能
        showBKViewBtn2.backgroundColor = .clear
        showBKViewBtn2.addTarget(self,action: #selector(self.clickShowBKView),for: .touchUpInside)
        self.view.addSubview(showBKViewBtn2)
        showBKViewBtn2.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(showBKViewBtn).inset(-spacing3)
        }
        isClosed = false
    }
    
    func setMenuButtonView(){
        let navBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navBarHeight)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        titleLabel.backgroundColor = .clear
        titleLabel.text = localString(key: "CONNECTION", table: .pageTitle)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        ///客製化按鈕 取代storyboard的MenuButton
        MenuButton2.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
        MenuButton2.tintColor = SecondaryColor3
        MenuButton2.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
        self.view.addSubview(MenuButton2)
        MenuButton2.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel)
            makes.width.height.equalTo(buttonSize)
            makes.left.equalToSuperview().offset(spacing3)
        }
        ///客製化按鈕 取代storyboard的RefreshButton
        RefreshButton2.setImage(UIImage(named: "reload-outline")!.withRenderingMode(.alwaysTemplate), for: .normal)
        RefreshButton2.tintColor = SecondaryColor3
        RefreshButton2.addTarget(self,action: #selector(self.refreshClick),for: .touchUpInside)
        self.view.addSubview(RefreshButton2)
        RefreshButton2.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel)
            makes.width.height.equalTo(buttonSize)
            makes.right.equalToSuperview().offset(-spacing3)
        }
        
//        MenuButton.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
//        MenuButton.tintColor = SecondaryColor3
//        MenuButton.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
//        self.view.addSubview(MenuButton)
//        MenuButton.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(titleLabel)
//            makes.width.height.equalTo(loginLabelHeight)
//            makes.left.equalToSuperview().offset(spacing3)
//        }
//
//        RefreshButton.setImage(UIImage(named: "reload-outline")!.withRenderingMode(.alwaysTemplate), for: .normal)
//        RefreshButton.tintColor = SecondaryColor3
//        RefreshButton.addTarget(self,action: #selector(self.refreshClick),for: .touchUpInside)
//        self.view.addSubview(RefreshButton)
//        RefreshButton.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(titleLabel)
//            makes.width.height.equalTo(loginLabelHeight)
//            makes.right.equalToSuperview().offset(-spacing3)
//        }
        
    }
    
    /** 切換頁面時的動作 */
    @objc func menuValueChange(sender: KMPageMenu) {
        print("a)selectIndex == \(sender.selectIndex)")
    }
    
    func callClick(){
        navigationController?.view.menu()
    }
    /**
     執行刷新資料
     - Date: 08/26 Yang
    */
    @IBAction func refreshClick(_ sender: UIButton) {
        self.setActView(actView: self.actView)
        getData{
            ///同時刷新StatusVC searchInfo()
            let name = Notification.Name("Refresh_StatusVC")
            NotificationCenter.default.post(name: name, object: nil, userInfo:
                nil)
            self.RefreshConnectVC()
        }
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    
}


// MARK: - 指示器类型
extension KMPageMenu {
    /// 标题固定宽度
    func applyLabelWidthFixed() {
        var aStyle = self.style
        aStyle.labelWidthType = .fixed(width: UIScreen.main.bounds.size.width/3)
        self.style = aStyle
    }
    
    // 横线指示器固定宽度
    func applyIndicatorLineFixed() {
        var aStyle = self.style
        aStyle.indicatorColor = SecondaryColor3
        aStyle.indicatorPendingHorizontal = 8
        aStyle.indicatorCorner = .none
        aStyle.indicatorStyle = .line(widthType: .fixed(width: UIScreen.main.bounds.size.width/3), position: .bottom((margin: 0, height: 5)))
        self.style = aStyle
    }
    
    // 横线指示器在下
    func applyLinePositionBottom() {
        var aStyle = self.style
        let widthType = aStyle.indicatorStyle.widthType
        aStyle.indicatorColor = .orange
        aStyle.indicatorPendingHorizontal = 8
        aStyle.indicatorStyle = .line(widthType: widthType, position: .bottom((margin: 1, height: 2)))
        self.style = aStyle
    }
}

extension ConnectViewController: KMPagingViewControllerDelegate {
    func pagingController(_ pagingController: KMPagingViewController, didFinish currentViewController: UIViewController, currentIndex: Int) {
        print("b)selectIndex == \(currentIndex)")
        //        menu.setSelectIndex(index: currentIndex, animated: true)
    }
}

extension ConnectViewController: PageViewDelegate{
    /**
     讓 LiveTvView 可以改變 RecordListView 上面的 presentChannelID
     - Parameter id: 傳入的ID，去讓 RecordListView 上面的 presentChannelID 變成跟 LiveTvView 一樣
     */
    func changeRecordListChannelId(id: Int) {
        //        self.vc2.presentChannelID = id
    }
    /** For test */
    func sendSomeThing(_ url: String) {
        print("tap epg cell, url:\(url)")
        //for test, can change page by code
        //        self.page.pagingToViewController(at: 1)
    }
    
    /**
     讓分頁可以控制 MainPage 上面的 actView 顯示與否（用在讀取網路資料的等待時間上）
     - Parameter bool: 若是為 true 則隱藏，反之顯示是等待(讀取)中
     */
    func callActView(bool: Bool){
        DispatchQueue.main.async {
            //            self.actView.isHidden = bool
        }
    }
    //test
    func testToVC4(){
        self.page.pagingToViewController(at: 3)
    }
    
    func toDetailPage(device: Device) {
        vc4.deviceDetail = device
        self.page.pagingToViewController(at: 3)
    }
    
    func backToVc3(){
        self.page.pagingToViewController(at: 2)
    }
    
    func changeConnectNum(num: Int){
        self.connectNum = num
        DispatchQueue.main.async { //連線裝置數：
            self.connectNumLabel.text = "\(self.connectNum)"
        }
        //        self.connectNumLabel.text = String(num)
    }
    
}
