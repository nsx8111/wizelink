//
//  MeshViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/5/20.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

struct StationInfoDetail {
    var station_mac : String = ""
    var station_rssi : String = ""
    var station_uplink : String = ""
    var station_downlink : String = ""
    var station_connected_band : String = ""
}
/**
 根據不同的裝置數去顯示mesh圖
 - Date: 08/26 Yang
*/
enum DevicesMeshType:Int {
    case child_0 = 0 //裝置數1
    case child_1 = 1 //裝置數2
    case child_2 = 2 //裝置數3 並聯
    case child_3 = 3 //裝置數3 串聯
    case child_4 = 4 //裝置數4
    case child_5 = 5 //裝置數5
}
/**
 Mesh圖
 - Date: 08/25 Yang
*/
class MeshViewController: UIViewController {
    
    var currentMeshType: DevicesMeshType = .child_0
    
    let titleLabel = UILabel()
    let navigationBarView = UIImageView()
    let actView : UIView = UIView()
    
    let deviceButton = UIButton()
    let stationImage = UIImageView()
    var stationNumLabel = UILabel()
    var deviceNameLabel = UILabel()
    let deviceButton2 = UIButton()
    let stationImage2 = UIImageView()
    var stationNumLabel2 = UILabel()
    var deviceNameLabel2 = UILabel()
    let deviceButton3 = UIButton()
    let stationImage3 = UIImageView()
    var stationNumLabel3 = UILabel()
    var deviceNameLabel3 = UILabel()
    let deviceButton4 = UIButton()
    let stationImage4 = UIImageView()
    var stationNumLabel4 = UILabel()
    var deviceNameLabel4 = UILabel()
    let deviceButton5 = UIButton()
    let stationImage5 = UIImageView()
    var stationNumLabel5 = UILabel()
    var deviceNameLabel5 = UILabel()
    /**
     WAP圖的背景底圖
     - Date: 08/26 Yang
    */
    let bkView1 = UIImageView()
    let bkView2 = UIImageView()
    let bkView3 = UIImageView()
    let bkView4 = UIImageView()
    let bkView5 = UIImageView()
    let bkView6 = UIImageView()
    let bkView7 = UIImageView()
    let bkView8 = UIImageView()
    let bkView9 = UIImageView()
    let bkView10 = UIImageView()
    let bkView11 = UIImageView()
    let bkView12 = UIImageView()
    let bkView13 = UIImageView()
    let bkView14 = UIImageView()
    let bkView15 = UIImageView()
    let bkView16 = UIImageView()
    let bkView17 = UIImageView()
    let bkView18 = UIImageView()
    let bkView444 = UIImageView()
    /**
     WAP圖
     - Date: 08/26 Yang
    */
    let WAPView1 = UIImageView()
    let WAPView2 = UIImageView()
    let WAPView3 = UIImageView()
    let WAPView4 = UIImageView()
    let WAPView5 = UIImageView()
    let WAPView6 = UIImageView()
    let WAPView7 = UIImageView()
    let WAPView8 = UIImageView()
    let WAPView9 = UIImageView()
    /**
     連接WAP圖的虛線
     - Date: 08/26 Yang
    */
    var lineView1 = UIImageView()
    var lineView2 = UIImageView()
    var lineView3 = UIImageView()
    var lineView4 = UIImageView()
    var lineView5 = UIImageView()
    var lineView6 = UIImageView()
    var lineView7 = UIImageView()
    var lineView8 = UIImageView()
    var lineView9 = UIImageView()
    var lineView10 = UIImageView()
    var lineView11 = UIImageView()
    /**
     變換裝置位置的動態佈局
     - Date: 08/26 Yang
    */
    var constraint1 : Constraint?
    var constraint2 : Constraint?
    var constraint3 : Constraint?
    var constraint4 : Constraint?
    var constraint5 : Constraint?
    var constraint6 : Constraint?
    var constraint7 : Constraint?
    var constraint8 : Constraint?
    var constraint9 : Constraint?
    var constraint10 : Constraint?
    var constraint11 : Constraint?
    var constraint12 : Constraint?
    var constraint13 : Constraint?
    var constraint14 : Constraint?
    var constraint15 : Constraint?
    var constraint16 : Constraint?
    var constraint17 : Constraint?
    var constraint18 : Constraint?
    var constraint19 : Constraint?
    var constraint20 : Constraint?
    var constraint21 : Constraint?
    var constraint22 : Constraint?
    var constraint23 : Constraint?
    var constraint24 : Constraint?
    var constraint25 : Constraint?
    var constraint26 : Constraint?
    var constraint27 : Constraint?
    var constraint28 : Constraint?
    var constraint29 : Constraint?
    var constraint30 : Constraint?
    var constraint31 : Constraint?
    var constraint32 : Constraint?
    var constraint33 : Constraint?
    var constraint34 : Constraint?
    var constraint35 : Constraint?
    
    
    var childCount = 0
    var childCount2 = 0
    var newLineHeight = 0.0
    var newLineHeight2 = 0.0
    var childDevicesArray: [Dictionary<String,Any>] = []
    var childDevicesArray2: [Dictionary<String,Any>] = []
    var deviceName: String = ""
    var stationCount = 0
    var ipAddress: String = ""
    var macAddress: String = ""
    var deviceName2: String = ""
    var ipAddress2: String = ""
    var macAddress2: String = ""
    var stationCount2 = 0
    var deviceName3: String = ""
    var ipAddress3: String = ""
    var macAddress3: String = ""
    var stationCount3 = 0
    var deviceName4: String = ""
    var ipAddress4: String = ""
    var macAddress4: String = ""
    var stationCount4 = 0
    var deviceName5: String = ""
    var ipAddress5: String = ""
    var macAddress5: String = ""
    var stationCount5 = 0
    var stationMac: [String] = []
    var stationRssi: [String] = []
    var stationUplink: [String] = []
    var stationDownlink: [String] = []
    var stationConnectedBand: [String] = []
    var stationMac2: [String] = []
    var stationRssi2: [String] = []
    var stationUplink2: [String] = []
    var stationDownlink2: [String] = []
    var stationConnectedBand2: [String] = []
    var stationMac3: [String] = []
    var stationRssi3: [String] = []
    var stationUplink3: [String] = []
    var stationDownlink3: [String] = []
    var stationConnectedBand3: [String] = []
    var stationMac4: [String] = []
    var stationRssi4: [String] = []
    var stationUplink4: [String] = []
    var stationDownlink4: [String] = []
    var stationConnectedBand4: [String] = []
    var stationMac5: [String] = []
    var stationRssi5: [String] = []
    var stationUplink5: [String] = []
    var stationDownlink5: [String] = []
    var stationConnectedBand5: [String] = []
    var deviceAddress:[WapDevice] = []
    var deviceAddress2:[WapDevice] = []
    var deviceAddress3:[WapDevice] = []
    var deviceAddress4:[WapDevice] = []
    var deviceAddress5:[WapDevice] = []
    
    var pageLanguage : String = ""
//    var tempData: String = tempWizardHtmData_0
    
    @IBOutlet var MenuButton: UIButton!
    @IBOutlet weak var RefreshButton: UIButton!
    var MenuButton2 = UIButton(type: .custom)
    var RefreshButton2 = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = PrimaryColor1
        self.searchInfo()
        self.setViews()
        self.showMesh()
    }
    /**
     隱藏導航欄
     - Date: 08/26 Yang
    */
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        titleLabel.text = localString(key: "MESH_MAP", table: .pageTitle)
    }
    
    func searchInfo(){
//        let temp1 = searchFunc(src: tempData, str1: "var string_json = '", str2: "';")
        let temp1 = searchFunc(src: wizardHtmData, str1: "var string_json = '", str2: "';")
        print("temp_temp \(temp1) ")
        let temp2 = "[" + temp1 + "]"
        print("temp222 \(temp2) 〜(￣▽￣)〜")
        
        //        let testJson = JSON(temp2)
        //        print(testJson)
        //        print("testJson[\"device_name\"]:\(testJson["device_name"])")
        
        if let data = temp2.data(using: .utf8){
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                    ///若為離線狀態 則WAP圖count顯示為零 並彈出警告視窗
                    if jsonArray.count == 0{
                        alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                        return
                    }
                    print("jsonArray \(jsonArray[0])") // use the json here
                    for (key, value) in jsonArray[0] {
                        print("\(key) -> \(value)")
                    }
                    ///第一個裝置資料
                    deviceName = jsonArray[0]["device_name"]! as! String
                    ipAddress = jsonArray[0]["ip_addr"]! as! String
                    var mac_str = jsonArray[0]["mac_address"]! as! String
                    mac_str.insert(":", at: mac_str.index(mac_str.startIndex, offsetBy: 2))
                    mac_str.insert(":", at: mac_str.index(mac_str.startIndex, offsetBy: 5))
                    mac_str.insert(":", at: mac_str.index(mac_str.startIndex, offsetBy: 8))
                    mac_str.insert(":", at: mac_str.index(mac_str.startIndex, offsetBy: 11))
                    mac_str.insert(":", at: mac_str.index(mac_str.startIndex, offsetBy: 14))
                    macAddress = mac_str.uppercased()
                    deviceAddress.removeAll()
                    deviceAddress.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress))
                    deviceAddress.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress))
                    deviceNameLabel.text = deviceName
                    ///第一個裝置的station_info
                    if let stationArray = jsonArray[0]["station_info"] as? [Dictionary<String,Any>] {
                        print("stationArray0 \(stationArray)")
                        stationMac.removeAll()
                        stationRssi.removeAll()
                        stationUplink.removeAll()
                        stationDownlink.removeAll()
                        stationConnectedBand.removeAll()
                        
                        if stationArray.count != 0{
                            for stationInfo in stationArray{
                                var mac_str2 = stationInfo["station_mac"]! as! String
                                stationMac.append(getMacAddress(macStr: mac_str2))
                                stationRssi.append(stationInfo["station_rssi"]! as! String)
                                stationUplink.append(stationInfo["station_uplink"]! as! String)
                                stationDownlink.append(stationInfo["station_downlink"]! as! String)
                                stationConnectedBand.append(stationInfo["station_connected_band"]! as! String)
                            }
                        }
                        stationCount = stationMac.count
                        self.stationNumLabel.text = self.stationCount.description
                        print("stationCountXXXX \(self.stationCount)")
                    }
                    ///僅次於第一個裝置的下面第一列裝置陣列 ex:並聯時最下面兩個裝置陣列
                    childDevicesArray = jsonArray[0]["child_devices"] as? [Dictionary<String,Any>] ?? []
                    print("childDevicesArray \(childDevicesArray) 〜(￣▽￣)〜")
                    print(childDevicesArray.count)
                    childCount = childDevicesArray.count
                    
                    if childDevicesArray.count != 0{
                        if childDevicesArray.count > 4{
                            let range = 4...childDevicesArray.endIndex-1
                            childDevicesArray.removeSubrange(range)
                        }
                        childCount = childDevicesArray.count
                        ///第二個裝置資料
                        deviceName2 = childDevicesArray[0]["device_name"]! as! String
                        ipAddress2 = childDevicesArray[0]["ip_addr"]! as! String
                        var mac_str = childDevicesArray[0]["mac_address"]! as! String
                        macAddress2 = getMacAddress(macStr: mac_str)
                        deviceAddress2.removeAll()
                        deviceAddress2.append(WapDevice(titleLabel: "IP位址", detailLabel: ipAddress2))
                        deviceAddress2.append(WapDevice(titleLabel: "MAC位址", detailLabel: macAddress2))
                        deviceNameLabel2.text = deviceName2
                        
                        if let stationArray = childDevicesArray[0]["station_info"] as? [Dictionary<String,Any>] {
                            print("stationArray22 \(stationArray)")
                            
                            stationMac2.removeAll()
                            stationRssi2.removeAll()
                            stationUplink2.removeAll()
                            stationDownlink2.removeAll()
                            stationConnectedBand2.removeAll()
                            
                            if stationArray.count != 0{
                                for stationInfo in stationArray{
                                    var mac_str = stationInfo["station_mac"]! as! String
                                    stationMac2.append(getMacAddress(macStr: mac_str))
                                    stationRssi2.append(stationInfo["station_rssi"]! as! String)
                                    stationUplink2.append(stationInfo["station_uplink"]! as! String)
                                    stationDownlink2.append(stationInfo["station_downlink"]! as! String)
                                    stationConnectedBand2.append(stationInfo["station_connected_band"]! as! String)
                                }
                            }
                            stationCount2 = stationArray.count
                            self.stationNumLabel2.text = self.stationCount2.description
                        }
                        ///childDevicesArray2為第二列裝置陣列 ex:串聯時最下面一個裝置陣列
                        childDevicesArray2.removeAll()
                        for childArray in childDevicesArray{
                            if let childArray2 = childArray["child_devices"] as? [Dictionary<String,Any>]{
                                childDevicesArray2.append(contentsOf: childArray2)
                                childCount2 = childDevicesArray2.count
                                print("CA2 \(childDevicesArray2)")
                                print(childDevicesArray2.count)
                            }
                        }
                    }
                    ///根據裝置個數來判斷該顯示何種Mesh圖
                    switch childCount {
                    case 0:
                        currentMeshType = .child_0
                    case 1:
                        if childCount2 == 0 {
                            currentMeshType = .child_1
                        }else{
                            ///串聯
                            if childCount2 == 1 {
                                currentMeshType = .child_3
                                ///第三個裝置資料
                                deviceName3 = childDevicesArray2[0]["device_name"]! as! String
                                ipAddress3 = childDevicesArray2[0]["ip_addr"]! as! String
                                var mac_str2 = childDevicesArray2[0]["mac_address"]! as! String
                                macAddress3 = getMacAddress(macStr: mac_str2)
                                deviceAddress3.removeAll()
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                deviceNameLabel3.text = deviceName3
                                
                                if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac3.removeAll()
                                    stationRssi3.removeAll()
                                    stationUplink3.removeAll()
                                    stationDownlink3.removeAll()
                                    stationConnectedBand3.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac3.append(getMacAddress(macStr: mac_str))
                                            stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount3 = stationArray.count
                                    self.stationNumLabel3.text = self.stationCount3.description
                                }
                            }else{
                                if childCount2 == 2{
                                    currentMeshType = .child_4
                                    
                                    deviceName3 = childDevicesArray2[0]["device_name"]! as! String
                                    ipAddress3 = childDevicesArray2[0]["ip_addr"]! as! String
                                    var mac_str33 = childDevicesArray2[0]["mac_address"]! as! String
                                    macAddress3 = getMacAddress(macStr: mac_str33)
                                    deviceAddress3.removeAll()
                                    deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                    deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                    deviceNameLabel3.text = deviceName3
                                    
                                    if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                        print("stationArray \(stationArray)")
                                        
                                        stationMac3.removeAll()
                                        stationRssi3.removeAll()
                                        stationUplink3.removeAll()
                                        stationDownlink3.removeAll()
                                        stationConnectedBand3.removeAll()
                                        
                                        if stationArray.count != 0{
                                            for stationInfo in stationArray{
                                                var mac_str = stationInfo["station_mac"]! as! String
                                                stationMac3.append(getMacAddress(macStr: mac_str))
                                                stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                                stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                                stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                                stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                            }
                                        }
                                        stationCount3 = stationArray.count
                                        self.stationNumLabel3.text = self.stationCount3.description
                                    }
                                    
                                    deviceName4 = childDevicesArray2[1]["device_name"]! as! String
                                    ipAddress4 = childDevicesArray2[1]["ip_addr"]! as! String
                                    var mac_str2 = childDevicesArray2[1]["mac_address"]! as! String
                                    macAddress4 = getMacAddress(macStr: mac_str2)
                                    deviceAddress4.removeAll()
                                    deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                                    deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                                    deviceNameLabel4.text = deviceName4
                                    
                                    if let stationArray = childDevicesArray2[1]["station_info"] as? [Dictionary<String,Any>] {
                                        print("stationArray \(stationArray)")
                                        
                                        stationMac4.removeAll()
                                        stationRssi4.removeAll()
                                        stationUplink4.removeAll()
                                        stationDownlink4.removeAll()
                                        stationConnectedBand4.removeAll()
                                        
                                        if stationArray.count != 0{
                                            for stationInfo in stationArray{
                                                var mac_str = stationInfo["station_mac"]! as! String
                                                stationMac4.append(getMacAddress(macStr: mac_str))
                                                stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                                stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                                stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                                stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                            }
                                        }
                                        stationCount4 = stationArray.count
                                        self.stationNumLabel4.text = self.stationCount4.description
                                    }
                                    
                                }else{
                                    if childCount2 > 2{
                                        childCount2 = 3
                                        
                                        currentMeshType = .child_5
                                        
                                        deviceName3 = childDevicesArray2[0]["device_name"]! as! String
                                        ipAddress3 = childDevicesArray2[0]["ip_addr"]! as! String
                                        var mac_str33 = childDevicesArray2[0]["mac_address"]! as! String
                                        macAddress3 = getMacAddress(macStr: mac_str33)
                                        deviceAddress3.removeAll()
                                        deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                        deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                        deviceNameLabel3.text = deviceName3
                                        
                                        if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                            print("stationArray \(stationArray)")
                                            
                                            stationMac3.removeAll()
                                            stationRssi3.removeAll()
                                            stationUplink3.removeAll()
                                            stationDownlink3.removeAll()
                                            stationConnectedBand3.removeAll()
                                            
                                            if stationArray.count != 0{
                                                for stationInfo in stationArray{
                                                    var mac_str = stationInfo["station_mac"]! as! String
                                                    stationMac3.append(getMacAddress(macStr: mac_str))
                                                    stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                                    stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                                    stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                                    stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                                }
                                            }
                                            stationCount3 = stationArray.count
                                            self.stationNumLabel3.text = self.stationCount3.description
                                        }
                                        
                                        deviceName4 = childDevicesArray2[1]["device_name"]! as! String
                                        ipAddress4 = childDevicesArray2[1]["ip_addr"]! as! String
                                        var mac_str2 = childDevicesArray2[1]["mac_address"]! as! String
                                        macAddress4 = getMacAddress(macStr: mac_str2)
                                        deviceAddress4.removeAll()
                                        deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                                        deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                                        deviceNameLabel4.text = deviceName4
                                        
                                        if let stationArray = childDevicesArray2[1]["station_info"] as? [Dictionary<String,Any>] {
                                            print("stationArray \(stationArray)")
                                            
                                            stationMac4.removeAll()
                                            stationRssi4.removeAll()
                                            stationUplink4.removeAll()
                                            stationDownlink4.removeAll()
                                            stationConnectedBand4.removeAll()
                                            
                                            if stationArray.count != 0{
                                                for stationInfo in stationArray{
                                                    var mac_str = stationInfo["station_mac"]! as! String
                                                    stationMac4.append(getMacAddress(macStr: mac_str))
                                                    stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                                    stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                                    stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                                    stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                                }
                                            }
                                            stationCount4 = stationArray.count
                                            self.stationNumLabel4.text = self.stationCount4.description
                                        }
                                        
                                        deviceName5 = childDevicesArray2[2]["device_name"]! as! String
                                        ipAddress5 = childDevicesArray2[2]["ip_addr"]! as! String
                                        var mac_str3 = childDevicesArray2[2]["mac_address"]! as! String
                                        macAddress5 = getMacAddress(macStr: mac_str3)
                                        deviceAddress5.removeAll()
                                        deviceAddress5.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress5))
                                        deviceAddress5.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress5))
                                        deviceNameLabel5.text = deviceName5
                                        
                                        if let stationArray = childDevicesArray2[2]["station_info"] as? [Dictionary<String,Any>] {
                                            print("stationArray \(stationArray)")
                                            
                                            stationMac5.removeAll()
                                            stationRssi5.removeAll()
                                            stationUplink5.removeAll()
                                            stationDownlink5.removeAll()
                                            stationConnectedBand5.removeAll()
                                            
                                            if stationArray.count != 0{
                                                for stationInfo in stationArray{
                                                    var mac_str = stationInfo["station_mac"]! as! String
                                                    stationMac5.append(getMacAddress(macStr: mac_str))
                                                    stationRssi5.append(stationInfo["station_rssi"]! as! String)
                                                    stationUplink5.append(stationInfo["station_uplink"]! as! String)
                                                    stationDownlink5.append(stationInfo["station_downlink"]! as! String)
                                                    stationConnectedBand5.append(stationInfo["station_connected_band"]! as! String)
                                                }
                                            }
                                            stationCount5 = stationArray.count
                                            self.stationNumLabel5.text = self.stationCount5.description
                                        }
                                    }
                                }
                            }
                        }
                    case 2:
                        ///並聯
                        if childCount2 == 0 {
                            currentMeshType = .child_2
                            
                            deviceName3 = childDevicesArray[1]["device_name"]! as! String
                            ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                            var mac_str = childDevicesArray[1]["mac_address"]! as! String
                            macAddress3 = getMacAddress(macStr: mac_str)
                            deviceAddress3.removeAll()
                            deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                            deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                            deviceNameLabel3.text = deviceName3
                            
                            if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                                print("stationArray \(stationArray)")
                                
                                stationMac3.removeAll()
                                stationRssi3.removeAll()
                                stationUplink3.removeAll()
                                stationDownlink3.removeAll()
                                stationConnectedBand3.removeAll()
                                
                                if stationArray.count != 0{
                                    for stationInfo in stationArray{
                                        var mac_str = stationInfo["station_mac"]! as! String
                                        stationMac3.append(getMacAddress(macStr: mac_str))
                                        stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                        stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                        stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                        stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                    }
                                }
                                stationCount3 = stationArray.count
                                self.stationNumLabel3.text = self.stationCount3.description
                            }
                            
                        }else{
                            if childCount2 == 1 {
                                currentMeshType = .child_4
                                
                                deviceName3 = childDevicesArray[1]["device_name"]! as! String
                                ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                                var mac_str = childDevicesArray[1]["mac_address"]! as! String
                                macAddress3 = getMacAddress(macStr: mac_str)
                                deviceAddress3.removeAll()
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                deviceNameLabel3.text = deviceName3
                                
                                if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac3.removeAll()
                                    stationRssi3.removeAll()
                                    stationUplink3.removeAll()
                                    stationDownlink3.removeAll()
                                    stationConnectedBand3.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac3.append(getMacAddress(macStr: mac_str))
                                            stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount3 = stationArray.count
                                    self.stationNumLabel3.text = self.stationCount3.description
                                }
                                
                                deviceName4 = childDevicesArray2[0]["device_name"]! as! String
                                ipAddress4 = childDevicesArray2[0]["ip_addr"]! as! String
                                var mac_str2 = childDevicesArray2[0]["mac_address"]! as! String
                                macAddress4 = getMacAddress(macStr: mac_str2)
                                deviceAddress4.removeAll()
                                deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                                deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                                deviceNameLabel4.text = deviceName4
                                
                                if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac4.removeAll()
                                    stationRssi4.removeAll()
                                    stationUplink4.removeAll()
                                    stationDownlink4.removeAll()
                                    stationConnectedBand4.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac4.append(getMacAddress(macStr: mac_str))
                                            stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount4 = stationArray.count
                                    self.stationNumLabel4.text = self.stationCount4.description
                                }
                                
                            } else{
                                if childCount2 >= 2{
                                    childCount2 = 2
                                    print("xxxx")
                                    print(childDevicesArray2.count)
                                    print(childDevicesArray2.endIndex)
                                    
                                    currentMeshType = .child_5
                                    
                                    deviceName3 = childDevicesArray[1]["device_name"]! as! String
                                    ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                                    var mac_str = childDevicesArray[1]["mac_address"]! as! String
                                    macAddress3 = getMacAddress(macStr: mac_str)
                                    deviceAddress3.removeAll()
                                    deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                    deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                    deviceNameLabel3.text = deviceName3
                                    
                                    if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                                        print("stationArray \(stationArray)")
                                        
                                        stationMac3.removeAll()
                                        stationRssi3.removeAll()
                                        stationUplink3.removeAll()
                                        stationDownlink3.removeAll()
                                        stationConnectedBand3.removeAll()
                                        
                                        if stationArray.count != 0{
                                            for stationInfo in stationArray{
                                                var mac_str = stationInfo["station_mac"]! as! String
                                                stationMac3.append(getMacAddress(macStr: mac_str))
                                                stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                                stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                                stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                                stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                            }
                                        }
                                        stationCount3 = stationArray.count
                                        self.stationNumLabel3.text = self.stationCount3.description
                                    }
                                    
                                    deviceName4 = childDevicesArray2[0]["device_name"]! as! String
                                    ipAddress4 = childDevicesArray2[0]["ip_addr"]! as! String
                                    var mac_str2 = childDevicesArray2[0]["mac_address"]! as! String
                                    macAddress4 = getMacAddress(macStr: mac_str2)
                                    deviceAddress4.removeAll()
                                    deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                                    deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                                    deviceNameLabel4.text = deviceName4
                                    
                                    if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                        print("stationArray \(stationArray)")
                                        
                                        stationMac4.removeAll()
                                        stationRssi4.removeAll()
                                        stationUplink4.removeAll()
                                        stationDownlink4.removeAll()
                                        stationConnectedBand4.removeAll()
                                        
                                        if stationArray.count != 0{
                                            for stationInfo in stationArray{
                                                var mac_str = stationInfo["station_mac"]! as! String
                                                stationMac4.append(getMacAddress(macStr: mac_str))
                                                stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                                stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                                stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                                stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                            }
                                        }
                                        stationCount4 = stationArray.count
                                        self.stationNumLabel4.text = self.stationCount4.description
                                    }
                                    
                                    deviceName5 = childDevicesArray2[1]["device_name"]! as! String
                                    ipAddress5 = childDevicesArray2[1]["ip_addr"]! as! String
                                    var mac_str3 = childDevicesArray2[1]["mac_address"]! as! String
                                    macAddress5 = getMacAddress(macStr: mac_str3)
                                    deviceAddress5.removeAll()
                                    deviceAddress5.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress5))
                                    deviceAddress5.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress5))
                                    deviceNameLabel5.text = deviceName5
                                    
                                    if let stationArray = childDevicesArray2[1]["station_info"] as? [Dictionary<String,Any>] {
                                        print("stationArray \(stationArray)")
                                        
                                        stationMac5.removeAll()
                                        stationRssi5.removeAll()
                                        stationUplink5.removeAll()
                                        stationDownlink5.removeAll()
                                        stationConnectedBand5.removeAll()
                                        
                                        if stationArray.count != 0{
                                            for stationInfo in stationArray{
                                                var mac_str = stationInfo["station_mac"]! as! String
                                                stationMac5.append(getMacAddress(macStr: mac_str))
                                                stationRssi5.append(stationInfo["station_rssi"]! as! String)
                                                stationUplink5.append(stationInfo["station_uplink"]! as! String)
                                                stationDownlink5.append(stationInfo["station_downlink"]! as! String)
                                                stationConnectedBand5.append(stationInfo["station_connected_band"]! as! String)
                                            }
                                        }
                                        stationCount5 = stationArray.count
                                        self.stationNumLabel5.text = self.stationCount5.description
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    case 3:
                        if childCount2 == 0 {
                            currentMeshType = .child_4
                            
                            deviceName3 = childDevicesArray[1]["device_name"]! as! String
                            ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                            var mac_str = childDevicesArray[1]["mac_address"]! as! String
                            macAddress3 = getMacAddress(macStr: mac_str)
                            deviceAddress3.removeAll()
                            deviceAddress3.append(WapDevice(titleLabel: "IP位址", detailLabel: ipAddress3))
                            deviceAddress3.append(WapDevice(titleLabel: "MAC位址", detailLabel: macAddress3))
                            deviceNameLabel3.text = deviceName3
                            
                            if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                                print("stationArray \(stationArray)")
                                
                                stationMac3.removeAll()
                                stationRssi3.removeAll()
                                stationUplink3.removeAll()
                                stationDownlink3.removeAll()
                                stationConnectedBand3.removeAll()
                                
                                if stationArray.count != 0{
                                    for stationInfo in stationArray{
                                        var mac_str = stationInfo["station_mac"]! as! String
                                        stationMac3.append(getMacAddress(macStr: mac_str))
                                        stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                        stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                        stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                        stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                    }
                                }
                                stationCount3 = stationArray.count
                                self.stationNumLabel3.text = self.stationCount3.description
                            }
                            
                            deviceName4 = childDevicesArray[2]["device_name"]! as! String
                            ipAddress4 = childDevicesArray[2]["ip_addr"]! as! String
                            var mac_str2 = childDevicesArray[2]["mac_address"]! as! String
                            macAddress4 = getMacAddress(macStr: mac_str2)
                            deviceAddress4.removeAll()
                            deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                            deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                            deviceNameLabel4.text = deviceName4
                            
                            if let stationArray = childDevicesArray[2]["station_info"] as? [Dictionary<String,Any>] {
                                print("stationArray \(stationArray)")
                                
                                stationMac4.removeAll()
                                stationRssi4.removeAll()
                                stationUplink4.removeAll()
                                stationDownlink4.removeAll()
                                stationConnectedBand4.removeAll()
                                
                                if stationArray.count != 0{
                                    for stationInfo in stationArray{
                                        var mac_str = stationInfo["station_mac"]! as! String
                                        stationMac4.append(getMacAddress(macStr: mac_str))
                                        stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                        stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                        stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                        stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                    }
                                }
                                stationCount4 = stationArray.count
                                self.stationNumLabel4.text = self.stationCount4.description
                            }
                            
                        }else{
                            if childCount2 >= 1{
                                childCount2 = 1
                                currentMeshType = .child_5
                                
                                deviceName3 = childDevicesArray[1]["device_name"]! as! String
                                ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                                var mac_str = childDevicesArray[1]["mac_address"]! as! String
                                macAddress3 = getMacAddress(macStr: mac_str)
                                deviceAddress3.removeAll()
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                                deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                                deviceNameLabel3.text = deviceName3
                                
                                if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac3.removeAll()
                                    stationRssi3.removeAll()
                                    stationUplink3.removeAll()
                                    stationDownlink3.removeAll()
                                    stationConnectedBand3.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac3.append(getMacAddress(macStr: mac_str))
                                            stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount3 = stationArray.count
                                    self.stationNumLabel3.text = self.stationCount3.description
                                }
                                
                                deviceName4 = childDevicesArray[2]["device_name"]! as! String
                                ipAddress4 = childDevicesArray[2]["ip_addr"]! as! String
                                var mac_str2 = childDevicesArray[2]["mac_address"]! as! String
                                macAddress4 = getMacAddress(macStr: mac_str2)
                                deviceAddress4.removeAll()
                                deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                                deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                                deviceNameLabel4.text = deviceName4
                                
                                if let stationArray = childDevicesArray[2]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac4.removeAll()
                                    stationRssi4.removeAll()
                                    stationUplink4.removeAll()
                                    stationDownlink4.removeAll()
                                    stationConnectedBand4.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac4.append(getMacAddress(macStr: mac_str))
                                            stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount4 = stationArray.count
                                    self.stationNumLabel4.text = self.stationCount4.description
                                }
                                
                                deviceName5 = childDevicesArray2[0]["device_name"]! as! String
                                ipAddress5 = childDevicesArray2[0]["ip_addr"]! as! String
                                var mac_str3 = childDevicesArray2[0]["mac_address"]! as! String
                                macAddress5 = getMacAddress(macStr: mac_str3)
                                deviceAddress5.removeAll()
                                deviceAddress5.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress5))
                                deviceAddress5.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress5))
                                deviceNameLabel5.text = deviceName5
                                
                                if let stationArray = childDevicesArray2[0]["station_info"] as? [Dictionary<String,Any>] {
                                    print("stationArray \(stationArray)")
                                    
                                    stationMac5.removeAll()
                                    stationRssi5.removeAll()
                                    stationUplink5.removeAll()
                                    stationDownlink5.removeAll()
                                    stationConnectedBand5.removeAll()
                                    
                                    if stationArray.count != 0{
                                        for stationInfo in stationArray{
                                            var mac_str = stationInfo["station_mac"]! as! String
                                            stationMac5.append(getMacAddress(macStr: mac_str))
                                            stationRssi5.append(stationInfo["station_rssi"]! as! String)
                                            stationUplink5.append(stationInfo["station_uplink"]! as! String)
                                            stationDownlink5.append(stationInfo["station_downlink"]! as! String)
                                            stationConnectedBand5.append(stationInfo["station_connected_band"]! as! String)
                                        }
                                    }
                                    stationCount5 = stationArray.count
                                    self.stationNumLabel5.text = self.stationCount5.description
                                }
                            }
                        }
                        
                    case 4:
                        currentMeshType = .child_5
                        
                        deviceName3 = childDevicesArray[1]["device_name"]! as! String
                        ipAddress3 = childDevicesArray[1]["ip_addr"]! as! String
                        var mac_str = childDevicesArray[1]["mac_address"]! as! String
                        macAddress3 = getMacAddress(macStr: mac_str)
                        deviceAddress3.removeAll()
                        deviceAddress3.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress3))
                        deviceAddress3.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress3))
                        deviceNameLabel3.text = deviceName3
                        
                        if let stationArray = childDevicesArray[1]["station_info"] as? [Dictionary<String,Any>] {
                            print("stationArray \(stationArray)")
                            
                            stationMac3.removeAll()
                            stationRssi3.removeAll()
                            stationUplink3.removeAll()
                            stationDownlink3.removeAll()
                            stationConnectedBand3.removeAll()
                            
                            if stationArray.count != 0{
                                for stationInfo in stationArray{
                                    var mac_str = stationInfo["station_mac"]! as! String
                                    stationMac3.append(getMacAddress(macStr: mac_str))
                                    stationRssi3.append(stationInfo["station_rssi"]! as! String)
                                    stationUplink3.append(stationInfo["station_uplink"]! as! String)
                                    stationDownlink3.append(stationInfo["station_downlink"]! as! String)
                                    stationConnectedBand3.append(stationInfo["station_connected_band"]! as! String)
                                }
                            }
                            stationCount3 = stationArray.count
                            self.stationNumLabel3.text = self.stationCount3.description
                        }
                        
                        deviceName4 = childDevicesArray[2]["device_name"]! as! String
                        ipAddress4 = childDevicesArray[2]["ip_addr"]! as! String
                        var mac_str2 = childDevicesArray[2]["mac_address"]! as! String
                        macAddress4 = getMacAddress(macStr: mac_str2)
                        deviceAddress4.removeAll()
                        deviceAddress4.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress4))
                        deviceAddress4.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress4))
                        deviceNameLabel4.text = deviceName4
                        
                        if let stationArray = childDevicesArray[2]["station_info"] as? [Dictionary<String,Any>] {
                            print("stationArray \(stationArray)")
                            
                            stationMac4.removeAll()
                            stationRssi4.removeAll()
                            stationUplink4.removeAll()
                            stationDownlink4.removeAll()
                            stationConnectedBand4.removeAll()
                            
                            if stationArray.count != 0{
                                for stationInfo in stationArray{
                                    var mac_str = stationInfo["station_mac"]! as! String
                                    stationMac4.append(getMacAddress(macStr: mac_str))
                                    stationRssi4.append(stationInfo["station_rssi"]! as! String)
                                    stationUplink4.append(stationInfo["station_uplink"]! as! String)
                                    stationDownlink4.append(stationInfo["station_downlink"]! as! String)
                                    stationConnectedBand4.append(stationInfo["station_connected_band"]! as! String)
                                }
                            }
                            stationCount4 = stationArray.count
                            self.stationNumLabel4.text = self.stationCount4.description
                        }
                        
                        deviceName5 = childDevicesArray[3]["device_name"]! as! String
                        ipAddress5 = childDevicesArray[3]["ip_addr"]! as! String
                        var mac_str3 = childDevicesArray[3]["mac_address"]! as! String
                        macAddress5 = getMacAddress(macStr: mac_str3)
                        deviceAddress5.removeAll()
                        deviceAddress5.append(WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: ipAddress5))
                        deviceAddress5.append(WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: macAddress5))
                        deviceNameLabel5.text = deviceName5
                        
                        if let stationArray = childDevicesArray[3]["station_info"] as? [Dictionary<String,Any>] {
                            print("stationArray \(stationArray)")
                            
                            stationMac5.removeAll()
                            stationRssi5.removeAll()
                            stationUplink5.removeAll()
                            stationDownlink5.removeAll()
                            stationConnectedBand5.removeAll()
                            
                            if stationArray.count != 0{
                                for stationInfo in stationArray{
                                    var mac_str = stationInfo["station_mac"]! as! String
                                    stationMac5.append(getMacAddress(macStr: mac_str))
                                    stationRssi5.append(stationInfo["station_rssi"]! as! String)
                                    stationUplink5.append(stationInfo["station_uplink"]! as! String)
                                    stationDownlink5.append(stationInfo["station_downlink"]! as! String)
                                    stationConnectedBand5.append(stationInfo["station_connected_band"]! as! String)
                                }
                            }
                            stationCount5 = stationArray.count
                            self.stationNumLabel5.text = self.stationCount5.description
                        }
                    default:
                        break
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }else{
            alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
        }
        
    }
    /**
     根據不同的裝置資料來展示mesh圖佈局
     - Date: 08/26 Yang
    */
    func showMesh() -> Void {
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0, animations: {
            
            switch self.currentMeshType{
            case .child_0:
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = true
                self.deviceNameLabel3.isHidden = true
                self.deviceNameLabel4.isHidden = true
                self.deviceNameLabel5.isHidden = true
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = true
                self.stationImage2.isHidden = true
                self.deviceButton3.isHidden = true
                self.stationImage3.isHidden = true
                self.deviceButton5.isHidden = true
                self.deviceButton4.isHidden = true
                self.stationImage4.isHidden = true
                self.stationImage5.isHidden = true
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = true
                self.stationNumLabel3.isHidden = true
                self.stationNumLabel4.isHidden = true
                self.stationNumLabel5.isHidden = true
                self.lineView6.alpha = 0
                self.lineView7.alpha = 0
                self.lineView5.alpha = 0
                self.lineView3.alpha = 0
                self.lineView4.alpha = 0
                self.lineView8.alpha = 0
                self.lineView9.alpha = 0
                self.lineView10.alpha = 0
                self.lineView11.alpha = 0
                //1
                self.constraint32?.activate()
                self.constraint33?.deactivate()
                self.constraint34?.deactivate()
                self.constraint35?.deactivate()
                //2
                self.constraint10?.activate()
                self.constraint11?.deactivate()
                self.constraint26?.deactivate()
                self.constraint25?.activate()
                //8
                self.constraint4?.activate()
                self.constraint3?.deactivate()
                self.constraint2?.deactivate()
                self.constraint1?.deactivate()
                self.constraint5?.deactivate()
                self.constraint12?.deactivate()
                self.constraint28?.deactivate()
                //9
                self.constraint9?.activate()
                self.constraint8?.deactivate()
                self.constraint7?.deactivate()
                self.constraint6?.deactivate()
                //3
                self.constraint21?.activate()
                self.constraint22?.deactivate()
                self.constraint23?.deactivate()
                self.constraint24?.deactivate()
                self.constraint27?.deactivate()
                self.constraint31?.deactivate()
            case .child_1:
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = false
                self.deviceNameLabel3.isHidden = true
                self.deviceNameLabel4.isHidden = true
                self.deviceNameLabel5.isHidden = true
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = false
                self.stationImage2.isHidden = false
                self.deviceButton3.isHidden = true
                self.stationImage3.isHidden = true
                self.deviceButton5.isHidden = true
                self.deviceButton4.isHidden = true
                self.stationImage4.isHidden = true
                self.stationImage5.isHidden = true
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = false
                self.stationNumLabel3.isHidden = true
                self.stationNumLabel4.isHidden = true
                self.stationNumLabel5.isHidden = true
                self.lineView6.alpha = 0
                self.lineView7.alpha = 0
                self.lineView5.alpha = 1
                self.lineView3.alpha = 0
                self.lineView4.alpha = 0
                self.lineView8.alpha = 0
                self.lineView9.alpha = 0
                self.lineView10.alpha = 0
                self.lineView11.alpha = 0
                //1
                self.constraint32?.activate()
                self.constraint33?.deactivate()
                self.constraint34?.deactivate()
                self.constraint35?.deactivate()
                //2
                self.constraint10?.activate()
                self.constraint11?.deactivate()
                self.constraint26?.deactivate()
                self.constraint25?.activate()
                //8
                self.constraint3?.activate()
                self.constraint2?.activate()
                self.constraint5?.deactivate()
                self.constraint1?.activate()
                self.constraint4?.deactivate()
                self.constraint12?.deactivate()
                //9
                self.constraint9?.activate()
                self.constraint8?.deactivate()
                self.constraint7?.deactivate()
                self.constraint6?.deactivate()
                //3
                self.constraint21?.activate()
                self.constraint22?.deactivate()
                self.constraint23?.deactivate()
                self.constraint24?.deactivate()
                self.constraint27?.deactivate()
                self.constraint31?.deactivate()
            case .child_2:
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = false
                self.deviceNameLabel3.isHidden = false
                self.deviceNameLabel4.isHidden = true
                self.deviceNameLabel5.isHidden = true
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = false
                self.stationImage2.isHidden = false
                self.deviceButton3.isHidden = false
                self.stationImage3.isHidden = false
                self.deviceButton5.isHidden = true
                self.deviceButton4.isHidden = true
                self.stationImage4.isHidden = true
                self.stationImage5.isHidden = true
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = false
                self.stationNumLabel3.isHidden = false
                self.stationNumLabel4.isHidden = true
                self.stationNumLabel5.isHidden = true
                //1
                self.constraint32?.activate()
                self.constraint33?.deactivate()
                self.constraint34?.deactivate()
                self.constraint35?.deactivate()
                //2
                self.constraint10?.activate()
                self.constraint11?.deactivate()
                self.constraint26?.deactivate()
                self.constraint25?.activate()
                //8
                self.constraint3?.activate()
                self.constraint2?.activate()
                self.constraint5?.activate()
                self.constraint1?.deactivate()
                self.constraint4?.deactivate()
                self.constraint12?.deactivate()
                
                //9
                self.constraint9?.deactivate()
                self.constraint8?.activate()
                self.constraint7?.activate()
                self.constraint6?.activate()
                //3
                self.constraint21?.activate()
                self.constraint22?.deactivate()
                self.constraint23?.deactivate()
                self.constraint24?.deactivate()
                self.constraint27?.deactivate()
                self.constraint31?.deactivate()
                
                self.lineView10.alpha = 0
                self.lineView11.alpha = 0
                self.lineView8.alpha = 0
                self.lineView9.alpha = 0
                self.lineView6.alpha = 0
                self.lineView7.alpha = 0
                self.lineView5.alpha = 0
                self.lineView3.alpha = 0
                self.lineView4.alpha = 0
                self.lineView1.alpha = 1
                self.lineView2.alpha = 1
                self.lineView1.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (30))
                self.constraint15?.deactivate()
                self.constraint16?.activate()
                self.lineView2.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (330))
                self.constraint17?.deactivate()
                self.constraint18?.activate()
                
            case .child_3:
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = false
                self.deviceNameLabel3.isHidden = false
                self.deviceNameLabel4.isHidden = true
                self.deviceNameLabel5.isHidden = true
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = false
                self.stationImage2.isHidden = false
                self.deviceButton3.isHidden = false
                self.stationImage3.isHidden = false
                self.deviceButton5.isHidden = true
                self.deviceButton4.isHidden = true
                self.stationImage4.isHidden = true
                self.stationImage5.isHidden = true
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = false
                self.stationNumLabel3.isHidden = false
                self.stationNumLabel4.isHidden = true
                self.stationNumLabel5.isHidden = true
                //1
                self.constraint32?.activate()
                self.constraint33?.deactivate()
                self.constraint34?.deactivate()
                self.constraint35?.deactivate()
                self.lineView1.alpha = 0
                self.lineView2.alpha = 0
                self.lineView4.alpha = 1
                self.lineView3.alpha = 1
                self.lineView5.alpha = 0
                self.lineView6.alpha = 0
                self.lineView7.alpha = 0
                self.lineView8.alpha = 0
                self.lineView9.alpha = 0
                self.lineView10.alpha = 0
                self.lineView11.alpha = 0
                self.constraint19?.activate()
                self.constraint20?.activate()
                self.lineView3.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (315))
                self.lineView4.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (315))
                
                //2
                self.constraint10?.deactivate()
                self.constraint11?.activate()
                self.constraint26?.deactivate()
                self.constraint25?.activate()
                //8
                self.constraint4?.deactivate()
                self.constraint3?.activate()
                self.constraint2?.deactivate()
                self.constraint1?.activate()
                self.constraint5?.deactivate()
                self.constraint12?.activate()
                //9
                self.constraint9?.deactivate()
                self.constraint8?.activate()
                self.constraint7?.activate()
                self.constraint6?.activate()
                //3
                self.constraint21?.activate()
                self.constraint22?.deactivate()
                self.constraint23?.deactivate()
                self.constraint30?.deactivate()
                self.constraint24?.deactivate()
                self.constraint31?.activate()
                
            case .child_4:
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = false
                self.deviceNameLabel3.isHidden = false
                self.deviceNameLabel4.isHidden = false
                self.deviceNameLabel5.isHidden = true
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = false
                self.stationImage2.isHidden = false
                self.deviceButton3.isHidden = false
                self.stationImage3.isHidden = false
                self.deviceButton4.isHidden = false
                self.stationImage4.isHidden = false
                self.stationImage5.isHidden = true
                self.deviceButton5.isHidden = true
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = false
                self.stationNumLabel3.isHidden = false
                self.stationNumLabel4.isHidden = false
                self.stationNumLabel5.isHidden = true
                self.lineView5.alpha = 0
                self.lineView3.alpha = 0
                self.lineView4.alpha = 0
                self.lineView1.alpha = 0
                self.lineView2.alpha = 0
                self.lineView6.alpha = 1
                self.lineView7.alpha = 1
                self.lineView8.alpha = 0
                self.lineView9.alpha = 0
                self.lineView10.alpha = 1
                self.lineView11.alpha = 1
                self.lineView11.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (90))
                self.lineView10.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (90))
                //1
                self.constraint32?.activate()
                self.constraint33?.deactivate()
                self.constraint34?.deactivate()
                self.constraint35?.deactivate()
                //2
                self.constraint10?.deactivate()
                self.constraint11?.activate()
                self.constraint26?.deactivate()
                self.constraint25?.activate()
                //8
                self.constraint3?.activate()
                self.constraint2?.activate()
                self.constraint5?.activate()
                self.constraint1?.deactivate()
                self.constraint4?.deactivate()
                self.constraint12?.deactivate()
                //9
                self.constraint9?.deactivate()
                self.constraint8?.activate()
                self.constraint7?.activate()
                self.constraint6?.activate()
                //3
                self.constraint21?.deactivate()
                self.constraint22?.activate()
                self.constraint23?.activate()
                self.constraint30?.activate()
                self.constraint24?.deactivate()
                self.constraint31?.deactivate()
                
            case .child_5:
                //1
                self.constraint32?.deactivate()
                self.constraint33?.activate()
                self.constraint34?.activate()
                self.constraint35?.activate()
                //2
                self.constraint10?.activate()
                self.constraint11?.deactivate()
                self.constraint25?.deactivate()
                self.constraint26?.activate()
                //3
                self.constraint21?.deactivate()
                self.constraint22?.activate()
                self.constraint23?.deactivate()
                self.constraint24?.activate()
                self.constraint27?.activate()
                self.constraint31?.deactivate()
                
                //8
                self.constraint3?.activate()
                self.constraint2?.deactivate()
                self.constraint28?.activate()
                self.constraint5?.activate()
                self.constraint1?.deactivate()
                self.constraint4?.deactivate()
                self.constraint12?.deactivate()
                //9
                self.constraint9?.deactivate()
                self.constraint8?.activate()
                self.constraint7?.deactivate()
                self.constraint6?.activate()
                self.constraint29?.activate()
                
                self.deviceNameLabel.isHidden = false
                self.deviceNameLabel2.isHidden = false
                self.deviceNameLabel3.isHidden = false
                self.deviceNameLabel4.isHidden = false
                self.deviceNameLabel5.isHidden = false
                self.deviceButton.isHidden = false
                self.stationImage.isHidden = false
                self.deviceButton2.isHidden = false
                self.stationImage2.isHidden = false
                self.deviceButton3.isHidden = false
                self.stationImage3.isHidden = false
                self.deviceButton4.isHidden = false
                self.deviceButton5.isHidden = false
                self.stationImage4.isHidden = false
                self.stationImage5.isHidden = false
                self.stationNumLabel.isHidden = false
                self.stationNumLabel2.isHidden = false
                self.stationNumLabel3.isHidden = false
                self.stationNumLabel4.isHidden = false
                self.stationNumLabel5.isHidden = false
                self.lineView5.alpha = 0
                self.lineView3.alpha = 1
                self.lineView4.alpha = 1
                self.lineView1.alpha = 0
                self.lineView2.alpha = 0
                self.lineView6.alpha = 1
                self.lineView7.alpha = 1
                self.lineView8.alpha = 1
                self.lineView9.alpha = 1
                self.lineView10.alpha = 1
                self.lineView11.alpha = 1
                ///lineView傾斜角度
                self.lineView8.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (45))
                self.lineView9.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (45))
                self.lineView11.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (90))
                self.lineView10.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (90))
                self.lineView3.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (315))
                self.lineView4.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 180 * (315))
                
            default:
                break
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func setViews(){
        if let navBarHeight =
            UserDefaults.standard.string(forKey: "navBarHeight") , let navBarH = (Int(navBarHeight)) {
            navigationBarView.backgroundColor = PrimaryColor1
            self.view.addSubview(navigationBarView)
            navigationBarView.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(0)
                makes.width.equalTo(fullScreenSize.width)
                makes.height.equalTo(navBarH)
            }
            
            switch currentDevice.deviceType {
            case .iPad:
                titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
            default:
                titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
            }
            titleLabel.backgroundColor = .clear
            titleLabel.text = localString(key: "MESH_MAP", table: .pageTitle)
            titleLabel.textColor = .white
            titleLabel.textAlignment = .center
            self.view.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
                makes.width.equalTo(fullScreenSize.width*0.6)
            }
            
            MenuButton2.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
            MenuButton2.tintColor = SecondaryColor3
            MenuButton2.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
            self.view.addSubview(MenuButton2)
            MenuButton2.snp.makeConstraints { (makes) in
                makes.centerY.equalTo(titleLabel)
                makes.width.height.equalTo(buttonSize)
                makes.left.equalToSuperview().offset(spacing3)
            }
            
            RefreshButton2.setImage(UIImage(named: "reload-outline")!.withRenderingMode(.alwaysTemplate), for: .normal)
            RefreshButton2.tintColor = SecondaryColor3
            RefreshButton2.addTarget(self,action: #selector(self.refreshClick),for: .touchUpInside)
            self.view.addSubview(RefreshButton2)
            RefreshButton2.snp.makeConstraints { (makes) in
                makes.centerY.equalTo(titleLabel)
                makes.width.height.equalTo(buttonSize)
                makes.right.equalToSuperview().offset(-spacing3)
            }
        }
        ///不顯示 純粹用來作為佈局基準
        bkView444.layer.cornerRadius = wapWidth
        bkView444.backgroundColor = .clear
        view.addSubview(bkView444)
        bkView444.snp.makeConstraints{ (makes) in
            makes.centerX.equalToSuperview()
            makes.centerY.equalToSuperview().offset(-(fullScreenSize.height/2-fullScreenSize.height/3.2))
            makes.width.height.equalTo(wapWidth*2)
        }
        
        ///2  這裡的數字為九宮格的位置編號 從左至右從上到下算起 不代表有九個裝置數 目前只能顯示最多五個裝置
        bkView4.layer.cornerRadius = wapWidth
        bkView4.backgroundColor = LightSecondaryColor
        view.addSubview(bkView4)
        bkView4.snp.makeConstraints{ (makes) in
            constraint11 = makes.centerX.equalToSuperview().offset(-(wapWidth*2+spacing3)).constraint
            constraint10 = makes.centerX.equalToSuperview().constraint
            constraint25 = makes.centerY.equalToSuperview().offset(-(fullScreenSize.height/2-fullScreenSize.height/3.2)).constraint
            constraint26 = makes.centerY.equalTo(bkView444).offset(+(wapWidth*2+spacing3)).constraint
            makes.width.height.equalTo(wapWidth*2)
        }
        constraint10?.activate()
        constraint11?.deactivate()
        constraint26?.deactivate()
        constraint25?.activate()
        
        bkView3.layer.cornerRadius = wapWidth-spacing1/2
        bkView3.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView3)
        bkView3.snp.makeConstraints{ (makes) in
            makes.top.bottom.left.right.equalTo(bkView4).inset(spacing1/2)
        }
        
        WAPView2.image = UIImage(named: "3514-mesh")
        view.addSubview(WAPView2)
        WAPView2.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView3).inset(spacing1)
        }
        
        ///1
        bkView2.layer.cornerRadius = wapWidth
        bkView2.backgroundColor = LightSecondaryColor
        view.addSubview(bkView2)
        bkView2.snp.makeConstraints{ (makes) in
            constraint32 = makes.top.bottom.left.right.equalTo(bkView4).constraint
            constraint33 = makes.centerX.equalTo(bkView4).offset(-(wapWidth*2+spacing3)).constraint
            constraint34 = makes.centerY.equalTo(bkView444).constraint
            constraint35 = makes.width.height.equalTo(wapWidth*2).constraint
        }
        constraint32?.activate()
        constraint33?.deactivate()
        constraint34?.deactivate()
        constraint35?.deactivate()
        
        bkView1.layer.cornerRadius = wapWidth-spacing1/2
        bkView1.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView1)
        bkView1.snp.makeConstraints{ (makes) in
            makes.top.bottom.left.right.equalTo(bkView2).inset(spacing1/2)
        }
        
        WAPView1.image = UIImage(named: "3514-mesh")
        view.addSubview(WAPView1)
        WAPView1.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView1).inset(spacing1)
        }
        
        //3
        bkView6.layer.cornerRadius = wapWidth
        bkView6.backgroundColor = LightSecondaryColor
        view.addSubview(bkView6)
        bkView6.snp.makeConstraints{ (makes) in
            constraint24 = makes.centerX.equalTo(bkView4).offset(+(wapWidth*2+spacing3)).constraint
            constraint30 = makes.centerX.equalTo(bkView444).offset(+(wapWidth*2+spacing3)).constraint
            constraint31 = makes.centerX.equalTo(bkView4).constraint
            constraint23 = makes.centerY.equalTo(bkView4).constraint
            constraint27 = makes.centerY.equalTo(bkView444).constraint
            constraint22 = makes.width.height.equalTo(wapWidth*2).constraint
            constraint21 = makes.top.bottom.left.right.equalTo(bkView444).constraint
        }
        constraint21?.activate()
        constraint22?.deactivate()
        constraint23?.deactivate()
        constraint24?.deactivate()
        
        bkView5.layer.cornerRadius = wapWidth-spacing1/2
        bkView5.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView5)
        bkView5.snp.makeConstraints{ (makes) in
            makes.top.bottom.left.right.equalTo(bkView6).inset(spacing1/2)
        }
        
        WAPView3.image = UIImage(named: "3514-mesh")
        view.addSubview(WAPView3)
        WAPView3.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView5).inset(spacing1)
        }
        
        //8
        bkView16.layer.cornerRadius = wapWidth
        bkView16.backgroundColor = LightSecondaryColor
        view.addSubview(bkView16)
        bkView16.snp.makeConstraints{ (makes) in
            constraint1 = makes.centerX.equalToSuperview().constraint
            constraint2 = makes.centerY.equalTo(self.bkView4).offset(+(wapWidth*2+spacing3)*2).constraint
            constraint28 = makes.centerY.equalTo(self.bkView444).offset(+(wapWidth*2+spacing3)*2).constraint
            constraint3 = makes.width.height.equalTo(wapWidth*2).constraint
            constraint4 = makes.top.bottom.left.right.equalTo(bkView444).constraint
            constraint5 = makes.centerX.equalToSuperview().offset(-(wapWidth*2+spacing3)).constraint
            constraint12 = makes.centerY.equalTo(bkView4).offset(+(wapWidth*2+spacing3)).constraint
        }
        constraint4?.activate()
        constraint3?.deactivate()
        constraint2?.deactivate()
        constraint1?.deactivate()
        constraint5?.deactivate()
        constraint12?.deactivate()
        constraint28?.deactivate()
        
        bkView15.layer.cornerRadius = wapWidth-spacing1/2
        bkView15.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView15)
        bkView15.snp.makeConstraints{ (makes) in
            makes.top.bottom.left.right.equalTo(bkView16).inset(spacing1/2)
        }
        
        WAPView8.image = UIImage(named: "3514-mesh")
        view.addSubview(WAPView8)
        WAPView8.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView15).inset(spacing1)
        }
        
        //9
        bkView18.layer.cornerRadius = wapWidth
        bkView18.backgroundColor = LightSecondaryColor
        view.addSubview(bkView18)
        bkView18.snp.makeConstraints{ (makes) in
            constraint6 = makes.centerX.equalToSuperview().offset(+(wapWidth*2+spacing3)*1).constraint
            constraint7 = makes.centerY.equalTo(bkView4).offset(+(wapWidth*2+spacing3)*2).constraint
            constraint29 = makes.centerY.equalTo(bkView444).offset(+(wapWidth*2+spacing3)*2).constraint
            constraint8 = makes.width.height.equalTo(wapWidth*2).constraint
            constraint9 = makes.top.bottom.left.right.equalTo(bkView444).constraint
        }
        constraint9?.activate()
        constraint8?.deactivate()
        constraint7?.deactivate()
        constraint6?.deactivate()
        constraint29?.deactivate()
        
        bkView17.layer.cornerRadius = wapWidth-spacing1/2
        bkView17.backgroundColor = LightPrimaryColor2
        view.addSubview(bkView17)
        bkView17.snp.makeConstraints{ (makes) in
            makes.top.bottom.left.right.equalTo(bkView18).inset(spacing1/2)
        }
        
        WAPView9.image = UIImage(named: "3514-mesh")
        view.addSubview(WAPView9)
        WAPView9.snp.makeConstraints{ (makes) in
            makes.left.right.top.bottom.equalTo(bkView17).inset(spacing1)
        }
        ///利用數學公式計算虛線在傾斜時的長度
        let x = Double((wapWidth+spacing3)*2)
        //        let result = sqrt(25)
        let y = 3.squareRoot()
        let (dx, dy) = (x/y , x)
        let distance = hypotenuse(dx, dy)
        newLineHeight = distance
        print("distance \(distance)")
        
        ///讓實線變成虛線的設定
        lineView1 = UIImageView(frame: CGRect(x: fullScreenSize.width/2-(wapWidth+spacing2), y: fullScreenSize.height/3.2+wapWidth-spacing3, width: 3, height: CGFloat(newLineHeight)+spacing1))
        self.view.addSubview(lineView1)
        UIGraphicsBeginImageContext(lineView1.frame.size) // 位图上下文绘制区域
        lineView1.image?.draw(in: lineView1.bounds)
        let context2:CGContext = UIGraphicsGetCurrentContext()!
        context2.setLineCap(CGLineCap.square)
        let lengths2:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context2.setStrokeColor(PrimaryColor2.cgColor)
        context2.setLineWidth(5)
        context2.setLineDash(phase: 0, lengths: lengths2)
        context2.move(to: CGPoint(x: 0, y: 12))
        context2.addLine(to: CGPoint(x: 3, y: CGFloat(newLineHeight)+spacing1)) //虛線顯示區域
        context2.strokePath()
        lineView1.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView1.alpha = 0
        
        //        lineView1.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView1)
        //        lineView1.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth-spacing1)
        //            makes.centerX.equalToSuperview().offset(-wapWidth-spacing2)
        //            constraint15 = makes.height.equalTo((wapWidth+spacing3)*2).constraint
        //            constraint16 = makes.height.equalTo(newLineHeight).constraint
        //            makes.width.equalTo(1.0)
        //        }
        //        constraint15?.activate()
        //        constraint16?.deactivate()
        
        lineView2 = UIImageView(frame: CGRect(x: fullScreenSize.width/2+(wapWidth+spacing2), y: fullScreenSize.height/3.2+wapWidth-spacing3, width: 3, height: CGFloat(newLineHeight)+spacing1))
        self.view.addSubview(lineView2)
        UIGraphicsBeginImageContext(lineView2.frame.size) // 位图上下文绘制区域
        lineView2.image?.draw(in: lineView2.bounds)
        let context3:CGContext = UIGraphicsGetCurrentContext()!
        context3.setLineCap(CGLineCap.square)
        let lengths3:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context3.setStrokeColor(PrimaryColor2.cgColor)
        context3.setLineWidth(5)
        context3.setLineDash(phase: 0, lengths: lengths3)
        context3.move(to: CGPoint(x: 0, y: 12))
        context3.addLine(to: CGPoint(x: 3, y: CGFloat(newLineHeight)+spacing1)) //虛線顯示區域
        context3.strokePath()
        lineView2.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView2.alpha = 0
        
        //        lineView2.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView2)
        //        lineView2.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth-spacing1)
        //            makes.centerX.equalToSuperview().offset(wapWidth+spacing2)
        //            constraint17 = makes.height.equalTo((wapWidth+spacing3)*2).constraint
        //            constraint18 = makes.height.equalTo(newLineHeight).constraint
        //            makes.width.equalTo(1.0)
        //        }
        //        constraint17?.activate()
        //        constraint18?.deactivate()
        
        
        //        let x2 = Double((wapWidth+spacing3)*1)
        //        //        let result = sqrt(25)
        //        let y2 = 1.squareRoot()
        //        let (dx2, dy2) = (x2/y2 , x2)
        //        let distance2 = hypotenuse(dx2, dy2)
        //        newLineHeight2 = distance2
        
        
        lineView3 = UIImageView(frame: CGRect(x: fullScreenSize.width/2+wapWidth+spacing1, y: fullScreenSize.height/3.2+wapWidth*3-0, width: 3, height: wapWidth+spacing3))
        self.view.addSubview(lineView3)
        UIGraphicsBeginImageContext(lineView3.frame.size) // 位图上下文绘制区域
        lineView3.image?.draw(in: lineView3.bounds)
        let context5:CGContext = UIGraphicsGetCurrentContext()!
        context5.setLineCap(CGLineCap.square)
        let lengths5:[CGFloat] = [8,12] // 绘制 跳过 无限循环
        context5.setStrokeColor(PrimaryColor2.cgColor)
        context5.setLineWidth(5)
        context5.setLineDash(phase: 0, lengths: lengths5)
        context5.move(to: CGPoint(x: 0, y: 8))
        context5.addLine(to: CGPoint(x: 3, y: wapWidth+spacing3)) //虛線顯示區域
        context5.strokePath()
        lineView3.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView3.alpha = 0
        
        //        lineView3.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView3)
        //        lineView3.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth*3)
        //            makes.centerX.equalToSuperview().offset(wapWidth+spacing1)
        //            constraint19 = makes.height.equalTo((wapWidth+spacing3)*1).constraint
        //            makes.width.equalTo(1.0)
        //        }
        //        constraint19?.activate()
        
        lineView4 = UIImageView(frame: CGRect(x: fullScreenSize.width/2-wapWidth-spacing1, y: fullScreenSize.height/3.2+wapWidth*1-spacing3, width: 3, height: wapWidth+spacing3))
        self.view.addSubview(lineView4)
        UIGraphicsBeginImageContext(lineView4.frame.size) // 位图上下文绘制区域
        lineView4.image?.draw(in: lineView4.bounds)
        let context4:CGContext = UIGraphicsGetCurrentContext()!
        context4.setLineCap(CGLineCap.square)
        let lengths4:[CGFloat] = [8,12] // 绘制 跳过 无限循环
        context4.setStrokeColor(PrimaryColor2.cgColor)
        context4.setLineWidth(5)
        context4.setLineDash(phase: 0, lengths: lengths4)
        context4.move(to: CGPoint(x: 0, y: 8))
        context4.addLine(to: CGPoint(x: 3, y: wapWidth+spacing3)) //虛線顯示區域
        context4.strokePath()
        lineView4.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView4.alpha = 0
        
        //        lineView4.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView4)
        //        lineView4.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth*1-spacing3)
        //            makes.centerX.equalToSuperview().offset(-(wapWidth+spacing1))
        //            constraint20 = makes.height.equalTo((wapWidth+spacing3)*1).constraint
        //            makes.width.equalTo(1.0)
        //        }
        //        constraint20?.activate()
        
        lineView5 = UIImageView(frame: CGRect(x: fullScreenSize.width/2, y: fullScreenSize.height/3.2+wapWidth-spacing1/2, width: 3, height: (wapWidth+spacing3)*2+spacing1/4))
        self.view.addSubview(lineView5)
        UIGraphicsBeginImageContext(lineView5.frame.size) // 位图上下文绘制区域
        lineView5.image?.draw(in: lineView5.bounds)
        let context:CGContext = UIGraphicsGetCurrentContext()!
        context.setLineCap(CGLineCap.square)
        let lengths:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context.setStrokeColor(PrimaryColor2.cgColor)
        context.setLineWidth(5)
        context.setLineDash(phase: 0, lengths: lengths)
        context.move(to: CGPoint(x: 0, y: 12))
        context.addLine(to: CGPoint(x: 3, y: (wapWidth+spacing3)*2+spacing1/4)) //虛線顯示區域
        context.strokePath()
        lineView5.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView5.alpha = 0
        
        //        lineView5.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView5)
        //        lineView5.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth-0)
        //            makes.centerX.equalToSuperview()
        //            makes.height.equalTo((wapWidth+spacing3)*2)
        //            makes.width.equalTo(1.0)
        //        }
        
        lineView6 = UIImageView(frame: CGRect(x: fullScreenSize.width/2-(wapWidth*2+spacing3), y: fullScreenSize.height/3.2+wapWidth-spacing1/2, width: 3, height: (wapWidth+spacing3)*2+spacing1/4))
        self.view.addSubview(lineView6)
        UIGraphicsBeginImageContext(lineView6.frame.size) // 位图上下文绘制区域
        lineView6.image?.draw(in: lineView6.bounds)
        let context8:CGContext = UIGraphicsGetCurrentContext()!
        context8.setLineCap(CGLineCap.square)
        let lengths8:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context8.setStrokeColor(PrimaryColor2.cgColor)
        context8.setLineWidth(5)
        context8.setLineDash(phase: 0, lengths: lengths8)
        context8.move(to: CGPoint(x: 0, y: 12))
        context8.addLine(to: CGPoint(x: 3, y: (wapWidth+spacing3)*2+spacing1/4)) //虛線顯示區域
        context8.strokePath()
        lineView6.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView6.alpha = 0
        
        //        lineView6.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView6)
        //        lineView6.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth-0)
        //            makes.centerX.equalToSuperview().offset(-(wapWidth*2+spacing3))
        //            makes.height.equalTo((wapWidth+spacing3)*2)
        //            makes.width.equalTo(1.0)
        //        }
        
        lineView7 = UIImageView(frame: CGRect(x: fullScreenSize.width/2+(wapWidth*2+spacing3), y: fullScreenSize.height/3.2+wapWidth-spacing1/2, width: 3, height: (wapWidth+spacing3)*2+spacing1/4))
        self.view.addSubview(lineView7)
        UIGraphicsBeginImageContext(lineView7.frame.size) // 位图上下文绘制区域
        lineView7.image?.draw(in: lineView7.bounds)
        let context9:CGContext = UIGraphicsGetCurrentContext()!
        context9.setLineCap(CGLineCap.square)
        let lengths9:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context9.setStrokeColor(PrimaryColor2.cgColor)
        context9.setLineWidth(5)
        context9.setLineDash(phase: 0, lengths: lengths9)
        context9.move(to: CGPoint(x: 0, y: 12))
        context9.addLine(to: CGPoint(x: 3, y: (wapWidth+spacing3)*2+spacing1/4)) //虛線顯示區域
        context9.strokePath()
        lineView7.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView7.alpha = 0
        
        //        lineView7.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView7)
        //        lineView7.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth-0)
        //            makes.centerX.equalToSuperview().offset(+(wapWidth*2+spacing3))
        //            makes.height.equalTo((wapWidth+spacing3)*2)
        //            makes.width.equalTo(1.0)
        //        }
        
        lineView8 = UIImageView(frame: CGRect(x: fullScreenSize.width/2+wapWidth+spacing1, y: fullScreenSize.height/3.2+wapWidth*1-spacing3, width: 3, height: wapWidth+spacing3))
        self.view.addSubview(lineView8)
        UIGraphicsBeginImageContext(lineView8.frame.size) // 位图上下文绘制区域
        lineView8.image?.draw(in: lineView8.bounds)
        let context6:CGContext = UIGraphicsGetCurrentContext()!
        context6.setLineCap(CGLineCap.square)
        let lengths6:[CGFloat] = [8,12] // 绘制 跳过 无限循环
        context6.setStrokeColor(PrimaryColor2.cgColor)
        context6.setLineWidth(5)
        context6.setLineDash(phase: 0, lengths: lengths6)
        context6.move(to: CGPoint(x: 0, y: 8))
        context6.addLine(to: CGPoint(x: 3, y: wapWidth+spacing3)) //虛線顯示區域
        context6.strokePath()
        lineView8.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView8.alpha = 0
        
        //        lineView8.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView8)
        //        lineView8.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth*1-spacing3)
        //            makes.centerX.equalToSuperview().offset(wapWidth+spacing1)
        //            makes.height.equalTo((wapWidth+spacing3)*1)
        //            makes.width.equalTo(1.0)
        //        }
        
        lineView9 = UIImageView(frame: CGRect(x: fullScreenSize.width/2-wapWidth-spacing1, y: fullScreenSize.height/3.2+wapWidth*3, width: 3, height: wapWidth+spacing3))
        self.view.addSubview(lineView9)
        UIGraphicsBeginImageContext(lineView9.frame.size) // 位图上下文绘制区域
        lineView9.image?.draw(in: lineView9.bounds)
        let context7:CGContext = UIGraphicsGetCurrentContext()!
        context7.setLineCap(CGLineCap.square)
        let lengths7:[CGFloat] = [8,12] // 绘制 跳过 无限循环
        context7.setStrokeColor(PrimaryColor2.cgColor)
        context7.setLineWidth(5)
        context7.setLineDash(phase: 0, lengths: lengths7)
        context7.move(to: CGPoint(x: 0, y: 8))
        context7.addLine(to: CGPoint(x: 3, y: wapWidth+spacing3)) //虛線顯示區域
        context7.strokePath()
        lineView9.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView9.alpha = 0
        
        //        lineView9.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView9)
        //        lineView9.snp.makeConstraints{ (makes) in
        //            makes.top.equalToSuperview().offset(fullScreenSize.height/3.2+wapWidth*3)
        //            makes.centerX.equalToSuperview().offset(-(wapWidth+spacing1))
        //            makes.height.equalTo((wapWidth+spacing3)*1)
        //            makes.width.equalTo(1.0)
        //        }
        
        lineView10 = UIImageView(frame: CGRect(x: fullScreenSize.width/2, y: fullScreenSize.height/3.2-wapWidth*1-spacing3, width: 3, height: (wapWidth+spacing3)*2+spacing1/4))
        self.view.addSubview(lineView10)
        UIGraphicsBeginImageContext(lineView10.frame.size) // 位图上下文绘制区域
        lineView10.image?.draw(in: lineView10.bounds)
        let context10:CGContext = UIGraphicsGetCurrentContext()!
        context10.setLineCap(CGLineCap.square)
        let lengths10:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context10.setStrokeColor(PrimaryColor2.cgColor)
        context10.setLineWidth(5)
        context10.setLineDash(phase: 0, lengths: lengths10)
        context10.move(to: CGPoint(x: 0, y: 12))
        context10.addLine(to: CGPoint(x: 3, y: (wapWidth+spacing3)*2+spacing1/4)) //虛線顯示區域
        context10.strokePath()
        lineView10.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView10.alpha = 0
        
        //        lineView10.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView10)
        //        lineView10.snp.makeConstraints{ (makes) in
        //            makes.centerX.equalTo(bkView444)
        //            makes.centerY.equalTo(bkView444)
        //            makes.width.equalTo((wapWidth+spacing3)*2)
        //            makes.height.equalTo(1.0)
        //        }
        
        lineView11 = UIImageView(frame: CGRect(x: fullScreenSize.width/2, y: fullScreenSize.height/3.2+wapWidth*3+spacing3, width: 3, height: (wapWidth+spacing3)*2+spacing1/4))
        self.view.addSubview(lineView11)
        UIGraphicsBeginImageContext(lineView11.frame.size) // 位图上下文绘制区域
        lineView11.image?.draw(in: lineView11.bounds)
        let context11:CGContext = UIGraphicsGetCurrentContext()!
        context11.setLineCap(CGLineCap.square)
        let lengths11:[CGFloat] = [6,12] // 绘制 跳过 无限循环
        context11.setStrokeColor(PrimaryColor2.cgColor)
        context11.setLineWidth(5)
        context11.setLineDash(phase: 0, lengths: lengths11)
        context11.move(to: CGPoint(x: 0, y: 12))
        context11.addLine(to: CGPoint(x: 3, y: (wapWidth+spacing3)*2+spacing1/4)) //虛線顯示區域
        context11.strokePath()
        lineView11.image = UIGraphicsGetImageFromCurrentImageContext()
        lineView11.alpha = 0
        
        //        lineView11.backgroundColor = PrimaryColor2
        //        view.addSubview(lineView11)
        //        lineView11.snp.makeConstraints{ (makes) in
        //            makes.centerX.equalTo(bkView444)
        //            makes.centerY.equalTo(bkView444).offset(wapWidth*4+spacing3*2)
        //            makes.width.equalTo((wapWidth+spacing3)*2)
        //            makes.height.equalTo(1.0)
        //        }
        
        ///按下去可以跳轉到detail頁面
        deviceButton.backgroundColor = .clear
        deviceButton.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        view.addSubview(deviceButton)
        deviceButton.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView4)
        }
        
        stationImage.layer.cornerRadius = labelFontSize2
        stationImage.backgroundColor = PrimaryColor2
        self.view.addSubview(stationImage)
        stationImage.snp.makeConstraints{ (makes) in
            makes.centerX.equalTo(bkView4).offset(wapWidth-spacing1)
            makes.centerY.equalTo(bkView4).offset(-wapWidth+spacing1)
            makes.width.height.equalTo(labelFontSize2*2)
        }
        
        stationNumLabel.backgroundColor = .clear
        stationNumLabel.textAlignment = .center
        stationNumLabel.text = stationCount.description
        stationNumLabel.textColor = .white
        stationNumLabel.font = UIFont.systemFont(ofSize: 16)
        view.addSubview(stationNumLabel)
        stationNumLabel.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(stationImage)
        }
        
        deviceNameLabel.textAlignment = .center
        deviceNameLabel.text = "\(deviceName)"
        deviceNameLabel.textColor = .white
        deviceNameLabel.font = UIFont.systemFont(ofSize: 13.5)
        view.addSubview(deviceNameLabel)
        deviceNameLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView4)
            makes.width.equalTo(wapWidth*4)
            makes.height.equalTo(spacing3)
            makes.top.equalTo(bkView4.snp.bottom)
        }
        
        deviceButton2.backgroundColor = .clear
        deviceButton2.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        self.view.addSubview(deviceButton2)
        deviceButton2.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView16)
        }
        
        stationImage2.layer.cornerRadius = labelFontSize2
        stationImage2.backgroundColor = PrimaryColor2
        self.view.addSubview(stationImage2)
        stationImage2.snp.makeConstraints{ (makes) in
            makes.centerX.equalTo(bkView16).offset(wapWidth-spacing1)
            makes.centerY.equalTo(bkView16).offset(-wapWidth+spacing1)
            makes.width.height.equalTo(labelFontSize2*2)
        }
        
        stationNumLabel2.backgroundColor = .clear
        stationNumLabel2.textAlignment = .center
        stationNumLabel2.text = "\(stationCount2)"
        stationNumLabel2.textColor = .white
        stationNumLabel2.font = UIFont.systemFont(ofSize: 16)
        view.addSubview(stationNumLabel2)
        stationNumLabel2.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(stationImage2)
        }
        
        deviceNameLabel2.textAlignment = .center
        deviceNameLabel2.text = "\(deviceName2)"
        deviceNameLabel2.textColor = .white
        deviceNameLabel2.font = UIFont.systemFont(ofSize: 13.5)
        view.addSubview(deviceNameLabel2)
        deviceNameLabel2.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView16)
            makes.width.equalTo(wapWidth*4)
            makes.height.equalTo(spacing3)
            makes.top.equalTo(bkView16.snp.bottom)
        }
        
        deviceButton3.backgroundColor = .clear
        deviceButton3.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        self.view.addSubview(deviceButton3)
        deviceButton3.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView18)
        }
        
        stationImage3.layer.cornerRadius = labelFontSize2
        stationImage3.backgroundColor = PrimaryColor2
        self.view.addSubview(stationImage3)
        stationImage3.snp.makeConstraints{ (makes) in
            makes.centerX.equalTo(bkView18).offset(wapWidth-spacing1)
            makes.centerY.equalTo(bkView18).offset(-wapWidth+spacing1)
            makes.width.height.equalTo(labelFontSize2*2)
        }
        
        stationNumLabel3.backgroundColor = .clear
        stationNumLabel3.textAlignment = .center
        stationNumLabel3.text = "\(stationCount3)"
        stationNumLabel3.textColor = .white
        stationNumLabel3.font = UIFont.systemFont(ofSize: 16)
        view.addSubview(stationNumLabel3)
        stationNumLabel3.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(stationImage3)
        }
        
        deviceNameLabel3.textAlignment = .center
        deviceNameLabel3.text = deviceName3
        deviceNameLabel3.textColor = .white
        deviceNameLabel3.font = UIFont.systemFont(ofSize: 13.5)
        view.addSubview(deviceNameLabel3)
        deviceNameLabel3.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView18)
            makes.width.equalTo(wapWidth*4)
            makes.height.equalTo(spacing3)
            makes.top.equalTo(bkView18.snp.bottom)
        }
        
        deviceButton4.backgroundColor = .clear
        deviceButton4.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        self.view.addSubview(deviceButton4)
        deviceButton4.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView6)
        }
        
        stationImage4.layer.cornerRadius = labelFontSize2
        stationImage4.backgroundColor = PrimaryColor2
        self.view.addSubview(stationImage4)
        stationImage4.snp.makeConstraints{ (makes) in
            makes.centerX.equalTo(bkView6).offset(wapWidth-spacing1)
            makes.centerY.equalTo(bkView6).offset(-wapWidth+spacing1)
            makes.width.height.equalTo(labelFontSize2*2)
        }
        
        stationNumLabel4.backgroundColor = .clear
        stationNumLabel4.textAlignment = .center
        stationNumLabel4.text = "\(stationCount4)"
        stationNumLabel4.textColor = .white
        stationNumLabel4.font = UIFont.systemFont(ofSize: 16)
        view.addSubview(stationNumLabel4)
        stationNumLabel4.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(stationImage4)
        }
        
        deviceNameLabel4.textAlignment = .center
        deviceNameLabel4.text = deviceName4
        deviceNameLabel4.textColor = .white
        deviceNameLabel4.font = UIFont.systemFont(ofSize: 13.5)
        view.addSubview(deviceNameLabel4)
        deviceNameLabel4.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView6)
            makes.width.equalTo(wapWidth*4)
            makes.height.equalTo(spacing3)
            makes.top.equalTo(bkView6.snp.bottom)
        }
        
        deviceButton5.backgroundColor = .clear
        deviceButton5.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        self.view.addSubview(deviceButton5)
        deviceButton5.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView2)
        }
        
        stationImage5.layer.cornerRadius = labelFontSize2
        stationImage5.backgroundColor = PrimaryColor2
        self.view.addSubview(stationImage5)
        stationImage5.snp.makeConstraints{ (makes) in
            makes.centerX.equalTo(bkView2).offset(wapWidth-spacing1)
            makes.centerY.equalTo(bkView2).offset(-wapWidth+spacing1)
            makes.width.height.equalTo(labelFontSize2*2)
        }
        
        stationNumLabel5.backgroundColor = .clear
        stationNumLabel5.textAlignment = .center
        stationNumLabel5.text = "\(stationCount5)"
        stationNumLabel5.textColor = .white
        stationNumLabel5.font = UIFont.systemFont(ofSize: 16)
        view.addSubview(stationNumLabel5)
        stationNumLabel5.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(stationImage5)
        }
        
        deviceNameLabel5.textAlignment = .center
        deviceNameLabel5.text = deviceName5
        deviceNameLabel5.textColor = .white
        deviceNameLabel5.font = UIFont.systemFont(ofSize: 13.5)
        view.addSubview(deviceNameLabel5)
        deviceNameLabel5.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(bkView2)
            makes.width.equalTo(wapWidth*4)
            makes.height.equalTo(spacing3)
            makes.top.equalTo(bkView2.snp.bottom)
        }
        
    }
    /**
     開根號三角函數公式
     - Date: 08/26 Yang
    */
    func hypotenuse(_ a: Double, _ b: Double) -> Double {
        return (a * a + b * b).squareRoot()
    }
    /**
     傳資料到detail頁顯示
     - Date: 08/26 Yang
    */
    @objc func clickToDetail(_ sender: UIButton){
        if navigationController?.view.superview?.frame.origin.x == 0 {
            if sender == deviceButton{
                device_name = deviceName
                station_mac = stationMac
                station_rssi = stationRssi
                station_uplink = stationUplink
                station_downlink = stationDownlink
                station_connected_band = stationConnectedBand
                let vc = getVC(storyboardName: "Main", identifier: "DeviceDetailViewController") as! DeviceDetailViewController
                vc.wapDevicesDetail = deviceAddress
                self.navigationController?.pushViewController(vc, animated: true)
            }else if sender == deviceButton2{
                device_name = deviceName2
                station_mac = stationMac2
                station_rssi = stationRssi2
                station_uplink = stationUplink2
                station_downlink = stationDownlink2
                station_connected_band = stationConnectedBand2
                let vc = getVC(storyboardName: "Main", identifier: "DeviceDetailViewController") as! DeviceDetailViewController
                vc.wapDevicesDetail = deviceAddress2
                self.navigationController?.pushViewController(vc, animated: true)
            }else if sender == deviceButton3{
                device_name = deviceName3
                station_mac = stationMac3
                station_rssi = stationRssi3
                station_uplink = stationUplink3
                station_downlink = stationDownlink3
                station_connected_band = stationConnectedBand3
                let vc = getVC(storyboardName: "Main", identifier: "DeviceDetailViewController") as! DeviceDetailViewController
                vc.wapDevicesDetail = deviceAddress3
                self.navigationController?.pushViewController(vc, animated: true)
            }else if sender == deviceButton4{
                device_name = deviceName4
                station_mac = stationMac4
                station_rssi = stationRssi4
                station_uplink = stationUplink4
                station_downlink = stationDownlink4
                station_connected_band = stationConnectedBand4
                let vc = getVC(storyboardName: "Main", identifier: "DeviceDetailViewController") as! DeviceDetailViewController
                vc.wapDevicesDetail = deviceAddress4
                self.navigationController?.pushViewController(vc, animated: true)
            }else if sender == deviceButton5{
                device_name = deviceName5
                station_mac = stationMac5
                station_rssi = stationRssi5
                station_uplink = stationUplink5
                station_downlink = stationDownlink5
                station_connected_band = stationConnectedBand5
                let vc = getVC(storyboardName: "Main", identifier: "DeviceDetailViewController") as! DeviceDetailViewController
                vc.wapDevicesDetail = deviceAddress5
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            navigationController?.view.menu()
        }
    }
    /**
     刷新Mesh圖資料
     - Date: 08/26 Yang
    */
    @IBAction func refreshClick(_ sender: UIButton) {
    ///測試假資料 可以顯示五種Mesh圖
        //show five devices test
//                ccc += 1
//                if ccc == 7{
//                    ccc = 1
//                }
//
//                switch ccc{
//                case 1:
//                    self.tempData = tempWizardHtmData_0
//                    self.searchInfo()
//                    self.showMesh()
//                case 2:
//                    self.tempData = tempWizardHtmData_8
//                    self.searchInfo()
//                    self.showMesh()
//                case 3:
//                    self.tempData = tempWizardHtmData_1
//                    self.searchInfo()
//                    self.showMesh()
//                case 4:
//                    self.tempData = tempWizardHtmData_2
//                    self.searchInfo()
//                    self.showMesh()
//                case 5:
//                    self.tempData = tempWizardHtmData_7
//                    self.searchInfo()
//                    self.showMesh()
//                case 6:
//                    self.tempData = tempWizardHtmData_6
//                    self.searchInfo()
//                    self.showMesh()
//                default:
//                    break
//                }
        
        self.setActView(actView: self.actView)
        getData{
            ///同時刷新ConnectVC
            let name4 = Notification.Name("Refresh_ConnectVC")
            NotificationCenter.default.post(name: name4, object: nil, userInfo:
                nil)
            self.searchInfo()
            self.showMesh()
            self.actView.removeFromSuperview()
        }
    }
    /**
     出現側邊欄
     - Date: 08/26 Yang
    */
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    
}
