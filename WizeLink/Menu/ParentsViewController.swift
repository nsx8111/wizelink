//
//  ParentsViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
/**
 家長控制頁(親子鎖)
 - Date: 08/25 Yang
*/
class ParentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UnBlockedTableView == tableView{
            return unblockedCount
        }else{
            return blockedCount
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        //        print("accessory Button Tapped = \(indexPath.row)")
    }
    
    //用Frame決定Button寬度
//    func blockBtnView(_ title:String ,_ frame:CGRect) -> UIView{
//        let blockView = UIView()
//        blockView.frame = frame
//        blockView.backgroundColor = SecondaryColor3
//        blockView.layer.cornerRadius = 12
//
//        let blockButton = UIButton(type: .custom)
//        if blockButton.isHighlighted{
//            blockButton.backgroundColor = SecondaryColor3
//        }else{
//            blockButton.backgroundColor = .white
//        }
//
//        blockButton.backgroundColor = .white
//        blockButton.layer.cornerRadius = 11
//        blockButton.setTitle(title, for: [])
//        blockButton.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize)
//        blockButton.setTitleColor(SecondaryColor3, for: .normal)
//        blockButton.setTitleColor(.white, for: .highlighted)
//
////        blockButton.setImage(UIImage(named: "S__33390595"), for: .normal)
//        blockView.addSubview(blockButton)
//        blockButton.snp.makeConstraints { (makes) in
//            makes.left.right.top.bottom.equalTo(blockView).inset(1.0)
//        }
//        if blockButton.title(for: []) == LocalizeUtils.shared.localized(withKey: "BLOCK", withTableName: localTable.button.rawValue){
//            blockButton.addTarget(self,action: #selector(self.clickBlock),for: .touchUpInside)
//        }else{
//            blockButton.addTarget(self,action: #selector(self.clickUnblock),for: .touchUpInside)
//        }
//        return blockView
//    }
    
    /**
     封鎖按鈕
     用FontSize決定Button寬度 隨者字元數增加按鈕寬度自動延長 非固定寬度元件 多國語系適用
     - Date: 08/26 Yang
    */
    func blockBtnView(_ title:String) -> UIButton{
        let blockView = UIButton()
        let blockButton = UIButton(type: .custom)
        let blockButton2 = HighlightButton()
        
        let index = title.index(title.startIndex, offsetBy: title.count/2)
        let title2 = title[..<index]
        print("title2 \(title2)")
        
        let index2 = title.index(title.startIndex, offsetBy: title.count/3)
        let title3 = title[..<index2]
        print("title3 \(title3)")
        
        blockView.backgroundColor = SecondaryColor3
        blockView.layer.cornerRadius = 14
        blockView.setTitleColor(.clear, for: .normal)
        blockView.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        
        blockButton.backgroundColor = .white
        blockButton.layer.cornerRadius = 13
        blockButton.setTitleColor(.clear, for: .normal)
        blockButton.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        
        if bundleLocalizeFileName == "ja" || bundleLocalizeFileName == "de" || bundleLocalizeFileName == "es"  || bundleLocalizeFileName == "pt" {
            blockView.setTitle(title+title3, for: [])
            blockButton.setTitle(title+title3, for: [])
        }else{
            blockView.setTitle(title+title2, for: [])
            blockButton.setTitle(title+title2, for: [])
        }
        
        blockButton2.setTitle(title, for: [])
        blockButton2.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        blockButton2.setTitleColor(SecondaryColor3, for: .normal)
        blockButton2.normalBackgroundColor = .clear
        blockButton2.highlightedTitleColor = .white
        blockButton2.highlightedBackgroundColor = SecondaryColor3
        blockButton2.cornerRadius = 13
        if blockButton2.isHighlighted == false{
            blockButton2.highlightDuration = 0.3
        }
        
        blockView.sizeToFit()
        blockButton.sizeToFit()
        blockButton2.sizeToFit()

        blockView.addSubview(blockButton)
        blockButton.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(blockView).inset(1)
        }
        blockButton.addSubview(blockButton2)
        blockButton2.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(blockButton).inset(0)
            makes.left.right.equalTo(blockButton).inset(0)
        }
        blockButton2.addTarget(self,action: #selector(self.clickBlock),for: .touchUpInside)
        return blockView
    }
    /**
     解除封鎖按鈕
     用FontSize決定Button寬度 隨者字元數增加按鈕寬度自動延長 非固定寬度元件 多國語系適用
     - Date: 08/26 Yang
    */
    func unblockBtnView(_ title:String) -> UIButton{
        let blockView = UIButton()
        let blockButton = UIButton(type: .custom)
        let blockButton2 = HighlightButton()
 
        let index2 = title.index(title.startIndex, offsetBy: title.count/4)
        let title3 = title[..<index2]
        print("title3 \(title3)")
        
        blockView.backgroundColor = SecondaryColor3
        blockView.layer.cornerRadius = 14
        
        blockView.setTitle(title+title3, for: [])
        blockView.setTitleColor(.clear, for: .normal)
        blockView.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        
        blockButton.backgroundColor = .white
        blockButton.layer.cornerRadius = 13
        blockButton.setTitleColor(.clear, for: .normal)
        blockButton.setTitle(title+title3, for: [])
        blockButton.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        
        blockButton2.setTitle(title, for: [])
        blockButton2.titleLabel?.font = UIFont.systemFont(ofSize: labelFontSize-1)
        blockButton2.setTitleColor(SecondaryColor3, for: .normal)
        blockButton2.highlightedTitleColor = .white
        blockButton2.normalBackgroundColor = .clear
        blockButton2.highlightedBackgroundColor = SecondaryColor3
        blockButton2.cornerRadius = 13
        if blockButton2.isHighlighted == false{
            blockButton2.highlightDuration = 0.3
        }
        
        blockView.sizeToFit()
        blockButton.sizeToFit()
        blockButton2.sizeToFit()

        blockView.addSubview(blockButton)
        blockButton.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(blockView).inset(1)
        }
        blockButton.addSubview(blockButton2)
        blockButton2.snp.makeConstraints { (makes) in
            makes.top.bottom.left.right.equalTo(blockButton).inset(0)
        }
        blockButton2.addTarget(self,action: #selector(self.clickUnblock),for: .touchUpInside)
        return blockView
    }
    
    @objc func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "UnlockedCell")
        let cell2 = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "lockedCell")
        
        if UnBlockedTableView == tableView{
            cell.backgroundColor = .white
            cell.selectionStyle = .none
            cell.textLabel?.text = unblockDevice[indexPath.row]
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.textColor = .black
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
            
            cell.detailTextLabel?.text = unblockMac[indexPath.row].uppercased()
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.detailTextLabel?.textAlignment = NSTextAlignment.center
            cell.detailTextLabel?.textColor = PrimaryColor2
            
            switch currentDevice.deviceType {
            case .iPhone40:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13)
            case .iPad:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 23)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 20)
            default:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
            }
            
//            cell.accessoryType = UITableViewCell.AccessoryType.detailButton
            
//            if  bundleLocalizeFileName == "ja" || bundleLocalizeFileName == "de"{
//                cell.accessoryView = blockBtnView(LocalizeUtils.shared.localized(withKey: "BLOCK", withTableName: localTable.button.rawValue), CGRect(x: 0, y: 0, width: 75, height: 25))
//            }else{
//                cell.accessoryView = blockBtnView(LocalizeUtils.shared.localized(withKey: "BLOCK", withTableName: localTable.button.rawValue), CGRect(x: 0, y: 0, width: 50, height: 25))
//            }
            
            cell.accessoryView = blockBtnView(localString(key: "BLOCK", table: .button))
            return cell
        }else{
            cell2.backgroundColor = .white
            cell2.selectionStyle = .none
            cell2.detailTextLabel?.text = blockedMac[indexPath.row].uppercased()
            cell2.detailTextLabel?.textAlignment = NSTextAlignment.center
            cell2.detailTextLabel?.textColor = PrimaryColor2
            
            switch currentDevice.deviceType {
            case .iPhone40:
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 13)
            case .iPad:
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 20)
            default:
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
            }
            
//            if bundleLocalizeFileName == "ja" || bundleLocalizeFileName == "de"{
//                cell2.accessoryView = blockBtnView(LocalizeUtils.shared.localized(withKey: "UNBLOCK", withTableName: localTable.button.rawValue), CGRect(x: 0, y: 0, width: 100, height: 25))
//            }else{
//                cell2.accessoryView = blockBtnView(LocalizeUtils.shared.localized(withKey: "UNBLOCK", withTableName: localTable.button.rawValue), CGRect(x: 0, y: 0, width: 75, height: 25))
//            }
            
            cell2.accessoryView = unblockBtnView(localString(key: "UNBLOCK", table: .button))
            return cell2
        }
        
    }
 
    
    let titleLabel = UILabel()
    let actView : UIView = UIView()
    let navigationBarView = UIImageView()
    let bkView = UIView()
    let bkView2 = UIView()
    let bkView3 = UIView()
    let bkView4 = UIView()
    let bkView5 = UIView()
    let unblockedLabel = UILabel()
    let blockedLabel = UILabel()
    let blockedCountBtn = UIButton()
    let BKBtn2 = UIButton()
    let unblockedCountBtn = UIButton()
    let BKBtn1 = UIButton()
    let showBtn1 = UIButton()
    let showBtn2 = UIButton()
    
    var temp1 : [String] = []
    var temp2 : [String] = []
    var blockedMac : [String] = []
    var unblockDevice : [String] = []
    var unblockMac : [String] = []
    var onlineDevice : [Device] = []
    var offlineDevice : [Device] = []
    var selectedMac : String = ""
    var selectedIndex : String = ""
    var pageLanguage : String = ""

    var unblockedCount:Int = 0
    var blockedCount:Int = 0
    
    let ContentView = UIView()
    let RefreshScrollView = UIScrollView()
    private var BlockedTableView: UITableView!
    private var UnBlockedTableView: UITableView!
    
    @IBOutlet weak var RefreshButton: UIButton!
    @IBOutlet var MenuButton : UIButton!
    var MenuButton2 = UIButton(type: .custom)
    var RefreshButton2 = UIButton(type: .custom)
    /**
     一進來頁面先用searchInfo撈資料 viewDidLoad只會撈一次 若要更新searchInfo資料必須按RefreshButton 否則每次進來都是舊資料
     - Date: 08/26 Yang
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        isShowAlert = false
        self.searchInfo()
        self.searchInfo2()
        self.unblockedCount = unblockMac.count
        ///若無任何已封鎖用戶 Mac陣列第一個index為空字串
        for (_ , _) in self.blockedMac.enumerated(){
            if self.blockedMac.contains(""){
                self.blockedCount = 0
            }else{
                self.blockedCount = self.blockedMac.count
            }
        }
        self.setViews()
        self.setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /// Mike 只有有跟 server 互動才會去呼叫 getData()
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        isShowAlert = false
        reloadData2()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    /**
     多國語系切換文字
     - Date: 08/26 Yang
    */
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        titleLabel.text = localString(key: "PARENT_CTRL", table: .pageTitle)
        unblockedLabel.text = localString(key: "UNBLOCKED_CLIENT", table: .dataTitle)
        blockedLabel.text = localString(key: "BLOCKED_CLIENT", table: .dataTitle)
    }
    ///測試資料
    func test(){
//        mac_filter_app_HtmData = mac_filter_app_tempHtmData
//        mac_filter_app_HtmData = mac_filter_app_tempHtmData1
//        mac_filter_app_HtmData = mac_filter_app_tempHtmData2
//        mac_filter_app_HtmData = mac_filter_app_tempHtmData3
        
//        print("(〜￣▽￣)〜")
//        print("(〜￣▽￣)〜(〜￣▽￣)〜")
    }
    /**
     reloadData1~reloadData3為三種刷新頁面情況下 產生不同的佈局
     - Date: 08/26 Yang
    */
    ///for SendBlock & Refresh
    func reloadData1() {
        self.searchInfo()
        self.searchInfo2()
        self.unblockedCount = self.unblockMac.count
        for (_ , _) in self.blockedMac.enumerated(){
            if self.blockedMac.contains(""){
                self.blockedCount = 0
            }else{
                self.blockedCount = self.blockedMac.count
            }
        }
        self.setUnblockConstraints()
        self.blockedCountBtn.setTitle((self.blockedCount).description, for: [])
        self.unblockedCountBtn.setTitle((self.unblockedCount).description, for: [])
        self.UnBlockedTableView.reloadData()
        self.BlockedTableView.reloadData()
        self.actView.removeFromSuperview()
    }
    ///for viewWillAppear
    func reloadData2() {
        self.searchInfo()
        self.searchInfo2()
        self.unblockedCount = self.unblockMac.count
        for (_ , _) in self.blockedMac.enumerated(){
            if self.blockedMac.contains(""){
                self.blockedCount = 0
            }else{
                self.blockedCount = self.blockedMac.count
            }
        }
        self.blockedCountBtn.setTitle((self.blockedCount).description, for: [])
        self.unblockedCountBtn.setTitle((self.unblockedCount).description, for: [])
        self.UnBlockedTableView.reloadData()
        self.BlockedTableView.reloadData()
        self.actView.removeFromSuperview()
    }
    ///for SendUnblock
    func reloadData3() {
        self.searchInfo()
        self.searchInfo2()
        self.unblockedCount = self.unblockMac.count
        for (_ , _) in self.blockedMac.enumerated(){
            if self.blockedMac.contains(""){
                self.blockedCount = 0
            }else{
                self.blockedCount = self.blockedMac.count
            }
        }
        self.setBlockConstraints()
        self.blockedCountBtn.setTitle((self.blockedCount).description, for: [])
        self.unblockedCountBtn.setTitle((self.unblockedCount).description, for: [])
        self.UnBlockedTableView.reloadData()
        self.BlockedTableView.reloadData()
        self.actView.removeFromSuperview()
    }
    /**
     LoadData
     先撈所有裝置資料 扣除已封鎖裝置後(searchInfo2) 就是未封鎖裝置資料
     - Date: 08/26 Yang
    */
    func searchInfo(){
        //            let data1 = rangeFunc(src: clientsHtmData, str1: "var buffer ='", str2: "';\n    var res = buffer.split")
        let data1 = searchFunc(src: clientsHtmData, str1: "var buffer ='", str2: "';\n    var res = buffer.split")
        let deviceArray = data1.components(separatedBy: ">>>")
        print("deviceArray.count:\(deviceArray.count)")
        
        onlineDevice = []
        offlineDevice = []
        
        for device in deviceArray{
            let deviceDataArray = device.components(separatedBy: " ")
            var tempDevice = Device()
            tempDevice.deviceName = deviceDataArray[0]
            if deviceDataArray.count > 1{
                tempDevice.ipAddress = deviceDataArray[1]
                tempDevice.macAddress = deviceDataArray[2]
                tempDevice.unknownNum = deviceDataArray[3]
                tempDevice.connectType = deviceDataArray[4]
                tempDevice.brand = deviceDataArray[6]
                
                if deviceDataArray[5] == "on"{
                    tempDevice.isOnline = true
                    onlineDevice.append(tempDevice)
                }else{
                    offlineDevice.append(tempDevice)
                }
            }
        }
        unblockDevice.removeAll()
        unblockMac.removeAll()
        for i in 0 ..< onlineDevice.count{
            unblockDevice.append(onlineDevice[i].deviceName)
            unblockMac.append(onlineDevice[i].macAddress)
        }
        print("Device_Name \(unblockDevice)")
        print("Mac_ID \(unblockMac)")
    }
    /**
     LoadData
     已封鎖裝置
     - Date: 08/26 Yang
    */
    func searchInfo2(){
        temp1 = mac_filter_app_HtmData.components(separatedBy: "macfilter_buffer='")
        print("temp1_count:\(temp1.count)")
        for i in 0..<temp1.count{
            print("*** temp1[\(i)]: \(temp1[i])")
        }
        
        if temp1.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            isShowAlert = false
            self.actView.removeFromSuperview()
            return
        }
        
        temp2 = temp1[1].components(separatedBy: "';")
        print("temp2.count:\(temp2.count)")
        for i in 0..<temp2.count{
            print("*** temp2[\(i)]: \(temp2[i])")
        }
        
        temp1 = temp2[0].components(separatedBy: ">>>>>")
        print("temp1.count:\(temp1.count)")
        blockedMac.removeAll()
        for i in 0..<temp1.count{
            print("*** temp1[\(i)]: \(temp1[i])")
            temp2 = temp1[i].components(separatedBy: "___>")
            print("********** Get Mac : \(temp2[0]) ")
            blockedMac.append(temp2[0])
        }
        print("Blocked_Mac \(blockedMac)")
        
        for (index, _) in blockedMac.enumerated(){
            print("index \(index)")
            self.selectedIndex = (index+1).description
            if blockedMac.contains(""){
//                blockedMac.remove(at: index)
            }
        }
//        print("Blocked_Mac2 \(blockedMac)")
        print(unblockMac.count)
        for (_ , _) in unblockMac.enumerated(){
            for mac_filter in blockedMac{
                if unblockMac.contains(mac_filter){
                    print("Unblock_Device11 = \(unblockDevice)")
                    unblockMac.firstIndex(of: mac_filter).map {
                        unblockMac.remove(at: $0)
                        unblockDevice.remove(at: $0)
                    }
//                    unblockDevice.remove(at: index)
//                    unblockMac.remove(at: index)
                    print("Unblock_Device = \(unblockDevice)")
                    print("Unblock_Mac = \(unblockMac)")
                }
            }
        }
    }
    /**
     SendData
     封鎖用戶並短開機
     - Date: 08/26 Yang
    */
    func SendBlockData(){
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        let requestStr : String = "\(formFilterStr)?enabled=ON&mac=" + selectedMac + "&comment=124&addFilterMac=Save&addFilterMacFlag=1"
        AF.request(requestStr, headers: headers)
            .responseJSON { response in
//                print("response_11 \(response)")
                if case .failure(let error) = response.result {
                    print("error \(error.localizedDescription)")
                } else if case .success = response.result {
                    print("success \(response.result)")
                }
                AF.request(rebootRequestStr,headers: headers)
                    .responseJSON { response in
                        getData{
                            DispatchQueue.main.asyncAfter(deadline:DispatchTime.now() + 1.0) {
                                self.reloadData1()
                            }
                        }
                
                }
                
        }
    }
    /**
     SendData
     解除封鎖用戶並短開機
     - Date: 08/26 Yang
    */
    func SendUnblockData(){
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        let requestStr : String = "\(formFilterStr)?select" + selectedIndex + "=ON&deleteSelFilterMac=Delete%20Selected"
        AF.request(requestStr, headers: headers)
            .responseJSON { response in
                print("requestStr_Index \(requestStr)")
                AF.request(rebootRequestStr,headers: headers)
                    .responseJSON { response in
                        getData{
                            DispatchQueue.main.asyncAfter(deadline:DispatchTime.now() + 1.0) {
                                self.reloadData3()
                            }
                        }
                        
                }
                
        }
    }
    /**
     按下封鎖按鈕後執行的動作
     - Date: 08/26 Yang
    */
    @objc func clickBlock(_ sender: UIButton){
        let point = sender.convert(CGPoint.zero, to: UnBlockedTableView)
        if let indexPath = UnBlockedTableView.indexPathForRow(at: point) {
            let controller = UIAlertController(title: unblockMac[indexPath.row].uppercased(), message: localString(key: "BLOCK", table: .alertMsg), preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default, handler: {(alert: UIAlertAction!) in
                self.setActView(actView: self.actView)
                self.selectedMac = self.unblockMac[indexPath.row]
                let vowels: Set<Character> = [":"]
                self.selectedMac.removeAll(where: { vowels.contains($0) })
                print("selected_Mac \(self.selectedMac)")
                self.SendBlockData()
            })
            let cancelAction2 = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
            controller.addAction(cancelAction2)
            controller.addAction(cancelAction)
            present(controller, animated: true, completion: nil)
        }
    }
    /**
     按下解除封鎖按鈕後執行的動作
     - Date: 08/26 Yang
    */
    @objc func clickUnblock(_ sender: UIButton){
        let point = sender.convert(CGPoint.zero, to: BlockedTableView)
        if let indexPath = BlockedTableView.indexPathForRow(at: point) {
            let controller = UIAlertController(title: blockedMac[indexPath.row].uppercased(), message: localString(key: "UNBLOCK", table: .alertMsg), preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .default, handler: {(alert: UIAlertAction!) in
                self.setActView(actView: self.actView)
                self.selectedIndex = (indexPath.row+1).description
                print("selected_Index \(self.selectedIndex)")
                self.SendUnblockData()
            })
            let cancelAction2 = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
            controller.addAction(cancelAction2)
            controller.addAction(cancelAction)
            present(controller, animated: true, completion: nil)
        }
    }
    /**
     在線未封鎖用戶TableView更新佈局
     - Date: 08/26 Yang
    */
    func setUnblockConstraints(){
        UnBlockedTableView.snp.updateConstraints { (makes) in
            makes.height.equalTo(Int(UnBlockedTableView.rowHeight) * unblockedCount)
        }
        BlockedTableView.snp.updateConstraints { (makes) in
            makes.height.equalTo(0)
        }
    }
    /**
     已封鎖用戶TableView更新佈局
     - Date: 08/26 Yang
    */
    func setBlockConstraints(){
        UnBlockedTableView.snp.updateConstraints { (makes) in
            makes.height.equalTo(0)
        }
        BlockedTableView.snp.updateConstraints { (makes) in
            makes.height.equalTo(Int(BlockedTableView.rowHeight) * blockedCount)
        }
    }
    /**
     按下按鈕展開在線未封鎖用戶TableView
     - Date: 08/26 Yang
    */
    @objc func showUnblockedUser(){
        if navigationController?.view.superview?.frame.origin.x == 0 {
            showBtn2.setImage(UIImage(named: "minus-b"), for: .normal)
            showBtn1.setImage(UIImage(named: "down-b"), for: .normal)
            setUnblockConstraints()
        }else{
            navigationController?.view.menu()
        }
    }
    /**
     按下按鈕展開已封鎖用戶TableView
     - Date: 08/26 Yang
    */
    @objc func showBlockedUser(){
        if navigationController?.view.superview?.frame.origin.x == 0 {
            showBtn1.setImage(UIImage(named: "minus-b"), for: .normal)
            showBtn2.setImage(UIImage(named: "down-b"), for: .normal)
            setBlockConstraints()
        }else{
            navigationController?.view.menu()
        }
    }
    /**
     初始畫面佈局
     - Date: 08/26 Yang
    */
    func setViews(){
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        if let navBarHeight =
            UserDefaults.standard.string(forKey: "navBarHeight") , let navBarH = (Int(navBarHeight)) {
            navigationBarView.backgroundColor = PrimaryColor1
            self.view.addSubview(navigationBarView)
            navigationBarView.snp.makeConstraints { (makes) in
                makes.top.equalToSuperview().offset(0)
                makes.width.equalTo(fullScreenSize.width)
                makes.height.equalTo(navBarH)
            }
            
            switch currentDevice.deviceType {
            case .iPad:
                titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
            default:
                titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
            }
            titleLabel.backgroundColor = .clear
            titleLabel.text = localString(key: "PARENT_CTRL", table: .pageTitle)
            titleLabel.textColor = .white
            titleLabel.textAlignment = .center
            self.view.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
                makes.width.equalTo(fullScreenSize.width*0.6)
            }
            
            MenuButton2.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
            MenuButton2.tintColor = SecondaryColor3
            MenuButton2.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
            self.view.addSubview(MenuButton2)
            MenuButton2.snp.makeConstraints { (makes) in
                makes.centerY.equalTo(titleLabel)
                makes.width.height.equalTo(buttonSize)
                makes.left.equalToSuperview().offset(spacing3)
            }
            
            RefreshButton2.setImage(UIImage(named: "reload-outline")!.withRenderingMode(.alwaysTemplate), for: .normal)
            RefreshButton2.tintColor = SecondaryColor3
            RefreshButton2.addTarget(self,action: #selector(self.refreshClick),for: .touchUpInside)
            self.view.addSubview(RefreshButton2)
            RefreshButton2.snp.makeConstraints { (makes) in
                makes.centerY.equalTo(titleLabel)
                makes.width.height.equalTo(buttonSize)
                makes.right.equalToSuperview().offset(-spacing3)
            }
            ///用來下滑刷新資料
            RefreshScrollView.bounces = true
            RefreshScrollView.delegate = self
            bkView.addSubview(RefreshScrollView)
            RefreshScrollView.snp.makeConstraints { (makes) in
                makes.left.right.equalTo(bkView)
                makes.top.equalTo(navigationBarView.snp.bottom)
                makes.bottom.equalTo(bkView)
            }
            
            RefreshScrollView.addSubview(ContentView)
            ContentView.snp.makeConstraints { (makes) in
                makes.top.equalTo(RefreshScrollView).offset(0)
                makes.bottom.equalTo(RefreshScrollView)
                makes.left.right.equalTo(bkView)
            }
            
            bkView2.backgroundColor = .lightGray
            bkView2.layer.cornerRadius = 8
            ContentView.addSubview(bkView2)
            bkView2.snp.makeConstraints { (makes) in
                makes.left.right.equalToSuperview().inset(spacing3)
                makes.top.equalTo(RefreshScrollView).offset(spacing3)
            }
            
        }
        
        bkView3.backgroundColor = .white
        bkView3.layer.cornerRadius = 8
        ContentView.addSubview(bkView3)
        bkView3.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView2).inset(0.5)
        }
        
        showBtn1.setImage(UIImage(named: "down-b"), for: .normal)
        showBtn1.addTarget(self,action: #selector(self.showUnblockedUser),for: .touchUpInside)
        ContentView.addSubview(showBtn1)
        showBtn1.snp.makeConstraints { (makes) in
            makes.left.top.equalTo(bkView3).offset(spacing3)
            makes.width.height.equalTo(spacing2)
        }
        
        unblockedLabel.text = localString(key: "UNBLOCKED_CLIENT", table: .dataTitle)
        unblockedLabel.textColor = .black
        unblockedLabel.textAlignment = .left
        unblockedLabel.numberOfLines = 0
        ContentView.addSubview(unblockedLabel)
        unblockedLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(showBtn1.snp.right).offset(spacing1)
            makes.centerY.equalTo(showBtn1)
//            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        BKBtn1.backgroundColor = SecondaryColor3
        ContentView.addSubview(BKBtn1)
        BKBtn1.snp.makeConstraints { (makes) in
            makes.right.equalTo(bkView3).offset(-spacing3*1.5)
            makes.left.greaterThanOrEqualTo(unblockedLabel.snp.right).offset(0)
            makes.width.height.equalTo(spacing1*2.5)
            makes.centerY.equalTo(unblockedLabel)
        }
        
        unblockedCountBtn.backgroundColor = .white
        unblockedCountBtn.setTitle((unblockedCount).description, for: [])
        unblockedCountBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: spacing2+1)
        unblockedCountBtn.setTitleColor(SecondaryColor3, for: .normal)
        unblockedCountBtn.addTarget(self,action: #selector(self.showUnblockedUser),for: .touchUpInside)
        ContentView.addSubview(unblockedCountBtn)
        unblockedCountBtn.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(BKBtn1).inset(1.2)
        }
        
        print("HeightForScroll \(HeightForScroll)")
        bkView4.backgroundColor = .lightGray
        bkView4.layer.cornerRadius = 8
        ContentView.addSubview(bkView4)
        //scrollView最底部元件一定要makes.bottom.equalToSuperview() 不然不能够确定contentSize
        bkView4.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview().inset(spacing3)
            makes.top.equalTo(bkView2.snp.bottom).offset(spacing3*2)
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                makes.bottom.equalToSuperview().offset(-spacing3*20*HeightForScroll)
            case .phone:
                makes.bottom.equalToSuperview().offset(-spacing3*15*HeightForScroll)
            default:
                print("3 Device Error")
            }
        }
        
        bkView5.backgroundColor = .white
        bkView5.layer.cornerRadius = 8
        ContentView.addSubview(bkView5)
        bkView5.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView4).inset(0.5)
        }
        
        showBtn2.setImage(UIImage(named: "minus-b"), for: .normal)
        showBtn2.addTarget(self,action: #selector(self.showBlockedUser),for: .touchUpInside)
        ContentView.addSubview(showBtn2)
        showBtn2.snp.makeConstraints { (makes) in
            makes.left.top.equalTo(bkView5).offset(spacing3)
            makes.width.height.equalTo(spacing2)
        }
        
        blockedLabel.text = localString(key: "BLOCKED_CLIENT", table: .dataTitle)
        blockedLabel.textColor = .black
        blockedLabel.textAlignment = .left
        ContentView.addSubview(blockedLabel)
        blockedLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(showBtn2.snp.right).offset(spacing1)
            makes.centerY.equalTo(showBtn2)
            makes.height.equalTo(spacing3)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        BKBtn2.backgroundColor = SecondaryColor3
        ContentView.addSubview(BKBtn2)
        BKBtn2.snp.makeConstraints { (makes) in
            makes.right.equalTo(bkView5).offset(-spacing3*1.5)
            makes.width.height.equalTo(spacing1*2.5)
            makes.centerY.equalTo(blockedLabel)
        }
        
        blockedCountBtn.backgroundColor = .white
        blockedCountBtn.setTitle((blockedCount).description, for: [])
        blockedCountBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: spacing2+1)
        blockedCountBtn.setTitleColor(SecondaryColor3, for: .normal)
        blockedCountBtn.addTarget(self,action: #selector(self.showBlockedUser),for: .touchUpInside)
        ContentView.addSubview(blockedCountBtn)
        blockedCountBtn.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(BKBtn2).inset(1.2)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            unblockedLabel.font = UIFont.boldSystemFont(ofSize: 23)
            blockedLabel.font = UIFont.boldSystemFont(ofSize: 23)
        default:
            unblockedLabel.font = UIFont.boldSystemFont(ofSize: 17)
            blockedLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
    }
    /**
     未封鎖用戶與已封鎖用戶的表格佈局
     - Date: 08/26 Yang
    */
    func setTableView(){
        UnBlockedTableView = UITableView()
        UnBlockedTableView.register(UITableViewCell.self, forCellReuseIdentifier: "UnlockedCell")
        UnBlockedTableView.tableFooterView = UIView(frame: CGRect.zero)
        UnBlockedTableView.delegate = self
        UnBlockedTableView.dataSource = self
        UnBlockedTableView.bounces = false
        UnBlockedTableView.allowsSelection = true
        UnBlockedTableView.backgroundColor = .white
        UnBlockedTableView.rowHeight = fullScreenSize.height*0.075
        ContentView.addSubview(UnBlockedTableView)
        UnBlockedTableView.snp.makeConstraints { (makes) in
            makes.top.equalTo(unblockedLabel).offset(spacing2*3)
            makes.height.equalTo(Int(UnBlockedTableView.rowHeight) * unblockedCount * 1)
            makes.left.equalTo(unblockedLabel).offset(-spacing3)
            makes.right.equalTo(bkView3).offset(-spacing1/2)
            makes.bottom.equalTo(bkView3).offset(-spacing3)
        }
        
        BlockedTableView = UITableView()
        BlockedTableView.register(UITableViewCell.self, forCellReuseIdentifier: "lockedCell")
        BlockedTableView.delegate = self
        BlockedTableView.dataSource = self
        BlockedTableView.bounces = false
        BlockedTableView.allowsSelection = true
        BlockedTableView.backgroundColor = .white
        BlockedTableView.rowHeight = fullScreenSize.height*0.075
        ContentView.addSubview(BlockedTableView)
        BlockedTableView.snp.makeConstraints { (makes) in
            makes.top.equalTo(blockedLabel).offset(spacing2*3)
            makes.height.equalTo(0)
            makes.left.equalTo(blockedLabel).offset(-spacing3)
            makes.right.equalTo(bkView5).offset(-spacing1/2)
            makes.bottom.equalTo(bkView5).offset(-spacing3)
        }
        UnBlockedTableView.separatorStyle = .none
        BlockedTableView.separatorStyle = .none
        print("RefreshScrollViewY \(RefreshScrollView.contentOffset.y)")
    }
    /**
     設定scrollView下滑刷新資料的幅度
     - Date: 08/26 Yang
    */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == RefreshScrollView {
            if scrollView.contentOffset.y < -labelWidth*1.3*HeightForScroll {
                print("contentOffsetY \(scrollView.contentOffset.y)")
                isShowAlert = true
                self.setActView(actView: self.actView)
                getData{
                    self.reloadData1()
                }
            }
        }
    }
    /**
     刷新此頁資料
     - Date: 08/26 Yang
    */
    @IBAction func refreshClick(_ sender: UIButton) {
        isShowAlert = true
        self.setActView(actView: self.actView)
        getData{
            self.reloadData1()
            ///同時刷新ConnectVC
            let name4 = Notification.Name("Refresh_ConnectVC")
            NotificationCenter.default.post(name: name4, object: nil, userInfo:
                nil)
        }
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    
}
