//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit

//extension UITableView {
//  func reloadDataSmoothly() {
//    UIView.setAnimationsEnabled(false)
//    CATransaction.begin()
//    CATransaction.setCompletionBlock { () -> Void in
//      UIView.setAnimationsEnabled(true)
//    }
//    reloadData()
//    beginUpdates()
//    endUpdates()
//    CATransaction.commit()
//  }
//}

struct LanguageType{
    var buttonChecked: UIImageView!
    var language_type: String = ""
    var version_type: String = ""
    var isSelected: Bool = false
}

class LanguageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
//        let location = sender.location(in: nil)
//        navigationController?.view.slideByFinger(location: location, state: sender.state)
//        ButtonDelegate?.clickLeft()
    }
    @IBOutlet weak var recognizer: UISwipeGestureRecognizer!
    
    @IBAction func swipe(_ sender: UISwipeGestureRecognizer) {
//        let point = self.view.center
//        if recognizer.direction == .right {
//            if point.x <= fullScreenSize.width - 150 {
//                self.view.center = CGPoint(
//                    x: self.view.center.x + 100,
//                    y: self.view.center.y)
//            } else {
//                self.view.center = CGPoint(
//                    x: fullScreenSize.width - 50,
//                    y: self.view.center.y)
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        LanguageTableView.reloadDataSmoothly()
        if let indexPath = LanguageTableView.indexPathForSelectedRow{
            if indexPath.row == 0 || indexPath.row == 1{
//                LanguageTableView.deselectRow(at: indexPath, animated: true)
            }else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 {
                if let cell = LanguageTableView.cellForRow(at: indexPath){
                    print(cell.textLabel?.textColor)
                    languageType = types[indexPath.row].language_type
//                    langSelectedColor =
                    types[indexPath.row].isSelected = true
                                  LanguageTableView.reloadData()
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "LanguageTypeCell")
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].language_type
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        
        if indexPath.row == 0 || indexPath.row == 1 {
            if indexPath.row == 0{
                if let languageType = UserDefaults.standard.object(forKey:"languageType") as? String {
                    cell.detailTextLabel?.text = languageType
                    cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
                    cell.detailTextLabel?.textAlignment = NSTextAlignment.center
                    cell.detailTextLabel?.textColor = PrimaryColor2
                }
                cell.textLabel?.textColor = .black
            }else{
            }
        }else{  //indexPath.row == 2....8
            if types[indexPath.row].isSelected == false{
                cell.textLabel?.textColor = .black
                types[indexPath.row].buttonChecked = UIImageView(image: UIImage(named: "button_unchecked"))
            }else{
                if let userSelectedColorData = UserDefaults.standard.object(forKey: "SelectedColor") as? Data {
                    if let userSelectedColor = NSKeyedUnarchiver.unarchiveObject(with:userSelectedColorData as Data) as? UIColor {
                        cell.textLabel?.textColor = userSelectedColor
                    }
                }
                //                cell.textLabel?.textColor = PrimaryColor2
                
                types[indexPath.row].buttonChecked = UIImageView(image: UIImage(named: "button_checked"))
            }
            cell.imageView?.image = types[indexPath.row].buttonChecked.image
            
            let itemSize = CGSize.init(width: 20, height: 20)
            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
            let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
            cell.imageView?.image!.draw(in: imageRect)
            cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
        }
        types[indexPath.row].isSelected = false
        return cell
    }
    
       func setTableView(){
           let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
               self.navigationController!.navigationBar.frame.height
           
           LanguageTableView = UITableView()
           LanguageTableView.register(UITableViewCell.self, forCellReuseIdentifier: "LanguageTypeCell")
           LanguageTableView.delegate = self
           LanguageTableView.dataSource = self
           LanguageTableView.bounces = false
           LanguageTableView.allowsSelection = true
           LanguageTableView.backgroundColor = .white
           LanguageTableView.rowHeight = fullScreenSize.height*0.065
           self.view.addSubview(LanguageTableView)
           
           LanguageTableView.snp.makeConstraints { (make) in
               make.top.equalToSuperview().offset(navigationBarHeight)
               make.width.equalTo(fullScreenSize.width)
               make.height.equalTo(LanguageTableView.rowHeight*9)
               make.centerX.equalToSuperview()
        }
                
    }
    
    var types = [
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "")), language_type: "目前語言", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "")), language_type: "選擇您的語言", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "English", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Español", version_type: "",isSelected: false),
        LanguageType(buttonChecked:UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Português", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "Deutsch", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "日本語", version_type: "",isSelected: false),
        LanguageType(buttonChecked: UIImageView(image: UIImage(named: "button_unchecked")), language_type: "繁體中文", version_type: "",isSelected: false),
        LanguageType(buttonChecked:UIImageView(image: UIImage(named: "button_unchecked")), language_type: "简体中文", version_type: "",isSelected: false)
    ]
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView2 = UIImageView()
    let backButton = UIButton()
    let changeButton = UIButton()
    private var LanguageTableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = .white
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        navBarLabel.backgroundColor = .clear
        navBarLabel.attributedText = NSAttributedString(string: "語言", attributes:
            nil)
        navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight/1.5)
            makes.left.equalToSuperview().offset(spacing2)
        }
        setTableView()
        
        changeButton.backgroundColor = SecondaryColor3
        changeButton.layer.cornerRadius = 23
        changeButton.setTitle("確認變更", for: [])
        changeButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        changeButton.titleLabel?.textColor = .white
        changeButton.isEnabled = true
        changeButton.addTarget(self,action: #selector(self.clickChangeButton),for: .touchUpInside)
        self.view.addSubview(changeButton)
        
        changeButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(LanguageTableView.snp.bottom).offset(spacing)
            makes.width.equalTo(fullScreenSize.width*0.48)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
//        let swipeRight = UISwipeGestureRecognizer(
//            target:self,
//            action:#selector(self.swipe(_:)))
        //        swipeRight.direction = .right
        //        self.view.addGestureRecognizer(swipeRight)
            
    }
    
    @objc func clickChangeButton(){
        let data : Data = NSKeyedArchiver.archivedData(withRootObject: PrimaryColor2) as Data
        UserDefaults.standard.set(data, forKey: "SelectedColor")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(languageType, forKey:"languageType")
        LanguageTableView.reloadData()
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
