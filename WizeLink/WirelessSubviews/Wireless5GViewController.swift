
//  Wireless5GTableViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/16.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
/**
 無線網路分頁
 - Date: 08/25 Yang
*/
///scrollView最底部元件一定要makes.bottom.equalToSuperview()，不然不能够确定contentSize。
class Wireless5GViewController: UIViewController , UITextFieldDelegate , UIScrollViewDelegate {
    
    var vc24G: Wireless24GViewController?

    let actView : UIView = UIView()
    let bkView = UIView()
    let bkView2 = UIView()
    let bkView3 = UIView()
    let scrollView = UIScrollView()
    let contentView = UIView()
    let label1 = UILabel()
    let label2 = UILabel()
    let label3 = UILabel()
    let wifiNameLabel = UILabel()
    let nameTextField = UITextField()
    let WifiPasswordLabel = UILabel()
    let passwordTextField = CustomTextField2()
    let channelLabel = UILabel()
    let wifiLabel = UILabel()
    let GuestNetLabel = UILabel()
    let channelButton = UIButton()
    let channelCountButton = UIButton()
    let WiFiSwitch = UISwitch()
    let GuestNetSwitch = UISwitch()
    let confirmChangeBtn = UIButton()
    var constraintBtn1 : Constraint?
    var constraintBtn2 : Constraint?
    var constraintBtn3 : Constraint?
    var constraintBtn4 : Constraint?
    var constraintBtn5 : Constraint?
    var constraintBtn6 : Constraint?
    /**
    Copy From 2.4GVC
    - TODO: 目前這個算法，在別的高度不同的裝置上，會出現問題
    
    - Date: 07/27 Mike
    */
    var constraintForBottom1 : Constraint?
    
    let bkView4 = UIView()
    let bkView5 = UIView()
    let label4 = UILabel()
    let label5 = UILabel()
    let label6 = UILabel()
    let wifiNameLabel2 = UILabel()
    let nameTextField2 = UITextField()
    let checkBoxBtn = UIButton()
    let checkBoxBtn2 = UIButton()
    let checkLabelBtn = UIButton()
    let checkLabelBtn2 = UIButton()
    let WifiPasswordLabel2 = UILabel()
    let passwordTextField2 = CustomTextField2()
    let rightViewBtn = UIButton(type: .custom)
    let rightViewBtn2 = UIButton(type: .custom)
    
    var channelNumNow : String = ""
    var channelArray : [String] = []
    var arrayCount:Int = 0
    var pageLanguage : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        channelArray = ["AUTO", "36", "40", "44","48", "52", "56", "60","64", "100", "104", "108","112", "116", "136", "140"]
//        arrayCount = channelArray[1] as? Int ?? 0
        
        isShowAlert = false
        setViews()
        // Mike, 根據 2.4GVC mark 掉
//        searchInfo()
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        let info = sender.userInfo!
        let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        let kbSize = rect.size
        let activeField: UITextField? = [nameTextField2, passwordTextField2].first { $0.isFirstResponder }
        if activeField != nil {
            self.view.frame.origin.y = -kbSize.height
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 
    }
    /**
    - 當 scrollView 往下拉超過一定距離的時候 refresh Data
    - scrollViewWillEndDragging部分是 Mike 寫的
    - contentOffset.y & Notification 的部分是 Yang 寫的
    
    - Date:07/23 Mike
    */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.contentOffset.y < -labelWidth*1.2*HeightForScroll {
            print("contentOffsetY \(scrollView.contentOffset.y)")
            let name = Notification.Name("ScrollViewRefresh")
            NotificationCenter.default.post(name: name, object: nil, userInfo: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        setViewByGNSwitch(isOn: self.GuestNetSwitch.isOn)
        isShowAlert = false
        searchInfo()
        checkLanguage()
        
        let name = NSNotification.Name("channelNumNow2")
        NotificationCenter.default.addObserver(self, selector:
            #selector(channelNumNow2(noti:)), name: name, object: nil)
    }
    
    @objc func channelNumNow2(noti: Notification) {
        searchInfo()
        self.channelNumNow = channelNum2
        print("vc5G.channelNumNow:\(self.channelNumNow)")
        if self.channelNumNow == "0"{
            self.channelCountButton.setTitle("AUTO", for: .normal)
        }else{
            self.channelCountButton.setTitle(self.channelNumNow, for: .normal)
        }
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        label1.text = localString(key: "WLAN_GUIDE_TITLE", table: .otherText)
        label2.text = "1. \(localString(key: "WLAN_5G_GUIDE_1", table: .otherText))"
        label3.text = "2. \(localString(key: "WLAN_5G_GUIDE_2", table: .otherText))"
        wifiNameLabel.text = localString(key: "WIFI_NAME", table: .dataTitle)
        WifiPasswordLabel.text = localString(key: "WIFI_PASSWORD", table: .dataTitle)
        channelLabel.text = localString(key: "CHANNEL", table: .dataTitle)
        wifiLabel.text = localString(key: "WIFI_ROAMING", table: .dataTitle)
        GuestNetLabel.text = localString(key: "ENABLE_GUEST", table: .dataTitle)
        label4.text = localString(key: "WLAN_GUIDE_TITLE", table: .otherText)
        label5.text = "1. \(localString(key: "WLAN_GUEST_GUIDE_1", table: .otherText))"
        label6.text = "2. \(localString(key: "WLAN_GUEST_GUIDE_2", table: .otherText))"
        wifiNameLabel2.text = localString(key: "WIFI_NAME", table: .dataTitle)
        checkLabelBtn.setTitle(localString(key: "NO_SECURITY", table: .otherText), for: [])
        checkLabelBtn2.setTitle("WPA/WPA2-\(localString(key: "PERSONAL", table: .otherText))", for: [])
        WifiPasswordLabel2.text = localString(key: "WIFI_PASSWORD", table: .dataTitle)
        confirmChangeBtn.setTitle(localString(key: "APPLY", table: .button), for: [])
    }
    
    func searchInfo(){
        //for test
        //        statusHtmData = tempStatusHtmData
        //        wizardHtmData = tempWizardHtmData
        print("isShowAlert5G \(isShowAlert)")

        let regDomain : String = searchFunc(src: wizardHtmData, str1: "regDomain[0]=", str2: ";")
        channelArray = getChannelArray5G(regDomain: regDomain)
        
        channelNumNow = searchFunc(src: wizardHtmData, str1: "defaultChan[0]=", str2: ";")
        print("channelNumNow @ wireless5g.searchInfo() :\(channelNumNow)")
        // Mike for AUTO
        if channelNumNow == "0"{
            channelCountButton.setTitle("AUTO", for: .normal)
        }else{
            channelCountButton.setTitle(channelNumNow, for: .normal)
        }
        
        var temp1 : [String] = []
        var temp2 : [String] = []
        
        temp1 = statusHtmData.components(separatedBy: "ssid_drv[0] ='")
        if temp1.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        self.nameTextField.text = temp2[0]
        
        temp1 = wizardHtmData.components(separatedBy: "pskValue[0]='")
        if temp1.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        self.passwordTextField.text = temp2[0]
        
        temp1 = wizardHtmData.components(separatedBy: "IEEE80211r_enable[0]=")
        if temp1.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: ";")
        let isRoaminStr = temp2[0]
        //        print("isRoaminStr:\(isRoaminStr)")
        if isRoaminStr == "1"{
            self.WiFiSwitch.isOn = true
        }else{
            self.WiFiSwitch.isOn = false
        }
        
        temp1 = statusHtmData.components(separatedBy: "mssid_ssid_drv[0][1] ='")
        if temp1.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        temp2 = temp1[1].components(separatedBy: "';")
        // Mike mark
        print("temp2.count:\(temp2.count),temp2[0]:\(temp2[0])")
        if temp2[0] == ""{
            // 沒有開 GN
            // Mike mark
            //            print("沒有開 GN")
            self.nameTextField2.text = temp2[0]
            self.GuestNetSwitch.isOn = false
            setViewByGNSwitch(isOn: false)
            GNisOn2 = false
        }else{
            GNisOn2 = true
            self.GuestNetSwitch.isOn = true
            setViewByGNSwitch(isOn: true)
            self.nameTextField2.text = temp2[0]
            SSID2 = temp2[0]

            var temp3 : [String] = []
            temp3 = statusHtmData.components(separatedBy: "mssid_clientnum[0][1] ='")
            temp2 = temp3[1].components(separatedBy: "';")
            print("mssid_clientnum_temp2 \(temp2[0])")
            clientNum2 = temp2[0]

            temp1 = statusHtmData.components(separatedBy: "mssid_wep[0][1] ='")
            if temp1.count < 2 {
                if isShowAlert == true{
                    self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                }
                self.actView.removeFromSuperview()
                return
            }
            temp2 = temp1[1].components(separatedBy: "';")
            if temp2[0] == "Open"{
                wep2 = temp2[0]
                print("無安全性")
            }else{
                self.checkBoxSelected(self.checkBoxBtn2)
                temp1 = statusHtmData.components(separatedBy: "mssid_psk[0][1] ='")
                if temp1.count < 2 {
                    if isShowAlert == true{
                        self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                    }
                    self.actView.removeFromSuperview()
                    return
                }
                temp2 = temp1[1].components(separatedBy: "';")
                self.passwordTextField2.text = temp2[0]
            }
        }
    }
    
    func setViews(){
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        scrollView.delegate = self
        bkView.addSubview(scrollView)
        scrollView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView)
        }
        //        print("scrollView \(scrollView.frame)")
        // 添加容器视图
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(scrollView)
            makes.left.right.equalTo(bkView) //确定的宽度，因为垂直滚动
        }
        //        contentView.backgroundColor = .red
        //        print("contentView \(contentView.frame)")
        
        bkView2.backgroundColor = .lightGray
        bkView2.layer.cornerRadius = 8
        contentView.addSubview(bkView2)
        bkView2.snp.makeConstraints { (makes) in
            makes.left.right.top.equalTo(contentView).inset(spacing2)
        }
        
        bkView3.backgroundColor = .white
        bkView3.layer.cornerRadius = 8
        contentView.addSubview(bkView3)
        bkView3.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView2).inset(0.5)
        }
        
        label1.text = localString(key: "WLAN_GUIDE_TITLE", table: .otherText)
        label1.font = UIFont.boldSystemFont(ofSize: 17)
        label1.textColor = .black
        label1.textAlignment = .left
        label1.numberOfLines = 0
        contentView.addSubview(label1)
        label1.snp.makeConstraints { (makes) in
            makes.left.top.right.equalTo(bkView3).inset(spacing3)
        }
        
        label2.text = "1. \(localString(key: "WLAN_5G_GUIDE_1", table: .otherText))"
        label2.font = UIFont.systemFont(ofSize: 13)
        label2.numberOfLines = 0
        label2.textColor = .black
        label2.textAlignment = .left
        contentView.addSubview(label2)
        label2.snp.makeConstraints { (makes) in
            makes.left.equalTo(label1)
            makes.top.equalTo(label1.snp.bottom).offset(spacing1)
            makes.centerX.equalToSuperview()
        }
        
        label3.text = "2. \(localString(key: "WLAN_5G_GUIDE_2", table: .otherText))"
        label3.font = UIFont.systemFont(ofSize: 13)
        label3.numberOfLines = 0
        label3.textColor = .black
        label3.textAlignment = .left
        contentView.addSubview(label3)
        label3.snp.makeConstraints { (makes) in
            makes.left.equalTo(label2)
            makes.top.equalTo(label2.snp.bottom).offset(spacing1)
            makes.centerX.equalToSuperview()
        }
        
        wifiNameLabel.text = localString(key: "WIFI_NAME", table: .dataTitle)
        wifiNameLabel.font = UIFont.systemFont(ofSize: labelFontSize+2)
        wifiNameLabel.textColor = PrimaryColor2
        wifiNameLabel.textAlignment = .left
        contentView.addSubview(wifiNameLabel)
        wifiNameLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(label2)
            makes.top.equalTo(label3).offset(spacing3*2)
            makes.width.equalTo(label3)
            makes.height.equalTo(spacing3)
        }
        
        nameTextField.font = UIFont.systemFont(ofSize: 17)
        nameTextField.textAlignment = .left
        nameTextField.clearButtonMode = .whileEditing
        nameTextField.returnKeyType = .done
        nameTextField.textColor = .black
        nameTextField.backgroundColor = TextFieldColor
        nameTextField.layer.cornerRadius = 5
        nameTextField.keyboardType = .namePhonePad
        nameTextField.delegate = self
        self.view.addSubview(nameTextField)
        nameTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.left.equalTo(label2)
            makes.top.equalTo(wifiNameLabel.snp.bottom).offset(spacing1)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        WifiPasswordLabel.backgroundColor = .clear
        WifiPasswordLabel.text = localString(key: "WIFI_PASSWORD", table: .dataTitle)
        WifiPasswordLabel.font = UIFont.systemFont(ofSize: labelFontSize+2)
        WifiPasswordLabel.textColor = PrimaryColor2
        WifiPasswordLabel.textAlignment = .left
        contentView.addSubview(WifiPasswordLabel)
        WifiPasswordLabel.snp.makeConstraints { (makes) in
            makes.left.equalTo(label2)
            makes.top.equalTo(nameTextField.snp.bottom).offset(spacing1)
            makes.width.equalTo(label3)
            makes.height.equalTo(spacing3)
        }
        
        passwordTextField.font = UIFont.systemFont(ofSize: 17)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .black
        passwordTextField.backgroundColor = TextFieldColor
        passwordTextField.layer.cornerRadius = 5
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.left.equalTo(label2)
            makes.top.equalTo(WifiPasswordLabel.snp.bottom).offset(spacing1)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        channelLabel.text = localString(key: "CHANNEL", table: .dataTitle)
        channelLabel.font = UIFont.boldSystemFont(ofSize: 17)
        channelLabel.textColor = .black
        channelLabel.textAlignment = .left
        contentView.addSubview(channelLabel)
        channelLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing3)
            makes.left.equalTo(label2)
            makes.height.equalTo(spacing3)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        wifiLabel.text = localString(key: "WIFI_ROAMING", table: .dataTitle)
        wifiLabel.font = UIFont.boldSystemFont(ofSize: 17)
        wifiLabel.textColor = .black
        wifiLabel.textAlignment = .left
        contentView.addSubview(wifiLabel)
        wifiLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(channelLabel.snp.bottom).offset(spacing3*1.2)
            makes.left.equalTo(label2)
            makes.height.equalTo(spacing3)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        GuestNetLabel.text = localString(key: "ENABLE_GUEST", table: .dataTitle)
        GuestNetLabel.font = UIFont.boldSystemFont(ofSize: 17)
        GuestNetLabel.textColor = .black
        GuestNetLabel.textAlignment = .left
        GuestNetLabel.numberOfLines = 0
        contentView.addSubview(GuestNetLabel)
        GuestNetLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(wifiLabel.snp.bottom).offset(spacing3*1.2)
            makes.left.equalTo(label2)
            makes.width.equalTo(fullScreenSize.width*0.6)
            makes.bottom.equalTo(bkView3.snp.bottom).offset(-spacing3)
        }
        
        channelButton.setImage(UIImage(named: "channel-blue"), for: .normal)
        channelButton.isEnabled = true
        channelButton.addTarget(self,action: #selector(self.channelSelect),for: .touchUpInside)
        self.view.addSubview(channelButton)
        channelButton.snp.makeConstraints { (makes) in
            makes.right.equalTo(bkView3).offset(-spacing3)
            makes.centerY.equalTo(channelLabel)
            makes.width.height.equalTo(spacing3)
        }
        
        // Mike 0508 for AUTO
        if channelNumNow == "0"{
            channelCountButton.setTitle("AUTO", for: .normal)
        }else{
            channelCountButton.setTitle(channelNumNow, for: .normal)
        }
        //        channelCountButton.setTitle(arrayCount.description, for: .normal)
        channelCountButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        channelCountButton.setTitleColor(PrimaryColor2, for: .normal)
        channelCountButton.isEnabled = true
        channelCountButton.addTarget(self,action: #selector(self.channelSelect),for: .touchUpInside)
        self.view.addSubview(channelCountButton)
        channelCountButton.snp.makeConstraints { (makes) in
            makes.right.equalTo(bkView3).offset(-spacing3*2-spacing1/4)
            makes.centerY.equalTo(channelLabel)
            makes.height.equalTo(spacing3)
            makes.width.equalTo(spacing3*2.5)
        }
        
        WiFiSwitch.isOn = false
        // 設置滑桿鈕的顏色
        WiFiSwitch.thumbTintColor = .white
        // 設置未選取時( off )的外觀顏色
        WiFiSwitch.tintColor = SecondaryColor1
        // 設置選取時( on )的外觀顏色
        WiFiSwitch.onTintColor = SecondaryColor1
//        WiFiSwitch.addTarget(self,action: #selector(self.onChange),for: .valueChanged)
        self.view.addSubview(WiFiSwitch)
        WiFiSwitch.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(wifiLabel)
            makes.right.equalTo(bkView3).offset(-spacing3)
        }
        
        GuestNetSwitch.thumbTintColor = .white
        GuestNetSwitch.tintColor = SecondaryColor1
        GuestNetSwitch.onTintColor = SecondaryColor1
        GuestNetSwitch.addTarget(self,action: #selector(self.GNSwitchChange),for: .valueChanged)
        self.view.addSubview(GuestNetSwitch)
        GuestNetSwitch.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(GuestNetLabel)
            makes.right.equalTo(bkView3).offset(-spacing3)
        }
        
        bkView4.alpha = 0
        bkView4.backgroundColor = .lightGray
        bkView4.layer.cornerRadius = 8
        contentView.addSubview(bkView4)
        bkView4.snp.makeConstraints { (makes) in
            makes.left.right.equalTo(contentView).inset(spacing2)
            makes.top.equalTo(bkView2.snp.bottom).offset(spacing3*2)
        }
        
        bkView5.alpha = 0
        bkView5.backgroundColor = .white
        bkView5.layer.cornerRadius = 8
        contentView.addSubview(bkView5)
        bkView5.snp.makeConstraints { (makes) in
            makes.left.right.top.bottom.equalTo(bkView4).inset(0.5)
        }
        
        label4.alpha = 0
        label4.text = localString(key: "WLAN_GUIDE_TITLE", table: .otherText)
        label4.font = UIFont.boldSystemFont(ofSize: 17)
        label4.numberOfLines = 0
        label4.textColor = .black
        label4.textAlignment = .left
        contentView.addSubview(label4)
        label4.snp.makeConstraints { (makes) in
            makes.left.top.right.equalTo(bkView5).inset(spacing3)
        }
        
        label5.alpha = 0
        label5.text = "1. \(localString(key: "WLAN_GUEST_GUIDE_1", table: .otherText))"
        label5.font = UIFont.systemFont(ofSize: 13)
        label5.numberOfLines = 0
        label5.textColor = .black
        label5.textAlignment = .left
        contentView.addSubview(label5)
        label5.snp.makeConstraints { (makes) in
            makes.left.equalTo(label4)
            makes.top.equalTo(label4.snp.bottom).offset(spacing1)
            makes.centerX.equalToSuperview()
        }
        
        label6.alpha = 0
        label6.text = "1. \(localString(key: "WLAN_GUEST_GUIDE_2", table: .otherText))"
        label6.font = UIFont.systemFont(ofSize: 13)
        label6.numberOfLines = 0
        label6.textColor = .black
        label6.textAlignment = .left
        contentView.addSubview(label6)
        label6.snp.makeConstraints { (makes) in
            makes.left.equalTo(label5)
            makes.top.equalTo(label5.snp.bottom).offset(spacing1)
            makes.centerX.equalToSuperview()
        }
        
        wifiNameLabel2.alpha = 0
        wifiNameLabel2.text = localString(key: "WIFI_NAME", table: .dataTitle)
        wifiNameLabel2.font = UIFont.systemFont(ofSize: labelFontSize+2)
        wifiNameLabel2.textColor = PrimaryColor2
        wifiNameLabel2.textAlignment = .left
        contentView.addSubview(wifiNameLabel2)
        wifiNameLabel2.snp.makeConstraints { (makes) in
            makes.left.equalTo(label5)
            makes.top.equalTo(label6).offset(spacing3*2)
            makes.width.equalTo(label6)
            makes.height.equalTo(spacing3)
        }
        
        nameTextField2.isHidden = true
        nameTextField2.font = UIFont.systemFont(ofSize: 17)
        nameTextField2.textAlignment = .left
        nameTextField2.clearButtonMode = .whileEditing
        nameTextField2.returnKeyType = .done
        nameTextField2.textColor = .black
        nameTextField2.backgroundColor = TextFieldColor
        nameTextField2.layer.cornerRadius = 5
        nameTextField2.keyboardType = .namePhonePad
        nameTextField2.delegate = self
        self.view.addSubview(nameTextField2)
        nameTextField2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.left.equalTo(label5)
            makes.top.equalTo(wifiNameLabel2.snp.bottom).offset(spacing1)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        
        checkBoxBtn.setImage(UIImage(named: "button_checked"), for: .normal)
        checkBoxBtn.isEnabled = true
        checkBoxBtn.addTarget(self,action: #selector(self.checkBoxSelected),for: .touchUpInside)
        self.view.addSubview(checkBoxBtn)
        checkBoxBtn.snp.makeConstraints { (makes) in
            makes.left.equalTo(label5).offset(spacing2)
            makes.top.equalTo(nameTextField2.snp.bottom).offset(spacing3)
            makes.width.height.equalTo(spacing3)
        }
        
        checkBoxBtn2.setImage(UIImage(named: "button_unchecked"), for: .normal)
        checkBoxBtn2.isEnabled = true
        checkBoxBtn2.addTarget(self,action: #selector(self.checkBoxSelected),for: .touchUpInside)
        self.view.addSubview(checkBoxBtn2)
        checkBoxBtn2.snp.makeConstraints { (makes) in
            makes.left.equalTo(checkBoxBtn)
            makes.top.equalTo(checkBoxBtn.snp.bottom).offset(spacing1*3)
            makes.width.height.equalTo(spacing3)
        }
        
        checkBoxBtn.isHidden = true
        checkBoxBtn2.isHidden = true
        checkLabelBtn.isHidden = true
        checkLabelBtn2.isHidden = true
        
        checkLabelBtn.isEnabled = true
        checkLabelBtn.backgroundColor = .clear
        checkLabelBtn.setTitle(localString(key: "NO_SECURITY", table: .otherText), for: [])
        checkLabelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        checkLabelBtn.setTitleColor(PrimaryColor2, for: .normal)
        checkLabelBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        checkLabelBtn.addTarget(self,action: #selector(self.checkBoxSelected),for: .touchUpInside)
        self.view.addSubview(checkLabelBtn)
        checkLabelBtn.snp.makeConstraints { (makes) in
            makes.height.equalTo(spacing3)
            makes.width.equalTo(fullScreenSize.width*0.7)
            makes.left.equalTo(checkBoxBtn.snp.right).offset(spacing2)
            makes.centerY.equalTo(checkBoxBtn)
        }
        
        checkLabelBtn2.isEnabled = true
        checkLabelBtn2.backgroundColor = .clear
        checkLabelBtn2.setTitle("WPA/WPA2-\(localString(key: "PERSONAL", table: .otherText))", for: [])
        checkLabelBtn2.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        checkLabelBtn2.setTitleColor(.black, for: .normal)
        checkLabelBtn2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        checkLabelBtn2.addTarget(self,action: #selector(self.checkBoxSelected),for: .touchUpInside)
        self.view.addSubview(checkLabelBtn2)
        checkLabelBtn2.snp.makeConstraints { (makes) in
            makes.height.equalTo(spacing3)
            makes.width.equalTo(fullScreenSize.width*0.7)
            makes.left.equalTo(checkBoxBtn2.snp.right).offset(spacing2)
            makes.centerY.equalTo(checkBoxBtn2)
            constraintBtn4 = makes.bottom.equalTo(bkView5.snp.bottom).offset(-spacing3).constraint
            constraintBtn5 = makes.bottom.equalTo(bkView5.snp.bottom).offset(-spacing3*2-spacing1*2-fullScreenSize.height*0.07).constraint
        }
        constraintBtn4?.deactivate()
        constraintBtn5?.deactivate()
        
        
        WifiPasswordLabel2.backgroundColor = .clear
        WifiPasswordLabel2.text = localString(key: "WIFI_PASSWORD", table: .dataTitle)
        WifiPasswordLabel2.font = UIFont.systemFont(ofSize: labelFontSize+2)
        WifiPasswordLabel2.textColor = PrimaryColor2
        WifiPasswordLabel2.textAlignment = .left
        contentView.addSubview(WifiPasswordLabel2)
        WifiPasswordLabel2.snp.makeConstraints { (makes) in
            makes.left.equalTo(label5)
            makes.top.equalTo(checkLabelBtn2.snp.bottom).offset(spacing3)
            makes.width.equalTo(label6)
            makes.height.equalTo(spacing3)
        }
        
        passwordTextField2.font = UIFont.systemFont(ofSize: 17)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .black
        passwordTextField2.backgroundColor = TextFieldColor
        passwordTextField2.layer.cornerRadius = 5
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.left.equalTo(label5)
            makes.top.equalTo(WifiPasswordLabel2.snp.bottom).offset(spacing1)
            makes.height.equalTo(fullScreenSize.height*0.07)
            constraintBtn6 = makes.bottom.equalTo(bkView5.snp.bottom).offset(-spacing1).constraint
        }
        WifiPasswordLabel2.alpha = 0
        passwordTextField2.alpha = 0
        constraintBtn6?.deactivate()
        
        confirmChangeBtn.backgroundColor = SecondaryColor3
        confirmChangeBtn.layer.cornerRadius = 21
        confirmChangeBtn.setTitle(localString(key: "APPLY", table: .button), for: [])
        confirmChangeBtn.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        confirmChangeBtn.titleLabel?.textColor = .white
        confirmChangeBtn.isEnabled = true
        confirmChangeBtn.addTarget(self,action: #selector(self.clickChangeButton),for: .touchUpInside)
        contentView.addSubview(confirmChangeBtn)  //self.view 無法上下滾動
        confirmChangeBtn.snp.makeConstraints { (makes) in
            constraintBtn1 = makes.top.equalTo(bkView4.snp.top).offset(0).constraint
            constraintBtn2 = makes.top.equalTo(bkView4.snp.bottom).offset(spacing3*2).constraint
            makes.centerX.equalToSuperview()
            makes.width.equalTo(fullScreenSize.width*0.45)
            makes.height.equalTo(fullScreenSize.height*0.06)
            //至少距離scrollView底部有spacing3以上的距離 (>= spacing3) 確保可上下滾動
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                constraintForBottom1 = makes.bottom.equalToSuperview().offset(-83*3.7*HeightForScroll).constraint
                constraintBtn3 = makes.bottom.equalToSuperview().offset(-spacing3*4.7*HeightForScroll).constraint
            case .phone:
                constraintForBottom1 = makes.bottom.equalToSuperview().offset(-83*HeightForScroll).constraint
                constraintBtn3 = makes.bottom.equalToSuperview().offset(-wapWidth*HeightForScroll).constraint
            default:
                print("3 Device Error")
            }
        }
        constraintBtn1?.activate()
        constraintBtn2?.deactivate()
        constraintBtn3?.deactivate()
        constraintForBottom1?.activate()
        
        passwordTextField.isSecureTextEntry = false
        passwordTextField2.isSecureTextEntry = false
        
        if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
            rightViewBtn2.addTarget(self,action: #selector(self.Security_Click2),for: .touchUpInside)
            rightViewBtn2.setImage(smallImage, for: .normal)
            passwordTextField2.rightView = rightViewBtn2
            passwordTextField2.rightViewMode = .always
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            confirmChangeBtn.layer.cornerRadius = 17
        case .iPhone40:
            confirmChangeBtn.layer.cornerRadius = 17
        case .iPhone47:
            confirmChangeBtn.layer.cornerRadius = 20
        case .iPhone55:
            confirmChangeBtn.layer.cornerRadius = 22
        case .iPhone60:
            confirmChangeBtn.layer.cornerRadius = 24
        case .iPad:
            confirmChangeBtn.layer.cornerRadius = 30
        default:
            confirmChangeBtn.layer.cornerRadius = 24
        }
        
    }
    
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
        
    }
    
    @objc func Security_Click2() {
        if passwordTextField2.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.1)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = true
        }
        
    }
    /**
     GuestNetSwitch 開關打開時會做的處理
     - Note: 建議更名, guestNetSwitchOn or etc
     - Date: 07/24 Mike
     */
    func layout_isOn() {
        if checkLabelBtn.titleColor(for: .normal) == PrimaryColor2{
            constraintBtn4?.activate()
            constraintBtn5?.deactivate()
            constraintBtn6?.deactivate()
        }else{
            constraintBtn6?.activate()
            constraintBtn5?.activate()
            constraintBtn4?.deactivate()
            WifiPasswordLabel2.alpha = 1
            passwordTextField2.alpha = 1
        }
        
        label4.alpha = 1
        label5.alpha = 1
        label6.alpha = 1
        wifiNameLabel2.alpha = 1
        nameTextField2.isHidden = false
        bkView5.alpha = 1
        bkView4.alpha = 1
        checkBoxBtn.isHidden = false
        checkBoxBtn2.isHidden = false
        checkLabelBtn.isHidden = false
        checkLabelBtn2.isHidden = false
        constraintBtn2?.activate()
        constraintBtn1?.deactivate()
        constraintBtn3?.activate()
    }
    /**
    GuestNetSwitch 開關關閉時會做的處理
    - Note: 建議更名, guestNetSwitchOff or etc
    - Date: 07/24 Mike
    */
    func layout_isOff() {
        label4.alpha = 0
        label5.alpha = 0
        label6.alpha = 0
        wifiNameLabel2.alpha = 0
        nameTextField2.isHidden = true
        bkView4.alpha = 0
        bkView5.alpha = 0
        checkBoxBtn.isHidden = true
        checkBoxBtn2.isHidden = true
        checkLabelBtn.isHidden = true
        checkLabelBtn2.isHidden = true
        WifiPasswordLabel2.alpha = 0
        passwordTextField2.alpha = 0
        constraintBtn1?.activate()
        constraintBtn2?.deactivate()
        //        constraintBtn3?.deactivate()
        constraintBtn3?.activate()
        constraintBtn4?.activate()
        constraintBtn5?.deactivate()
    }
    /**
     For GuestNetSwitch 切換時的處理動作
     
     - Note: 建議更名
     - Date: 07/24 Mike
     */
    @objc func onChange2(sender: UISwitch) {
        if sender.isOn == true{
            layout_isOn()
        } else {
            layout_isOff()
        }
    }
    /**
     Copy From 2.4GVC
     
     - Date: 07/24 Mike
     */
    @objc func GNSwitchChange(sender: UISwitch) {
        let isOn : Bool = sender.isOn
        setViewByGNSwitch(isOn: isOn)
    }
    /**
     Copy From 2.4GVC
     
     - Parameter isOn: guestNetSwitch.sender.isOn
     - Date: 07/27 Mike
     */
    func setViewByGNSwitch(isOn : Bool){
        let forAlpha : CGFloat = isOn ? 1 : 0
        if isOn {
            if checkLabelBtn.titleColor(for: .normal) == PrimaryColor2{
                constraintBtn4?.activate()
                constraintBtn5?.deactivate()
                constraintBtn6?.deactivate()
            }else{
                constraintBtn6?.activate()
                constraintBtn5?.activate()
                constraintBtn4?.deactivate()
                WifiPasswordLabel2.alpha = 1
                passwordTextField2.alpha = 1
            }
            constraintBtn2?.activate()
            constraintBtn1?.deactivate()
            constraintForBottom1?.deactivate()
            constraintBtn3?.activate()
        } else {
            WifiPasswordLabel2.alpha = 0
            passwordTextField2.alpha = 0
            constraintBtn1?.activate()
            constraintBtn2?.deactivate()
            constraintBtn4?.activate()
            constraintBtn5?.deactivate()
            constraintBtn3?.deactivate()
            constraintForBottom1?.activate()
        }
        nameTextField2.isHidden = !isOn
        checkBoxBtn.isHidden = !isOn
        checkBoxBtn2.isHidden = !isOn
        checkLabelBtn.isHidden = !isOn
        checkLabelBtn2.isHidden = !isOn
        
        label4.alpha = forAlpha
        label5.alpha = forAlpha
        label6.alpha = forAlpha
        wifiNameLabel2.alpha = forAlpha
        bkView5.alpha = forAlpha
        bkView4.alpha = forAlpha
    }
    
//    @objc func onChange(sender: UISwitch) {
//        if sender.isOn == true{
//        } else {
//        }
//    }
    
    @objc func checkBoxSelected(_ sender: UIButton){
        if sender == checkBoxBtn || sender == checkLabelBtn{
            constraintBtn6?.deactivate()
            constraintBtn4?.activate()
            constraintBtn5?.deactivate()
            WifiPasswordLabel2.alpha = 0
            passwordTextField2.alpha = 0
            checkLabelBtn.setTitleColor(PrimaryColor2, for: .normal)
            checkLabelBtn2.setTitleColor(.black, for: .normal)
            checkBoxBtn.setImage(UIImage(named: "button_checked"), for: .normal)
            checkBoxBtn2.setImage(UIImage(named: "button_unchecked"), for: .normal)
        }else{
            constraintBtn6?.activate()
            constraintBtn4?.deactivate()
            constraintBtn5?.activate()
            WifiPasswordLabel2.alpha = 1
            passwordTextField2.alpha = 1
            checkLabelBtn.setTitleColor(.black, for: .normal)
            checkLabelBtn2.setTitleColor(PrimaryColor2, for: .normal)
            checkBoxBtn.setImage(UIImage(named: "button_unchecked"), for: .normal)
            checkBoxBtn2.setImage(UIImage(named: "button_checked"), for: .normal)
        }
        
    }
    
    @objc func channelSelect(){
        let alertController = UIAlertController(title: "", message: localString(key: "CHANNEL", table: .dataTitle), preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        for i in channelArray {
            // Mike channelArray 從 [Any] 改為 [String] 就不用 as?
            alertController.addAction(UIAlertAction(title: i, style: .default, handler: { alertController in
                self.channelCountButton.setTitle( i, for: .normal)
                self.channelNumNow = i
            }))
        }
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX+0, y: self.view.bounds.midY+0, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func clickChangeButton(){
        self.view.frame.origin.y = 0
        textFieldResignFirstResponder()
       
        let actView : UIView = UIView()
        self.setActView(actView: actView)
        // let -> var
        var check : Bool?
        var check2 : Bool?
        //        var check2 : Bool?
        if checkBoxBtn.imageView?.image == UIImage(named: "button_checked"){
            check = true
            check2 = false
        }else{
            check = false
            check2 = true
        }
        
        if !GuestNetSwitch.isOn{
            nameTextField2.text = ""
            passwordTextField2.text = ""
            checkBoxSelected(self.checkBoxBtn)
            check = true
            check2 = false
        }
        
        print("""
            === clickChangeButton Test ===
            Wifi name : \(nameTextField.text ?? "no name")
            Wifi pwd : \(passwordTextField.text ?? "no pwd")
            wifi 漫遊? : \(WiFiSwitch.isOn)
            GuestNetwork? : \(GuestNetSwitch.isOn)
            GN name? : \(nameTextField2.text ?? "no name2")
            無安全性? : \(String(describing: check))
            WPA~~~? : \(String(describing: check2))
            GN pwd? : \(passwordTextField2.text ?? "np pwd2")
            === Test End ===
            """)
       
        var isRominStr : String = "false"
        if WiFiSwitch.isOn{
            isRominStr = "true"
        }
        
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        
        /// 送出 formWirelessPage API 的字串  Mike
        let formWirelessPageStrRequest : String = "\(formWirelessPageStr)?wlan_id=wlan0&SSID=\(nameTextField.text!)&PSK=\(passwordTextField.text!)&ROMIN=\(isRominStr)&channelNumber=\(channelNumNow)&"
        
        /// 送出 formWirelessVap API 的字串  Mike
        var formWirelessVapStr : String = "\(machineUrl)boafrm/formWirelessVap?"
        
        if GuestNetSwitch.isOn{
            formWirelessVapStr += "enable_vap=true&radio=5GHz&SSID_vap=\(nameTextField2.text!)&encrypt_vap="
            // Guest Enable
            if check! {
                // 無安全性
                formWirelessVapStr += "false&"
                
            }else{
                // 有安全性(WPA)
                formWirelessVapStr += "true&PSK_vap=\(passwordTextField2.text!)&"
            }
        }else{
            // Guest Disable
            formWirelessVapStr += "enable_vap=false&radio=5GHz&"
        }
        
        /// 下面兩個 request 都送出以後，才會送出 reBoot API，避免先送出而導致資料不正確  Mike
        var count: Int = 0
        
        AF.request(formWirelessPageStrRequest,headers: headers)
            .responseJSON { response in
                print("\(formWirelessPageStrRequest) API Done")
                if count == 0{
                    count += 1
                }else if count == 1{
                    self.reBoot {
                        let IpValue = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
                        if IpValue == "" || IpValue == "0.0.0.0" {
                            self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                            actView.removeFromSuperview()
                            return
                        }
                        actView.removeFromSuperview()
                    }
                }
                
        }
        
        AF.request(formWirelessVapStr,headers: headers)
            .responseJSON { response in
                print("\(formWirelessVapStr) API Done")
                
                if count == 0{
                    count += 1
                }else if count == 1{
                    self.reBoot {
                        let IpValue = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
                        if IpValue == "" || IpValue == "0.0.0.0" {
                            self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                            actView.removeFromSuperview()
                            return
                        }
                        actView.removeFromSuperview()
                    }
                }
                
        }
    }
    
    func reBoot(completion : @escaping ()->()){
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        AF.request(rebootRequestStr,headers: headers)
            .responseJSON { response in
                print("reboot done")
                DispatchQueue.main.asyncAfter(deadline:DispatchTime.now() + 5.0) {
                    getData{
                        self.searchInfo()
                        completion()
                    }
                }
                
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        nameTextField.resignFirstResponder()
        nameTextField2.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
    }
    
}




