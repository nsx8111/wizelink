//
//  ViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/25.
//  Copyright © 2020 YangYang. All rights reserved.


import UIKit
import SnapKit
/**
無線網路導航欄之下的2.4G跟5G切換頁 用ScrollView進行左右滑動
 - Date: 08/25 Yang
*/
class WirelessContainer: UIViewController , UIScrollViewDelegate {
    /**
     無線網路2.4G 5G button
     - Date: 08/25 Yang
    */
    @IBAction func WirelessButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.2, delay: 0, animations: {
                self.MoveBar2.snp.updateConstraints { (makes) in
                    makes.left.equalToSuperview().offset(0)
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
            ScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.setTabBarButton(x: 0)
        case 1:
            //            MoveBar.center.x = sender.center.x
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.2, delay: 0, animations: {
                self.MoveBar2.snp.updateConstraints { (makes) in
                    makes.left.equalToSuperview().offset(fullScreenSize.width/2)
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
            ScrollView.setContentOffset(CGPoint(x: fullScreenSize.width, y: 0), animated: true)
            self.setTabBarButton(x: fullScreenSize.width)
        default:
            break
        }
        
    }
    
    let titleLabel = UILabel()
    let navigationBarView = UIImageView()
    @IBOutlet weak var Btn_5G: UIButton!
    @IBOutlet weak var Btn_24G: UIButton!
    @IBOutlet weak var MoveBar: UIView!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var scrollStackView: UIStackView!
    let MoveBar2 = UIView()
    let bkView = UIView()
    var constraint1 : Constraint?
    var constraint2 : Constraint?
    var pageLanguage : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Btn_24G.setTitle(localString(key: "WLAN_2.4G", table: .tabTitle), for: .normal)
        Btn_5G.setTitle(localString(key: "WLAN_5G", table: .tabTitle), for: .normal)
        setViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Container_origin_x \(String(describing: navigationController?.view.superview?.frame.origin.x))")
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName{
            reloadString()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        Btn_24G.setTitle(localString(key: "WLAN_2.4G", table: .tabTitle), for: .normal)
        Btn_5G.setTitle(localString(key: "WLAN_5G", table: .tabTitle), for: .normal)
    }
    
    func setViews(){
        
//        let navBarHeight = UIApplication.shared.statusBarFrame.height +
//            self.navigationController!.navigationBar.frame.height
//
//        navigationBarView.backgroundColor = PrimaryColor1
//        self.view.addSubview(navigationBarView)
//        navigationBarView.snp.makeConstraints { (makes) in
//            makes.top.equalToSuperview().offset(0)
//            makes.width.equalTo(fullScreenSize.width)
//            makes.height.equalTo(navBarHeight)
//        }
//
//        titleLabel.backgroundColor = .clear
//        titleLabel.attributedText = NSAttributedString(string: "無線網路", attributes:
//            nil)
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
//        titleLabel.textColor = .white
//        titleLabel.textAlignment = .center
//        self.view.addSubview(titleLabel)
//        titleLabel.snp.makeConstraints { (makes) in
//            makes.centerX.equalToSuperview()
//            makes.top.equalToSuperview().offset(loginLabelHeight)
//            makes.width.equalTo(fullScreenSize.width*0.6)
//        }
        
        buttonStack.addSubview(MoveBar2)
        MoveBar2.backgroundColor = SecondaryColor3
        MoveBar2.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width/2)
            makes.height.equalTo(5)
        }
        
        self.view.addSubview(bkView)
        bkView.backgroundColor = .white
        bkView.layer.shadowOffset = CGSize(width: 0, height: 1) //陰影在元件下方
        bkView.layer.shadowOpacity = 0.3 //透明度
        bkView.layer.shadowRadius = 3 //數字越低顏色越集中。反之越淡越分散
        bkView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalToSuperview().offset(0)
//            makes.top.equalTo(navigationBarView).offset(0)
            makes.bottom.equalTo(MoveBar2.snp.bottom).offset(0)
            makes.height.equalTo(ButtonHeight)
        }
        
        bkView.addSubview(buttonStack)
        buttonStack.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView)
        }
       
        ScrollView.delegate = self
        ScrollView.bounces = false
        ScrollView.snp.makeConstraints { (makes) in
            makes.left.right.bottom.equalToSuperview()
            makes.top.equalTo(buttonStack.snp.bottom).offset(0)
        }
        
    }
    /**
     scrollView左右滑動觸發事件
     - Date: 08/25 Yang
    */
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setTabBarButton(x: scrollView.contentOffset.x)
    }
    /**
     滑動會切換兩個頁面
     - Date: 08/25 Yang
    */
    func setTabBarButton(x:CGFloat){
        if x == 0{
            Btn_24G.setTitleColor(.black, for: .normal)
            Btn_5G.setTitleColor(.systemGray, for: .normal)
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.15, delay: 0, animations: {
                self.MoveBar2.snp.updateConstraints { (makes) in
                    makes.left.equalToSuperview().offset(0)
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
        }else if x == self.view.frame.size.width{
            Btn_5G.setTitleColor(.black, for: .normal)
            Btn_24G.setTitleColor(.systemGray, for: .normal)
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.15, delay: 0, animations: {
                self.MoveBar2.snp.updateConstraints { (makes) in
                    makes.left.equalToSuperview().offset(fullScreenSize.width/2)
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
}

