//
//  ButtonDelegate.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2019/5/21.
//  Copyright © 2019 蒼月喵. All rights reserved.
//

import Foundation
/**
 - 主要用途為控制 LeftMenuVC & 六個分類主頁 VCs 的 frame（ func clickLeft( ) ）
 - func toMeshVC( ), changeUserMenuView2(...) 是 Yang 寫的
 - 個人觀測
    + toMeshVC 似乎沒有用到
    + changeUserMenuView2 應該可以用 changeUserMenuView 就好(?
 
 - Date:07/15 Mike
 */
protocol ButtonDelegate:class {
    // 07/15 Mike 應該用不到這個 func
//    /**
//     - 控制 LeftMenuVC & 六個分類主頁 VCs 的 frame
//     - 實作在 MenuVC 裡面
//     - 六個分類主頁都有兩種情形會呼叫到
//        + @IBAction func menuClick(_ sender: Any){...}：點擊分類主頁左上方的 menc Icon 的時候會處理
//        + @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer)：點擊分類主頁空白處的時候會處理（通常是 LeftMenuVC 浮現的時候才會有相應動作）
//
//     - Date:07/15 Mike
//     */
//    func clickLeft()
    // 07/15 Mike 應該用不到這個 func
//    func clickRight()
    
    func toMeshVC()
    //yang test
//    func changeUserMenuView2(_ viewNum: Int)

////    func toPackPage()
//    func toPackPage(packData: Pack)
//    // 0729
//    func toPackPageNew(packDataNew: ProductCDetail)
//    func backPage()
//
//    func setPackDetailPage(setSometext: String)
}
