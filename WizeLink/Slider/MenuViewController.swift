//
//  MenuViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/6.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
/**
 控制所有分頁的 MenuVC
 包含控制左邊的 LeftMenuVC，以及 Menu 資料夾裡面的六個 VCs
 主要的 Delegate 為 MenuDelegate
 
 - Date:07/13 Mike
 */
class MenuViewController: UIViewController {
    ///在 Main.storyboard 上連結 LeftMenu View  07/13
    @IBOutlet weak var LeftMenu: UIView!
    ///在 Main.storyboard 上連結 Connect View  07/13
    @IBOutlet var ConnectView: UIView!
    ///在 Main.storyboard 上連結 Wireless View  07/13
    @IBOutlet var WirelessView: UIView!
    ///在 Main.storyboard 上連結 Status View  07/13
    @IBOutlet var StatusView: UIView!
    ///在 Main.storyboard 上連結 Parents View  07/13
    @IBOutlet var ParentsView: UIView!
    ///在 Main.storyboard 上連結 Other View  07/13
    @IBOutlet var OtherView: UIView!
    ///在 Main.storyboard 上連結 Mesh View  07/13
    @IBOutlet var MeshView: UIView!
    
    /// 在 func prepare 裡面會用到，用來連結 Delagate  07/14 Mike
    var LeftMenuViewController: LeftMenuViewController?
    /// 在 func prepare 裡面會用到，用來連結 Delagate  07/14 Mike
    var ConnectViewController: ConnectViewController?
    
    // Mike 07/13 mark 似乎沒有用到
    //    var tempView = UIView()
    
    /**
     用來記錄上線的裝置有哪些
     
     - TODO:這邊也許之後可以移動到 ConnectVC 裡面去做處理
     - Date:07/14 Mike
     */
    var onlineDevice : [Device] = []
    // Mike 07/14 離線應該用不到
    //    var offlineDevice : [Device] = []
    
    /**
     Load 此 VC 時，會做的動作
     1. searchInfo( )
     2. setViews( )
     
     - TODO:searchInfo() 也許之後可以移動到 ConnectVC 裡面去做處理
     - SeeAlso: `setViews()`, `searchInfo()`
     - Date:07/14 Mike
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("=== Start Menu ===")
        //        self.getStatusHtm(){
        //            // load temp data for server error
        //            if serverStatus == nil {
        //                print("serverStatus:\(String(describing: serverStatus))")
        //                //                statusHtmData = tempStatusHtmData
        //                //                clientsHtmData = tempClientsHtmData
        //            }else{
        //                print("statusHtmData:\(statusHtmData)")
        //                print("clientsHtmData:\(clientsHtmData)")
        //            }
        //        }
        setViews()
    }
    /**
     設定此 VC 中，Views 的各種設定
     會在 viewDidLoad( ) 裡面呼叫到
     
     - SeeAlso: `viewDidLoad()`
     - Date:07/14 Mike
     */
    func setViews(){
        LeftMenu.snp.makeConstraints { (makes) in
            makes.width.equalTo(fullScreenSize.width * MenuWidth)
            makes.height.equalTo(fullScreenSize.height)
            makes.top.left.equalToSuperview()
        }
        view.addSubview(WirelessView)
        WirelessView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
    }
    /**
     只要在 Main.storyboard 上，有用 segue 連接到的 VCs
     都可以用在此做連結，通常是指派數值給下層 VCs，或是設定 Delegates
     
     - Date:07/14 Mike
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        print(segue.identifier)
        if let vc1 = segue.destination as? LeftMenuViewController {
            LeftMenuViewController = vc1
            LeftMenuViewController?.MenuDelegate = self
        }
        if segue.identifier == "WirelessSegue" {
            let vc = segue.destination.children[0] as? WirelessViewController
        }
        else if segue.identifier == "ConnectSegue" {
            let vc = segue.destination.children[0] as? ConnectViewController
            vc?.MenuDelegate = self
            ConnectViewController = vc
        }else if segue.identifier == "StatusSegue"{
            let vc = segue.destination.children[0] as? StatusViewController
        }
        else if segue.identifier == "MeshSegue"{
            let vc = segue.destination.children[0] as? MeshViewController
        }else if segue.identifier == "OtherSegue"{
            let vc = segue.destination.children[0] as? OtherViewController
            vc?.LanguageDelegate = self
        }else if segue.identifier == "ParentsSegue"{
            let vc = segue.destination.children[0] as? ParentsViewController
        }
        
    }
    /**
     利用 func searchFunc，還有對應的 htmData & 關鍵字，去撈出所需要的 Datas
     
     - TODO:這邊也許之後可以移動到 ConnectVC 裡面去做處理
     - SeeAlso: `searchFunc(src:str1:str2:)`
     - Date:07/14 Mike
     */
    func searchInfo(){
        let data1 = searchFunc(src: clientsHtmData, str1: "var buffer ='", str2: "';\n    var res = buffer.split")
        
        let deviceArray = data1.components(separatedBy: ">>>")
        
        print("deviceArray.count:\(deviceArray.count)")
        
        onlineDevice = []
        // Mike 07/14
        //        offlineDevice = []
        
        if deviceArray.count != 0{
            for device in deviceArray{
                let deviceDataArray = device.components(separatedBy: " ")
                var tempDevice = Device()
                print(deviceDataArray.count)
                tempDevice.deviceName = deviceDataArray[0]
                
                if deviceDataArray.count > 1{  //登入後當掉
                    print("deviceDataArray11 \(deviceDataArray[1])")
                    tempDevice.ipAddress = deviceDataArray[1]
                    tempDevice.macAddress = deviceDataArray[2]
                    tempDevice.unknownNum = deviceDataArray[3]
                    tempDevice.connectType = deviceDataArray[4]
                    tempDevice.brand = deviceDataArray[6]
                    print("tempDevice \(tempDevice)")
                    
                    if deviceDataArray[5] == "on"{
                        tempDevice.isOnline = true
                        onlineDevice.append(tempDevice)
                        print("MenuVC \(onlineDevice)")
                    }
                    // Mike 07/14
                    //                    else{
                    //                        offlineDevice.append(tempDevice)
                    //                    }
                    
                }
                
            }
            
        }
        ConnectViewController?.connectNum = onlineDevice.count
    }
    
}
/**
 實作 MenuDelegate
 
 - SeeAlso: `MenuDelegate`
 - Date:07/14 Mike
 */
extension  MenuViewController: MenuDelegate {
    
    //    func reloadAfterLoad(){
    //        print("reloadAfterLoad")
    //        let workingView = view.subviews.last
    //        let tmpView = ConnectView
    //        if workingView != tmpView {
    //            self.view.addSubview(tmpView!)
    //            tmpView!.snp.makeConstraints { (makes) in
    //                makes.edges.equalToSuperview()
    //            }
    //            workingView?.removeFromSuperview()
    //        }else{
    //            tmpView!.frame = (workingView?.frame)!
    //            workingView?.removeFromSuperview()
    //            self.view.addSubview(tmpView!)
    //            //            CatalogMenuViewController?.viewDidLoad()
    //            tmpView!.subviews.last?.menu()
    //        }
    //    }
    
    
    /**
     切換 Views 用的，會在 LeftMenuVC ( 側邊 Menu 欄 ) 使用到
     
     - SeeAlso: `LeftMenuViewController.tableView(_:didSelectRowAt:)
     - Date:07/14 Mike``
     */
    func changeUserMenuView(_ viewNum: Int) {
        print("viewNum:\(viewNum)")
        var tmpView: UIView!
        let workingView = view.subviews.last
        
        if viewNum == 0 {
            //            tmpView = ConnectView
            tmpView = WirelessView
            if workingView != tmpView {
                print("enter test")
                workingView?.removeFromSuperview()
                view.addSubview(WirelessView)
                WirelessView.snp.makeConstraints { (makes) in
                    makes.top.bottom.width.equalToSuperview()
                    makes.left.equalToSuperview().offset(fullScreenSize.width * MenuWidth)
                    //                    makes.left.equalToSuperview().offset(fullScreenSize.width * 0)
                }
                //                                changeUserMenuView(0)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                    //                    print("时间2：", Date())
                    self.changeUserMenuView(0)
                }
                return
            }
            
        }
        else if viewNum == 1 {
            tmpView = ConnectView
        }
        else if viewNum == 2 {
            tmpView = MeshView
        }
        else if viewNum == 3 {
            tmpView = StatusView
        }
        else if viewNum == 4 {
            tmpView = ParentsView
        }
        else if viewNum == 5 {
            tmpView = OtherView
        }
        else if viewNum == 6 {
            /**
             For ConnectView to MeshView
             */
            print("viewNum == 6")
            tmpView = MeshView
            
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            return
        }
        else {
            return
        }
        
        if workingView != tmpView {
            print("workingView != tmpView")
            //            print("removeFromSuperview :\(workingView)")
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()
            
            self.view.addSubview(tmpView)
            print("tmpView.frame:\(tmpView.frame)")
            tmpView.subviews.last?.menu()
            print("tmpView.frame After menu():\(tmpView.frame)")
        }else{
            print("workingView == tmpView")
            tmpView.subviews.last?.menu()
        }
        
        //        UIView.animate(withDuration: 0.5) {
        //            self.superview?.frame = frame!
        //        }
        
    }
    
    //    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
    //        let location = sender.location(in: nil)
    //        navigationController?.view.slideByFinger(location: location, state: sender.state)
    //    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
}
/**
 實作 LanguageDelegate
 
 - SeeAlso: `LanguageDelegate`
 - Date:07/14 Mike
 */
extension  MenuViewController: LanguageDelegate{
    /**
     實作 LanguageDelegate.notiftNewLanguage( )
     若是語言有更動，則會接收到通知
     
     - SeeAlso: `LanguageDelegate`
     - Date:07/14 Mike
     */
    func notiftNewLanguage() {
        print("notiftNewLanguage @ MenuViewController")
        modifySubsViews()
    }
    /**
     實作 LanguageDelegate.modifySubsViews( )
     修改其下的子 Views 的語言
     
     - SeeAlso: `LanguageDelegate`
     - Date:07/14 Mike
     */
    func modifySubsViews() {
        print("modifySubsViews @ MenuViewController")
        LeftMenuViewController?.checkLanguage()
    }
}
