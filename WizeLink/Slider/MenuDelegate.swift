//
//  MenuDelegate.swift
//  Andromeda
//
//  Created by 蒼月喵 on 2018/10/3.
//  Copyright © 2018年 蒼月喵. All rights reserved.
//

import Foundation
/**
Delegate for Menu
主要溝通 MenuVC, LeftMenuVC & 六個分類的主頁
（ WirelessVC, ConnectVC, MeshVC, StatusVC, ParentsVC, OtherVC）

- Date: 07/15 Mike
*/
protocol MenuDelegate:class {
//    func reloadAfterLoad(ProductType: ProductType)
    // Mike 07/14
//    func reloadAfterLoad()
//    func changeViedoMenuView(_ viewNum: Int)
    /**
     根據傳入的參數，來決定 MenuVC 上方要放哪個分類的主頁
     
     - Parameter viewNum: 格式為 Int，決定 MenuVC 上方要放哪個分類的主頁
     - Example:
        *switch viewNum*
        + 0 : WirelessView
        + 1 : ConnectView
        + 2 : MeshView
        + 3 : StatusView
        + 4 : ParentsView
        + 5 : OtherView
     - Date: 07/15 Mike
     */
    func changeUserMenuView(_ viewNum: Int)

//    func toCatalogPage(_ int : Int)
//    func toCatalogPage(ProductType: ProductType)
//    func showLeft()
}
