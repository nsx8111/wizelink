//
//  LeftMenuViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/8.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
/**
應該是 For Menu List 用的 struct
分別紀錄圖檔跟標題

- Author: Yang
- Date: 07/15 Mike
*/
struct MenuList{
    var iconImage: UIImageView!
    var titleLabel: String = ""
}
/**
左邊 Menu 側邊欄的 ViewController
*/
class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    weak var MenuDelegate: MenuDelegate?
    var pageLanguage : String = ""
    
    var list = [
        MenuList(iconImage: UIImageView(image: UIImage(named: "Wireless-w")), titleLabel: localString(key: "WIRELESS", table: .pageTitle)),
        MenuList(iconImage: UIImageView(image: UIImage(named: "Connection-w")), titleLabel: localString(key: "CONNECTION", table: .pageTitle)),
        MenuList(iconImage: UIImageView(image: UIImage(named: "mesh")), titleLabel: localString(key: "MESH_MAP", table: .pageTitle)),
        MenuList(iconImage:UIImageView(image: UIImage(named: "status-w")), titleLabel: localString(key: "STATUS", table: .pageTitle)),
        MenuList(iconImage: UIImageView(image: UIImage(named: "Parent Control")), titleLabel: localString(key: "PARENT_CTRL", table: .pageTitle)),
        MenuList(iconImage: UIImageView(image: UIImage(named: "other-w")), titleLabel: localString(key: "OTHERS", table: .pageTitle))
    ]

    var userImageView = UIImageView()
    var mcLabel = UILabel()
    var numLabel = UILabel()
    var MenuTableView = UITableView()
    let bkView = UIView()
    let bkView2 = UIView()
    let bkView3 = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        searchInfo()
        setViews()
        setTable()
    }

    override func viewWillAppear(_ animated: Bool) {
        print("statusBarheight", UIApplication.shared.statusBarFrame.height)
        checkLanguage()
    }

    func checkLanguage(){
        print("checkLanguage @ LeftMenuVC")
        print("pageLanguage:\(pageLanguage)")
        print("bundleLocalizeFileName:\(bundleLocalizeFileName)")
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
            self.MenuTableView.reloadData()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        list[0].titleLabel = localString(key: "WIRELESS", table: .pageTitle)
        list[1].titleLabel = localString(key: "CONNECTION", table: .pageTitle)
        list[2].titleLabel = localString(key: "MESH_MAP", table: .pageTitle)
        list[3].titleLabel = localString(key: "STATUS", table: .pageTitle)
        list[4].titleLabel = localString(key: "PARENT_CTRL", table: .pageTitle)
        list[5].titleLabel = localString(key: "OTHERS", table: .pageTitle)
    }
    /**
     撈WAP名稱跟裝置名稱資料
     - Date: 08/26 Yang
    */
    func searchInfo(){
        let deviceName : String = searchFunc(src: wizardHtmData, str1: "var moduleName=\"", str2: "\";")
        let deviceSN : String = searchFunc(src: wizardHtmData, str1: "var SerialNum = '", str2: "';")
        moduleName = deviceName
        SerialNum = deviceSN
        print("module_Name \(deviceName)")
        print("Serial_Num \(deviceSN)")
    }

    func setViews(){
        ///上面背景色
        bkView.backgroundColor = LightSecondaryColor3
        view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview()
            makes.top.equalToSuperview().offset(0)
        }
        ///針對bkView調色用
        bkView3.backgroundColor = LightSecondaryColor4
        view.addSubview(bkView3)
        bkView3.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView)
        }
        ///下面背景色
        bkView2.backgroundColor = SecondaryColor2
        view.addSubview(bkView2)
        bkView2.snp.makeConstraints { (makes) in
            makes.left.right.bottom.equalToSuperview()
            makes.top.equalTo(bkView.snp.bottom).offset(0)
        }

        userImageView.image = UIImage(named: "user")
        view.addSubview(userImageView)
        userImageView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelWidth * 2 * HeightForScroll)
            makes.left.equalToSuperview().offset(spacing3*HeightForScroll)
            makes.height.width.equalTo(loginLabelHeight*2*HeightForScroll)
        }
        
        mcLabel.textColor = .white
        mcLabel.text = moduleName
        view.addSubview(mcLabel)
        mcLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(userImageView.snp.bottom).offset(spacing1*HeightForScroll)
            makes.left.equalTo(userImageView)
        }
     
        numLabel.textColor = SecondaryColor3
        numLabel.text = SerialNum
        view.addSubview(numLabel)
        numLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(mcLabel.snp.bottom).offset((spacing1/2)*HeightForScroll)
            makes.left.equalTo(userImageView)
            makes.bottom.equalTo(bkView.snp.bottom).offset(-spacing1*HeightForScroll)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            mcLabel.font = UIFont.systemFont(ofSize: 30)
            numLabel.font = UIFont.systemFont(ofSize: 20)
        default:
            mcLabel.font = UIFont.systemFont(ofSize: 20)
            numLabel.font = UIFont.systemFont(ofSize: 14)
        }
        
    }

    func setTable(){
        let listCount = list.count
        MenuTableView.delegate = self
        MenuTableView.dataSource = self
        MenuTableView.allowsSelection = true
        MenuTableView.allowsMultipleSelection = false
        MenuTableView.bounces = false
        MenuTableView.rowHeight = 55 * HeightForScroll
        MenuTableView.separatorColor = LightTextColor2
        MenuTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        view.addSubview(MenuTableView)
        MenuTableView.snp.makeConstraints { (makes) in
            makes.top.equalTo(bkView.snp.bottom).offset(spacing3)
            makes.width.equalTo(fullScreenSize.width * MenuWidth)
            makes.height.equalTo(CGFloat(MenuTableView.rowHeight) * CGFloat(listCount))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Cell")
        cell.imageView?.image = list[indexPath.row].iconImage.image!.maskWithColor(color: .white)
        ///壓縮cell左側icon大小
        let itemSize = CGSize.init(width: 20, height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
        let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();

        cell.textLabel?.text = list[indexPath.row].titleLabel
        cell.textLabel?.textColor = .white
        cell.backgroundColor = LightSecondaryColor3
//        cell.selectedBackgroundView?.backgroundColor = SecondaryColor3

        switch currentDevice.deviceType {
        case .iPhone40:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        case .iPhone47:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        case .iPhone55:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16.5)
        case .iPad:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 22.0)
        default:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14.5)
        }
        ///for connect to mesh
        if indexPath.row == 2{
            selectedIndexPath2 = indexPath
            leftCell = cell
        }
        if indexPath.row == 1{
            selectedIndexPath3 = indexPath
            leftCell2 = cell
        }
        return cell
    }
    /**
     for connect to mesh
     點擊到的cell背景色變橘色
     - Date: 08/26 Yang
    */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        MenuDelegate?.changeUserMenuView(indexPath.row)
        if indexPath.row != 2{
            leftCell.contentView.backgroundColor = LightSecondaryColor4
        }
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = SecondaryColor3
    }
    /**
     未點擊到的其他cell背景色變藍色
     - Date: 08/26 Yang
    */
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = LightSecondaryColor4
    }
    /**
     設定TableViewCell分隔線
     - Date: 08/26 Yang
    */
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

}
