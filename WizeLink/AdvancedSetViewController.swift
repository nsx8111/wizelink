//
//  AdvancedSetViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import WebKit
import SnapKit 
/**
 進階設定webView
 - Date: 08/25 Yang
*/
class AdvancedSetViewController: UIViewController,WKUIDelegate, WKNavigationDelegate {
    let webView = WKWebView()
    let navigationBarView = UIImageView()
    let backButton = UIButton()
    let goBackBtn = UIButton()
    let goForwardBtn = UIButton()
    
//    let homeHtm: String = "http://192.168.10.1/home.htm"
//    let statusHtm: String = "http://192.168.10.1/status.htm"
//    let clientsHtm: String = "http://192.168.10.1/clients.htm"
    
    func configureView() {
//        let username = "admin"
//        let password = "admin"
//        let userPasswordString = "\(username):\(password)"
//        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)
//        let base64EncodedCredential = userPasswordData!.base64EncodedString(options:[])
//        let authString = "Basic \(base64EncodedCredential)"
////        "http://192.168.10.1/title.html"
////        let url = URL(string: "http://192.168.10.1/status_network_map.asp")
        let url = URL(string: "\(machineUrl)home.htm")
//        wizard_mobile
//        let url = URL(string: "http://192.168.10.1/wizard_mobile.htm")
        // var -> let
        let request = URLRequest(url: url!)
//        request.setValue(authString, forHTTPHeaderField: "Authorization")
//        webView.scalesPageToFit = true
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        webView.contentMode = .scaleAspectFit

        view.addSubview(webView)
        webView.snp.makeConstraints { (makes) in
            makes.top.left.right.equalToSuperview()
            makes.bottom.equalTo(self.navigationBarView.snp.top)
        }
//        webView.loadFileURL(URL(string: "http://192.168.10.1/wizard_mobile.htm")!, allowingReadAccessTo: <#T##URL#>)
        webView.load(request)
//        let html = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"></header><body>\(htmlStr!)<body></html>"
//        webView.hasOnlySecureContent = true
//        webView.

//        webView.evaluateJavaScript(<#T##javaScriptString: String##String#>, completionHandler: <#T##((Any?, Error?) -> Void)?##((Any?, Error?) -> Void)?##(Any?, Error?) -> Void#>)
//        webView.loadFileURL(url!, allowingReadAccessTo: URL(string: "http://192.168.10.1/")!)
//        webView.l
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        if challenge.protectionSpace.host == "http://192.168.10.1" {
//            let user = "admin"
//            let password = "admin"
            let credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
            challenge.sender?.use(credential, for: challenge)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
//        }
        
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
//        let myURL = URL(string: statusHtm)
//        let myRequest = URLRequest(url: myURL!)
//        let myRequest2 = URLRequest(url: myURL!, method: .get, headers: .)
//        webView.load(myRequest)
        configureView()
        getData{ self.searchInfo() }
        
//        print("webView.frame:\(webView.frame)")
//        webView.load
        
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "go-back"), style: UIBarButtonItem.Style.plain, target: self, action:  Selector(("onBackButton_Clicked:")))
        
    }
    /**
     判斷是否離線 離線則彈出alert提示
     - Date: 08/25 Yang
    */
    func searchInfo(){
        let IpValue = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
        print("IpValue \(IpValue)")
        if IpValue == "" || IpValue == "0.0.0.0" {
            alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
        }
    }
    
    func setViews(){
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
//        let navigationBarHeight = 44.0
        navigationBarView.backgroundColor = TextColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.bottom.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight/1.5)
            makes.left.equalToSuperview().offset(0)
        }
           
        backButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+3)
        backButton.setTitle("back", for: .normal)
        backButton.setTitleColor(SecondaryColor4, for: .normal)
        backButton.backgroundColor = .clear
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.equalTo(navigationBarHeight/1.6)
            makes.height.equalTo(navigationBarHeight/4)
            makes.left.equalToSuperview().offset(spacing2+2)
        }
        
        goForwardBtn.setImage(UIImage(named: "S__33603593"), for: .normal)
        goForwardBtn.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(goForwardBtn)
        goForwardBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.equalTo(navigationBarHeight/3.2)
            makes.height.equalTo(navigationBarHeight/3.5)
            makes.right.equalToSuperview().offset(-spacing3)
        }
        
        goBackBtn.setImage(UIImage(named: "S__33603591"), for: .normal)
        goBackBtn.addTarget(self,action: #selector(self.onBackButton_Clicked),for: .touchUpInside)
        self.view.addSubview(goBackBtn)
        goBackBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navigationBarView)
            makes.width.height.equalTo(goForwardBtn)
            makes.right.equalTo(goForwardBtn.snp.left).offset(-spacing2*2)
        }
        
//        webView.snp.makeConstraints { (makes) in
//            makes.edges.equalToSuperview()
//        }

    }
    /**
     底下navigationBar箭頭可控制webView的上下頁切換瀏覽
     - Date: 08/25 Yang
    */
    @objc func onBackButton_Clicked(sender: UIButton) {
//        let url = URL(string: homeHtm)
//        var request = URLRequest(url: url!)
//        self.webView.load(request)
        
        user = "admin"
        password = "admin"
        
        if sender == backButton{
            self.webView.reload()
            self.navigationController?.popViewController(animated: true)
        }else if sender == goBackBtn{
            if(webView.canGoBack) {
                webView.goBack()
            }
        }else{
            if(webView.canGoForward) {
                webView.goForward()
            }
            
        }

    }
    
}

//
//class HTTPBasicAuthenticationSessionTaskDelegate : NSObject, URLSessionTaskDelegate {
//  let credential:URLCredential
//  init(user:String,password:String) {
//    self.credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
//  }
//
//  func urlSession(_ session: URLSession,
//                  task: URLSessionTask,
//                  didReceive challenge: URLAuthenticationChallenge,
//                  completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//    completionHandler(.useCredential,credential)
//  }
//}
//
//let delegate = HTTPBasicAuthenticationSessionTaskDelegate(user:"admin",password:"admin")
//let session = URLSession(configuration: URLSessionConfiguration.default, delegate: delegate, delegateQueue: nil)
