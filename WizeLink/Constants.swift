//
//  Constant.swift
//  Created by 洋洋 on 2020/3/25.
//  Copyright © 2020 YangYang. All rights reserved.
//


import UIKit
import Foundation

//// Mike move to getData
//// point
//var user : String = ""
//var password : String = ""

/**
 全域變數及常數預設值
 - Date: 08/25 Yang
*/
/** 取得裝置大小 */
let currentDevice = UIDevice()
let fullScreenSize = UIScreen.main.bounds.size
let statusBarHeight = UIApplication.shared.statusBarFrame.height
let MenuWidth : CGFloat = 0.66
let navBarHeight: CGFloat = 64
var navBarH: CGFloat = 0
var keyboardHeight: CGFloat = 260
var ButtonHeight: CGFloat = 48
var rowHeight: CGFloat = 50
var fontSize: CGFloat = 22
var labelFontSize: CGFloat = 14
var labelFontSize2: CGFloat = 12
var segmentFontSize: CGFloat = 16
let offsetA = fullScreenSize.width*0.02
var bkViewHeight: CGFloat = fullScreenSize.height*0.35
///適應各種不同裝置尺寸使用
var HeightForScroll: CGFloat = fullScreenSize.height/736
var WidthForDevices: CGFloat = fullScreenSize.height/414

///欄位間距
var viewOffset = 20
///間距
var spacing1: CGFloat = 10
var spacing2: CGFloat = 15
var spacing3: CGFloat = 20
var spacing4: CGFloat = 25
///元件佈局設定
var wapWidth: CGFloat = 45
var loginLabelWidth: CGFloat = 50
var loginLabelHeight: CGFloat = 30
var loginLabelHeight2: CGFloat = 33
var loginLabelHeight3: CGFloat = 37
var loginLabelHeight4: CGFloat = 27
var buttonSize: CGFloat = 28
var labelWidth: CGFloat = 80
var labelWidth2: CGFloat = 73
var ccc : Int = 1

///藍
let PrimaryColor1 = UIColor(red: 0/255.0, green: 73/255.0, blue: 143/255.0, alpha: 1.0)
let LightPrimaryColor1 = UIColor(red: 0/255.0, green: 73/255.0, blue: 143/255.0, alpha: 0.6)

///藍綠
let PrimaryColor2 = UIColor(red: 0/255.0, green: 143/255.0, blue: 167/255.0, alpha: 1.0)
let LightPrimaryColor2 = UIColor(red: 0/255.0, green: 143/255.0, blue: 167/255.0, alpha: 0.14)

///深藍
let SecondaryColor1 = UIColor(red: 33/255.0, green: 49/255.0, blue: 80/255.0, alpha: 1.0)

///淺藍
let SecondaryColor2 = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 1.0)
let LightSecondaryColor = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 0.4)
let LightSecondaryColor2 = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 0.18)
let LightSecondaryColor3 = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 0.88)
let LightSecondaryColor4 = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 0.08)
let LightSecondaryColor5 = UIColor(red: 65/255.0, green: 97/255.0, blue: 159/255.0, alpha: 0.82)
let SecondaryColor4 = UIColor(red: 110/255.0, green: 133/255.0, blue: 255/255.0, alpha: 1.0)

///橙
let SecondaryColor3 = UIColor(red: 247/255.0, green: 119/255.0, blue: 84/255.0, alpha: 1.0)

///深灰 //43 39 38
let TextColor1 = UIColor(red: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
//let LightTextColor1 = UIColor(red: 43/255.0, green: 39/255.0, blue: 38/255.0, alpha: 0.7)

///不知道啥顏色 很像白色
let TextColor2 = UIColor(red: 231/255.0, green: 237/255.0, blue: 243/255.0, alpha: 1.0)
let LightTextColor2 = UIColor(red: 231/255.0, green: 237/255.0, blue: 243/255.0, alpha: 0.4)
let LightTextColor3 = UIColor(red: 231/255.0, green: 237/255.0, blue: 243/255.0, alpha: 0.2)

///黑
let TextColor3 = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)

///白
let TextColor4 = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)

///淺灰
let TextFieldColor = UIColor(red: 211.0/255.0, green: 211.0/255.0, blue: 211.0/255.0, alpha: 0.45)


///其他專案內會用到的全域變數
var leftCell = UITableViewCell()
var leftCell2 = UITableViewCell()
var viewNumber: Int = 2
var typesNum: Int = 0
var selectedIndexPath2: IndexPath! = nil
var selectedIndexPath3: IndexPath! = nil

var SSID2 : String = ""
var wep2 : String = ""
var clientNum2 : String = ""
var SSID : String = ""
var wep : String = ""
var clientNum : String = ""
var channelNum2 : String = ""
var channelNum : String = ""
var moduleName: String = ""
var SerialNum: String = ""
var device_name: String = ""
var ip_address: String = ""
var mac_address: String = ""
var station_mac2: String = ""
var station_rssi2: String = ""
var station_uplink2: String = ""
var station_downlink2: String = ""
var station_connected_band2: String = ""
var station_mac: [String] = []
var station_rssi: [String] = []
var station_uplink: [String] = []
var station_downlink: [String] = []
var station_connected_band: [String] = []
var stationInfoDetails : [StationInfoDetail] = []
var types24G : [NetworkConfiguration] = []
var types5G : [NetworkConfiguration] = []

var languageType: String = ""
var networkNameLabel: String = ""
var GNLabel: String = ""
var ipValue: String = ""
var currentWiFiAddress: String = ""
var isShowAlert : Bool = false
var GNisOn : Bool = false
var GNisOn2 : Bool = false


//Mike move to getData
//var statusHtm: String = "http://192.168.10.1/status.htm"  //home
//var clientsHtm: String = "http://192.168.10.1/clients.htm"

