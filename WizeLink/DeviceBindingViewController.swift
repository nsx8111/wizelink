//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit

class DeviceBindingViewController: UIViewController,UITextFieldDelegate {
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView2 = UIImageView()
    let backButton = UIButton()
    let passwordLabel = UILabel()
    let passwordTextField = UITextField()
    let passwordLabel2 = UILabel()
    let titleLabel = UILabel()
    let emailLabel = UILabel()
    let emailTextField = UITextField()
    let bindingButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = .white
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        navBarLabel.backgroundColor = .clear
        navBarLabel.attributedText = NSAttributedString(string: "設備綁定", attributes:
            nil)
        navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight/1.5)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        titleLabel.backgroundColor = .clear
        titleLabel.attributedText = NSAttributedString(string: "請輸入您要綁定到此設備的雲端帳號資訊", attributes:
            nil)
        titleLabel.font = UIFont.boldSystemFont(ofSize: labelFontSize+2)
        titleLabel.textColor = PrimaryColor2
        titleLabel.textAlignment = .left
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(navigationBarHeight*1.7)
            makes.width.equalTo(fullScreenSize.width*0.9)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing*2)
        }
        
        emailLabel.backgroundColor = .clear
        emailLabel.attributedText = NSAttributedString(string: "電子郵件地址", attributes:
            nil)
        emailLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        emailLabel.textColor = PrimaryColor2
        emailLabel.textAlignment = .left
        self.view.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(spacing*2.5)
            makes.width.equalTo(labelWidth*4)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing*2)
        }
        
        emailTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        emailTextField.textAlignment = .left
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .white
        emailTextField.backgroundColor = TextColor2
        emailTextField.layer.cornerRadius = 1.0
        emailTextField.keyboardType = .namePhonePad
        emailTextField.delegate = self
        self.view.addSubview(emailTextField)
        
        emailTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(emailLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*4)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.attributedText = NSAttributedString(string: "登入密碼", attributes:
            nil)
        passwordLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(spacing2*2)
            makes.width.equalTo(labelWidth*4)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing*2)
        }
        
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = TextColor2
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*4)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        bindingButton.backgroundColor = SecondaryColor3
        bindingButton.layer.cornerRadius = 23
        bindingButton.setTitle("綁定", for: [])
        bindingButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        bindingButton.titleLabel?.textColor = .white
        bindingButton.isEnabled = true
        bindingButton.addTarget(self,action: #selector(self.clickButton),for: .touchUpInside)
        self.view.addSubview(bindingButton)
        
        bindingButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(fullScreenSize.height*0.07)
            makes.width.equalTo(fullScreenSize.width*0.45)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @objc func clickButton(){
        let controller = UIAlertController(title: "無效", message: "電子郵件地址格式不正確", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "確認", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
