//
//  AddAccountViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/31.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit

class AddAccountViewController: UIViewController,UITextFieldDelegate {
    
    let dismissButton = UIButton()
    let bkgImageView2 = UIImageView()
    let addAccountLabel = UILabel()
    let accountLabel = UILabel()
    let accountTextField = UITextField()
    let passwordLabel = UILabel()
    let passwordTextField = UITextField()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = UITextField()
    let saveButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = PrimaryColor1
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        dismissButton.setTitle("關閉", for: .normal)
        dismissButton.backgroundColor = UIColor.clear
        dismissButton.setTitleColor(SecondaryColor3, for: .normal)
        dismissButton.titleLabel?.font = UIFont(name: "Bold", size: fontSize)
        dismissButton.isEnabled = true
        dismissButton.addTarget(self,action: #selector(self.dismissVC),for: .touchUpInside)
        self.view.addSubview(dismissButton)
        
        dismissButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight-15)
            makes.width.height.equalTo(loginLabelHeight*2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        addAccountLabel.backgroundColor = .clear
        addAccountLabel.attributedText = NSAttributedString(string: "新增帳號", attributes:
            nil)
        addAccountLabel.font = UIFont(name: "Helvetica-Light", size: fontSize+2)
        addAccountLabel.textColor = .white
        addAccountLabel.textAlignment = .left
        self.view.addSubview(addAccountLabel)
        addAccountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(dismissButton.snp.bottom).offset(spacing)
            makes.width.equalTo(loginLabelWidth*2)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        accountLabel.backgroundColor = .clear
        accountLabel.attributedText = NSAttributedString(string: "登入名稱", attributes:
            nil)
        accountLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        accountLabel.textColor = PrimaryColor2
        accountLabel.textAlignment = .left
        self.view.addSubview(accountLabel)
        
        accountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(addAccountLabel.snp.bottom).offset(loginLabelHeight)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        accountTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        accountTextField.textAlignment = .left
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .white
        accountTextField.backgroundColor = LightSecondaryColor
        accountTextField.layer.cornerRadius = 1.0
        accountTextField.keyboardType = .namePhonePad
        accountTextField.delegate = self
        self.view.addSubview(accountTextField)
        
        accountTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.attributedText = NSAttributedString(string: "登入密碼", attributes:
            nil)
        passwordLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = LightSecondaryColor
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
        passwordLabel2.attributedText = NSAttributedString(string: "再次輸入密碼", attributes:
            nil)
        passwordLabel2.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        passwordLabel2.textColor = PrimaryColor2
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth*2)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .white
        passwordTextField2.backgroundColor = LightSecondaryColor
        passwordTextField2.layer.cornerRadius = 1.0
        passwordTextField2.keyboardType = .namePhonePad
        
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        saveButton.backgroundColor = SecondaryColor3
        saveButton.layer.cornerRadius = 23
        saveButton.setTitle("保存", for: [])
        saveButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: fontSize)
        saveButton.titleLabel?.textColor = .white
        saveButton.isEnabled = true
        saveButton.addTarget(self,action: #selector(self.clickSaveButton),for: .touchUpInside)
        self.view.addSubview(saveButton)
        
        saveButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.65)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
        hideKeyboardWhenTappedAround()
        
    }
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        accountTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
    }
    
    @objc func clickSaveButton(){
        let controller = UIAlertController(title: "無效", message: "登入名稱不可為空白", preferredStyle: .alert)
             let cancelAction = UIAlertAction(title: "確認", style: .cancel, handler: nil)
             controller.addAction(cancelAction)
             present(controller, animated: true, completion: nil)
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
