//
//  CoRoterViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
/**
 連線狀態的路由器頁
 - Date: 08/25 Yang
*/
class CoRouterViewController: UIViewController {
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    
    let titleLabel = UILabel()
    let macIpLabel = UILabel()
    let ipLabel = UILabel()
    let maskLabel = UILabel()
    let macIpValueLabel = UILabel()
    let ipValueLabel = UILabel()
    let maskValueLabel = UILabel()
    let addRouterLabel = UILabel()
    let addRouterView = UIImageView()
    let addSubRouterBtn = UIButton()
    let lineView1 = UIView()
    let lineView2 = UIView()
    let lineView3 = UIView()
    let lineView4 = UIView()
    
    var pageLanguage : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        searchInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        titleLabel.text = localString(key: "ROUTER_INFO", table: .headerTitle)
        macIpLabel.text = localString(key: "MAC", table: .dataTitle)
        ipLabel.text = localString(key: "IP", table: .dataTitle)
        maskLabel.text = localString(key: "MASK", table: .dataTitle)
        addRouterLabel.text = localString(key: "ADD_SUB_ROUTER", table: .srAndLoginInfo)
    }
    
    func setViews(){
        view.backgroundColor = .white
        
        let bkViewForTitle = UIView()
        bkViewForTitle.backgroundColor = TextColor2
        view.addSubview(bkViewForTitle)
        bkViewForTitle.snp.makeConstraints { (makes) in
            makes.top.right.left.width.equalToSuperview()
            makes.height.equalTo(40)
        }
        
        titleLabel.text = "路由器資訊"
        titleLabel.textColor = .black
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            //            makes.top.equalToSuperview().offset(10)
            makes.centerY.equalTo(bkViewForTitle)
            makes.left.equalToSuperview().offset(20)
        }
        
        macIpLabel.textColor = .darkGray
        macIpLabel.font = UIFont.boldSystemFont(ofSize: 16)
        macIpLabel.text = "MAC位址"
        view.addSubview(macIpLabel)
        macIpLabel.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(titleLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 1)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        macIpValueLabel.textColor = PrimaryColor2
        macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 16)
        macIpValueLabel.text = ""
        view.addSubview(macIpValueLabel)
        macIpValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(macIpLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
        lineView1.alpha = 0.3
        lineView1.backgroundColor = .lightGray
        view.addSubview(lineView1)
        lineView1.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(macIpLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        ipLabel.textColor = .darkGray
        ipLabel.font = UIFont.boldSystemFont(ofSize: 16)
        ipLabel.text = "IP位址"
        view.addSubview(ipLabel)
        ipLabel.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(macIpLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 2)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        ipValueLabel.textColor = PrimaryColor2
        ipValueLabel.font = UIFont.boldSystemFont(ofSize: 16)
        ipValueLabel.text = ""
        view.addSubview(ipValueLabel)
        ipValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(ipLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
        lineView2.alpha = 0.3
        lineView2.backgroundColor = .lightGray
        view.addSubview(lineView2)
        lineView2.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(ipLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        maskLabel.textColor = .darkGray
        maskLabel.font = UIFont.boldSystemFont(ofSize: 16)
        maskLabel.text = "子網路遮罩"
        view.addSubview(maskLabel)
        maskLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(ipLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 3)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        maskValueLabel.textColor = PrimaryColor2
        maskValueLabel.font = UIFont.boldSystemFont(ofSize: 16)
        maskValueLabel.text = ""
        view.addSubview(maskValueLabel)
        maskValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(maskLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
        lineView3.alpha = 0.3
        lineView3.backgroundColor = .lightGray
        view.addSubview(lineView3)
        lineView3.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(maskLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        addRouterView.image = UIImage(named: "60740")
        addRouterView.image = addRouterView.image?.maskWithColor(color: SecondaryColor3)
        view.addSubview(addRouterView)
        addRouterView.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 4)
            makes.left.equalTo(titleLabel).offset(5)
            makes.width.height.equalTo(spacing3)
        }
        
        addRouterLabel.textColor = .black
        addRouterLabel.font = UIFont.boldSystemFont(ofSize: 16)
        addRouterLabel.text = localString(key: "ADD_SUB_ROUTER", table: .srAndLoginInfo)
        view.addSubview(addRouterLabel)
        addRouterLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 4)
            makes.left.equalTo(addRouterView.snp.right).offset(loginLabelHeight)
        }
        
        lineView4.alpha = 0.3
        lineView4.backgroundColor = .lightGray
        view.addSubview(lineView4)
        lineView4.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(addRouterLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        addSubRouterBtn.backgroundColor = .clear
        addSubRouterBtn.setTitle("", for: [])
        addSubRouterBtn.addTarget(self,action: #selector(self.clickAddSubRouter),for: .touchUpInside)
        view.addSubview(addSubRouterBtn)
        addSubRouterBtn.snp.makeConstraints { (makes) in
            makes.right.left.equalToSuperview()
            makes.top.equalTo(lineView3)
            makes.bottom.equalTo(lineView4)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        default:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        }
    }
    /**
    - 進入子路由器頁面
    - Date: 08/25 Yang
     */
    @objc func clickAddSubRouter(){
        let vc = SubRouterViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    /**
    - ConnectViewController按下RefreshButton後 若離線狀態則會呼叫此方法
    - Date: 08/25 Yang
     */
    func connectFail(){
        self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
    }
    
    func searchInfo(){
//        self.macIpValueLabel.text = rangeFunc(src: statusHtmData, str1: "bssid_drv[0] ='", str2: "';\n        clientnum[0]").uppercased()
//
//        self.ipValueLabel.text = rangeFunc(src: statusHtmData, str1: "var lan_ip='", str2: "';\nvar lan_mask='")
//
//        self.maskValueLabel.text = rangeFunc(src: statusHtmData, str1: "var lan_mask='", str2: "';\nvar lan_gateway='")
        
        self.macIpValueLabel.text = searchFunc(src: statusHtmData, str1: "bssid_drv[0] ='", str2: "';").uppercased()
        self.ipValueLabel.text = searchFunc(src: statusHtmData, str1: "var lan_ip='", str2: "';")
        self.maskValueLabel.text = searchFunc(src: statusHtmData, str1: "var lan_mask='", str2: "';")
    }
    
}
