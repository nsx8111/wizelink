//
//  CoInternetViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
import SnapKit
/**
 連線狀態的網際網路頁
 - Date: 08/25 Yang
*/
class CoInternetViewController: UIViewController {
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    var ConnectViewController: ConnectViewController?

    let titleLabel = UILabel()
    let methodLabel = UILabel()
    let ipLabel = UILabel()
    let maskLabel = UILabel()
    let gatewayLabel = UILabel()
    let macIpLabel = UILabel()
    
    var methodValueLabel = UILabel()
    let ipValueLabel = UILabel()
    let maskValueLabel = UILabel()
    let gatewayValueLabel = UILabel()
    let macIpValueLabel = UILabel()
    
    let lineView1 = UIView()
    let lineView2 = UIView()
    let lineView3 = UIView()
    let lineView4 = UIView()
    let lineView5 = UIView()
    let bkViewForTitle = UIView()
    let bkView = UIView()
    let scrollView = UIScrollView()
    let contentView = UIView()
    let actView : UIView = UIView()

    //    var viewOffset = 20
    var pageLanguage : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        searchInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        searchInfo()
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    /**
    - 多國語系變換Label文字
    - Date: 08/25 Yang
     */
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        titleLabel.text = localString(key: "CONFIG_WAN", table: .pageTitle)
        methodLabel.text = localString(key: "AIPP", table: .dataTitle)
        ipLabel.text = localString(key: "IP", table: .dataTitle)
        maskLabel.text = localString(key: "MASK", table: .dataTitle)
        gatewayLabel.text = localString(key: "GATEWAY", table: .dataTitle)
        macIpLabel.text = localString(key: "MAC", table: .dataTitle)
    }
    
    func setViews(){
        self.view.addSubview(bkView)
        bkView.backgroundColor = .white
        bkView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        
        bkView.addSubview(scrollView)
        scrollView.snp.makeConstraints { (makes) in
            makes.edges.equalTo(bkView)
        }

        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(scrollView)
            makes.left.right.equalTo(bkView)
        }
        
        bkViewForTitle.backgroundColor = TextColor2
        contentView.addSubview(bkViewForTitle)
        bkViewForTitle.snp.makeConstraints { (makes) in
            makes.top.right.left.width.equalTo(contentView)
            makes.height.equalTo(40)
        }
        
        titleLabel.text = "外部網路配置"
        titleLabel.textColor = .black
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(bkViewForTitle)
            makes.left.equalToSuperview().offset(20)
        }

        methodLabel.textColor = .darkGray
        methodLabel.text = "取得IP方式"
        contentView.addSubview(methodLabel)
        methodLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(titleLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 1)
            makes.left.equalTo(titleLabel).offset(0)
        }
        
        methodValueLabel.textColor = PrimaryColor2
        methodValueLabel.text = ""
        contentView.addSubview(methodValueLabel)
        methodValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(methodLabel)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        lineView1.alpha = 0.3
        lineView1.backgroundColor = .lightGray
        contentView.addSubview(lineView1)
        lineView1.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(methodLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        ipLabel.textColor = .darkGray
        ipLabel.text = "IP位址"
        contentView.addSubview(ipLabel)
        ipLabel.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(methodLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 2)
            makes.left.equalTo(titleLabel).offset(0)
        }
        
        ipValueLabel.textColor = PrimaryColor2
        ipValueLabel.text = ""
        contentView.addSubview(ipValueLabel)
        ipValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(ipLabel)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        lineView2.alpha = 0.3
        lineView2.backgroundColor = .lightGray
        contentView.addSubview(lineView2)
        lineView2.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(ipLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        maskLabel.textColor = .darkGray
        maskLabel.text = "子網路遮罩"
        contentView.addSubview(maskLabel)
        maskLabel.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(ipLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 3)
            makes.left.equalTo(titleLabel).offset(0)
        }
        
        maskValueLabel.textColor = PrimaryColor2
        maskValueLabel.text = ""
        contentView.addSubview(maskValueLabel)
        maskValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(maskLabel)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        lineView3.alpha = 0.3
        lineView3.backgroundColor = .lightGray
        contentView.addSubview(lineView3)
        lineView3.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(maskLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        gatewayLabel.textColor = .darkGray
        gatewayLabel.text = "預設閘道"
        gatewayLabel.numberOfLines = 0
        contentView.addSubview(gatewayLabel)
        gatewayLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(maskLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 4)
            makes.left.equalTo(titleLabel).offset(0)
        }
        
        gatewayValueLabel.textColor = PrimaryColor2
        gatewayValueLabel.text = ""
        contentView.addSubview(gatewayValueLabel)
        gatewayValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(gatewayLabel)
//            makes.left.equalTo(gatewayLabel.snp.right).offset(-0)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        lineView4.alpha = 0.3
        lineView4.backgroundColor = .lightGray
        contentView.addSubview(lineView4)
        lineView4.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(gatewayLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
        }
        
        macIpLabel.textColor = .darkGray
        macIpLabel.text = "MAC位址"
        contentView.addSubview(macIpLabel)
        macIpLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(gatewayLabel.snp.bottom).offset(viewOffset)
            makes.centerY.equalTo(titleLabel).offset(rowHeight * 5)
            makes.left.equalTo(titleLabel).offset(0)
        }
        
        macIpValueLabel.textColor = PrimaryColor2
        macIpValueLabel.text = "2333"
        contentView.addSubview(macIpValueLabel)
        macIpValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(macIpLabel)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        lineView5.alpha = 0.3
        lineView5.backgroundColor = .lightGray
        contentView.addSubview(lineView5)
        lineView5.snp.makeConstraints{ (makes) in
            makes.right.left.equalToSuperview()
            makes.centerY.equalTo(macIpLabel).offset(rowHeight/2)
            makes.height.equalTo(1.0)
            makes.bottom.equalToSuperview().offset(-spacing1)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
            methodLabel.font = UIFont.boldSystemFont(ofSize: 18)
            methodValueLabel.font = UIFont.boldSystemFont(ofSize: 18)
            ipLabel.font = UIFont.boldSystemFont(ofSize: 18)
            ipValueLabel.font = UIFont.boldSystemFont(ofSize: 18)
            maskLabel.font = UIFont.boldSystemFont(ofSize: 18)
            maskValueLabel.font = UIFont.boldSystemFont(ofSize: 18)
            gatewayLabel.font = UIFont.boldSystemFont(ofSize: 18)
            gatewayValueLabel.font = UIFont.boldSystemFont(ofSize: 18)
            macIpLabel.font = UIFont.boldSystemFont(ofSize: 18)
            macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 18)
        case .iPhone40:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            methodLabel.font = UIFont.boldSystemFont(ofSize: 11)
            methodValueLabel.font = UIFont.boldSystemFont(ofSize: 11)
            ipLabel.font = UIFont.boldSystemFont(ofSize: 11)
            ipValueLabel.font = UIFont.boldSystemFont(ofSize: 11)
            maskLabel.font = UIFont.boldSystemFont(ofSize: 11)
            maskValueLabel.font = UIFont.boldSystemFont(ofSize: 11)
            gatewayLabel.font = UIFont.boldSystemFont(ofSize: 11)
            gatewayValueLabel.font = UIFont.boldSystemFont(ofSize: 11)
            macIpLabel.font = UIFont.boldSystemFont(ofSize: 11)
            macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 11)
        case .iPhone47:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            methodLabel.font = UIFont.boldSystemFont(ofSize: 14)
            methodValueLabel.font = UIFont.boldSystemFont(ofSize: 14)
            ipLabel.font = UIFont.boldSystemFont(ofSize: 14)
            ipValueLabel.font = UIFont.boldSystemFont(ofSize: 14)
            maskLabel.font = UIFont.boldSystemFont(ofSize: 14)
            maskValueLabel.font = UIFont.boldSystemFont(ofSize: 14)
            gatewayLabel.font = UIFont.boldSystemFont(ofSize: 14)
            gatewayValueLabel.font = UIFont.boldSystemFont(ofSize: 14)
            macIpLabel.font = UIFont.boldSystemFont(ofSize: 14)
            macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 14)
        case .iPhone55:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            methodLabel.font = UIFont.boldSystemFont(ofSize: 15)
            methodValueLabel.font = UIFont.boldSystemFont(ofSize: 15)
            ipLabel.font = UIFont.boldSystemFont(ofSize: 15)
            ipValueLabel.font = UIFont.boldSystemFont(ofSize: 15)
            maskLabel.font = UIFont.boldSystemFont(ofSize: 15)
            maskValueLabel.font = UIFont.boldSystemFont(ofSize: 15)
            gatewayLabel.font = UIFont.boldSystemFont(ofSize: 15)
            gatewayValueLabel.font = UIFont.boldSystemFont(ofSize: 15)
            macIpLabel.font = UIFont.boldSystemFont(ofSize: 15)
            macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 15)
        default:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            methodLabel.font = UIFont.boldSystemFont(ofSize: 13)
            methodValueLabel.font = UIFont.boldSystemFont(ofSize: 13)
            ipLabel.font = UIFont.boldSystemFont(ofSize: 13)
            ipValueLabel.font = UIFont.boldSystemFont(ofSize: 13)
            maskLabel.font = UIFont.boldSystemFont(ofSize: 13)
            maskValueLabel.font = UIFont.boldSystemFont(ofSize: 13)
            gatewayLabel.font = UIFont.boldSystemFont(ofSize: 13)
            gatewayValueLabel.font = UIFont.boldSystemFont(ofSize: 13)
            macIpLabel.font = UIFont.boldSystemFont(ofSize: 13)
            macIpValueLabel.font = UIFont.boldSystemFont(ofSize: 13)
        }
        
    }
    
    //    func setValues(){
    //        methodValueLabel.text = "122"
    //    }
    
    /**
    - 撈API資料
    - Date: 08/25 Yang
     */
    func searchInfo(){
        
//        self.methodValueLabel.text = rangeFunc(src: statusHtmData, str1: "var wan_current='", str2: "';\nvar wan_subMask='").uppercased()
//        self.ipValueLabel.text = rangeFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';\nvar wan_gateway='")
//        self.maskValueLabel.text = rangeFunc(src: statusHtmData, str1: "var wan_subMask='", str2: "';\nvar wan_ip='")
//        self.gatewayValueLabel.text = rangeFunc(src: statusHtmData, str1: "var wan_gateway='", str2: "';\nvar wan_mac='")
//        self.macIpValueLabel.text = rangeFunc(src: statusHtmData, str1: "var wan_mac='", str2: "';\n\nfunction showWan()").uppercased()
        
        //DHCP 這邊比較需要注意
        ///判斷是否離線狀態
        self.ipValueLabel.text = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
        print("ipValueLabel \(String(describing: self.ipValueLabel.text))")
        if let ip_Value = self.ipValueLabel.text{
            ipValue = ip_Value
        }
        
        if searchFunc(src: statusHtmData, str1: "var wan_current='", str2: "';").components(separatedBy: "_").count < 2 {
            self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            return
        }
        self.methodValueLabel.text = searchFunc(src: statusHtmData, str1: "var wan_current='", str2: "';").components(separatedBy: "_")[1].uppercased()
        self.maskValueLabel.text = searchFunc(src: statusHtmData, str1: "var wan_subMask='", str2: "';")
        self.gatewayValueLabel.text = searchFunc(src: statusHtmData, str1: "var wan_gateway='", str2: "';")
        self.macIpValueLabel.text = searchFunc(src: statusHtmData, str1: "var wan_mac='", str2: "';").uppercased()
    }
    
}
