////
////  ConnectViewController.swift
////  WizeLink
////
////  Created by 蒼月喵 on 2020/4/6.
////  Copyright © 2020 YangYang. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SnapKit
//import KMPageMenu
//
//class ConnectViewController: UIViewController {
//    
//    var connectNum : Int = 0
//    
//    let titleLabel = UILabel()
//    let connectNumLabel = UILabel()
//    
//    let titles : [String] = ["網際網路", "路由器", "裝置"]
//    //    let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: self.titles)
//    /** 分頁標籤 */
//    lazy var menu: KMPageMenu = {
//        let m = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: titles)
//        //        m.style.titleFont = UIFont.systemFont(ofSize: 14)
//        m.isScrollEnable = false
//        m.backgroundColor = .white
//        view.addSubview(m)
//        return m
//    }()
//    
//    /** 連結控制 CoInternetViewController */
//    let vc1: CoInternetViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoInternetViewController.self)) as! CoInternetViewController
//    /** 連結控制 CoRoterViewController */
//    let vc2: CoRoterViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoRoterViewController.self)) as! CoRoterViewController
//    /** 連結控制 CoDevicesViewController */
//    //    let vc3: CoDevicesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoDevicesViewController.self)) as! CoDevicesViewController
//    let vc3: CoDevicesTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CoDevicesTableViewController.self)) as! CoDevicesTableViewController
//    
//    let vc4: CDDetailTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: CDDetailTableViewController.self)) as! CDDetailTableViewController
//    
//    
//    /** 設定分頁 */
//    lazy var page: KMPagingViewController = {
//        var viewControllers: [UIViewController] = []
//        
//        vc1.pageViewDelegate = self
//        vc2.pageViewDelegate = self
//        vc3.pageViewDelegate = self
//        vc4.pageViewDelegate = self
//        
//        viewControllers.append(vc1)
//        viewControllers.append(vc2)
//        viewControllers.append(vc3)
//        viewControllers.append(vc4)
//        
//        let p = KMPagingViewController(viewControllers: viewControllers)
//        
//        //        p.view.snp.makeConstraints { (makes) in
//        //            makes.left.right.bottom.equalToSuperview()
//        //            makes.top.equalTo(self.menu.snp.bottom)
//        //        }
//        
//        //        p.view.frame = CGRect(x: 0,
//        //                              y: menu.frame.maxY,
//        //                              width: width,
//        //                              height: view.frame.height - menu.frame.maxY)
//        p.view.frame = CGRect(x: 0,
//                              y: 134,
//                              width: UIScreen.main.bounds.size.width,
//                              height: UIScreen.main.bounds.size.height - 134)
//        self.addChild(p)
//        p.didMove(toParent: self)
//        p.isScrollEnable = false
//        self.view.addSubview(p.view)
//        
//        return p
//    }()
//    
//    weak var ButtonDelegate: ButtonDelegate?
//    @IBOutlet var MenuButton : UIButton!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//           
//        MenuButton.snp.makeConstraints { (makes) in
//            makes.height.width.equalTo(44)
//        }
//        //        print("MenuButton.frame:\(MenuButton.frame)")
//        //        view.backgroundColor = .yellow
//        //        print("self.view.frame:\(self.view.frame)")
//        
//        
//        //        setViews()
//        
//        
//        view.backgroundColor = .blue
//        
////        self.getStatusHtm(){
////
////            // load temp data for server error
////            if serverStatus == false {
////                print("serverStatus:\(serverStatus)")
////                statusHtmData = tempStatusHtmData
////                clientsHtmData = tempClientsHtmData
////            }
//            
//            self.setViews()
//            self.setConnectSubviews()
////        }
//        
//        
//    }
//    
//    //    func setViews(){
//    //        let bkView = UIView()
//    //        bkView.backgroundColor = .green
//    //        self.view.addSubview(bkView)
//    //        bkView.snp.makeConstraints { (makes) in
//    //            makes.edges.equalToSuperview()
//    //        }
//    //    }
//    
//    func setViews(){
//        
//        titleLabel.text = "連線狀態"
//        titleLabel.textColor = .white
//        view.addSubview(titleLabel)
//        titleLabel.snp.makeConstraints { (makes) in
//            makes.centerX.equalToSuperview()
//            makes.top.equalToSuperview().offset(40)
//            makes.height.equalTo(20)
//        }
//        
//        
//        connectNumLabel.text = "連線裝置數：\(connectNum)"
//        connectNumLabel.textColor = .white
//        view.addSubview(connectNumLabel)
//        connectNumLabel.snp.makeConstraints { (makes) in
//            makes.centerX.equalToSuperview()
//            makes.top.equalTo(titleLabel.snp.bottom).offset(10)
//            makes.height.equalTo(20)
//        }
//        
//    }
//    
//    func setConnectSubviews(){
//        //        let titles : [String] = ["網際網路", "路由器", "裝置"]
//        //        let menu = KMPageMenu(frame: CGRect(x: 0, y: 90, width: UIScreen.main.bounds.size.width, height: 44), titles: titles)
//        //        let menu = KMPageMenu(titles: titles)
//        //        m.style.titleFont = UIFont.systemFont(ofSize: 14)
//        menu.applyLabelWidthFixed()
//        menu.applyLinePositionBottom()
//        menu.applyIndicatorLineFixed()
//        menu.addTarget(self, action: #selector(menuValueChange(sender:)), for: .valueChanged)
//        menu.valueChange = { [weak self] index in
//            self?.page.pagingToViewController(at: index)
//        }
//        
//        //        view.addSubview(menu)
//        
//        page.delegate = self
//        page.didFinishPagingCallBack = { [weak self] (currentViewController, currentIndex)in
//            self?.menu.setSelectIndex(index: currentIndex, animated: true)
//        }
//        
//        //        menu.snp.makeConstraints { (makes) in
//        //            makes.left.right.equalToSuperview()
//        //            makes.top.equalTo(connectNumLabel.snp.bottom).offset(10)
//        //            makes.height.equalTo(60)
//        //        }
//    }
//    
//    func getStatusHtm(completion : @escaping ()->()){
//        let user = "admin"
//        let password = "admin"
//        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
//        
//        AF.request("http://192.168.10.1/status.htm",headers: headers)
//            .responseJSON { response in
//                let res = String(response.debugDescription)
//                statusHtmData = res
//                
//        }
//        
//        AF.request("http://192.168.10.1/clients.htm",headers: headers)
//            .responseJSON { response in
//                let res = String(response.debugDescription)
//                clientsHtmData = res
//                if response.response == nil{
//                    serverStatus = false
//                }else{
//                    serverStatus = true
//                }
//                completion()
//        }
//    }
//    
//    func login(){
//        let user = "admin"
//        let password = "admin"
//        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
//        
//        AF.request("http://192.168.10.1/status.htm",headers: headers)
//            .responseJSON { response in
//                debugPrint(response)
//                let res = String(response.debugDescription)
//                //                    self.lll.text = res
//                let range:Range = res.range(of: "var lan_ip='")!
//                let range2:Range = res.range(of: "';\nvar lan_mask='")!
//                let lanipStr = res[range.upperBound ..< range2.lowerBound ]
//                print("lanipStr:\(lanipStr)")
//                
//                //                    if self.lanIpLabel.text == "Lan IP : "{
//                //                        self.lanIpLabel.text! += String(lanipStr)
//                //                    }
//                
//        }
//        
//    }
//    
//    /** 切換頁面時的動作 */
//    @objc func menuValueChange(sender: KMPageMenu) {
//        print("a)selectIndex == \(sender.selectIndex)")
//    }
//    
//    func callClick(){
//        navigationController?.view.menu()
//    }
//    
//    @IBAction func menuClick(_ sender: Any) {
//        navigationController?.view.menu()
//        ButtonDelegate?.clickLeft()
//    }
//    
//    @IBAction func handleTapGesture(_ sender: Any) {
//        if navigationController?.view.superview?.frame.origin.x != 0 {
//            navigationController?.view.menu()
//        }
//    }
//    
//    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
//        let location = sender.location(in: nil)
//        navigationController?.view.slideByFinger(location: location, state: sender.state)
//        ButtonDelegate?.clickLeft()
//    }
//    
//}
//
//
//// MARK: - 指示器类型
//extension KMPageMenu {
//    /// 标题固定宽度
//    func applyLabelWidthFixed() {
//        var aStyle = self.style
//        aStyle.labelWidthType = .fixed(width: UIScreen.main.bounds.size.width/3)
//        self.style = aStyle
//    }
//    
//    // 横线指示器固定宽度
//    func applyIndicatorLineFixed() {
//        var aStyle = self.style
//        aStyle.indicatorColor = .red
//        aStyle.indicatorPendingHorizontal = 8
//        aStyle.indicatorStyle = .line(widthType: .fixed(width: UIScreen.main.bounds.size.width/3), position: .bottom((margin: 1, height: 2)))
//        self.style = aStyle
//    }
//    
//    // 横线指示器在下
//    func applyLinePositionBottom() {
//        var aStyle = self.style
//        let widthType = aStyle.indicatorStyle.widthType
//        aStyle.indicatorColor = .orange
//        aStyle.indicatorPendingHorizontal = 8
//        aStyle.indicatorStyle = .line(widthType: widthType, position: .bottom((margin: 1, height: 2)))
//        self.style = aStyle
//    }
//}
//
//extension ConnectViewController: KMPagingViewControllerDelegate {
//    func pagingController(_ pagingController: KMPagingViewController, didFinish currentViewController: UIViewController, currentIndex: Int) {
//        print("b)selectIndex == \(currentIndex)")
//        //        menu.setSelectIndex(index: currentIndex, animated: true)
//    }
//}
//
//extension ConnectViewController: PageViewDelegate{
//    /**
//     讓 LiveTvView 可以改變 RecordListView 上面的 presentChannelID
//     - Parameter id: 傳入的ID，去讓 RecordListView 上面的 presentChannelID 變成跟 LiveTvView 一樣
//     */
//    func changeRecordListChannelId(id: Int) {
//        //        self.vc2.presentChannelID = id
//    }
//    /** For test */
//    func sendSomeThing(_ url: String) {
//        print("tap epg cell, url:\(url)")
//        //for test, can change page by code
//        //        self.page.pagingToViewController(at: 1)
//    }
//    
//    /**
//     讓分頁可以控制 MainPage 上面的 actView 顯示與否（用在讀取網路資料的等待時間上）
//     - Parameter bool: 若是為 true 則隱藏，反之顯示是等待(讀取)中
//     */
//    func callActView(bool: Bool){
//        DispatchQueue.main.async {
//            //            self.actView.isHidden = bool
//        }
//    }
//    //0415 test
//    func testToVC4(){
//        self.page.pagingToViewController(at: 3)
//    }
//    
//    func toDetailPage(device: Device) {
//        vc4.deviceDetail = device
//        self.page.pagingToViewController(at: 3)
//    }
//    
//    func backToVc3(){
//        self.page.pagingToViewController(at: 2)
//    }
//}
