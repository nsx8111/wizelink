//
//  CDDetailTableViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/15.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
/**
裝置詳細資訊頁
 - Date: 08/25 Yang
 */
class CDDetailTableViewController: UITableViewController {
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    var deviceDetail : Device?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
//        DispatchQueue.main.async {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.bounces = false
            self.tableView.tableFooterView = UIView(frame: CGRect.zero)
            self.tableView.register(CDDetailTableViewCell.self, forCellReuseIdentifier: "cddCell")
            self.tableView.rowHeight = 45
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        DispatchQueue.main.async {
            if self.deviceDetail != nil{
                print("CDDetailTableViewController's deviceDetail 2:\(self.deviceDetail!)")
                self.tableView.reloadData()
            }
//        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = TextColor2
        
        let titleLabel = UILabel()
        titleLabel.text = localString(key: "CONNECTION_DETAIL", table: .pageTitle)
        titleLabel.textColor = .black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        headerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.center.equalToSuperview()
        }
        
        let backBtn = UIButton()
        backBtn.setImage(UIImage(named: "go-back"), for: .normal)
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        headerView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalToSuperview()
            makes.left.equalToSuperview().offset(20)
            makes.width.height.equalTo(20)
        }
        return headerView
    }
    
    @objc func back(){
        self.pageViewDelegate?.backToVc3()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if deviceDetail != nil{
            return 6
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cddCell") as! CDDetailTableViewCell
        cell.backgroundColor = .white
        cell.selectionStyle = .none

        switch indexPath.row {
        case 0:
//            DispatchQueue.main.async {
                cell.titleLabel.text = localString(key: "DEVICE_NAME", table: .dataTitle)
                cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                cell.titleLabel.textColor = .darkGray
                if self.deviceDetail != nil{
                    cell.valueLabel.text = self.deviceDetail!.deviceName
                    cell.valueLabel.textColor = PrimaryColor2
                    cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
                }else{
                    cell.valueLabel.text = "---"
                    cell.valueLabel.textColor = PrimaryColor2
                }
//            }
        case 1:
            cell.titleLabel.text = localString(key: "IP", table: .dataTitle)
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            cell.titleLabel.textColor = .darkGray
            if deviceDetail != nil{
                cell.valueLabel.text = deviceDetail!.ipAddress
                cell.valueLabel.textColor = PrimaryColor2
                cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
            }else{
                cell.valueLabel.text = "---"
                cell.valueLabel.textColor = PrimaryColor2
            }
        case 2:
            cell.titleLabel.text = localString(key: "MAC", table: .dataTitle)
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            cell.titleLabel.textColor = .darkGray
            if deviceDetail != nil{
                cell.valueLabel.text = deviceDetail!.macAddress.uppercased()
                cell.valueLabel.textColor = PrimaryColor2
                cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
            }else{
                cell.valueLabel.text = "---"
                cell.valueLabel.textColor = PrimaryColor2
            }
        case 3:
            cell.titleLabel.text = localString(key: "CONNECT_TYPE", table: .dataTitle)
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            cell.titleLabel.textColor = .darkGray
            if deviceDetail != nil{
                cell.valueLabel.text = deviceDetail!.connectType
                cell.valueLabel.textColor = PrimaryColor2
                cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
            }else{
                cell.valueLabel.text = "---"
                cell.valueLabel.textColor = PrimaryColor2
            }
        case 4:
            cell.titleLabel.text = localString(key: "ON_OFF", table: .dataTitle)
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            cell.titleLabel.textColor = .darkGray
            if deviceDetail != nil{
                if deviceDetail!.isOnline{
                    cell.valueLabel.text = "on"
                    cell.valueLabel.textColor = PrimaryColor2
                    cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
                }else{
                    cell.valueLabel.text = "off"
                    cell.valueLabel.textColor = PrimaryColor2
                    cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
                }
                
            }else{
                cell.valueLabel.text = "---"
                cell.valueLabel.textColor = PrimaryColor2
            }
        case 5:
            cell.titleLabel.text = localString(key: "BRAND", table: .dataTitle)
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            cell.titleLabel.textColor = .darkGray
            if deviceDetail != nil{
                cell.valueLabel.text = deviceDetail!.brand
                cell.valueLabel.textColor = PrimaryColor2
                cell.valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
            }else{
                cell.valueLabel.text = "---"
                cell.valueLabel.textColor = PrimaryColor2
            }
        default:
            return cell
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
