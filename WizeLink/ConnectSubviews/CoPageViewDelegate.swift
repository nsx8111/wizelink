//
//  CoPageViewDelegate.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.
//

import Foundation

/**
 主要是要讓兩個分頁溝通用
 也可以呼叫 MainPage 上面的 actView，達到資料讀取中的效果
 */
protocol PageViewDelegate: AnyObject {
    /** For Test (可以在兩個分頁中傳值) */
    func sendSomeThing(_ url: String)
    /**
     讓分頁可以控制 MainPage 上面的 actView 顯示與否（用在讀取網路資料的等待時間上）
     - Parameter bool: 若是為 true 則隱藏，反之顯示是等待(讀取)中
     */
    func callActView(bool: Bool)
    /**
     讓 LiveTvView 可以改變 RecordListView 上面的 presentChannelID
     - Parameter id: 傳入的ID，去讓 RecordListView 上面的 presentChannelID 變成跟 LiveTvView 一樣
     */
    func changeRecordListChannelId(id: Int)
    
    func testToVC4()
    func toDetailPage(device: Device)
    func backToVc3()
    
    func changeConnectNum(num: Int)
}


protocol PageViewDelegate2: AnyObject {
    
    func toDetailPage(device: StationInfoDetail)
    
}
