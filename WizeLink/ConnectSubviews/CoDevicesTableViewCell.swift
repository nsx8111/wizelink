//
//  CoDevicesTableViewCell.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/13.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
import SnapKit

class CoDevicesTableViewCell: UITableViewCell {
    
    var connectTypeImage = UIImageView()
    var deviceNameLabel = UILabel()
    var macAddressLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
//        let cellHeight = contentView.frame.height - 10
//        print("cellHeight:\(cellHeight)")
        connectTypeImage.backgroundColor = .white
        contentView.addSubview(connectTypeImage)
        connectTypeImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(20)
            makes.centerY.equalToSuperview()
            
//            makes.top.bottom.equalToSuperview()
//            makes.bottom.equalToSuperview()
//            makes.width.equalTo(contentView.snp.height)
//            makes.height.width.equalTo(cellHeight)
            makes.height.width.equalTo(60)
        }
        
        deviceNameLabel.text = "---"
        contentView.addSubview(deviceNameLabel)
        deviceNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(connectTypeImage)
            makes.left.equalTo(connectTypeImage.snp.right).offset(10)
            makes.height.equalTo(28)
        }
        
        macAddressLabel.text = "xx:xx:xx:xx:xx:xx:xx:xx"
//        macAddressLabel.font.pointS
        contentView.addSubview(macAddressLabel)
        macAddressLabel.snp.makeConstraints { (makes) in
            makes.bottom.equalTo(connectTypeImage)
            makes.left.equalTo(connectTypeImage.snp.right).offset(10)
            makes.height.equalTo(28)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
