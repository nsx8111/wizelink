//
//  CoDevicesTableViewCell.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/13.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
import SnapKit
/**
 客製化裝置表格cell佈局
 - Date: 08/25 Yang
 */
class CoDevicesTableViewCell: UITableViewCell {
    
    var connectTypeImage = UIImageView()
    var userCircleImage = UIImageView()
    var deviceNameLabel = UILabel()
    var macAddressLabel = UILabel()
    var detailBtn = UIButton()
    var detailBtn2 = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        let cellHeight = contentView.frame.height - 10
//        print("cellHeight:\(cellHeight)")
        connectTypeImage.backgroundColor = .white
        contentView.addSubview(connectTypeImage)
        connectTypeImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(spacing3)
            makes.centerY.equalToSuperview()
            makes.height.width.equalTo(spacing3+spacing2)
        }
        
        contentView.addSubview(userCircleImage)
        userCircleImage.snp.makeConstraints { (makes) in
            makes.left.equalTo(connectTypeImage.snp.right).offset(-3)
            makes.top.equalTo(connectTypeImage.snp.top).offset(-3)
            makes.height.width.equalTo(spacing2)
        }
        
        deviceNameLabel.text = "---"
        contentView.addSubview(deviceNameLabel)
        deviceNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(connectTypeImage.snp.top).offset(-spacing1)
            makes.left.equalTo(connectTypeImage.snp.right).offset(loginLabelHeight4)
            makes.height.equalTo(28)
        }
        
        macAddressLabel.text = "xx:xx:xx:xx:xx:xx:xx:xx"
//        macAddressLabel.font.pointS
        contentView.addSubview(macAddressLabel)
        macAddressLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(deviceNameLabel.snp.bottom).offset(0)
            makes.left.equalTo(connectTypeImage.snp.right).offset(loginLabelHeight4)
            makes.height.equalTo(28)
        }
        
//        detailBtn.imageView?.image = UIImage(named: "nextBtnBlue")
        detailBtn.setImage(UIImage(named: "nextBtnBlue"), for: .normal)
        contentView.addSubview(detailBtn)
        detailBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalToSuperview()
            makes.right.equalToSuperview().offset(-spacing3)
            makes.width.height.equalTo(spacing3)
        }
        
        detailBtn2.backgroundColor = .clear
        contentView.addSubview(detailBtn2)
        detailBtn2.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
