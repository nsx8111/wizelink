//
//  CoDevicesTableViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/13.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
/**
 連線狀態的裝置頁
 - Date: 08/25 Yang
*/
class CoDevicesTableViewController: UITableViewController {
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    let actView : UIView = UIView()
    var onlineDevice : [Device] = []
    var offlineDevice : [Device] = []
    var pageLanguage : String = ""

    override func viewDidLoad() {
        searchInfo()
        super.viewDidLoad()
        tableView.bounces = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CoDevicesTableViewCell.self, forCellReuseIdentifier: "cdCell")
        tableView.rowHeight = labelWidth
        //        tableView.tableHeaderView?.backgroundColor = .red
        //        tableView.snp.makeConstraints { (makes) in
        //            makes.edges.equalToSuperview()
        //        }
        
        //        tableView.register(self, forCellReuseIdentifier: "cell")
        //        searchInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
        }
    }
    
    func reloadString(){
        print("reloadString @ CoDevicesTableViewController")
        pageLanguage = bundleLocalizeFileName
        tableView.reloadData()
    }
    
//    func cellImage() -> Void {
//
//    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        //        print()
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
//        print("section:\(section)")
        if section == 0{
            return onlineDevice.count
            //            return 3
        }
        return offlineDevice.count
        //        return 5
    }
    /**
    表格切成在線跟離線兩部分
    - Date: 08/25 Yang
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cdCell") as! CoDevicesTableViewCell
//        cell.tag = indexPath.row
//        print("indexPath:\(indexPath)")
//        print("cell.tag:\(cell.tag)")
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn2.tag = indexPath.row
        if indexPath.section == 0 {
            cell.backgroundColor = .white
            cell.deviceNameLabel.text = onlineDevice[indexPath.row].deviceName
            cell.macAddressLabel.text = onlineDevice[indexPath.row].macAddress.uppercased()
            cell.macAddressLabel.textColor = PrimaryColor2
            cell.deviceNameLabel.textColor = .darkGray
//            print(onlineDevice.count)
            ///當前裝置會顯示icon
            if onlineDevice[indexPath.row].ipAddress.contains(currentWiFiAddress){
                print("indexPath11 \(indexPath)")
                print(onlineDevice[indexPath.row].connectType)
                cell.userCircleImage.image = UIImage(named: "user-circle")
            }else{
                print(onlineDevice[indexPath.row].connectType)
                cell.userCircleImage.image = nil
            }
            
            switch onlineDevice[indexPath.row].connectType {
            case "ethernet":
                cell.connectTypeImage.image = UIImage(named: "CTEthernet")
            case "wifi_5G":
                cell.connectTypeImage.image = UIImage(named: "CT5G")
            case "Wi-Fi_2.4G":
                cell.connectTypeImage.image = UIImage(named: "CT2.4G")
            default:
                cell.connectTypeImage.image = UIImage(named: "CTUnknown")
            }
            cell.detailBtn.addTarget(self, action: #selector(toDetailPage0(_:)), for: .touchUpInside)
            cell.detailBtn2.addTarget(self, action: #selector(toDetailPage0(_:)), for: .touchUpInside)
        }else{
            cell.backgroundColor = .white
            cell.deviceNameLabel.text = offlineDevice[indexPath.row].deviceName
            cell.macAddressLabel.text = offlineDevice[indexPath.row].macAddress.uppercased()
            cell.macAddressLabel.textColor = PrimaryColor2
            cell.deviceNameLabel.textColor = .darkGray
            cell.connectTypeImage.image = UIImage(named: "CTOFF")
            cell.userCircleImage.image = nil
//            print("indexPath:\(indexPath)")
            //            print("offlineDevice[indexPath.row].macAddress:\(offlineDevice[indexPath.row].macAddress)")
//            print("offlineDevice[indexPath.row]:\(offlineDevice[indexPath.row])")
            cell.detailBtn.addTarget(self, action: #selector(toDetailPage1(_:)), for: .touchUpInside)
            cell.detailBtn2.addTarget(self, action: #selector(toDetailPage1(_:)), for: .touchUpInside)
//            cell.detailBtn.on(.touchUpInside) { (sender, event) in
//                // You can use any reference initialized before the code block here
//                // You can access self by adding [weak self] before (sender, event)
//                // You can then either make self strong by using a guard statement or use a optional operator (?)
//                print("user did press test button")
//            }
        }
//        print("cell's indexPath:\(indexPath)")
        return cell
    }
    /**
     用Button取代didSelectRow
     點擊進入detail頁
    - Date: 08/25 Yang
     */
    @objc func toDetailPage0(_ sender : UIButton){
        ///獲取點擊當前cell的indexPath
        let point = sender.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) {
            print("indexPath3:\(indexPath)")
        }
        //self.pageViewDelegate?.toDetailPage(device: onlineDevice[sender.tag]) //b2問題
        print("sender.tag:\(sender.tag)")
        /// 不知道為什麼 離線的很容易就連接到 toDetailPage0，正常來說應該是要連去 toDetailPage1
        /// 暫時用這個方法可以解決問題
        if onlineDevice.count > sender.tag{ //若是onlineDevice.count >＝ sender.tag 就會當在indexPath3:[1, 3]
            self.pageViewDelegate?.toDetailPage(device: onlineDevice[sender.tag])
        }else{
            self.pageViewDelegate?.toDetailPage(device: offlineDevice[sender.tag])
        }
    }
    
    @objc func toDetailPage1(_ sender : UIButton){
        self.pageViewDelegate?.toDetailPage(device: offlineDevice[sender.tag])
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "\(localString(key: "ONLINE", table: .headerTitle)) (\(onlineDevice.count))"
        }else{
            return "\(localString(key: "OFFLINE", table: .headerTitle)) (\(offlineDevice.count))"
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //        view.content
        //        view.tintColor = .lightGray
//        view.tintColor = UIColor(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
        view.tintColor = TextColor2
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = .black
        switch currentDevice.deviceType {
        case .iPad:
            header.textLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        default:
            header.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        }
    }
    /**
    - ConnectViewController按下RefreshButton後 若離線狀態則會呼叫此方法
    - Date: 08/25 Yang
     */
    func connectFail(){
        self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
    }
    
    func searchInfo(){
//        print("searchInfo @ CoDevie")
//
//        if let rangeA1:Range = clientsHtmData.range(of: "var buffer ='"), let rangeA2:Range = clientsHtmData.range(of: "';\n    var res = buffer.split") {
//
//            let rangeA1:Range = clientsHtmData.range(of: "var buffer ='")!
//            let rangeA2:Range = clientsHtmData.range(of: "';\n    var res = buffer.split")!
//            let data1 = String(clientsHtmData[rangeA1.upperBound ..< rangeA2.lowerBound ])

//            let data1 = rangeFunc(src: clientsHtmData, str1: "var buffer ='", str2: "';\n    var res = buffer.split")
        
            let data1 = searchFunc(src: clientsHtmData, str1: "var buffer ='", str2: "';")
            let deviceArray = data1.components(separatedBy: ">>>")
            
            print("deviceArray.count:\(deviceArray.count)")
            
            onlineDevice = []
            offlineDevice = []
            
            for device in deviceArray{
                let deviceDataArray = device.components(separatedBy: " ")
                var tempDevice = Device()
                
                tempDevice.deviceName = deviceDataArray[0]
                if deviceDataArray.count < 2 {
                    self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                    self.actView.removeFromSuperview()
                    return
                }
                tempDevice.ipAddress = deviceDataArray[1]
                tempDevice.macAddress = deviceDataArray[2]
                tempDevice.unknownNum = deviceDataArray[3]
                tempDevice.connectType = deviceDataArray[4]
                //    tempDevice.deviceName = deviceDataArray[5]
                tempDevice.brand = deviceDataArray[6]
                
                if deviceDataArray[5] == "on"{
                    tempDevice.isOnline = true
                    onlineDevice.append(tempDevice)
                    print("deviceName444 \(onlineDevice)")
                }else{
                    offlineDevice.append(tempDevice)
                }
                //            }
            }
            //        DispatchQueue.main.async {
            //            self.pageViewDelegate?.changeConnectNum(num: self.onlineDevice.count)
            //        }
            self.pageViewDelegate?.changeConnectNum(num: onlineDevice.count)
        }
        
}


