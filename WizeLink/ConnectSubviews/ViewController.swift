//
//  ViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by Natalie Ng on 2019/11/13.
//  Copyright © 2019 Natalie. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit

class ViewController: UIViewController {
    
    var bbb = UIButton()
    var lll = UITextView()
    var lanIpLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .lightGray
        setViews()
        login()
    }
    
    func setViews(){
        //        bbb.titleLabel?.text = "click"
        //        bbb.titleLabel?.textColor = .red
        bbb.setTitle("Clict", for: .normal)
        bbb.setTitleColor(.blue, for: .normal)
        bbb.backgroundColor = .red
        //        bbb.text
        bbb.addTarget(self,action: #selector(self.clickBBBButton),for: .touchUpInside)
        view.addSubview(bbb)
        bbb.snp.makeConstraints { (makes) in
            makes.top.left.equalToSuperview().offset(30)
            makes.width.equalTo(100)
            makes.height.equalTo(30)
        }
        
        lanIpLabel.text = "Lan IP : "
        lanIpLabel.textColor = .red
        view.addSubview(lanIpLabel)
        lanIpLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(bbb.snp.bottom).offset(10)
            makes.left.equalTo(bbb)
            makes.height.equalTo(30)
        }
        
        lll.backgroundColor = .white
        view.addSubview(lll)
        lll.snp.makeConstraints { (makes) in
            makes.top.equalTo(lanIpLabel.snp.bottom).offset(10)
            makes.left.bottom.equalToSuperview().offset(10)
            makes.right.equalToSuperview().offset(-10)
        }
    }
    
    @objc func clickBBBButton(){
        print("clickBBBButton")
        //        self.lll.text = "clickBBBButton"
        login()
    }
    
    func login(){
        let user = "admin"
        let password = "admin"
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        
        AF.request("http://192.168.10.1/status.htm",headers: headers)
            .responseJSON { response in
                debugPrint(response)
                let res = String(response.debugDescription)
                self.lll.text = res
                //                self.lll.text = String(response)
                let range:Range = res.range(of: "var lan_ip='")!
                let range2:Range = res.range(of: "';\nvar lan_mask='")!
                let lanipStr = res[range.upperBound ..< range2.lowerBound ]
                print("lanipStr:\(lanipStr)")
                
                if self.lanIpLabel.text == "Lan IP : "{
                    self.lanIpLabel.text! += String(lanipStr)
                }
                
        }
        
    }
}

