//
//  CoRoterViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit

class CoRouterViewController: UIViewController {
    
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    
    let titleLabel = UILabel()
    
    let macIpLabel = UILabel()
    let ipLabel = UILabel()
    let maskLabel = UILabel()
    
    let macIpValueLabel = UILabel()
    let ipValueLabel = UILabel()
    let maskValueLabel = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        searchInfo()
    }
    
    func setViews(){
        titleLabel.text = "路由器資訊"
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(10)
            makes.left.equalToSuperview().offset(20)
        }
        
        macIpLabel.text = "MAC位址"
        view.addSubview(macIpLabel)
        macIpLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(titleLabel.snp.bottom).offset(viewOffset)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        macIpValueLabel.text = ""
        view.addSubview(macIpValueLabel)
        macIpValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(macIpLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
        ipLabel.text = "IP位址"
        view.addSubview(ipLabel)
        ipLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(macIpLabel.snp.bottom).offset(viewOffset)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        ipValueLabel.text = ""
        view.addSubview(ipValueLabel)
        ipValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(ipLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
        maskLabel.text = "子網路遮罩"
        view.addSubview(maskLabel)
        maskLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(ipLabel.snp.bottom).offset(viewOffset)
            makes.left.equalTo(titleLabel).offset(5)
        }
        
        maskValueLabel.text = ""
        view.addSubview(maskValueLabel)
        maskValueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(maskLabel)
            makes.right.equalToSuperview().offset(-25)
        }
        
    }
    
    func searchInfo(){
        let rangeA1:Range = statusHtmData.range(of: "var wan_mac='")!
        let rangeA2:Range = statusHtmData.range(of: "';\n\nfunction showWan()")!
        self.macIpValueLabel.text = String(statusHtmData[rangeA1.upperBound ..< rangeA2.lowerBound ])
        
        let rangeB1:Range = statusHtmData.range(of: "var lan_ip='")!
        let rangeB2:Range = statusHtmData.range(of: "';\nvar lan_mask='")!
        self.ipValueLabel.text = String(statusHtmData[rangeB1.upperBound ..< rangeB2.lowerBound ])
        
        let rangeC1:Range = statusHtmData.range(of: "var lan_mask='")!
        let rangeC2:Range = statusHtmData.range(of: "';\nvar lan_gateway='")!
        self.maskValueLabel.text = String(statusHtmData[rangeC1.upperBound ..< rangeC2.lowerBound ])
    }
    
}
