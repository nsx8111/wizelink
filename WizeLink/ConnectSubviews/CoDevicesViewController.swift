//
//  CoDevicesViewController.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit

class CoDevicesViewController: UIViewController {
    
    /** 跟 MainPage 溝通用 */
    weak var pageViewDelegate: PageViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTest()
    }
    
    func setTest(){
        let bkView = UIView()
        bkView.backgroundColor = .green
        view.addSubview(bkView)
        bkView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
    }
    
}
