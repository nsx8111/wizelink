//
//  SubRouterViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/5/18.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
/**
 加入子路由器頁
 - Date: 08/25 Yang
*/
class SubRouterViewController: UIViewController, UIScrollViewDelegate {
    
    let ScrollView = UIScrollView()
    let ContentView = UIView()
    let actView : UIView = UIView()
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let closeButton = UIButton()
    let lineView = UIView()
    let waitConnectLabel = UILabel()
    let ethernet = UIImageView()
    let wps = UIImageView()
    let label1 = UILabel()
    let label2 = UILabel()
    let label3 = UILabel()
    let label4 = UILabel()
    let label5 = UILabel()
    /**
    轉圈畫面先暫時設定 之後連接機器時再重寫判斷邏輯
    - Date: 08/25 Yang
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        self.setActView(actView: self.actView)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
            self.actView.removeFromSuperview()
        }
    }
  
    func setViews(){
        view.backgroundColor = UIColor.white
        
        navigationBarView.backgroundColor = PrimaryColor1
        view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navBarH)
        }

        navBarLabel.backgroundColor = .clear
        navBarLabel.text = localString(key: "ADD_SUB_ROUTER", table: .srAndLoginInfo)
        navBarLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
        }

        closeButton.setTitle(localString(key: "CLOSE", table: .button), for: .normal)
        closeButton.backgroundColor = UIColor.clear
        closeButton.setTitleColor(SecondaryColor3, for: .normal)
        closeButton.titleLabel?.font = .boldSystemFont(ofSize: labelFontSize)
        closeButton.isEnabled = true
        closeButton.addTarget(self,action: #selector(self.dismissVC),for: .touchUpInside)
        view.addSubview(closeButton)
        closeButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(navBarLabel)
            makes.height.equalTo(loginLabelHeight*2)
            makes.left.greaterThanOrEqualTo(navBarLabel.snp.right).offset(spacing1/2*WidthForDevices)
            makes.right.greaterThanOrEqualToSuperview().offset(-spacing2*WidthForDevices)
        }
       
        ScrollView.bounces = true
        ScrollView.delegate = self
        view.addSubview(ScrollView)
        ScrollView.snp.makeConstraints { (makes) in
            makes.left.right.bottom.equalTo(view)
            makes.top.equalTo(navigationBarView.snp.bottom)
        }

        ScrollView.addSubview(ContentView)
        ContentView.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(ScrollView)
            makes.left.right.equalTo(view)
        }

        waitConnectLabel.backgroundColor = .clear
        waitConnectLabel.text = localString(key: "WAIT_ACCESS", table: .srAndLoginInfo)
        waitConnectLabel.font = UIFont.boldSystemFont(ofSize: 15.5)
        waitConnectLabel.numberOfLines = 0
        waitConnectLabel.textColor = .black
        waitConnectLabel.textAlignment = .center
        ContentView.addSubview(waitConnectLabel)
        waitConnectLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(ContentView).offset(navBarH*1.7*HeightForScroll)
            makes.left.right.equalTo(ContentView).inset(0)
        }

        lineView.backgroundColor = TextColor2
        ContentView.addSubview(lineView)
        lineView.snp.makeConstraints{ (makes) in
            makes.right.left.equalTo(ContentView)
            makes.top.equalTo(waitConnectLabel.snp.bottom).offset(navBarH*1.5*HeightForScroll)
            makes.height.equalTo(spacing1)
        }

        label1.text = localString(key: "ACCESS_ETHERNET", table: .srAndLoginInfo)
        label1.font = UIFont.systemFont(ofSize: 17)
        label1.numberOfLines = 0
        label1.textColor = PrimaryColor2
        label1.textAlignment = .left
        ContentView.addSubview(label1)
        label1.snp.makeConstraints { (makes) in
            makes.left.equalTo(ContentView).offset(loginLabelHeight4+loginLabelHeight3+fontSize)
            makes.top.equalTo(lineView.snp.bottom).offset(spacing3*HeightForScroll)
            makes.right.equalTo(ContentView)
        }

        label2.text = localString(key: "ACCESS_ETHERNET_VALUE", table: .srAndLoginInfo)
        label2.font = UIFont.systemFont(ofSize: 15.5)
        label2.numberOfLines = 0
        label2.textColor = .black
        label2.textAlignment = .left
        ContentView.addSubview(label2)
        label2.snp.makeConstraints { (makes) in
            makes.left.equalTo(label1)
            makes.right.equalTo(ContentView).offset(-spacing3)
            makes.top.equalTo(label1.snp.bottom).offset(spacing1/2)
        }

        label3.text = localString(key: "OR", table: .srAndLoginInfo)
        label3.font = UIFont.boldSystemFont(ofSize: 17)
        label3.textColor = .black
        label3.textAlignment = .center
        ContentView.addSubview(label3)
        label3.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(label2.snp.bottom).offset(spacing4*HeightForScroll)
        }

        label4.text = localString(key: "ACCESS_WPS", table: .srAndLoginInfo)
        label4.font = UIFont.systemFont(ofSize: 17)
        label4.textColor = PrimaryColor2
        label4.numberOfLines = 0
        label4.textAlignment = .left
        ContentView.addSubview(label4)
        label4.snp.makeConstraints { (makes) in
            makes.left.equalTo(label2)
            makes.top.equalTo(label3.snp.bottom).offset(spacing4*HeightForScroll)
            makes.right.equalTo(ContentView)
        }

        label5.text = localString(key: "ACCESS_WPS_VALUE", table: .srAndLoginInfo)
        label5.font = UIFont.systemFont(ofSize: 15.5)
        label5.textColor = .black
        label5.numberOfLines = 0
        label5.textAlignment = .left
        ContentView.addSubview(label5)
        label5.snp.makeConstraints { (makes) in
            makes.left.equalTo(label2)
            makes.top.equalTo(label4.snp.bottom).offset(spacing1/2)
            makes.right.equalTo(ContentView).offset(-spacing2)
            makes.bottom.equalToSuperview().offset(-loginLabelHeight*HeightForScroll)
        }

        ethernet.image = UIImage(named: "ethernet-3")
        ContentView.addSubview(ethernet)
        ethernet.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(label2)
            makes.left.equalTo(ContentView).offset(loginLabelHeight4)
            makes.height.width.equalTo(loginLabelHeight3)
        }

        wps.image = UIImage(named: "wps")
        ContentView.addSubview(wps)
        wps.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(ethernet)
            makes.centerY.equalTo(label5)
            makes.height.width.equalTo(loginLabelHeight3)
        }
        
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
