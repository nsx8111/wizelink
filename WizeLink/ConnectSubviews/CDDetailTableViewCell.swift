//
//  CDDetailTableViewCell.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/15.
//  Copyright © 2020 Natalie. All rights reserved.
//

import UIKit
import SnapKit
/**
客製化裝置表格detail cell佈局
- Date: 08/25 Yang
*/
class CDDetailTableViewCell: UITableViewCell {
    
    var titleLabel = UILabel()
    var valueLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalToSuperview()
            makes.left.equalToSuperview().offset(25)
        }
        
        contentView.addSubview(valueLabel)
        valueLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalToSuperview()
            makes.right.equalToSuperview().offset(-25)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
