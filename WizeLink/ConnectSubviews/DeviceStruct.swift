//
//  DeviceStruct.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/13.
//  Copyright © 2020 Natalie. All rights reserved.
//

import Foundation

struct Device{
    var deviceName : String = ""
    var ipAddress : String = ""
    var macAddress : String = ""
    var unknownNum : String = ""
    var connectType : String = ""
    var isOnline : Bool = false
    var brand : String = ""
}
