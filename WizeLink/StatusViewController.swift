//
//  StatusViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

struct NetworkType{
    var iconImageView: UIImageView!
    var network_type: String = ""
}
/**
 狀態資訊頁
 - Date: 08/25 Yang
*/
class StatusViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "NetworkTypeCell")
        cell.imageView?.image = types[indexPath.row].iconImageView.image
        let itemSize = CGSize.init(width: 20, height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
        let imageRect = CGRect.init(origin: CGPoint.zero, size: itemSize)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].network_type
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .black
        
        switch currentDevice.deviceType {
        case .iPhone40:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        case .iPhone47:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        case .iPad:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        default:
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        }
        
        let detailButton = UIButton()
        detailButton.backgroundColor = .clear
        cell.contentView.addSubview(detailButton)
        detailButton.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        detailButton.addTarget(self,action: #selector(self.clickToDetail),for: .touchUpInside)
        return cell
    }
    /**
     用Button取代didSelectRow 帶資料到detail頁
     - Date: 08/26 Yang
    */
    @objc func clickToDetail(_ sender: UIButton){
        if navigationController?.view.superview?.frame.origin.x == 0 {
            let point = sender.convert(CGPoint.zero, to: NetworkTypeTableView)
            if let indexPath = NetworkTypeTableView.indexPathForRow(at: point) {
                if indexPath.row == 0{
                    typesNum = 0
                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
                    networkNameLabel = types[indexPath.row].network_type
                    GNLabel = localString(key: "CONFIG_GUEST", table: .headerTitle)
                    vc.types = typesFor24G
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 1{
                    typesNum = 1
                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
                    networkNameLabel = types[indexPath.row].network_type
                    GNLabel = localString(key: "CONFIG_GUEST", table: .headerTitle)
                    vc.types = typesFor5G
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 2{
                    typesNum = 2
                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
                    networkNameLabel = types[indexPath.row].network_type
                    vc.types = typesForLan
                    vc.GuestNetworkLabel.isHidden = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if indexPath.row == 3{
                    typesNum = 3
                    let vc = getVC(storyboardName: "Main", identifier: "NetworkDetailViewController") as! NetworkDetailViewController
                    networkNameLabel = types[indexPath.row].network_type
                    vc.types = typesForWan
                    vc.GuestNetworkLabel.isHidden = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            navigationController?.view.menu()
        }
    }
    
    func setTableView(){
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        NetworkTypeTableView = UITableView()
        NetworkTypeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "NetworkTypeCell")
        NetworkTypeTableView.delegate = self
        NetworkTypeTableView.dataSource = self
        NetworkTypeTableView.bounces = false
        NetworkTypeTableView.allowsSelection = true
        NetworkTypeTableView.backgroundColor = .white
        NetworkTypeTableView.rowHeight = fullScreenSize.height*0.067
        self.view.addSubview(NetworkTypeTableView)
        NetworkTypeTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(navigationBarHeight)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(NetworkTypeTableView.rowHeight*4)
            make.centerX.equalToSuperview()
        }
    }
    
    var types = [
        NetworkType(iconImageView: UIImageView(image: UIImage(named: "wifi-b")), network_type: localString(key: "CONFIG_2.4G", table: .pageTitle)),
        NetworkType(iconImageView: UIImageView(image: UIImage(named: "wifi-b")), network_type: localString(key: "CONFIG_5G", table: .pageTitle)),
        NetworkType(iconImageView:UIImageView(image: UIImage(named: "LANConfig-b")), network_type: localString(key: "CONFIG_LAN", table: .pageTitle)),
        NetworkType(iconImageView: UIImageView(image: UIImage(named: "WAN-b")), network_type: localString(key: "CONFIG_WAN", table: .pageTitle))
    ]
    
    let actView : UIView = UIView()
    let titleLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkView = UIImageView()
    private var NetworkTypeTableView: UITableView!
    var typesFor24G_GN:[NetworkConfiguration] = []
    var typesFor5G_GN:[NetworkConfiguration] = []
    var typesFor24G:[NetworkConfiguration] = []
    var typesFor5G:[NetworkConfiguration] = []
    var typesForLan:[NetworkConfiguration] = []
    var typesForWan:[NetworkConfiguration] = []
    var NetworkTypes: [NetworkType] = []
    var pageLanguage : String = ""
    
    @IBOutlet var MenuButton : UIButton!
    var MenuButton2 = UIButton(type: .custom)

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let name = NSNotification.Name("Refresh_StatusVC")
        NotificationCenter.default.addObserver(self, selector:
            #selector(RefreshNoti(noti:)), name: name, object: nil)
    }
    
    @objc func RefreshNoti(noti: Notification) {
        searchInfo()
    }
    
    override func viewDidLoad() {
        isShowAlert = false
        searchInfo()
        super.viewDidLoad()
        //        print("viewDidLoad() @StatusVC")
        //        MenuButton.snp.makeConstraints { (makes) in
        //            makes.height.width.equalTo(44)
        //        }
        setViews()
        setTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isShowAlert = false
        checkLanguage()
    }
    
    func checkLanguage(){
        if pageLanguage != bundleLocalizeFileName {
            reloadString()
            setTableView()
        }
    }
    
    func reloadString(){
        pageLanguage = bundleLocalizeFileName
        types[0].network_type = localString(key: "CONFIG_2.4G", table: .pageTitle)
        types[1].network_type = localString(key: "CONFIG_5G", table: .pageTitle)
        types[2].network_type = localString(key: "CONFIG_LAN", table: .pageTitle)
        types[3].network_type = localString(key: "CONFIG_WAN", table: .pageTitle)
        searchInfo()
        titleLabel.text = localString(key: "STATUS", table: .pageTitle)
    }
    
    func searchInfo(){
        //for test
        //        statusHtmData = tempStatusHtmData
        //Mike debug
        // Mike 0602 mark
        //        print("""
        //            ==statusHtmData @ StatusVC.searchInfo()==
        //            \(statusHtmData)
        //            =========================================
        //            """)
        //record band

        let aa = statusHtmData.components(separatedBy: "band[0] =")
        if aa.count < 2 {
            if isShowAlert == true{
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
            }
            self.actView.removeFromSuperview()
            return
        }
        
        var tempValueString : String = ""
        //        var tempValueString2 : String = ""
        typesFor5G_GN = []
        typesFor5G = []
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "MODE", table: .dataTitle), detailLabel: "AP"))
        
        let aa2 = aa[1].components(separatedBy: ";")
        tempValueString = aa2[0]
        print("test_statusHtmData")
        //        let rangeA1:Range = statusHtmData.range(of: "band[0] =")!
        //        let rangeA2:Range = statusHtmData.range(of: ";\n        ssid_drv[0] ='")!
        //        tempValueString  = String(statusHtmData[rangeA1.upperBound ..< rangeA2.lowerBound ])
        
        let bandStringArray = statusHtmData.components(separatedBy: "if (band[i] == ")
        //        print("bandStringArray.count:\(bandStringArray.count)")
        for bandString in bandStringArray{
            let bbbb = bandString.split(separator: ")")
            //            print(bbbb[0])
            //要找到的頻段
            if bbbb[0] == tempValueString{
                let bbbb2 = bbbb[1].components(separatedBy: ".write( \"")
                let bbbb3 = bbbb2[1].components(separatedBy: "\");")
                //                print("bbbb3[0]:\(bbbb3[0]))")
                typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
                typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
            }
        }
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: SSID2))
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: wep2))
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: clientNum2))
        
        var cccc = statusHtmData.components(separatedBy: "ssid_drv[0] ='")
        var cccc2 = cccc[1].components(separatedBy: "';")
        
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "channel_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "CHANNEL", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "wep[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: "BSSID", detailLabel: "\(cccc2[0].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "clientnum[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        
        typesFor24G_GN = []
        typesFor24G = []
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "MODE", table: .dataTitle), detailLabel: "AP"))
        
        cccc = statusHtmData.components(separatedBy: "band[1] =")
        tempValueString = String(cccc[1].split(separator: ";")[0])
        //        bandStringArray = statusHtmData.components(separatedBy: "if (band[i] == ")
        //        print("bandStringArray.count:\(bandStringArray.count)")
        for bandString in bandStringArray{
            let bbbb = bandString.split(separator: ")")
            //            print(bbbb[0])
            //要找到的頻段
            if bbbb[0] == tempValueString{
                let bbbb2 = bbbb[1].components(separatedBy: ".write( \"")
                let bbbb3 = bbbb2[1].components(separatedBy: "\");")
                //                print("bbbb3[0]:\(bbbb3[0]))")
                typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
                typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
            }
        }
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: SSID))
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: wep))
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: clientNum))
        
        cccc = statusHtmData.components(separatedBy: "ssid_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        //        print("")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "channel_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "CHANNEL", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "wep[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: "BSSID", detailLabel: "\(cccc2[0].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "clientnum[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        
        typesForLan = []
        cccc = statusHtmData.components(separatedBy: "var lan_ip='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var lan_mask='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "MASK", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var lan_gateway='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "GATEWAY", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        // dhcp be care
        print("DHCP")
        cccc = statusHtmData.components(separatedBy: "var choice=")
        //        print("cccc.count:\(cccc.count)")
        //        for c in cccc{
        //            print("c@cccc:\(c)")
        //        }
        cccc2 = cccc[1].components(separatedBy: ";")
        let choice = cccc2[0]
        
        cccc = statusHtmData.components(separatedBy: "if ( choice == \(choice) ) document.write(status_")
        cccc2 = cccc[1].components(separatedBy: ");")
        // mike mark
        //        print("cccc[1]:\(cccc[1])")
        let indices = [0]
        print(cccc2[0].uppercased(at: indices))
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "DHCP_SERVER", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased(at: indices))"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased())"))
        
        //var wan_current='
        typesForWan = []
        cccc = statusHtmData.components(separatedBy: "var wan_current='")
        cccc2 = cccc[1].components(separatedBy: "';")
        cccc = cccc2[0].components(separatedBy: "_")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "AIPP", table: .dataTitle), detailLabel: "\(cccc[1].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_ip='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_subMask='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "MASK", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_gateway='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "GATEWAY", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_mac='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased())"))
    }
    
    func setViews(){
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        switch currentDevice.deviceType {
        case .iPad:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        default:
            titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        titleLabel.backgroundColor = .clear
        titleLabel.text = localString(key: "STATUS", table: .pageTitle)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        MenuButton2.setImage(UIImage(named: "menu-options")!.withRenderingMode(.alwaysTemplate), for: .normal)
        MenuButton2.tintColor = SecondaryColor3
        MenuButton2.addTarget(self,action: #selector(self.menuClick),for: .touchUpInside)
        self.view.addSubview(MenuButton2)
        MenuButton2.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(titleLabel)
            makes.width.height.equalTo(buttonSize)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
    }
    
    @IBAction func menuClick(_ sender: Any) {
        navigationController?.view.menu()
    }
    
    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
    
    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let location = sender.location(in: nil)
        navigationController?.view.slideByFinger(location: location, state: sender.state)
    }
    
}
