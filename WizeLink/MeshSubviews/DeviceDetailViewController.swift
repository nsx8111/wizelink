//
//  DeviceDetailViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/5/29.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct WapDevice {
    var titleLabel: String = ""
    var detailLabel: String = ""
}
/**
 在線裝置頁
 - Date: 08/25 Yang
*/
class DeviceDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var onlineDevicesCount = 0
    let backButton = UIButton()
    let editButton = UIButton(type: .custom)
    let editImage = UIImage(named: "create-outline")?.withRenderingMode(.alwaysTemplate)
    let bkView = UIImageView()
    let deviceNameLabel = UILabel()
    let onlineDeviceLabel = UILabel()
    private var DeviceTableView: UITableView!
    private var StationTableView: UITableView!
    weak var pageViewDelegate: PageViewDelegate2?
    
    var wapDevicesDetail = [
        WapDevice(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: "ip_address"),
        WapDevice(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: "mac_address")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        self.setTableView()
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editVC(){
    }
    
    func setViews(){
        bkView.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        deviceNameLabel.backgroundColor = .clear
        deviceNameLabel.text = device_name
        deviceNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        deviceNameLabel.textColor = .white
        deviceNameLabel.textAlignment = .left
        self.view.addSubview(deviceNameLabel)
        deviceNameLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(backButton.snp.bottom).offset(loginLabelHeight)
            makes.width.equalTo(loginLabelWidth*5)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        editButton.setImage(editImage, for: .normal)
        editButton.tintColor = SecondaryColor3
        editButton.backgroundColor = UIColor.clear
        editButton.isEnabled = true
        editButton.addTarget(self,action: #selector(self.editVC),for: .touchUpInside)
        self.view.addSubview(editButton)
        editButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(deviceNameLabel)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.right.equalToSuperview().offset(-spacing1)
        }
        
    }
    
    func setTableView(){
        DeviceTableView = UITableView()
        DeviceTableView.register(UITableViewCell.self, forCellReuseIdentifier: "DeviceTableViewCell")
        DeviceTableView.delegate = self
        DeviceTableView.dataSource = self
        DeviceTableView.bounces = false
        DeviceTableView.allowsSelection = true
        DeviceTableView.backgroundColor = LightSecondaryColor2
        DeviceTableView.rowHeight = fullScreenSize.height*0.08
        self.view.addSubview(DeviceTableView)
        DeviceTableView.snp.makeConstraints { (make) in
            make.top.equalTo(deviceNameLabel.snp.bottom).offset(loginLabelHeight4)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(DeviceTableView.rowHeight * CGFloat(wapDevicesDetail.count))
            make.centerX.equalToSuperview()
        }
        DeviceTableView.separatorStyle = .none
        
        onlineDevicesCount = station_mac.count
        onlineDeviceLabel.text = "\(localString(key: "ONLINE_DEVICES", table: .pageTitle)) (\(onlineDevicesCount))"
        onlineDeviceLabel.font = UIFont.systemFont(ofSize: 20)
        onlineDeviceLabel.textColor = .white
        onlineDeviceLabel.textAlignment = .left
        self.view.addSubview(onlineDeviceLabel)
        onlineDeviceLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(DeviceTableView.snp.bottom).offset(loginLabelHeight2)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        StationTableView = UITableView()
        StationTableView.delegate = self
        StationTableView.dataSource = self
        StationTableView.bounces = false ///禁止TableView彈跳
        StationTableView.register(StationTableViewCell.self, forCellReuseIdentifier: "StationCell")
        StationTableView.rowHeight = fullScreenSize.height*0.09
        StationTableView.backgroundColor = PrimaryColor1
        self.view.addSubview(StationTableView)
        StationTableView.snp.makeConstraints { (make) in
            make.width.equalTo(fullScreenSize.width)
            //            make.height.equalTo(StationTableView.rowHeight * CGFloat(station_mac.count))
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(0)
            make.top.equalTo(onlineDeviceLabel.snp.bottom).offset(spacing1)
        }
        StationTableView.tableFooterView = UIView(frame: CGRect.zero)
        StationTableView.separatorColor = LightTextColor3
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DeviceTableView == tableView{
            return wapDevicesDetail.count
        }else{
            return station_mac.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "DeviceTableViewCell")
        let cell2 = StationTableView.dequeueReusableCell(withIdentifier: "StationCell") as! StationTableViewCell
        
        if DeviceTableView == tableView{
            cell.selectionStyle = .none
            cell.textLabel?.text = wapDevicesDetail[indexPath.row].titleLabel
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.textColor = PrimaryColor2
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            
            cell.detailTextLabel?.text = wapDevicesDetail[indexPath.row].detailLabel
            cell.detailTextLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
            cell.detailTextLabel?.textAlignment = NSTextAlignment.center
            cell.detailTextLabel?.textColor = .white
            
            if (indexPath.row) % 2 == 0{
                cell.backgroundColor = LightSecondaryColor2
            }else{
                cell.backgroundColor = PrimaryColor1
            }
            return cell
        }else{
            cell2.detailBtn.tag = indexPath.row
            cell2.selectionStyle = .none
            cell2.backgroundColor = LightSecondaryColor
            cell2.macAddressLabel.text = station_mac[indexPath.row].uppercased()
            cell2.macAddressLabel.textColor = .white
            cell2.ethernetImage.image = UIImage(named: "Ethernet-1")
            cell2.detailBtn.addTarget(self, action: #selector(toDetailPage0(_:)), for: .touchUpInside)
            return cell2
        }
        
    }
    /**
     進入在線裝置detail頁
     - Date: 08/25 Yang
    */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if StationTableView == tableView{
            //            print(stationInfoDetails[indexPath.row].station_mac)
            station_mac2 = station_mac[indexPath.row]
            station_rssi2 = station_rssi[indexPath.row]
            station_uplink2 = station_uplink[indexPath.row]
            station_downlink2 = station_downlink[indexPath.row]
            station_connected_band2 = station_connected_band[indexPath.row]

            let vc = getVC(storyboardName: "Main", identifier: "StationInfoViewController") as! StationInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    /**
     進入在線裝置detail頁(cell右邊箭頭用)
     - Date: 08/25 Yang
    */
    @objc func toDetailPage0(_ sender : UIButton){
        //        self.pageViewDelegate?.toDetailPage(device: stationInfoDetails[sender.tag])
        let point = sender.convert(CGPoint.zero, to: StationTableView)
        if let indexPath = StationTableView.indexPathForRow(at: point) {
            station_mac2 = station_mac[indexPath.row]
            station_rssi2 = station_rssi[indexPath.row]
            station_uplink2 = station_uplink[indexPath.row]
            station_downlink2 = station_downlink[indexPath.row]
            station_connected_band2 = station_connected_band[indexPath.row]
        }
        let vc = getVC(storyboardName: "Main", identifier: "StationInfoViewController") as! StationInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
