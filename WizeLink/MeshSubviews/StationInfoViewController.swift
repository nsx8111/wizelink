//
//  StationInfoViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/6/1.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct DetailInfo {
    var titleLabel: String = ""
    var detailLabel: String = ""
}
/**
 在線裝置detail頁
 - Date: 08/25 Yang
*/
class StationInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let backButton = UIButton()
    let bkView = UIImageView()
    let DetailInfoLabel = UILabel()
    private var DetailInfoTableView: UITableView!
    
    var detailInfos = [
        //"RSSI" 不在字典檔
        DetailInfo(titleLabel: localString(key: "MAC", table: .dataTitle
        ), detailLabel: station_mac2),
        DetailInfo(titleLabel: "RSSI", detailLabel: station_rssi2),
        //        DetailInfo(titleLabel: "上行鏈接", detailLabel: station_uplink2),
        DetailInfo(titleLabel: localString(key: "UPLINK", table: .dataTitle), detailLabel: station_uplink2),
        //        DetailInfo(titleLabel: "下行鏈接", detailLabel: station_downlink2),
        DetailInfo(titleLabel: localString(key: "DOWNLINK", table: .dataTitle), detailLabel: station_downlink2),
        //        DetailInfo(titleLabel: "連線頻段", detailLabel: station_connected_band2)
        DetailInfo(titleLabel: localString(key: "CONNECT_BAND", table: .dataTitle), detailLabel: station_connected_band2)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setViews(){
        bkView.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        DetailInfoLabel.backgroundColor = .clear
        DetailInfoLabel.text = localString(key: "DETAILS", table: .pageTitle)
        DetailInfoLabel.font = UIFont.boldSystemFont(ofSize: 20)
        DetailInfoLabel.textColor = .white
        DetailInfoLabel.textAlignment = .left
        self.view.addSubview(DetailInfoLabel)
        DetailInfoLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(backButton.snp.bottom).offset(loginLabelHeight)
            makes.width.equalTo(loginLabelWidth*5)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        DetailInfoTableView = UITableView()
        DetailInfoTableView.register(UITableViewCell.self, forCellReuseIdentifier: "DetailInfoTableViewCell")
        DetailInfoTableView.delegate = self
        DetailInfoTableView.dataSource = self
        DetailInfoTableView.bounces = false
        DetailInfoTableView.allowsSelection = true
        DetailInfoTableView.backgroundColor = LightSecondaryColor2
        DetailInfoTableView.rowHeight = fullScreenSize.height*0.08
        self.view.addSubview(DetailInfoTableView)
        DetailInfoTableView.snp.makeConstraints { (make) in
            make.top.equalTo(DetailInfoLabel.snp.bottom).offset(loginLabelHeight4)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(DetailInfoTableView.rowHeight * CGFloat(detailInfos.count))
            make.centerX.equalToSuperview()
        }
        DetailInfoTableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailInfos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "DetailInfoTableViewCell")
        
        cell.selectionStyle = .none
        cell.textLabel?.text = detailInfos[indexPath.row].titleLabel
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = PrimaryColor2
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
        cell.detailTextLabel?.text = detailInfos[indexPath.row].detailLabel
        cell.detailTextLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
        cell.detailTextLabel?.textColor = .white
        
        if (indexPath.row) % 2 == 0{
            cell.backgroundColor = LightSecondaryColor2
        }else{
            cell.backgroundColor = PrimaryColor1
        }
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
