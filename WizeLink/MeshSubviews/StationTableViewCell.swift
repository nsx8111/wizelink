//
//  StationTableViewCell.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/6/1.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
/**
 客製化detail頁cell
 - Date: 08/25 Yang
*/
class StationTableViewCell: UITableViewCell {
    
    var ethernetImage = UIImageView()
    var macAddressLabel = UILabel()
    var detailBtn = UIButton(type: .custom)
    let nextBtnImage = UIImage(named: "nextBtnBlue")?.withRenderingMode(.alwaysTemplate)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        ethernetImage.backgroundColor = .clear
        contentView.addSubview(ethernetImage)
        ethernetImage.snp.makeConstraints { (makes) in
            makes.left.equalToSuperview().offset(spacing3)
            makes.centerY.equalToSuperview()
            makes.height.width.equalTo(spacing3+spacing2)
        }
        
        macAddressLabel.text = "xx:xx:xx:xx:xx:xx:xx:xx"
        contentView.addSubview(macAddressLabel)
        macAddressLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(ethernetImage)
            makes.left.equalTo(ethernetImage.snp.right).offset(loginLabelHeight3)
            makes.height.equalTo(loginLabelHeight2)
        }
        
        detailBtn.setImage(nextBtnImage, for: .normal)
        detailBtn.tintColor = .white
        contentView.addSubview(detailBtn)
        detailBtn.snp.makeConstraints { (makes) in
            makes.centerY.equalToSuperview()
            makes.right.equalToSuperview().offset(-spacing3)
            makes.width.height.equalTo(fontSize)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
