//
//  CompatibilityViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/31.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct SupportType{
    var support_type: String = ""
    var version_type: String = ""
}

class CompatibilityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        SupportTypeTableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportTypeCell", for: indexPath)
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "SupportTypeCell")
        
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].support_type
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont(name: "Avenir", size:14)
        
        cell.detailTextLabel?.text = types[indexPath.row].version_type
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
        cell.detailTextLabel?.textColor = .white
        
        if (indexPath.row) % 2 == 0{
            cell.backgroundColor = LightSecondaryColor2
        }else{
            cell.backgroundColor = PrimaryColor1
        }
        return cell
    }
    
    func setTableView(){
        SupportTypeTableView = UITableView()
        SupportTypeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "SupportTypeCell")
        SupportTypeTableView.delegate = self
        SupportTypeTableView.dataSource = self
        SupportTypeTableView.bounces = false
        SupportTypeTableView.allowsSelection = true
        SupportTypeTableView.backgroundColor = LightSecondaryColor2
        SupportTypeTableView.rowHeight = fullScreenSize.height*0.067
        self.view.addSubview(SupportTypeTableView)
        
        SupportTypeTableView.snp.makeConstraints { (make) in
            make.top.equalTo(supportTypeLabel.snp.bottom).offset(spacing2)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(SupportTypeTableView.rowHeight*7) //預設先七列
            make.centerX.equalToSuperview()
        }
        
    }
    
    var types = [
        SupportType(support_type: "WAP-3512", version_type: "1.1.2.1"),
        SupportType(support_type: "WAP-3514", version_type: "1.0.0.3"),
        SupportType(support_type: "WAP-3516", version_type: "0.0.1.8"),
        SupportType(support_type: "WAP-7530", version_type: "0.3.0.8"),
        SupportType(support_type: "WAP-8000", version_type: "0.1.0.6"),
        SupportType(support_type: "WAP-8010", version_type: "1.0.0.5"),
        SupportType(support_type: "WAP-8020", version_type: "0.0.2.8")
    ]
    
    
    let dismissButton = UIButton()
    let bkgImageView2 = UIImageView()
    let compatibilityLabel = UILabel()
    let supportTypeLabel = UILabel()
    let versionLabel = UILabel()
    private var SupportTypeTableView: UITableView!
    var SupportTypes: [SupportType] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = PrimaryColor1
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        dismissButton.setTitle("關閉", for: .normal)
        dismissButton.backgroundColor = UIColor.clear
        dismissButton.setTitleColor(SecondaryColor3, for: .normal)
        dismissButton.titleLabel?.font = UIFont(name: "Bold", size: fontSize)
        dismissButton.isEnabled = true
        dismissButton.addTarget(self,action: #selector(self.dismissVC),for: .touchUpInside)
        self.view.addSubview(dismissButton)
        
        dismissButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight-15)
            makes.width.height.equalTo(loginLabelHeight*2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        compatibilityLabel.backgroundColor = .clear
        compatibilityLabel.attributedText = NSAttributedString(string: "相容性", attributes:
            nil)
        compatibilityLabel.font = UIFont(name: "Bold", size: fontSize+6)
        compatibilityLabel.textColor = .white
        compatibilityLabel.textAlignment = .left
        self.view.addSubview(compatibilityLabel)
        compatibilityLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(dismissButton.snp.bottom).offset(spacing/10)
            makes.width.equalTo(loginLabelWidth*2)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        supportTypeLabel.backgroundColor = .clear
        supportTypeLabel.attributedText = NSAttributedString(string: "支援機型", attributes:
            nil)
        supportTypeLabel.font = UIFont(name: "Bold", size: labelFontSize+4)
        supportTypeLabel.textColor = SecondaryColor3
        supportTypeLabel.textAlignment = .left
        self.view.addSubview(supportTypeLabel)
        supportTypeLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(compatibilityLabel.snp.bottom).offset(loginLabelHeight-5)
            makes.width.equalTo(loginLabelWidth*2)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing)
        }
        
        versionLabel.backgroundColor = .clear
        versionLabel.attributedText = NSAttributedString(string: "最低相容版本", attributes:
            nil)
        versionLabel.font = UIFont(name: "Bold", size: labelFontSize+4)
        versionLabel.textColor = SecondaryColor3
        versionLabel.textAlignment = .right
        self.view.addSubview(versionLabel)
        versionLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(supportTypeLabel)
            makes.width.equalTo(loginLabelWidth*4)
            makes.height.equalTo(loginLabelHeight)
            makes.right.equalToSuperview().offset(-spacing)
        }
        
        setTableView()
        
    }
    
    
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
