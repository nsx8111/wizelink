//
//  LoginViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/26.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON
/**
對應 Login Page 的 VC
- Date:07/13 Mike
*/
class LoginViewController: UIViewController,UITextFieldDelegate {
    /**
     返回用的 Button
    test for jazzy
    
    - Date:07/13 Mike
    */
    let backButton = UIButton()
    var rightViewBtn = UIButton(type: .system)
    let rightViewBtn2 = UIButton(type: .system)
    let editAccountButton = UIButton(type: .custom)
    //    let editAccountImage = UIImage(named: "istockphoto-538980940-1024x1024")?.withRenderingMode(.alwaysTemplate)
    let editAccountImage = UIImage(named: "create-outline")?.withRenderingMode(.alwaysTemplate)
    let compatibilityButton = UIButton(type: .custom)
    let compatibilityImage = UIImage(named: "exclamation_mark_PNG65")?.withRenderingMode(.alwaysTemplate)
    let loginButton = UIButton()
    let wifiLabel = UILabel()
    let bkgImageView = UIImageView()
    let bkgImageView2 = UIImageView()
    let loginLabel = UILabel()
    let WizeLinkLOGO = UIImageView()
    let WizeLinkLabel = UILabel()
    let loginSegmentedControl = UISegmentedControl(
        items: [localString(key: "GENERAL_LOGIN", table: .tabTitle),localString(key: "CLOUD_LOGIN", table: .tabTitle)])
    let accountLabel = UILabel()
    let passwordLabel = UILabel()
    var accountTextField = UITextField()
    var passwordTextField = CustomTextField()
    let moduleLabel = UILabel()
    let routerMode = UIButton()
    let routerButton = UIButton()
    let inputSwitch = UISwitch()
    let inputLabel = UILabel()
    let emailLabel = UILabel()
    var emailTextField = UITextField()
    let cloudPasswordLabel = UILabel()
    var cloudPasswordTextField = CustomTextField()
    
    var constraint1 : Constraint?
    var constraint2 : Constraint?
    var constraint3 : Constraint?
    var modeArray : [String] = [localString(key: "ROUTER_MODE", table: .dataTitle),localString(key: "BRIDGE_MODE", table: .dataTitle)]
    /**
    - viewDidLoad客製化statusBar的背景色 專案內各介面都是
    - setInitialViews setBackGroundView setLoginViews 產生初始畫面元件UI
    - currentDevice.deviceType 調整各裝置button跟Label的圓角跟字體大小
    - Date:07/23 Mike 08/24 Yang
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = PrimaryColor1
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        }
        UIApplication.shared.statusBarView?.backgroundColor = PrimaryColor1
        
        setBackGroundView()
        setLoginViews()
        setInitialViews()
        hideKeyboardWhenTappedAround()
        ///根據不同裝置調整元件尺寸
        switch currentDevice.deviceType {
        case .iPhone35:
            loginButton.layer.cornerRadius = 20
        case .iPhone40:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            emailLabel.font = UIFont.systemFont(ofSize: spacing2)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing2)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
            loginLabel.font = UIFont.systemFont(ofSize: spacing4)
            loginButton.titleLabel?.font = .systemFont(ofSize: fontSize)
            loginButton.layer.cornerRadius = 20
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/2.1
        case .iPhone47:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            emailLabel.font = UIFont.systemFont(ofSize: spacing2)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing2)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
            loginLabel.font = UIFont.systemFont(ofSize: spacing4)
            loginButton.titleLabel?.font = .systemFont(ofSize: fontSize)
            loginButton.layer.cornerRadius = 23
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/1.8
        case .iPhone55:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            emailLabel.font = UIFont.systemFont(ofSize: spacing2)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing2)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
            loginLabel.font = UIFont.systemFont(ofSize: spacing4)
            loginButton.titleLabel?.font = .systemFont(ofSize: fontSize)
            loginButton.layer.cornerRadius = 25
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/1.7
        case .iPhone60:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            emailLabel.font = UIFont.systemFont(ofSize: spacing2)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing2)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
            loginLabel.font = UIFont.systemFont(ofSize: spacing4)
            loginButton.titleLabel?.font = .systemFont(ofSize: fontSize)
            loginButton.layer.cornerRadius = 28
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/1.4
        case .iPad:
            accountLabel.font = UIFont.systemFont(ofSize: spacing3)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing3)
            emailLabel.font = UIFont.systemFont(ofSize: spacing3)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing3)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing3)
            loginLabel.font = UIFont.systemFont(ofSize: buttonSize)
            loginButton.titleLabel?.font = .systemFont(ofSize: loginLabelHeight)
            loginButton.layer.cornerRadius = 36
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/1.2
        default:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            emailLabel.font = UIFont.systemFont(ofSize: spacing2)
            cloudPasswordLabel.font = UIFont.systemFont(ofSize: spacing2)
            wifiLabel.font = UIFont.boldSystemFont(ofSize: spacing2)
            loginLabel.font = UIFont.systemFont(ofSize: spacing4)
            loginButton.titleLabel?.font = .systemFont(ofSize: fontSize)
            loginButton.layer.cornerRadius = 28
            loginSegmentedControl.layer.cornerRadius = loginSegmentedControl.bounds.height/1.4
        }
        ///自動顯示上次儲存的帳密
        let accountText =
            UserDefaults.standard.string(forKey: "accountText")
        accountTextField.text = accountText
        let passwordText =
            UserDefaults.standard.string(forKey: "passwordText")
        passwordTextField.text = passwordText
        
        ///監聽鍵盤 鍵盤出現要在輸入框輸入文字時 整個畫面會往上飄移 避免鍵盤遮住輸入框妨礙打字
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        serverType = .local
        changeHtms()
    }
    /**
    - 監聽鍵盤 鍵盤出現要在輸入框輸入文字時 整個畫面會往上飄移 避免鍵盤遮住輸入框妨礙打字
    - keyboardWillShow鍵盤出現的畫面原點位置 keyboardWillHide鍵盤收起的畫面原點位置
    - Date: 08/24 YangYang
     */
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            if inputSwitch.isOn == true && loginSegmentedControl.selectedSegmentIndex == 1 {
                let info = sender.userInfo!
                let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
                let kbSize = rect.size
                let activeField: UITextField? = [accountTextField, passwordTextField].first { $0.isFirstResponder }
                if activeField != nil {
                    self.view.frame.origin.y = -kbSize.height/1.5
                }
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    /**
    - 鍵盤輸入完觸控屏幕會自動收起
    - Date: 08/24 Yang
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        accountTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        cloudPasswordTextField.resignFirstResponder()
    }
    
    func setBackGroundView(){
        //UIColorbkgImageView
        bkgImageView.image = UIImage(named: "所有產品線稿")
        bkgImageView.alpha = 1
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        bkgImageView2.backgroundColor = PrimaryColor1
        bkgImageView2.alpha = 0
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
    }
    
    func setInitialViews(){
        WizeLinkLOGO.alpha = 1
        WizeLinkLOGO.backgroundColor = .clear
        WizeLinkLOGO.image = UIImage(named: "WizeLink-LOGO")
        self.view.addSubview(WizeLinkLOGO)
        WizeLinkLOGO.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.24)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.075)
        }
        
        WizeLinkLabel.alpha = 1
        WizeLinkLabel.backgroundColor = .clear
        WizeLinkLabel.text = "Be Wise to Link, it's WizeLink"
        WizeLinkLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize+1)
        WizeLinkLabel.textColor = .white
        WizeLinkLabel.textAlignment = .center
        self.view.addSubview(WizeLinkLabel)
        WizeLinkLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalTo(WizeLinkLOGO)
            makes.width.equalTo(fullScreenSize.width*0.8)
            makes.height.equalTo(spacing1*2.5)
            makes.top.equalTo(WizeLinkLOGO.snp.bottom).offset(spacing2)
        }
        
        loginButton.backgroundColor = SecondaryColor3
        loginButton.setTitle(localString(key: "LOGIN", table: .button), for: [])
        loginButton.titleLabel?.textColor = .white
        loginButton.isEnabled = true
        loginButton.addTarget(self,action: #selector(self.clickLoginButton),for: .touchUpInside)
        self.view.addSubview(loginButton)
        loginButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(fullScreenSize.height*0.23)
            makes.width.equalTo(fullScreenSize.width*0.6)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        wifiLabel.backgroundColor = .clear
        wifiLabel.numberOfLines = 0
        wifiLabel.text = localString(key: "WIFI_CHECK", table: .otherText)
        wifiLabel.textColor = .black
        wifiLabel.textAlignment = .center
        self.view.addSubview(wifiLabel)
        wifiLabel.snp.makeConstraints { (makes) in
            makes.left.right.equalToSuperview().inset(spacing3*WidthForDevices)
            makes.top.greaterThanOrEqualTo(loginButton.snp.bottom).offset(spacing3*1)
//            makes.bottom.equalToSuperview().offset(spacing3*3)
        }
        
    }
    
    func setLoginViews(){
        backButton.alpha = 0
        backButton.isHidden = true
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.clickBackButton),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)*HeightForScroll)
            makes.width.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        compatibilityButton.alpha = 0
        compatibilityButton.isHidden = true
        compatibilityButton.tintColor = SecondaryColor3
        compatibilityButton.setImage(compatibilityImage, for: .normal)
        compatibilityButton.isEnabled = true
        compatibilityButton.addTarget(self,action: #selector(self.clickCompatibility),for: .touchUpInside)
        self.view.addSubview(compatibilityButton)
        compatibilityButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
            makes.width.height.equalTo(loginLabelHeight-4)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        editAccountButton.setImage(editAccountImage, for: .normal)
        editAccountButton.tintColor = SecondaryColor3
        editAccountButton.alpha = 0
        editAccountButton.isHidden = true
        editAccountButton.isEnabled = true
        editAccountButton.addTarget(self,action: #selector(self.clickEditAccount),for: .touchUpInside)
        self.view.addSubview(editAccountButton)
        editAccountButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(compatibilityButton)
            makes.width.height.equalTo(loginLabelHeight+1)
            makes.right.equalTo(compatibilityButton.snp.left).offset(-spacing2)
        }
        
        loginLabel.alpha = 0
        loginLabel.backgroundColor = .clear
        //Mike
        loginLabel.tappable = true
        loginLabel.callback = {
//            self.forTest()
        }
       
        loginLabel.text = localString(key: "LOGIN", table: .button)
        loginLabel.textColor = .white
        loginLabel.textAlignment = .left
        self.view.addSubview(loginLabel)
        loginLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(backButton.snp.bottom).offset(spacing2*HeightForScroll)
            makes.width.equalTo(loginLabelWidth*3)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        loginSegmentedControl.isHidden = true
        loginSegmentedControl.alpha = 0
        loginSegmentedControl.selectedSegmentIndex = 0
        loginSegmentedControl.layer.borderWidth = 0.9
        loginSegmentedControl.layer.borderColor = SecondaryColor3.cgColor
        loginSegmentedControl.layer.masksToBounds = true
        loginSegmentedControl.backgroundColor = PrimaryColor1
        
        if #available(iOS 13.0, *) {
            loginSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white,.font: UIFont.systemFont(ofSize: segmentFontSize)], for: .selected)
            loginSegmentedControl.setTitleTextAttributes([.foregroundColor: SecondaryColor3,.font: UIFont.systemFont(ofSize: segmentFontSize)], for: .normal)
            loginSegmentedControl.selectedSegmentTintColor = SecondaryColor3
        } else {
            loginSegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white,.font: UIFont.systemFont(ofSize: segmentFontSize)], for: .selected)
            loginSegmentedControl.setTitleTextAttributes([.foregroundColor: SecondaryColor3,.font: UIFont.systemFont(ofSize: segmentFontSize)], for: .normal)
            loginSegmentedControl.tintColor = SecondaryColor3
        }
        
        loginSegmentedControl.addTarget(self, action: #selector(self.onChange2), for: .valueChanged)
        self.view.addSubview(loginSegmentedControl)
        loginSegmentedControl.snp.makeConstraints { (makes) in
            makes.top.equalTo(loginLabel.snp.bottom).offset(spacing2*HeightForScroll)
            makes.left.equalToSuperview().offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.047)
        }
      
        inputSwitch.alpha = 0
        inputSwitch.isHidden = true
        inputSwitch.isOn = false
        // 設置滑桿鈕的顏色
        inputSwitch.thumbTintColor = PrimaryColor2
        // 設置未選取時( off )的外觀顏色
        inputSwitch.tintColor = .lightGray
        // 設置選取時( on )的外觀顏色
        inputSwitch.onTintColor = TextColor2
        inputSwitch.addTarget(self,action: #selector(self.onChange),for: .valueChanged)
        self.view.addSubview(inputSwitch)
        inputSwitch.snp.makeConstraints { (makes) in
            makes.top.equalTo(loginSegmentedControl.snp.bottom).offset(spacing2*HeightForScroll)
            makes.right.equalTo(loginSegmentedControl.snp.right).offset(-spacing2)
        }
        
        inputLabel.alpha = 0
        inputLabel.backgroundColor = .clear
        inputLabel.text = localString(key: "ENTER_MANUALLY", table: .dataTitle)
        inputLabel.font = UIFont(name: "Helvetica-Light", size: spacing2)
        inputLabel.textColor = SecondaryColor3
        inputLabel.textAlignment = .left
        self.view.addSubview(inputLabel)
        inputLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(inputSwitch.snp.top)
            makes.left.equalTo(loginSegmentedControl).offset(spacing3)
            makes.right.greaterThanOrEqualTo(inputSwitch.snp.left).offset(-spacing1)
        }
        
        emailLabel.alpha = 0
        emailLabel.backgroundColor = .clear
        emailLabel.text = localString(key: "EMAIL_ADDRESS", table: .inputTitle)
        emailLabel.textColor = PrimaryColor2
        emailLabel.textAlignment = .left
        self.view.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(inputSwitch.snp.bottom).offset(spacing2*HeightForScroll)
            makes.left.equalToSuperview().offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        emailTextField.alpha = 0
        emailTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        emailTextField.textAlignment = .left
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .white
        emailTextField.backgroundColor = LightSecondaryColor
        emailTextField.layer.cornerRadius = 1.0
        emailTextField.keyboardType = .emailAddress
        emailTextField.delegate = self
        self.view.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(emailLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        cloudPasswordLabel.alpha = 0
        cloudPasswordLabel.backgroundColor = .clear
        cloudPasswordLabel.text = localString(key: "CLOUD_PASSWORD", table: .inputTitle)
        cloudPasswordLabel.textColor = PrimaryColor2
        cloudPasswordLabel.textAlignment = .left
        self.view.addSubview(cloudPasswordLabel)
        cloudPasswordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(emailTextField.snp.bottom).offset(spacing2*HeightForScroll)
            makes.left.equalToSuperview().offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        cloudPasswordTextField.alpha = 0
        cloudPasswordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        cloudPasswordTextField.textAlignment = .left
        cloudPasswordTextField.clearButtonMode = .whileEditing
        cloudPasswordTextField.returnKeyType = .done
        cloudPasswordTextField.textColor = .white
        cloudPasswordTextField.backgroundColor = LightSecondaryColor
        cloudPasswordTextField.layer.cornerRadius = 1.0
        cloudPasswordTextField.keyboardType = .namePhonePad
        cloudPasswordTextField.delegate = self
        cloudPasswordTextField.isSecureTextEntry = true
        self.view.addSubview(cloudPasswordTextField)
        cloudPasswordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(cloudPasswordLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysTemplate) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
            rightViewBtn2.setImage(smallImage, for: .normal)
            rightViewBtn2.addTarget(self,action: #selector(self.cloud_Security_Click),for: .touchUpInside)
//            rightViewBtn2.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
            cloudPasswordTextField.rightView = rightViewBtn2
            cloudPasswordTextField.rightViewMode = .always
        }
        
        accountLabel.alpha = 0
        accountLabel.backgroundColor = .clear
        accountLabel.text = localString(key: "LOGIN_NAME", table: .inputTitle)
        accountLabel.textColor = PrimaryColor2
        accountLabel.textAlignment = .left
        self.view.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { (makes) in
            constraint1 = makes.top.equalTo(inputSwitch.snp.bottom).offset(spacing2*HeightForScroll).constraint
            constraint2 = makes.top.equalTo(cloudPasswordTextField.snp.bottom).offset(spacing2*HeightForScroll).constraint
            constraint3 = makes.top.equalTo(inputLabel.snp.bottom).offset(0).constraint
            makes.left.equalToSuperview().offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()
        
        accountTextField.alpha = 0
        accountTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        accountTextField.textAlignment = .left
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .white
        accountTextField.backgroundColor = LightSecondaryColor
        accountTextField.layer.cornerRadius = 1.0
        accountTextField.keyboardType = .namePhonePad
        accountTextField.delegate = self
        self.view.addSubview(accountTextField)
        accountTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.alpha = 0
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = localString(key: "PASSWORD", table: .inputTitle)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2*HeightForScroll)
            makes.left.equalToSuperview().offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
        }
        
        passwordTextField.alpha = 0
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = LightSecondaryColor
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        ///密碼輸入框右側切換是否隱藏密碼icon
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysTemplate) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
            rightViewBtn.setImage(smallImage, for: .normal)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        moduleLabel.alpha = 0
        moduleLabel.backgroundColor = .clear
        moduleLabel.text = localString(key: "MODULE_TYPE", table: .dataTitle)
        moduleLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        moduleLabel.textColor = PrimaryColor2
        moduleLabel.textAlignment = .left
        self.view.addSubview(moduleLabel)
        moduleLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(labelWidth*2)
            makes.height.equalTo(spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        routerButton.isHidden = true
        routerButton.isEnabled = true
        routerButton.setImage(UIImage(named: "channel-blue"), for: .normal)
        routerButton.addTarget(self,action: #selector(self.clickRouter),for: .touchUpInside)
        self.view.addSubview(routerButton)
        routerButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(moduleLabel)
            makes.width.height.equalTo(spacing1*2)
            makes.right.equalTo(passwordTextField.snp.right).offset(-spacing1)
        }
        
        routerMode.isHidden = true
        routerMode.isEnabled = true
        routerMode.setTitle(localString(key: "ROUTER_MODE", table: .dataTitle), for: .normal)
        routerMode.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        routerMode.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        routerMode.addTarget(self,action: #selector(self.clickRouter),for: .touchUpInside)
        self.view.addSubview(routerMode)
        routerMode.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(moduleLabel)
            makes.width.equalTo(labelWidth*1.5)
            makes.height.equalTo(loginLabelHeight)
            makes.right.equalTo(routerButton.snp.left).offset(-spacing1/2)
        }
        
    }
    /**
    - 雲端登入時inputSwitch開啟狀態畫面佈局
    - Date: 08/24 Yang
     */
    func layout_isOn() {
        moduleLabel.alpha = 0
        routerMode.isHidden = true
        routerButton.isHidden = true
        emailLabel.alpha = 1
        emailTextField.alpha = 1
        cloudPasswordLabel.alpha = 1
        cloudPasswordTextField.alpha = 1
        emailTextField.isHidden = false
        cloudPasswordTextField.isHidden = false
        
        constraint1?.deactivate()
        constraint2?.activate()
        constraint3?.deactivate()
        
        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1*HeightForScroll)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2*HeightForScroll)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1*HeightForScroll)
        }
        
        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing1*2.0*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width*0.55)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
    }
    /**
    - 雲端登入時inputSwitch關閉狀態畫面佈局
    - Date: 08/24 Yang
     */
    func layout_isOff() {
        moduleLabel.alpha = 0
        routerMode.isHidden = true
        routerButton.isHidden = true
        emailLabel.alpha = 0
        emailTextField.alpha = 0
        cloudPasswordLabel.alpha = 0
        cloudPasswordTextField.alpha = 0
        emailTextField.isHidden = true
        cloudPasswordTextField.isHidden = true
        
        constraint1?.activate()
        constraint2?.deactivate()
        constraint3?.deactivate()
        
        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2*HeightForScroll)
            makes.right.equalToSuperview().offset(-spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing1*2*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width*0.55)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
    }
    /**
    - 一般登入與雲端登入切換後顯示的佈局
    - Date: 08/24 Yang
     */
    @objc func onChange2(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            clickGeneralLogin()
        }else{
            clickCloudLogin()
        }
        // 印出這個選項的文字
        //        print(sender.titleForSegment(at: sender.selectedSegmentIndex))
    }
    /**
    - 雲端登入時inputSwitch切換開關時的佈局
    - Date: 08/24 Yang
     */
    @objc func onChange(sender: UISwitch) {
        if sender.isOn == true{
            layout_isOn()
        } else {
            layout_isOff()
        }
    }
    /**
    - 密碼輸入框切換隱藏密碼icon變換功能
    - Date: 08/24 Yang
     */
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysTemplate) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysTemplate) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
    }
    /**
    - 雲端密碼輸入框切換隱藏密碼icon變換功能
    - Date: 08/24 Yang
     */
    @objc func cloud_Security_Click() {
        if cloudPasswordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysTemplate) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            cloudPasswordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysTemplate) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            cloudPasswordTextField.isSecureTextEntry = true
        }
    }
    /**
    - 點選路由器按鈕彈出視窗 目前已隱藏此功能
    - Date: 08/24 Yang
     */
    @objc func clickRouter(){
        let alertController = UIAlertController(title: "", message: localString(key: "MODULE_TYPE", table: .dataTitle), preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: localString(key: "CANCEL", table: .button), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        for i in modeArray {
            alertController.addAction(UIAlertAction(title: i, style: .default, handler: { alertController in
                self.routerMode.setTitle( i, for: .normal)
            }))
        }
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX+0, y: self.view.bounds.midY+0, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
    }
    /**
    - 點選一般登入後顯示的佈局
    - Date: 08/24 Yang
     */
    @objc func clickGeneralLogin(){
        serverType = .local
        changeHtms()
        
        emailLabel.alpha = 0
        emailTextField.alpha = 0
        cloudPasswordLabel.alpha = 0
        cloudPasswordTextField.alpha = 0
        emailTextField.isHidden = true
        cloudPasswordTextField.isHidden = true
        inputLabel.alpha = 0
        inputSwitch.alpha = 0
        inputSwitch.isHidden = true
        
        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()
        
        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2*HeightForScroll)
            makes.right.equalToSuperview().offset(-spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        self.loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(loginLabelHeight*1.8*HeightForScroll)
            makes.width.equalTo(fullScreenSize.width*0.72)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            loginButton.layer.cornerRadius = 20
        case .iPhone40:
            loginButton.layer.cornerRadius = 20
        case .iPhone47:
            loginButton.layer.cornerRadius = 23
        case .iPhone55:
            loginButton.layer.cornerRadius = 25
        case .iPhone60:
            loginButton.layer.cornerRadius = 28
        case .iPad:
            loginButton.layer.cornerRadius = 36
        default:
            loginButton.layer.cornerRadius = 28
        }
        
        backButton.isHidden = false
        backButton.alpha = 1
        wifiLabel.alpha = 0
        bkgImageView.alpha = 0
        bkgImageView2.alpha = 1
        loginLabel.alpha = 1
        WizeLinkLOGO.alpha = 0
        accountLabel.alpha = 1
        passwordLabel.alpha = 1
        accountTextField.alpha = 1
        accountTextField.isHidden = false
        passwordTextField.alpha = 1
        passwordTextField.isHidden = false
        moduleLabel.alpha = 0
//        moduleLabel.alpha = 1
//        routerMode.isHidden = false
//        routerButton.isHidden = false
        routerMode.isHidden = true
        routerButton.isHidden = true
    }
    /**
    點擊雲端登入標籤時，會做的處理（變換下方 views 為雲端登入模式）
    點選雲端登入後顯示的佈局
    - Date:07/13 Mike
    */
    @objc func clickCloudLogin(){
        
        switch currentDevice.deviceType {
        case .iPhone35:
            loginButton.layer.cornerRadius = 17
        case .iPhone40:
            loginButton.layer.cornerRadius = 17
        case .iPhone47:
            loginButton.layer.cornerRadius = 20
        case .iPhone55:
            loginButton.layer.cornerRadius = 22
        case .iPhone60:
            loginButton.layer.cornerRadius = 24
        case .iPad:
            loginButton.layer.cornerRadius = 30
        default:
            loginButton.layer.cornerRadius = 24
        }
        
        /// 把 serverType 改為 雲端
        serverType = .cloud
        //        changeHtms()
        //for test
        emailTextField.text = "mikelily@gmail.com"
        cloudPasswordTextField.text = "0000"
        
        inputLabel.alpha = 1
        inputSwitch.alpha = 1
        inputSwitch.isHidden = false
        
        if inputSwitch.isOn == true{
            layout_isOn()
        }else{
            layout_isOff()
        }
    }
    /**
    - 點擊進入相容性頁面
    - Date: 08/25 Yang
     */
    @objc func clickCompatibility(){
        let vc = CompatibilityViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    /**
    - 點擊進入修改登入資訊頁面
    - Date: 08/25 Yang
     */
    @objc func clickEditAccount(){
        let vc = EditAccountViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    /**
    - 點擊返回歡迎畫面setInitialViews
    - Date: 08/25 Yang
     */
    @objc func clickBackButton(){
        
        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()
        
        loginSegmentedControl.selectedSegmentIndex = 0
        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField.snp.bottom).offset(fullScreenSize.height*0.23)
            makes.width.equalTo(fullScreenSize.width*0.6)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            loginButton.layer.cornerRadius = 20
        case .iPhone40:
            loginButton.layer.cornerRadius = 20
        case .iPhone47:
            loginButton.layer.cornerRadius = 23
        case .iPhone55:
            loginButton.layer.cornerRadius = 25
        case .iPhone60:
            loginButton.layer.cornerRadius = 28
        case .iPad:
            loginButton.layer.cornerRadius = 36
        default:
            loginButton.layer.cornerRadius = 28
        }
        
        UIView.animate(withDuration: 0.55, animations: { [weak self] in
            self?.view.layoutIfNeeded()
            self?.backButton.isHidden = true
            self?.compatibilityButton.alpha = 0
            self?.compatibilityButton.isHidden = true
            self?.editAccountButton.alpha = 0
            self?.editAccountButton.isHidden = true
            self?.wifiLabel.alpha = 1
            self?.loginLabel.alpha = 0
            self?.backButton.alpha = 0
            self?.bkgImageView.alpha = 1
            self?.bkgImageView2.alpha = 0
            self?.WizeLinkLOGO.alpha = 1
            self?.WizeLinkLabel.alpha = 1
            self?.accountLabel.alpha = 0
            self?.passwordLabel.alpha = 0
            self?.accountTextField.alpha = 0
            self?.accountTextField.isHidden = true
            self?.passwordTextField.alpha = 0
            self?.passwordTextField.isHidden = true
            self?.moduleLabel.alpha = 0
            self?.routerMode.isHidden = true
            self?.routerButton.isHidden = true
            self?.inputSwitch.alpha = 0
            self?.inputSwitch.isHidden = true
            self?.inputLabel.alpha = 0
            self?.emailLabel.alpha = 0
            self?.cloudPasswordLabel.alpha = 0
            self?.emailTextField.alpha = 0
            self?.emailTextField.isHidden = true
            self?.cloudPasswordTextField.alpha = 0
            self?.cloudPasswordTextField.isHidden = true
            self?.loginSegmentedControl.alpha = 0
            self?.loginSegmentedControl.isHidden = true
        })
        
    }
    /**
    測試方便用，節省輸入時間
    - Warning:發布程式時，記得把這邊刪除
    - Date:07/13 Mike
    */
    func forTest(){
        accountTextField.text = "admin"
        passwordTextField.text = "admin"
    }
    /**
    點擊 Login Button 會做的處理
    - 如果此時 bkgImageView2.alpha == 1，代表為登入畫面
       + routerMode.title(for: .normal) == localString(key: "BRIDGE_MODE", table: .dataTitle)
           - 這邊我不太清楚是什麼情形，要請教 Yang
       + routerMode.title(for: .normal) != localString(key: "BRIDGE_MODE", table: .dataTitle)
           - 判斷 serverType 為本地還是雲端
           - serverType == .cloud，進行雲端登入處理
           - serverType != .cloud，進行本地登入處理（此時 serverType == .local）
    - 若是此時 bkgImageView2.alpha != 1，代表為歡迎畫面
       + 進行畫面 UI 的變化
    
    - Date:0713 Mike
    */
    @objc func clickLoginButton(){
        if bkgImageView2.alpha == 1{
            if routerMode.title(for: .normal) == localString(key: "BRIDGE_MODE", table: .dataTitle){
                print("rootViewController \(String(describing: UIApplication.shared.keyWindow?.rootViewController))")
                let vc = getVC(storyboardName: "Main", identifier: "SelectAccessPointViewController") as! SelectAccessPointViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if serverType == .cloud {
                    if self.emailTextField.text == "" || self.cloudPasswordTextField.text == ""{
                        print("雲端帳密輸入有誤,請確認帳密是否正確")
                        alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "LOGIN_BOTH_EMPTY", table: .alertMsg))
                    }
                    emailStr = self.emailTextField.text!
                    passStr = self.cloudPasswordTextField.text!
                    cloudLogin {
                        if cloudState{
                            cloudGetDevice {
                                print("cloudGetDevice done")
                                // Mike 非同步
                                if self.accountTextField.text == "" || self.passwordTextField.text == ""{
                                    print("帳密輸入有誤,請確認帳密是否正確")
                                    self.alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "LOGIN_FAILED", table: .alertMsg))
                                }
                                
                                user = self.accountTextField.text!
                                password = self.passwordTextField.text!
                                
                                print("user:\(user),password:\(password)")
                                
                                let actView : UIView = UIView()
                                self.setActView(actView: actView)
                                
                                //                                print("before getData")
                                //                                print("statusHtm:\(statusHtm)")
                                getData {
                                    actView.removeFromSuperview()
                                    
                                    print("serverStatus:\(serverStatus ?? false)")
                                    if serverStatus != true{
                                        //                                        print("登入失敗")
                                        self.alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "LOGIN_FAILED", table: .alertMsg))
                                    }else{
                                        //自動登入
                                        UserDefaults.standard.set(self.accountTextField.text,forKey: "accountText")
                                        UserDefaults.standard.set(self.passwordTextField.text,forKey: "passwordText")
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuView")
                                        vc!.modalPresentationStyle = .fullScreen
                                        guard let window = UIApplication.shared.keyWindow else {
                                            return
                                        }
                                        self.dismiss(animated: true, completion: nil)
                                        window.rootViewController = vc
                                        return
                                    }
                                }
                            }
                        }else{
                            //                            print("雲端登入失敗")
                            self.alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "LOGIN_FAILED", table: .alertMsg))
                        }
                    }
                }else{
                    if accountTextField.text == "" || passwordTextField.text == ""{
                        //                        print("帳密輸入有誤,請確認帳密是否正確")
                        alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "LOGIN_BOTH_EMPTY", table: .alertMsg))
                    }
                    
                    user = accountTextField.text!
                    password = passwordTextField.text!
                    
                    print("user:\(user),password:\(password)")
                    
                    let actView : UIView = UIView()
                    self.setActView(actView: actView)
                    
                    print("before getData")
                    print("statusHtm:\(statusHtm)")
                    getData {
                        actView.removeFromSuperview()
                        
                        print("serverStatus:\(serverStatus ?? false)")
                        if serverStatus != true{
                            print("登入失敗")
                            self.alertMsg(title: localString(key: "LOGIN_FAILED", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                        }else{
                            //自動登入
                            UserDefaults.standard.set(self.accountTextField.text,forKey: "accountText")
                            UserDefaults.standard.set(self.passwordTextField.text,forKey: "passwordText")
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuView")
                            vc!.modalPresentationStyle = .fullScreen
                            guard let window = UIApplication.shared.keyWindow else {
                                return
                            }
                            self.dismiss(animated: true, completion: nil)
                            window.rootViewController = vc
                            return
                        }
                    }
                }
            }
            
        }else{
            print("segment \(loginSegmentedControl.selectedSegmentIndex)")
            
            emailLabel.alpha = 0
            emailTextField.alpha = 0
            cloudPasswordLabel.alpha = 0
            cloudPasswordTextField.alpha = 0
            
            inputLabel.alpha = 0
            inputSwitch.alpha = 0
            inputSwitch.isHidden = true
            
            constraint3?.activate()
            constraint1?.deactivate()
            constraint2?.deactivate()
            
            accountTextField.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1*HeightForScroll)
                makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
                makes.height.equalTo(fullScreenSize.height*0.07)
            }
            passwordLabel.snp.updateConstraints { (makes) in
                makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2*HeightForScroll)
                makes.right.equalToSuperview().offset(-spacing2)
                makes.left.equalToSuperview().offset(spacing2)
            }
            passwordTextField.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1*HeightForScroll)
                makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
                makes.height.equalTo(fullScreenSize.height*0.07)
            }
            
            self.loginButton.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalTo(passwordTextField.snp.bottom).offset(loginLabelHeight*1.8*HeightForScroll)
                makes.width.equalTo(fullScreenSize.width*0.72)
                makes.height.equalTo(fullScreenSize.height*0.07)
            }
            
            UIView.animate(withDuration: 0.55, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                self?.backButton.isHidden = false
                self?.backButton.alpha = 1
                self?.compatibilityButton.alpha = 1
                self?.compatibilityButton.isHidden = false
                self?.editAccountButton.alpha = 1
                self?.editAccountButton.isHidden = false
                self?.wifiLabel.alpha = 0
                self?.bkgImageView.alpha = 0
                self?.bkgImageView2.alpha = 1
                self?.loginLabel.alpha = 1
                self?.WizeLinkLOGO.alpha = 0
                self?.WizeLinkLabel.alpha = 0
                self?.accountLabel.alpha = 1
                self?.passwordLabel.alpha = 1
                self?.accountTextField.alpha = 1
                self?.accountTextField.isHidden = false
                self?.passwordTextField.alpha = 1
                self?.passwordTextField.isHidden = false
                self?.moduleLabel.alpha = 0
//                self?.moduleLabel.alpha = 1
//                self?.routerMode.isHidden = false
//                self?.routerButton.isHidden = false
                self?.routerMode.isHidden = true
                self?.routerButton.isHidden = true
                self?.loginSegmentedControl.alpha = 1
                self?.loginSegmentedControl.isHidden = false
            })
            
        }
        
        //            } else {
        
        //                self.actView.isHidden = false
        
        //                loginTest(uid: self.accountTextField.text!,pwd: self.passwordTextField.text!){
        //                    //                self.actView.isHidden = true
        //                }
        //            }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

