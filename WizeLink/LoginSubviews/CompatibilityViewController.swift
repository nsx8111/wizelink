//
//  CompatibilityViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/31.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation

struct SupportType{
    var support_type: String = ""
    var version_type: String = ""
}
/**
 相容性頁
 - Date: 08/25 Yang
*/
class CompatibilityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        SupportTypeTableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportTypeCell", for: indexPath)
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "SupportTypeCell")
        
        cell.selectionStyle = .none
        cell.textLabel?.text = types[indexPath.row].support_type
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont(name: "Avenir", size:14)
        
        cell.detailTextLabel?.text = types[indexPath.row].version_type
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
        cell.detailTextLabel?.textColor = .white
        
        if (indexPath.row) % 2 == 0{
            cell.backgroundColor = LightSecondaryColor2
        }else{
            cell.backgroundColor = PrimaryColor1
        }
        return cell
    }
    
    func setTableView(){
        SupportTypeTableView = UITableView()
        SupportTypeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "SupportTypeCell")
        SupportTypeTableView.delegate = self
        SupportTypeTableView.dataSource = self
        SupportTypeTableView.bounces = false
        SupportTypeTableView.allowsSelection = true
        SupportTypeTableView.backgroundColor = LightSecondaryColor2
        SupportTypeTableView.rowHeight = fullScreenSize.height*0.067
        self.view.addSubview(SupportTypeTableView)
        SupportTypeTableView.snp.makeConstraints { (make) in
            make.top.equalTo(supportTypeLabel.snp.bottom).offset(spacing2)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(SupportTypeTableView.rowHeight * CGFloat(types.count))
            make.bottom.equalToSuperview().offset(-0)
        }
        SupportTypeTableView.separatorStyle = .none
    }
    
    var types = [
        SupportType(support_type: "WAP-3512", version_type: "1.1.2.1"),
        SupportType(support_type: "WAP-3514", version_type: "1.0.0.3"),
        SupportType(support_type: "WAP-3516", version_type: "1.0.2.2"),
        SupportType(support_type: "WAP-3518", version_type: "1.0.2.2"),
        SupportType(support_type: "WAP-7530", version_type: "1.0.4.1"),
        SupportType(support_type: "WAP-8000", version_type: "1.0.2.4"),
        SupportType(support_type: "WAP-8010", version_type: "1.0.2.4"),
        SupportType(support_type: "WAP-8020", version_type: "1.0.2.4"),
        SupportType(support_type: "WAP-8111", version_type: "1.0.3.4"),
        SupportType(support_type: "WAP-8121M", version_type: "1.0.3.4"),
        SupportType(support_type: "WAP-7531", version_type: "3.0.0.9")
    ]
    
    let closeButton = UIButton()
    let bkView = UIImageView()
    let compatibilityLabel = UILabel()
    let supportTypeLabel = UILabel()
    let versionLabel = UILabel()
    private var SupportTypeTableView: UITableView!
    var SupportTypes: [SupportType] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        setTableView()
    }
    
    func setViews(){
        bkView.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        closeButton.setTitle(localString(key: "CLOSE", table: .button), for: .normal)
        closeButton.backgroundColor = UIColor.clear
        closeButton.setTitleColor(SecondaryColor3, for: .normal)
        closeButton.titleLabel?.font = UIFont(name: "Bold", size: fontSize)
        closeButton.titleLabel?.textAlignment = .right
        closeButton.isEnabled = true
        closeButton.addTarget(self,action: #selector(self.dismissVC),for: .touchUpInside)
        self.view.addSubview(closeButton)
        closeButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(statusBarHeight)
            makes.height.equalTo(spacing3*2)
            makes.width.equalTo(labelWidth)
            makes.right.equalToSuperview().offset(-spacing1)
        }
        
        compatibilityLabel.backgroundColor = .clear
        compatibilityLabel.text = localString(key: "COMPATIBILITY", table: .headerTitle)
        compatibilityLabel.textColor = .white
        compatibilityLabel.textAlignment = .left
        self.view.addSubview(compatibilityLabel)
        compatibilityLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(closeButton.snp.bottom).offset(spacing2/10)
            makes.width.equalTo(loginLabelWidth*4)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        supportTypeLabel.backgroundColor = .clear
        supportTypeLabel.text = localString(key: "SUPPORTED_MODEL", table: .dataTitle)
        supportTypeLabel.textColor = SecondaryColor3
        supportTypeLabel.textAlignment = .left
        supportTypeLabel.numberOfLines = 0
        self.view.addSubview(supportTypeLabel)
        supportTypeLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(compatibilityLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo((fullScreenSize.width-loginLabelHeight)/2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        versionLabel.backgroundColor = .clear
        versionLabel.text = localString(key: "MIN_COMPATIBLE_VERSION", table: .dataTitle)
        versionLabel.textColor = SecondaryColor3
        versionLabel.textAlignment = .right
        versionLabel.numberOfLines = 0
        self.view.addSubview(versionLabel)
        versionLabel.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(supportTypeLabel)
            makes.width.equalTo((fullScreenSize.width-loginLabelHeight)/2)
            makes.right.equalToSuperview().offset(-spacing2)
        }

        switch currentDevice.deviceType {
        case .iPad:
            compatibilityLabel.font = UIFont.boldSystemFont(ofSize: fontSize+1)
            supportTypeLabel.font = UIFont.boldSystemFont(ofSize: fontSize)
            versionLabel.font = UIFont.boldSystemFont(ofSize: fontSize)
        default:
            compatibilityLabel.font = UIFont.boldSystemFont(ofSize: 18)
            supportTypeLabel.font = UIFont.boldSystemFont(ofSize: 17)
            versionLabel.font = UIFont.boldSystemFont(ofSize: 17)
        }
        
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
