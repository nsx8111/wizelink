//
//  SelectAccessPointViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/5/20.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
/**
 橋接器頁 暫時隱藏不執行
 - Date: 08/25 Yang
*/
class SelectAccessPointViewController: UIViewController {
    
    let backButton = UIButton()
    let reloadButton = UIButton(type: .custom)
    let reloadImage = UIImage(named: "reload-outline")?.withRenderingMode(.alwaysTemplate)
    let navigationBarView = UIImageView()
    let bkView = UIImageView()
    let navBarLabel = UILabel()
    let notConnectlabel = UILabel()
    let confirmBtn = UIButton()
    let actView : UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        detectDevice()
    }
    
    func detectDevice(){
        self.setActView(actView: self.actView)
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
            self.actView.removeFromSuperview()
            self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "NO_7531_FOUND", table: .alertMsg))
        }
    }
    
    func setViews(){
        bkView.backgroundColor = .white
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        reloadButton.setImage(reloadImage, for: .normal)
        reloadButton.tintColor = SecondaryColor3
        reloadButton.backgroundColor = UIColor.clear
        reloadButton.isEnabled = true
        reloadButton.addTarget(self,action: #selector(self.reloadVC),for: .touchUpInside)
        self.view.addSubview(reloadButton)
        reloadButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(backButton)
            makes.width.height.equalTo(loginLabelHeight/1.2)
            makes.right.equalToSuperview().offset(-spacing1)
        }
        
        navBarLabel.backgroundColor = .clear
        navBarLabel.text = localString(key: "SELECT_AP", table: .pageTitle)
        navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(statusBarHeight+spacing1)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        notConnectlabel.text = localString(key: "NO_AP_CONNECTED", table: .dataValue)
        notConnectlabel.font = UIFont.systemFont(ofSize: 16)
        notConnectlabel.textColor = .gray
        notConnectlabel.textAlignment = .center
        self.view.addSubview(notConnectlabel)
        notConnectlabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.centerY.equalToSuperview()
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        confirmBtn.backgroundColor = SecondaryColor3
        confirmBtn.layer.cornerRadius = 23
        confirmBtn.setTitle(localString(key: "OK", table: .button), for: [])
        confirmBtn.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
        confirmBtn.titleLabel?.textColor = .white
        confirmBtn.isEnabled = true
        confirmBtn.addTarget(self,action: #selector(self.clickConfirm),for: .touchUpInside)
        self.view.addSubview(confirmBtn)
        confirmBtn.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.width.equalTo(fullScreenSize.width*0.47)
            makes.height.equalTo(fullScreenSize.height*0.06)
            makes.bottom.equalToSuperview().offset(-loginLabelWidth)
        }
        
    }
    
    @objc func clickConfirm(){
        self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "SEARCH_AP", table: .alertMsg))
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func reloadVC(){
        detectDevice()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
