//
//  AddAccountViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/31.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Alamofire
/**
 修改登錄資訊頁
 - Date: 08/25 Yang
*/
class EditAccountViewController: UIViewController,UITextFieldDelegate {
    
    let closeButton = UIButton()
    let bkView = UIImageView()
    let editAccountLabel = UILabel()
    let accountLabel = UILabel()
    let accountTextField = UITextField()
    let passwordLabel = UILabel()
    let passwordTextField = CustomTextField()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = CustomTextField()
    let saveButton = UIButton()
    let rightViewBtn = UIButton(type: .system)
    let rightViewBtn2 = UIButton(type: .system)
    
    /// 存放 testAccount( ) 的 result
    var resultOfTest : Bool = false
    var tempUser = ""
    var tempPassword = ""
    
    /**
    - 監聽鍵盤 鍵盤出現要在輸入框輸入文字時 整個畫面會往上飄移 避免鍵盤遮住輸入框妨礙打字
    - Date: 08/25 Yang
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.userInterfaceIdiom == .phone {
            let info = sender.userInfo!
            let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
            let kbSize = rect.size
            let activeField: UITextField? = [passwordTextField2].first { $0.isFirstResponder }
            if activeField != nil {
                self.view.frame.origin.y = -kbSize.height/3
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    /**
    - 初始畫面
    - Date: 08/25 Yang
     */
    func setViews(){
        bkView.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        closeButton.setTitle(localString(key: "CLOSE", table: .button), for: .normal)
        closeButton.backgroundColor = UIColor.clear
        closeButton.setTitleColor(SecondaryColor3, for: .normal)
        closeButton.titleLabel?.font = UIFont(name: "Bold", size: fontSize)
        closeButton.isEnabled = true
        closeButton.addTarget(self,action: #selector(self.dismissVC),for: .touchUpInside)
        self.view.addSubview(closeButton)
        closeButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(statusBarHeight)
            makes.height.equalTo(spacing3*2)
            makes.width.equalTo(labelWidth)
            makes.right.equalToSuperview().offset(-spacing1)
        }
        
        editAccountLabel.backgroundColor = .clear
        editAccountLabel.text = localString(key: "Modify_LoginInfo", table: .srAndLoginInfo)
        editAccountLabel.font = UIFont(name: "Helvetica-Light", size: fontSize+2)
        editAccountLabel.textColor = .white
        editAccountLabel.textAlignment = .left
        // FOR TEST !!!
        editAccountLabel.tappable = true
        editAccountLabel.callback = {
            self.forTest()
        }
        self.view.addSubview(editAccountLabel)
        editAccountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(closeButton.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        accountLabel.backgroundColor = .clear
        accountLabel.text = localString(key: "LOGIN_NAME", table: .inputTitle)
        accountLabel.textColor = PrimaryColor2
        accountLabel.textAlignment = .left
        self.view.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(editAccountLabel.snp.bottom).offset(loginLabelHeight)
            makes.right.equalToSuperview().offset(-spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        accountTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        accountTextField.textAlignment = .left
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .white
        accountTextField.backgroundColor = LightSecondaryColor
        accountTextField.layer.cornerRadius = 1.0
        accountTextField.keyboardType = .namePhonePad
        accountTextField.delegate = self
        self.view.addSubview(accountTextField)
        accountTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing1)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.text = localString(key: "PASSWORD", table: .inputTitle)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = LightSecondaryColor
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing1)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
        passwordLabel2.text = localString(key: "ENTER_PASSWORD_AGAIN", table: .inputTitle)
        passwordLabel2.textColor = PrimaryColor2
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing2)
            makes.right.equalToSuperview().offset(-spacing2)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .white
        passwordTextField2.backgroundColor = LightSecondaryColor
        passwordTextField2.layer.cornerRadius = 1.0
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(spacing1)
            makes.width.equalTo(fullScreenSize.width-(spacing2)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        saveButton.backgroundColor = SecondaryColor3
        saveButton.layer.cornerRadius = 21
        saveButton.setTitle(localString(key: "SAVE", table: .button), for: [])
        saveButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: fontSize)
        saveButton.titleLabel?.textColor = .white
        saveButton.isEnabled = true
        saveButton.addTarget(self,action: #selector(self.clickSaveButton),for: .touchUpInside)
        self.view.addSubview(saveButton)
        saveButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField2.snp.bottom).offset(loginLabelHeight3)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            accountLabel.font = UIFont.systemFont(ofSize: spacing3)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing3)
            passwordLabel2.font = UIFont.systemFont(ofSize: spacing3)
        case .phone:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel2.font = UIFont.systemFont(ofSize: spacing2)
        default:
            accountLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel.font = UIFont.systemFont(ofSize: spacing2)
            passwordLabel2.font = UIFont.systemFont(ofSize: spacing2)
        }
        
        switch currentDevice.deviceType {
        case .iPhone35:
            saveButton.layer.cornerRadius = 17
        case .iPhone40:
            saveButton.layer.cornerRadius = 17
        case .iPhone47:
            saveButton.layer.cornerRadius = 20
        case .iPhone55:
            saveButton.layer.cornerRadius = 22
        case .iPad:
            saveButton.layer.cornerRadius = 30
        default:
            saveButton.layer.cornerRadius = 24
        }
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField2.isSecureTextEntry = true
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
            rightViewBtn.addTarget(self,action: #selector(self.Security_Click),for: .touchUpInside)
//            rightViewBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
            rightViewBtn.setImage(smallImage, for: .normal)
            passwordTextField.rightView = rightViewBtn
            passwordTextField.rightViewMode = .always
        }
        
        if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
            let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
            rightViewBtn2.addTarget(self,action: #selector(self.Security_Click2),for: .touchUpInside)
            rightViewBtn2.setImage(smallImage, for: .normal)
            passwordTextField2.rightView = rightViewBtn2
            passwordTextField2.rightViewMode = .always
        }
    }
    /**
    - 密碼輸入框切換隱藏密碼icon變換功能
    - Date: 08/25 Yang
     */
    @objc func Security_Click() {
        if passwordTextField.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn.setImage(smallImage, for: .normal)
            }
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    @objc func Security_Click2() {
        if passwordTextField2.isSecureTextEntry == true{
            if let image = UIImage(named: "eye-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = false
        }else{
            if let image = UIImage(named: "hide-b")?.withRenderingMode(.alwaysOriginal) {
                let smallImage = resizeImage(image: image, width: fullScreenSize.height*0.07/2.3)
                rightViewBtn2.setImage(smallImage, for: .normal)
            }
            passwordTextField2.isSecureTextEntry = true
        }
        
    }
    
    /// For Test
    func forTest(){
        // clean  userdefault data
        UserDefaults.standard.set("",forKey: "accountText")
        UserDefaults.standard.set("",forKey: "passwordText")
        self.alertMsg(title: "FOR TEST", msg: "clean  userdefault data")
    }
    /**
    - 鍵盤輸入完觸控屏幕會自動收起
    - Date: 08/24 Yang
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        accountTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        passwordTextField2.resignFirstResponder()
    }
    /**
    - 儲存按鈕
    - Date: 08/25 Yang
     */
    @objc func clickSaveButton(){
        print("clickSaveButton")
        user = UserDefaults.standard.string(forKey: "accountText") ?? ""
        password = UserDefaults.standard.string(forKey: "passwordText") ?? ""
        
        print("""
            user:\(user)
            password:\(password)
            """)
        //        //for test
        //        user = "admin"
        //        password = "admin"
        
        //用儲存的 user, password 去測試是否正確
        testAccount {
            print("testAccount done")
            print("resultOfTest:\(self.resultOfTest)")
            
            // 根據 resultOfTest 的值來決定後續動作
            if self.resultOfTest{
                // 若為 true 才會處理之後的更改步驟
                //                self.checkFormat()
                self.sendData()
            }else{
                // 若為 false 則跳出提示，並且返回上一頁
                // 或是之後可以調整流程，先輸入原先的帳密
                //TODO 這邊的提示訊息要改
                self.alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "INVALID", table: .alertTitle))
            }
        }
        //0716 mark for test fast
        //        checkFormat()
        //        sendData()
    }
    
    func checkFormat(){
        // 07/17 nil to ""
        if accountTextField.text != "" && passwordTextField.text != "" && passwordTextField2.text != ""{
            if passwordTextField.text != passwordTextField2.text{
                alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_DIFFERENT", table: .alertMsg))
            }else{
                
            }
        }else{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_BOTH_EMPTY", table: .alertMsg))
        }
        //        user = accountTextField.text!
        //        password = passwordTextField.text!
        tempUser = accountTextField.text!
        tempPassword = passwordTextField.text!
    }
    
    func sendData(){
        // 07/17 checkFormat in
        // 07/17 nil to ""
        if accountTextField.text != "" && passwordTextField.text != "" && passwordTextField2.text != ""{
            if passwordTextField.text != passwordTextField2.text{
                alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_PASSWORD_DIFFERENT", table: .alertMsg))
                return
            }
        }else{
            alertMsg(title: localString(key: "INVALID", table: .alertTitle), msg: localString(key: "LOGIN_BOTH_EMPTY", table: .alertMsg))
            return
        }
        //        user = accountTextField.text!
        //        password = passwordTextField.text!
        tempUser = accountTextField.text!
        tempPassword = passwordTextField.text!
        
        print("sendData")
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        
        // 07/17 user -> tempUser, password -> tempPassword
        let requestStr : String = "\(formPasswordSetupStr)?userName=\(tempUser)&newPass=\(tempPassword)"
        print("user:\(user),pwd:\(password)")
        
        let actView : UIView = UIView()
        self.setActView(actView: actView)
        
        AF.request(requestStr,headers: headers)
            .responseJSON { response in
                print("sent")
                print(requestStr)
                actView.removeFromSuperview()
                
                user = self.tempUser
                password = self.tempPassword
                UserDefaults.standard.set(user,forKey: "accountText")
                UserDefaults.standard.set(password,forKey: "passwordText")
                
                let alertController = UIAlertController(title: localString(key: "PASSWORD_CHANGED", table: .alertTitle), message: localString(key: "PASSWORD_CHANGE_COMPLETED", table: .alertMsg), preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
        }
    }
    /**
     - 因為 sendData( ) 裡面的 formPasswordSetupStr API 無法正確顯示是否更改成功
     + 如果修改成功，因為帳密已改，所以讀取網頁會回傳 401 無權限
     + 而如果送出的時候，沒有輸入帳密、或是帳密錯誤，一樣也是回傳 401 無權限
     - 所以多寫一個 testAccount( ) 來測試新的帳密是否有更改成功
     - 也可以在 sendData( ) 前，測試有無存到帳密（預設 admin/admin）
     
     - Date: 07/16 Mike
     */
    func testAccount(completion : @escaping ()->()){
        print("testAccount")
        print("""
            clientsHtm:\(clientsHtm)
            user:\(user)
            password:\(password)
            """)
        
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        
        AF.request(clientsHtm,headers: headers)
            .responseJSON { response in
                //                print(response.debugDescription)
                if response.data != nil{
                    print("response.data != nil")
                    if response.data!.count <= 200{
                        //                        serverStatus = false
                        //                        return false
                        completion()
                    }else{
                        //                        serverStatus = true
                        //                        let res = String(response.debugDescription)
                        //                        clientsHtmData = res
                        //                        return true
                        self.resultOfTest = true
                        completion()
                    }
                }else{
                    //                    print("response.data == nil")
                    //                    serverStatus = false
                    completion()
                    //                    return true
                }
                
        }
    }
    /**
    返回上一頁
    - Date: 08/25 Yang
     */
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
