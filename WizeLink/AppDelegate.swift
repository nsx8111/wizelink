//
//  AppDelegate.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/25.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SwiftUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    ///宣告window即可在ios12以下執行app
    var window: UIWindow?
    /**
     APP開啟時全域執行的代碼
     - Date: 08/26 Yang
    */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ///LaunchScreen畫面持續時間 畫面結束後則出現登入頁
        Thread.sleep(forTimeInterval: 1.8)
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        UIBarButtonItem.appearance().tintColor = SecondaryColor3

        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}
/**
 根據不同裝置來調整UI佈局尺寸
 - Date: 08/26 Yang
*/
extension UIDevice {
    //iPhone後面數字單位為吋 例如iPhone55為5.5吋手機
    enum DeviceType {
        case iPhone35
        case iPhone40
        case iPhone47
        case iPhone55
        case iPhone60
        case iPad
        case TV
        case unspecified
        case carPlay

        var isPhone: Bool {
            return [ .iPhone35, .iPhone40, .iPhone47, .iPhone55, .iPhone60 ].contains(self)
        }
    }
    
    var deviceType: DeviceType? {
        //計算不同裝置的屏幕長度 例如5.5吋的height為736
        let screenSize = UIScreen.main.bounds.size
        let height = max(screenSize.width, screenSize.height)
        print("deviceHeight \(height)")
        
        switch UIDevice.current.userInterfaceIdiom {
        case .tv:
            return .TV
        case .pad:
            switch height {
            case 1024:
                return .iPad
            case 1366:
                return .iPad
            default:
                return .iPad
            }
        case .phone:
            switch height {
            case 480:
                return .iPhone35
            case 568:  //iPhoneSE
                return .iPhone40
            case 667:  //iPhone8
                return .iPhone47
            case 736:  //iPhone6S PLUS
                return .iPhone55
            case 812:  //iPhoneXR
                return .iPhone60
            default:
                return .iPhone60
            }
        case .unspecified:
            return .unspecified
        case .carPlay:
            return .carPlay
        @unknown default:
            return nil
        }
    }
}
/**
 客製化狀態列背景色樣式
 - Date: 08/26 Yang
*/
extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 3848245
            let keyWindow = UIApplication.shared.connectedScenes
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows.first
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let height = keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
                let statusBarView = UIView(frame: height)
                statusBarView.tag = tag
                statusBarView.layer.zPosition = 999999
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}
/**
 取得當前裝置IP位址
 - Date: 08/26 Yang
*/
func getWiFiAddress() -> String? {
    var address : String?

    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return nil }
    guard let firstAddr = ifaddr else { return nil }

    // For each interface ...
    for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
        let interface = ifptr.pointee

        // Check for IPv4 or IPv6 interface:
        let addrFamily = interface.ifa_addr.pointee.sa_family
        if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

            // Check interface name:
            let name = String(cString: interface.ifa_name)
            if  name == "en0" {

                // Convert interface address to a human readable string:
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                            &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST)
                address = String(cString: hostname)
            }
        }
    }
    freeifaddrs(ifaddr)
    return address
}

//extension UITableView {
//  func reloadDataSmoothly() {
//    UIView.setAnimationsEnabled(false)
//    CATransaction.begin()
//    CATransaction.setCompletionBlock { () -> Void in
//      UIView.setAnimationsEnabled(true)
//    }
//    reloadData()
//    beginUpdates()
//    endUpdates()
//    CATransaction.commit()
//  }
//}

/**
 專案中尚未使用到此func
 - Date: 08/26 Yang
*/
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
/**
 改變icon的tintColor
 - Date: 08/26 Yang
*/
extension UIImage {
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}
/**
 切換側邊欄開合
 - Date: 08/26 Yang
*/
extension UIView {
    func menu() {
//        print("==== @UIView.menu() ====")
        var frame = self.superview?.frame
//        print("@menu() frame:\(String(describing: frame))")
        print("frame?.origin.x:\(String(describing: frame?.origin.x))")
        if frame?.origin.x == 0{
            frame?.origin.x = fullScreenSize.width * MenuWidth
            //            frame?.origin.x = 200
        } else{
            frame?.origin.x = 0
        }
        UIView.animate(withDuration: 0.25) {
            self.superview?.frame = frame!
        }
        print("self.superview?.frame:\(String(describing: self.superview?.frame))")
        
        let name = Notification.Name("ContentView_Notification")
        NotificationCenter.default.post(name: name, object: nil, userInfo:
         nil)
//        print("==== Leave UIView.menu() ====")
    }
    
    func slideByFinger(location: CGPoint, state: UIGestureRecognizer.State) {
        
        struct FirstTouch {
            static var location: CGPoint? = nil
        }
        
        var frame = (self.superview?.frame)!
        
        switch state {
        case .began:
            FirstTouch.location = location
            
        case .changed:
            frame.origin.x = location.x - FirstTouch.location!.x
            
            if frame.origin.x < 0 {
                frame.origin.x = 0
            } else if frame.origin.x > fullScreenSize.width*0.5 {
                frame.origin.x = fullScreenSize.width * MenuWidth
            }
            self.superview?.frame = frame
            
        default:
            if frame.origin.x > 40 {
                frame.origin.x = fullScreenSize.width * MenuWidth
            } else {
                frame.origin.x = 0
            }
            
            UIView.animate(withDuration: 0.1) {
                self.superview?.frame = frame
            }
        }
    }
}
/**
 警告視窗
 - Date: 08/26 Yang
*/
extension UIViewController {
    func alertMsg(title: String , msg: String) {
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: localString(key: "OK", table: .button), style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
}
/**
 讓字串字母變大寫
 - Date: 08/26 Yang
*/
extension String {
    func uppercased(at indices: [Int]) -> String {
        return enumerated().map { indices.contains($0.offset) ? $0.element.uppercased() : String($0.element) }.joined()
    }
}
/**
 取得Storyboard上的ViewController
 - Date: 08/26 Yang
*/
public func getVC(storyboardName:String,identifier:String)->UIViewController{
    return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: identifier)
}
/**
 取得完整的Mac位址
 - Date: 08/26 Yang
*/
public func getMacAddress(macStr: String) -> String {
    var str = macStr
    str.insert(":", at: str.index(str.startIndex, offsetBy: 2))
    str.insert(":", at: str.index(str.startIndex, offsetBy: 5))
    str.insert(":", at: str.index(str.startIndex, offsetBy: 8))
    str.insert(":", at: str.index(str.startIndex, offsetBy: 11))
    str.insert(":", at: str.index(str.startIndex, offsetBy: 14))
    return str.uppercased()
}
/**
 改變icon尺寸 目前專案中大部分為壓縮圖片用
 - Date: 08/26 Yang
*/
func resizeImage(image: UIImage, width: CGFloat) -> UIImage {
        let size = CGSize(width: width, height:
            image.size.height * width / image.size.width)
        let renderer = UIGraphicsImageRenderer(size: size)
        let newImage = renderer.image { (context) in
            image.draw(in: renderer.format.bounds)
        }
        return newImage
}
/**
 過濾掉陣列中所有重複的element
 專案中尚未使用到此func
 - Date: 08/26 Yang
*/
func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}
/**
 主要在調整TextField的rightViewButton樣式 偏移量跟tintColor
 - Date: 08/26 Yang
*/
class CustomTextField: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.frame = CGRect(x: fullScreenSize.width-(spacing2)*2-20*WidthForDevices, y: fullScreenSize.height*0.07/4, width: fullScreenSize.height*0.07/2.0, height: fullScreenSize.height*0.07/2.0)
                button.tintColor = .white
            }
        }
    }
}
class CustomTextField2: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.frame = CGRect(x: fullScreenSize.width-(spacing2+spacing3+0.5)*2-20*WidthForDevices, y: fullScreenSize.height*0.07/4, width: fullScreenSize.height*0.07/2, height: fullScreenSize.height*0.07/2)
                button.tintColor = .black
            }
        }
    }
}
class CustomTextField3: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.frame = CGRect(x: fullScreenSize.width-(spacing2+spacing3+0.5)*2-25*WidthForDevices, y: fullScreenSize.height*0.07/4, width: fullScreenSize.height*0.07/2, height: fullScreenSize.height*0.07/2)
                button.tintColor = .black
            }
        }
    }
}

//class extensionTextField: UITextField {
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//        UIApplication.shared.keyWindow?.addGestureRecognizer(tap)
//    }
//    @objc func dismissKeyboard() {
//        UIApplication.shared.keyWindow?.endEditing(true)
//    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textFieldResignFirstResponder()
//        UIApplication.shared.keyWindow?.endEditing(true)
//        return true
//    }
//    func textFieldResignFirstResponder() {
//        self.resignFirstResponder()
//    }
//}


/**
 SwiftUI預覽畫面用
 - Date: 08/26 Yang
*/
//@available(iOS 13.0, *)
//struct ViewControllerView: UIViewControllerRepresentable {
//    func makeUIViewController(context: Context) -> LoginViewController {
//        UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginViewController") as! LoginViewController
//    }
//    func updateUIViewController(_ uiViewController: LoginViewController, context: Context) {
//    }
//    typealias UIViewControllerType = LoginViewController
//}
//
//@available(iOS 13.0.0, *)
//struct ViewControllerView_Previews: PreviewProvider {
//    static var previews: some View {
//        ViewControllerView()
//    }
//}
