//
//  ServerDatas.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/4/10.
//  Copyright © 2020 Natalie. All rights reserved.

import Foundation
import Alamofire
/**
 API資料
 - Date: 08/25 Yang
*/
var statusHtmData : String = ""
var clientsHtmData : String = ""
var wizardHtmData : String = ""

var user : String = ""
var password : String = ""

enum serverUrl {
    case local
    case cloud
}
var serverType : serverUrl = .local
var serverUrlDict: [String: String] = [
    "local" : "http://192.168.10.1/",
    "cloud" : ""]
var machineUrl : String = "http://192.168.10.1/"

var statusHtm: String = "\(machineUrl)status.htm"
var clientsHtm: String = "\(machineUrl)clients.htm"
var wizardHtm: String = "\(machineUrl)wizard.htm"
var macfilterHtm: String = "\(machineUrl)mac_filter_app.htm"
var rebootRequestStr = "\(machineUrl)boafrm/formRebootCheck"
var formFilterStr = "\(machineUrl)boafrm/formFilter"
var formPasswordSetupStr = "\(machineUrl)boafrm/formPasswordSetup"
var formWirelessPageStr = "\(machineUrl)boafrm/formWirelessPage"
var formWirelessVapStr = "\(machineUrl)boafrm/formWirelessVap"

var serverStatus : Bool?

func changeHtms(){
    switch serverType {
    case .local:
        machineUrl = serverUrlDict["local"]!
    case .cloud:
        machineUrl = serverUrlDict["cloud"]!
    }
    
    print("@changeHtms machineUrl : \(machineUrl)")
    
    statusHtm = "\(machineUrl)status.htm"
    clientsHtm = "\(machineUrl)clients.htm"
    wizardHtm = "\(machineUrl)wizard.htm"
    macfilterHtm = "\(machineUrl)mac_filter_app.htm"
    
    rebootRequestStr = "\(machineUrl)boafrm/formRebootCheck"
    formFilterStr = "\(machineUrl)boafrm/formFilter"
    formPasswordSetupStr = "\(machineUrl)boafrm/formPasswordSetup"
    
    formWirelessPageStr = "\(machineUrl)boafrm/formWirelessPage"
    formWirelessVapStr = "\(machineUrl)boafrm/formWirelessVap"
}

/**
 登入後或重刷更新資料時使用
 - Date: 08/26 Yang
*/
func getData(completion : @escaping ()->()){
    ///四個API全部request完成後才會登入或跳出此function 故cc必須要累加到4
    var cc : Int = 0
    let headers: HTTPHeaders = [.authorization(username: user, password: password)]
    print("statusHtm:\(statusHtm)")
    AF.request(statusHtm,headers: headers)
        .responseJSON { response in
            let res = String(response.debugDescription)
            statusHtmData = res
            cc += 1
            if cc == 4{
                completion()
            }
    }
    
    AF.request(wizardHtm,headers: headers)
        .responseJSON { response in
            let res = String(response.debugDescription)
//            print(res)
            wizardHtmData = res
            cc += 1
            if cc == 4{
                completion()
            }
    }
    
    AF.request(macfilterHtm,headers: headers)
        .responseJSON { response in
            if case .failure(let error) = response.result {
                print("error_macfilter \(error.localizedDescription)")
            } else if case .success = response.result {
                print("success_macfilter \(response.result)")
            }
            let res = String(response.debugDescription)
            mac_filter_app_HtmData = res
            cc += 1
            if cc == 4{
                completion()
            }
            
    }

    AF.request(clientsHtm,headers: headers)
        .responseJSON { response in
            if response.data != nil{
                if response.data!.count <= 200{
                    serverStatus = false
                }else{
                    serverStatus = true
                    let res = String(response.debugDescription)
                    clientsHtmData = res
                }
                cc += 1
//                completion()
                if cc == 4{
                    completion()
                }
            }else{
                serverStatus = false
                cc += 1
//                completion()
                if cc == 4{
                    completion()
                }
            }
            
    }
}

/**
 for searchInfo
 撈API資料時使用
 - Date: 08/26 Yang
*/
func searchFunc(src: String,str1: String,str2: String) -> String{
    var cc2: [String] = [""]
    let cc1 = src.components(separatedBy: str1)
    // 07/21 Mike mark
//    print("cc1_count \(cc1.count)")
    
    if cc1.count >= 2 {
        cc2 = cc1[1].components(separatedBy: str2)
        // 07/21 Mike mark
//        print("cc2cc2 \(cc2[0])")
//        print("connect_Success")
    }else{
        print("connect_failure")
    }
    // 07/21 Mike mark
//    print("cc2cc2 \(cc2[0])")
    return cc2[0]
}


// 轉寫 java code For get channel data
func getChannelArray2_4G(regDomain: String) -> [String]{
//    var tempArray : [String] = []
    
    switch regDomain {
    case "1","2","11":
        return ["AUTO", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
    case "3","4","12","13","15":
        return ["AUTO", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"]
    case "5":
        return ["AUTO", "10", "11", "12", "13"]
    case "6","8","9","10","14","16":
        return ["AUTO", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"]
    case "7":
        return ["AUTO", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"]
    default:
        return []
    }
//    return tempArray
}

// Mike
// 轉寫 java code For get channel data
func getChannelArray5G(regDomain: String) -> [String]{
    
    switch regDomain {
    case "1":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "100", "104", "108", "112", "116", "136", "140", "149", "153", "157", "161", "165"]
    case "2":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "149", "153", "157", "161"]
    case "3","4","5","6","7":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "100", "104", "108", "112", "116", "120", "124", "128", "132", "136", "140"]
    case "8":
        return ["AUTO", "34", "38", "42", "46"]
    case "9":
        return ["AUTO", "36", "40", "44", "48"]
    case "10":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64"]
    case "11":
        return ["AUTO", "56", "60", "64", "100", "104", "108", "112", "116", "136", "140", "149", "153", "157", "161", "165"]
    case "12":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "132", "136", "140", "149", "153", "157", "161", "165"]
    case "13":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "149", "153", "157", "161", "165"]
    case "14","15":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "100", "104", "108", "112", "116", "136", "140", "149", "153", "157", "161", "165"]
    case "16":
        return ["AUTO", "36", "40", "44", "48", "52", "56", "60", "64", "100", "104", "108", "112", "116", "120", "124", "128", "132", "136", "140", "144", "149", "153", "157", "161", "165", "169", "173", "177"]
    default:
        return []
    }
}


//Mike 0506 先移過來 之後應該要 mark 起來禁用
//給"網際網路,路由器,裝置"三個分頁呼叫用的
//func rangeFunc(src: String,str1: String,str2:String)->String {
//    if let rangeA1:Range = src.range(of: str1){
//        if let rangeA2:Range = src.range(of: str2){
//            return String(src[rangeA1.upperBound ..< rangeA2.lowerBound ])
//        }else{
//            print("can't find : \(str2) @ \(src)")
//        }
//    }else{
//        print("can't find : \(str1) @ \(src)")
//    }
//    return "---"
//}



/**
 以下皆為假資料測試用 正式專案需mark掉下列變數
 - Date: 08/26 Yang
*/
let station_3516 : String = """

[
    ["station_mac": 787b8a454b3f, "station_downlink": 780, "station_rssi": 52, "station_connected_band": 5G, "station_uplink": 24],

    ["station_mac": 5499637b3953, "station_uplink": 24, "station_downlink": 144, "station_rssi": 76, "station_connected_band": 2G]
]

"""

//WAP-3516 example:
let testMesh_3516 : String = """
{
  "device_name": "EasyMesh_Device",
  "ip_addr": "192.168.5.1",
  "mac_address": "ac6fbb1f03ac",
  "neighbor_devices": [
    {
      "neighbor_mac": "ac6fbb1f03b5",
      "neighbor_name": "EasyMesh_Device",
      "neighbor_rssi": "35",
      "neighbor_band": "5G"
    }
  ],
  "station_info": [
    {
      "station_mac": "0cdd240380d6",
      "station_rssi": "62",
      "station_connected_band": "5G",
      "station_downlink": "292",
      "station_uplink": "6"
    }
  ],
  "child_devices": [
    {
      "device_name": "EasyMesh_Device",
      "ip_addr": "192.168.5.202",
      "mac_address": "ac6fbb1f03b5",
      "neighbor_devices": [
      ],
      "station_info": [
        {
          "station_mac": "abcdef123456",
          "station_rssi": "30",
          "station_connected_band": "2.4G",
          "station_downlink": "300",
          "station_uplink": "66"
        }
      ],
      "child_devices": []
    },
    {
      "device_name": "EasyMesh_Device",
      "ip_addr": "192.168.5.203",
      "mac_address": "ac6fbb1f03b8",
      "neighbor_devices": [
      ],
      "station_info": [],
      "child_devices": []
    }
  ]
}
"""


//WAP-7530 example:
let testMesh_7530 : String = """

{
"name": "Wizelink",
"role": "master",
    "mac": "AC:6F:BB:19:4B:D0",
    "ip": "192.168.1.100",
    "client": [
      {
        "name": "Wizelink",
        "role": "slave",
        "ip": "192.168.1.102",
        "mac": "AC:6F:BB:19:4B:D4",
        "link": "5G",
        "rssi": -69,
        "phyrate": 780,
        "client": [
          {
            "name": "Wizelink",
            "role": "slave",
            "ip": "192.168.1.103",
            "mac": "AC:6F:BB:19:4B:D8",
            "link": "5G",
            "rssi": -69,
            "phyrate": 780,
            "client": []
          },
          {
            "name": "TatungTe_19:4b:d7",
            "role": "client",
            "ip": "192.168.1.252",
            "mac": "AC:6F:BB:19:4B:D7",
            "link": "ethernet",
            "phyrate": 1000
          },
          {
            "name": "LAPTOP-RCSA94DL",
            "role": "client",
            "ip": "192.168.1.103",
            "mac": "00:E0:4C:68:18:64",
            "link": "ethernet",
            "phyrate": 1000
          }
        ]
      }
    ],
    "link": "ethernet",
    "phyrate": "1000"
  }

"""




var mac_filter_app_HtmData : String = ""

let mac_filter_app_tempHtmData : String = """
<script type="text/javascript">

var macfilter_buffer='';
var res = macfilter_buffer.split(">>>>>");
document.getElementById("demo").innerHTML = res;
"""


let mac_filter_app_tempHtmData1 : String = """
<script type="text/javascript">

var macfilter_buffer='f0:79:60:22:40:8e___>124___>1';
var res = macfilter_buffer.split(">>>>>");
document.getElementById("demo").innerHTML = res;
"""


let mac_filter_app_tempHtmData2 : String = """
<script type="text/javascript">

var macfilter_buffer='f0:79:60:22:40:8e___>124___>1>>>>>f0:79:60:c0:26:bd___>124___>2';
var res = macfilter_buffer.split(">>>>>");
document.getElementById("demo").innerHTML = res;
"""


let mac_filter_app_tempHtmData3 : String = """
<script type="text/javascript">

var macfilter_buffer='___>124___>1>>>>>f0:79:60:22:40:8e___>124___>2>>>>>f0:79:60:c0:26:bd___>124___>3';
var res = macfilter_buffer.split(">>>>>");
document.getElementById("demo").innerHTML = res;
"""




let tempClientsHtmData : String = """
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta http-equiv="Pragma" content="no-cache">
    <meta HTTP-equiv="Cache-Control" content="no-cache">
    <meta HTTP-EQUIV="Expires" CONTENT="Mon, 01 Jan 1990 00:00:01 GMT">

    <title>Active DHCP Client Table</title>
    <link href="style/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <blockquote>
        <h2>LAN Clients</h2>

        <table border=0 width="480" cellspacing=0 cellpadding=0>
            <tr>
                <font size=2>
                    This table shows the messages for all of clients.
            </tr>
            <tr>
                <hr size=1 noshade align=top>
            </tr>
        </table>


        <table border='1' width="80%" id="clients_table">
        </table>
        <input type="hidden" value="/clients.htm" name="submit-url">
        <p id="demo"></p>
        <p id="demo2"></p>
        <script type="text/javascript">
            var buffer ='--- 0.0.0.0 b0:6e:bf:d1:73:c3 0 ethernet off --->>>--- 0.0.0.0 00:05:1b:d2:0a:c7 0 ethernet off --->>>--- 0.0.0.0 a6:6f:bb:19:45:2c 0 ethernet off --->>>--- 0.0.0.0 e8:94:f6:e2:b4:d5 0 ethernet off --->>>--- 0.0.0.0 00:0c:29:11:9e:a6 0 ethernet off --->>>--- 0.0.0.0 ac:22:0b:b3:2b:13 0 ethernet off --->>>--- 0.0.0.0 04:92:26:4d:43:a8 0 ethernet off --->>>--- 0.0.0.0 74:d0:2b:97:6d:6f 0 ethernet off --->>>--- 0.0.0.0 44:8a:5b:ce:e0:99 0 ethernet off --->>>--- 0.0.0.0 38:d5:47:79:5a:af 0 ethernet off --->>>--- 0.0.0.0 00:0c:29:97:88:04 0 ethernet off --->>>--- 0.0.0.0 18:31:bf:6c:b7:72 0 ethernet off --->>>--- 0.0.0.0 cc:e1:7f:c4:da:0b 0 ethernet off --->>>--- 0.0.0.0 6c:e8:73:cb:06:7d 0 ethernet off --->>>--- 0.0.0.0 ac:1f:6b:02:c8:5e 0 ethernet off --->>>--- 0.0.0.0 fc:f5:28:da:5b:f4 0 ethernet off --->>>--- 0.0.0.0 4c:72:b9:9d:e7:a8 0 ethernet off --->>>--- 0.0.0.0 44:8a:5b:d2:37:27 0 ethernet off --->>>--- 0.0.0.0 d0:17:c2:be:fb:29 0 ethernet off --->>>--- 0.0.0.0 10:c3:7b:45:d1:4c 0 ethernet off --->>>--- 0.0.0.0 5c:45:27:b0:31:c1 0 ethernet off --->>>--- 0.0.0.0 00:e0:4c:88:11:b1 0 ethernet off --->>>--- 0.0.0.0 00:e0:4c:88:11:b2 0 ethernet off --->>>--- 0.0.0.0 2c:fd:a1:72:a5:37 0 ethernet off --->>>--- 0.0.0.0 00:e0:4c:88:11:a4 0 ethernet off --->>>--- 0.0.0.0 64:6e:69:6a:51:cb 0 ethernet off --->>>--- 0.0.0.0 00:e0:4c:88:11:a3 0 ethernet off --->>>--- 0.0.0.0 e8:40:f2:8b:cf:be 0 ethernet off --->>>--- 0.0.0.0 60:45:cb:a2:4f:a8 0 ethernet off --->>>--- 0.0.0.0 38:18:4c:7a:2e:97 0 ethernet off --->>>--- 0.0.0.0 84:a9:3e:58:24:dd 0 ethernet off --->>>Links-iPad-pro 192.168.10.11 78:7b:8a:45:4b:3f -345702400 wifi_5G on --->>>--- 0.0.0.0 9c:d6:43:ca:19:0c 0 ethernet off --->>>--- 0.0.0.0 70:85:c2:df:bf:8f 0 ethernet off --->>>--- 0.0.0.0 38:60:77:4b:dc:61 0 ethernet off --->>>--- 0.0.0.0 e8:37:7a:a4:c1:25 0 ethernet off --->>>--- 0.0.0.0 44:8a:5b:98:7e:98 0 ethernet off --->>>MacBook-Pro-2 192.168.10.10 00:e0:4c:68:0f:39 -446365696 ethernet on --->>>hayashikiiPhone 192.168.10.12 0c:d7:46:cc:67:39 963248128 ----- on ---';
    var res = buffer.split(">>>");
    document.getElementById("demo").innerHTML = res;

    var macfilter_buffer='';
    var res = macfilter_buffer.split(">>>>>");
    document.getElementById("demo2").innerHTML = res;
    var mesh_info='';//used for WPA-3514 APP 20200227
        </script>
    </blockquote>
</body>

</html>
"""

let tempStatusHtmData : String = """
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta HTTP-equiv="Cache-Control" content="no-cache">
    <meta HTTP-EQUIV="Expires" CONTENT="Mon, 01 Jan 1990 00:00:01 GMT">

    <title>Access Point Status</title>
    <link href="style/style.css" rel="stylesheet" type="text/css">
    <SCRIPT language=Javascript src="language_en.js"></SCRIPT>
    <script>
        var wlanmode, wlanclientnum;
var isAP = 0;
var isCTCOMSupport=1 ;
var lan_ip='192.168.10.1';
var lan_mask='255.255.255.0';
var lan_gateway='192.168.10.1';
var choice=2;
//wan
var wan_current='fmget_dhcp';
var wan_subMask='255.255.255.0';
var wan_ip='10.118.209.125';
var wan_gateway='10.118.209.254';
var wan_mac='00:e0:4c:88:11:a2';

function showWan()
{
    var MultiPppoeEnable= 0;
    var number = 0;
    var dhcptype = 1;
    var ipv6Enable = 0;
    
    var multipleWanEnable = 1;
    var wan1Enable = 1;
    var wan2Enable = 0 //getIndex("WanEnabled","second"); ;
    var wan3Enable = 0// getIndex("WanEnabled","third"); ;
    var wan4Enable = 0//getIndex("WanEnabled","forth"); ;
    
    if(multipleWanEnable)
    {
    
        if(wan1Enable)
        {
            document.write('\
             <tr>\
             <td width=100% colspan=2 class=\"tbl_title\">'+status_wan1_config+'</td>\
               </tr>');
      
          document.write('<tr bgcolor="#EEEEEE">\
            <td width=40%><font size=2><b>'+status_attain_ip+'</b></td>\
            <td width=60%><font size=2>'+wan_current+'</td>\
          </tr>\
          <tr bgcolor="#DDDDDD">\
            <td width=40%><font size=2><b>'+status_ip+'</b></td>\
            <td width=60%><font size=2>'+wan_ip+'</td>\
          </tr>\
          <tr bgcolor="#EEEEEE">\
            <td width=40%><font size=2><b>'+status_subnet_mask+'</b></td>\
            <td width=60%><font size=2>'+wan_subMask+'</td>\
          </tr>\
          <tr bgcolor="#DDDDDD">\
            <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
            <td width=60%><font size=2>'+wan_gateway+'</td>\
          </tr>\
          <tr bgcolor="#EEEEEE">\
            <td width=40%><font size=2><b>'+status_mac+'</b></td>\
            <td width=60%><font size=2>'+wan_mac+'</td>\
          </tr>');
        }
                
        
    }
    else
    {
        document.write('\
         <tr>\
         <td width=100% colspan=2 class=\"tbl_title\">'+status_wan_config+'</td>\
      </tr>');
      document.write('<tr bgcolor="#EEEEEE">\
        <td width=40%><font size=2><b>'+status_attain_ip+'</b></td>\
        <td width=60%><font size=2>'+fmget_ip_dhcp+'</td>\
      </tr>\
      <tr bgcolor="#DDDDDD">\
        <td width=40%><font size=2><b>'+status_ip+'</b></td>\
        <td width=60%><font size=2></td>\
      </tr>\
      <tr bgcolor="#EEEEEE">\
        <td width=40%><font size=2><b>'+status_subnet_mask+'</b></td>\
        <td width=60%><font size=2></td>\
      </tr>\
      <tr bgcolor="#DDDDDD">\
        <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
        <td width=60%><font size=2></td>\
      </tr>\
      <tr bgcolor="#EEEEEE">\
        <td width=40%><font size=2><b>'+status_mac+'</b></td>\
        <td width=60%><font size=2></td>\
      </tr>');
    }
    
    if(ipv6Enable)
    {

        if(multipleWanEnable)
        {
            var ipv6wan1Enable = +0;
            var ipv6wan2Enable = +0;
            var ipv6wan3Enable = +0;
            var ipv6wan4Enable = +0;

            document.write('\
             <tr>\
             <td width=100% colspan=2 class=\"tbl_title\">'+status_ipv6_lan+'</td>\
              </tr>');

              document.write('<tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                 <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                  <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                <td width=60%><font size=2></td>\
                 </tr>\
                  <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                <td width=60%><font size=2>00:e0:4c:88:11:a1</td>\
                  </tr>');

            if(ipv6wan1Enable)
            {

                document.write('\
                 <tr>\
                 <td width=100% colspan=2 class=\"tbl_title\">'+status_wan1_ipv6_config+'</td>\
                   </tr>');

                      document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_link+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');
                
                    document.write('<tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_conn+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');

                  document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                     <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                      <tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                    <td width=60%><font size=2></td>\
                     </tr>\
                      <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                    <td width=60%><font size=2>00:e0:4c:88:11:a2</td>\
                      </tr>');
            }
            
            if(ipv6wan2Enable)
            {
                document.write('\
                 <tr>\
                 <td width=100% colspan=2 class=\"tbl_title\">'+status_wan2_ipv6_config+'</td>\
                   </tr>');

                      document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_link+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');
                
                    document.write('<tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_conn+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');

                  document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                     <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                      <tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                    <td width=60%><font size=2></td>\
                     </tr>\
                      <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                    <td width=60%><font size=2>00:e0:4c:88:11:a2</td>\
                      </tr>');
            }
            
            if(ipv6wan3Enable)
            {
                document.write('\
                 <tr>\
                 <td width=100% colspan=2 class=\"tbl_title\">'+status_wan3_ipv6_config+'</td>\
                   </tr>');

                      document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_link+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');
                
                    document.write('<tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_conn+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');

                  document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                     <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                      <tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                    <td width=60%><font size=2></td>\
                     </tr>\
                      <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                    <td width=60%><font size=2>00:e0:4c:88:11:a2</td>\
                      </tr>');
            }
            
            if(ipv6wan4Enable)
            {
                document.write('\
                 <tr>\
                 <td width=100% colspan=2 class=\"tbl_title\">'+status_wan4_ipv6_config+'</td>\
                   </tr>');

                      document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_link+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');
                
                    document.write('<tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_conn+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>');

                  document.write('<tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                     <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                    <td width=60%><font size=2></td>\
                      </tr>\
                      <tr bgcolor="#EEEEEE">\
                    <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                    <td width=60%><font size=2></td>\
                     </tr>\
                      <tr bgcolor="#DDDDDD">\
                    <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                    <td width=60%><font size=2>00:e0:4c:88:11:a2</td>\
                      </tr>');
            }
        }else

        {
            document.write('\
             <tr>\
             <td width=100% colspan=2 class=\"tbl_title\">'+status_ipv6_lan+'</td>\
              </tr>');

              document.write('<tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                 <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                  <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                <td width=60%><font size=2></td>\
                 </tr>\
                  <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                <td width=60%><font size=2>00:e0:4c:88:11:a1</td>\
                  </tr>');
                  
            document.write('\
             <tr>\
             <td width=100% colspan=2 class=\"tbl_title\">'+status_ipv6_wan+'</td>\
              </tr>');
        
                  document.write('<tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_ipv6_link+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>');
            
                document.write('<tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_ipv6_conn+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>');

              document.write('<tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_ipv6_global_ip+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                 <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_ipv6_ll+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>\
                  <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_default_gw+'</b></td>\
                <td width=60%><font size=2></td>\
                 </tr>\
                <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_dns_server+'</b></td>\
                <td width=60%><font size=2></td>\
                </tr>\
                <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_mac+'</b></td>\
                <td width=60%><font size=2></td>\
                  </tr>');
        }
    }
}
    </script>

</head>

<body>
    <blockquote>

        <h2>
            <script>
                dw(status_access_point)
            </script>
        </h2>

        <table border=0 width="400" cellspacing=0 cellpadding=0>
            <tr>
                <td>
                    <font size=2>
                        <script>
                            dw(status_page_information)
                        </script>
                    </font>
                </td>
            </tr>



            <tr>
                <td>
                    <hr size=1 noshade align=top><br></td>
            </tr>
        </table>


        <table width=550 border=0">
            <tr>
                <td width=100% colspan="2" class="tbl_title">
                    <script>
                        dw(status_sys)
                    </script>
                </td>
            </tr>
            <tr bgcolor="#DDDDDD">
                <td width=40%>
                    <font size=2><b><script>dw(status_uptime)</script></b>
                </td>
                <td width=60%>
                    <font size=2>0day:0h:58m:10s
                </td>
            </tr>
            <!-- TTI John 20200213 for Solving the problem that Firmware Version is confusing -->
            <!--
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><script>dw(status_version)</script></b></td>
    <td width=60%><font size=2>v3.4T-CT-pre5</td>
  </tr>
  -->
            <tr>
                <td width="50%">
                    <font size=2><b id="Q5"><script>dw(status_version)</script></b>
                </td>
                <td width="50%">
                    <font size=2>0.0.0.8
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td width=40%>
                    <font size=2><b id="Q6"><script>dw(status_sn)</script></b>
                </td>
                <td width=60%>
                    <font size=2>0000000000
                </td>
            </tr>
            <tr bgcolor="#DDDDDD">
                <td width=40%>
                    <font size=2><b><script>dw(status_build_time)</script></b>
                </td>
                <td width=60%>
                    <font size=2>Thu Mar 26 14:49:51 KST 2020
                </td>
            </tr>
            <script>
                var wlan_num = 2;
     var isNewMeshUI =  0 ;
      var wlanMode =new Array();
      var networkType =new Array();
      var band=new Array();
      var ssid_drv=new Array();
      var channel_drv=new Array();
      var wep=new Array();
      var wdsEncrypt=new Array();
      var meshEncrypt=new Array();
      var bssid_drv=new Array();
      var clientnum=new Array();
      var state_drv=new Array();
      var rp_enabled=new Array();
    var rp_mode=new Array();
      var rp_encrypt=new Array();
      var rp_clientnum=new Array();
      var rp_ssid=new Array();
      var rp_bssid=new Array();
      var rp_state=new Array();
    var wlanDisabled=new Array();
    
    var mssid_num=4;
    
    var mssid_disable=new Array(wlan_num);
    var mssid_bssid_drv=new Array(wlan_num);
    var mssid_clientnum=new Array(wlan_num);
    var mssid_band=new Array(wlan_num);
    var mssid_ssid_drv=new Array(wlan_num);
    var mssid_wep=new Array(wlan_num);
    var mssid_psk=new Array(wlan_num);
    
    for(i=0; i<wlan_num; i++)
    {
        mssid_disable[i] = new Array(mssid_num);
        mssid_bssid_drv[i] = new Array(mssid_num);
        mssid_clientnum[i] = new Array(mssid_num);
        mssid_band[i] = new Array(mssid_num);
        mssid_ssid_drv[i] = new Array(mssid_num);
        mssid_wep[i] = new Array(mssid_num);
        mssid_psk[i] = new Array(mssid_num);

    }
      
    if (wlan_num > 0)
    {
        
        wlanMode[0] =0;
        networkType[0] =0;
        band[0] =76;
        ssid_drv[0] ='CLARO_WIF_5G_M';
        channel_drv[0] ='44';
        wep[0] ='WPA2';
        wdsEncrypt[0] ='Open';
         if (0 == 1)
            meshEncrypt[0] ='';
        else
            meshEncrypt[0] ='';

        bssid_drv[0] ='00:e0:4c:88:11:a3';
        clientnum[0] ='1';
        state_drv[0] =status_start;
        wlanDisabled[0] =0;

        if(0)
            rp_enabled[0] = 0;
        else
            rp_enabled[0] =0;
        rp_mode[0] =0;
        rp_encrypt[0] ='Open';
        rp_ssid[0] ='';
        rp_bssid[0] ='00:00:00:00:00:00';
        rp_state[0] =status_disabled;
        rp_clientnum[0] ='0';
        
        if (mssid_num > 0) {
            mssid_ssid_drv[0][0] ='EasyMeshBH-3DXJH3SKE';
            mssid_band[0][0] =76;
            mssid_disable[0][0] =0;
            mssid_bssid_drv[0][0] ='00:e0:4c:88:11:a4';
            mssid_clientnum[0][0] ='0';
            mssid_wep[0][0] ='WPA2';
            mssid_psk[0][0] ='78[ezbPZ_ql3I.uqzoz;rd^UYlY}wL';
        }

        if (mssid_num > 1) {
            mssid_ssid_drv[0][1] ='';
            mssid_band[0][1] =76;
            mssid_disable[0][1] =1;
            mssid_bssid_drv[0][1] ='00:00:00:00:00:00';
            mssid_clientnum[0][1] ='0';
            mssid_wep[0][1] ='Open';
            mssid_psk[0][1] ='';
        }
        
        if (mssid_num > 2) {
            mssid_ssid_drv[0][2] ='';
            mssid_band[0][2] =76;
            mssid_disable[0][2] =1;
            mssid_bssid_drv[0][2] ='00:00:00:00:00:00';
            mssid_clientnum[0][2] ='0';
            mssid_wep[0][2] ='Open';
            mssid_psk[0][2] ='';
        }
        
        if (mssid_num > 3) {
            mssid_ssid_drv[0][3] ='';
            mssid_band[0][3] =76;
            mssid_disable[0][3] =1;
            mssid_bssid_drv[0][3] ='00:00:00:00:00:00';
            mssid_clientnum[0][3] ='0';
            mssid_wep[0][3] ='Open';
            mssid_psk[0][3] ='';
        }

        if (mssid_num > 4) {
            mssid_ssid_drv[0][4] ='';
            mssid_band[0][4] =0;
            mssid_disable[0][4] =1;
            mssid_bssid_drv[0][4] ='';
            mssid_clientnum[0][4] ='0';
            mssid_wep[0][4] ='Open';
            mssid_psk[0][4] ='';
        }
        
        if (mssid_num > 5) {
            mssid_ssid_drv[0][5] ='0';
            mssid_band[0][5] =0;
            mssid_disable[0][5] =0;
            mssid_bssid_drv[0][5] ='0';
            mssid_clientnum[0][5] ='0';
            mssid_wep[0][5] ='0';
            mssid_psk[0][5] ='0';
        }
        
        if (mssid_num > 6) {
            mssid_ssid_drv[0][6] ='0';
            mssid_band[0][6] =0;
            mssid_disable[0][6] =0;
            mssid_bssid_drv[0][6] ='0';
            mssid_clientnum[0][6] ='0';
            mssid_wep[0][6] ='0';
            mssid_psk[0][6] ='0';
        }
        
        if (mssid_num > 7) {
            mssid_ssid_drv[0][7] ='0';
            mssid_band[0][7] =0;
            mssid_disable[0][7] =0;
            mssid_bssid_drv[0][7] ='0';
            mssid_clientnum[0][7] ='0';
            mssid_wep[0][7] ='0';
            mssid_psk[0][7] ='0';
        }
    } /*if (wlan_num > 0)*/

    if (wlan_num > 1)
    {
        
        wlanMode[1] =0;
        networkType[1] =0;
        band[1] =11;
        ssid_drv[1] ='CLARO_WIF_2.4G_M';
        channel_drv[1] ='11';
        wep[1] ='WPA2';
        wdsEncrypt[1] ='Open';
         if (0 == 1)
            meshEncrypt[1] ='';
        else
            meshEncrypt[1] ='';

        bssid_drv[1] ='00:e0:4c:88:11:b1';
        clientnum[1] ='0';
        state_drv[1] =status_start;
        wlanDisabled[1] =0;

        if(0)
            rp_enabled[0] = 0;
        else
        rp_enabled[1] =0;
        rp_mode[1] =0;
        rp_encrypt[1] ='Open';
        rp_ssid[1] ='';
        rp_bssid[1] ='00:00:00:00:00:00';
        rp_state[1] =status_disabled;
        rp_clientnum[1] ='0';

        if (mssid_num > 0) {
            mssid_ssid_drv[1][0] ='EasyMeshBH-C90w5R5DV';
            mssid_band[1][0] =11;
            mssid_disable[1][0] =0;
            mssid_bssid_drv[1][0] ='00:e0:4c:88:11:b2';
            mssid_clientnum[1][0] ='0';
            mssid_wep[1][0] ='WPA2';
            mssid_psk[1][0] ='XbFBsoH^q{Bjh6;7!ai79UZ.F(K.;Y';
        }

        if (mssid_num > 1) {
            mssid_ssid_drv[1][1] ='';
            mssid_band[1][1] =11;
            mssid_disable[1][1] =1;
            mssid_bssid_drv[1][1] ='00:00:00:00:00:00';
            mssid_clientnum[1][1] ='0';
            mssid_wep[1][1] ='Open';
            mssid_psk[1][1] ='';
        }
        
        if (mssid_num > 2) {
            mssid_ssid_drv[1][2] ='';
            mssid_band[1][2] =11;
            mssid_disable[1][2] =1;
            mssid_bssid_drv[1][2] ='00:00:00:00:00:00';
            mssid_clientnum[1][2] ='0';
            mssid_wep[1][2] ='Open';
            mssid_psk[1][2] ='';
        }
        
        if (mssid_num > 3) {
            mssid_ssid_drv[1][3] ='';
            mssid_band[1][3] =11;
            mssid_disable[1][3] =1;
            mssid_bssid_drv[1][3] ='00:00:00:00:00:00';
            mssid_clientnum[1][3] ='0';
            mssid_wep[1][3] ='Open';
            mssid_psk[1][3] ='';
        }

        if (mssid_num > 4) {
            mssid_ssid_drv[1][4] ='';
            mssid_band[1][4] =0;
            mssid_disable[1][4] =1;
            mssid_bssid_drv[1][4] ='';
            mssid_clientnum[1][4] ='0';
            mssid_wep[1][4] ='Open';
            mssid_psk[1][4] ='';
        }
        
        if (mssid_num > 5) {
            mssid_ssid_drv[1][5] ='0';
            mssid_band[1][5] =0;
            mssid_disable[1][5] =0;
            mssid_bssid_drv[1][5] ='0';
            mssid_clientnum[1][5] ='0';
            mssid_wep[1][5] ='0';
            mssid_psk[1][5] ='0';
        }
        
        if (mssid_num > 6) {
            mssid_ssid_drv[1][6] ='0';
            mssid_band[1][6] =0;
            mssid_disable[1][6] =0;
            mssid_bssid_drv[1][6] ='0';
            mssid_clientnum[1][6] ='0';
            mssid_wep[1][6] ='0';
        }
        
        if (mssid_num > 7) {
            mssid_ssid_drv[1][7] ='0';
            mssid_band[1][7] =0;
            mssid_disable[1][7] =0;
            mssid_bssid_drv[1][7] ='0';
            mssid_clientnum[1][7] ='0';
            mssid_wep[1][7] ='0';
        }
    } /*if (wlan_num > 1)*/
            
    for(i=0; i < wlan_num ; i++)
       {
        if(ssid_drv[i]=="")
            mssid_num=0;
        else
            mssid_num=4;
           
        if(wlanDisabled[i]==1 || ssid_drv[i]=="")
            continue;

        document.write('<tr bgcolor="#EEEEEE">\
        <td width=100% colspan="2" class=\"tbl_title\">');

        if (wlan_num > 1) {
            if(i==0)
                var num = "(5G)";
            else
                var num = "(2.4G)";

            document.write(status_wl + num + status_config);
        }
        else
            document.write(status_wireless_config);
        document.write('</td></tr>');
    
        document.write('<tr bgcolor="#EEEEEE">\
        <td width=40%><font size=2><b>'+status_mode+'</b></td>\
        <td width=60%><font size=2>' );
        /* mode */
        if(wlanMode[i] == 0)
            document.write(status_ap);
        if (wlanMode[i] == 1) {
            if (networkType[i] == 0)
                document.write( status_client_mode_inf);
            else
                document.write( status_client_mode_adhoc);
        }
        if ( wlanMode[i] == 2 )
            document.write(status_wds);
        if ( wlanMode[i] == 3 )
            document.write(status_ap_wds);
        /*#ifdef CONFIG_NEW_MESH_UI*/
        if( isNewMeshUI ==1 )
        {
            if ( wlanMode[i] == 4 )
                document.write(status_ap_mesh);
            if ( wlanMode[i] == 5 )
                document.write(status_mesh);
        }
        else
        {
            if ( wlanMode[i] == 4 )
                document.write(status_ap_mpp);
            if ( wlanMode[i] == 5 )
                document.write(status_mpp);
            if ( wlanMode[i] == 6 )
                document.write(status_map);
            if ( wlanMode[i] == 7 )
                document.write(status_mp);
        }
        /* band */
        document.write('</td>\
            <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_band+'</b></td>\
                <td width=60%><font size=2>');
        if (band[i] == 1)
               document.write( "2.4 GHz (B)");
        else
        if (band[i] == 2)
               document.write( "2.4 GHz (G)");
        else
        if (band[i] == 8)
           {
                if(channel_drv[i] > 14)
                    document.write( "5 GHz (N)");
                else
                   document.write( "2.4 GHz (N)");
           }
           else
           if (band[i] == 3)
               document.write( "2.4 GHz (B+G)");
        else
        if (band[i] == 4)
               document.write( "5 GHz (A)");
           else
           if (band[i] == 10)
               document.write( "2.4 GHz (G+N)");
           else
           if (band[i] == 11)
               document.write( "2.4 GHz (B+G+N)");
           else
        if (band[i] == 75)
            document.write( "2.4 GHz (B+G+N+AC)");
        else
           if (band[i] == 12)
               document.write( "5 GHz (A+N)");
        else
        if (band[i] == 15)
               document.write( "2.4GHz+5 GHz (A+B+G+N)");
           else
           if (band[i] == 76)
               document.write( "5 GHz (A+N+AC)");
           else
           if (band[i] == 64)
               document.write( "5 GHz (AC)");
           else
           if (band[i] == 72)
               document.write( "5 GHz (N+AC)");
           else
           if (band[i] == 68)
               document.write( "5 GHz (A+AC)");
        document.write('</tr>\
            <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_ssid+'</b></td>\
                <td width=60%><font size=2>');
        if (wlanMode[i] != 2) {
            document.write(ssid_drv[i]);
        }
        document.write('</td>\
            </tr>\
            <tr bgcolor="#DDDDDD">\
            <td width=40%><font size=2><b>'+status_channel_num+'</b></td>\
            <td width=60%><font size=2>'+channel_drv[i] +'</td>\
            </tr>\
            <tr bgcolor="#EEEEEE">\
            <td width=40%><font size=2><b>'+status_encrypt+'</b></td>\
            <td width=60%><font size=2>');
        if (wlanMode[i] == 0 || wlanMode[i] == 1)
                document.write(wep[i]);
        else if (wlanMode[i] == 2)
                document.write(wdsEncrypt[i]);
        else if (wlanMode[i] == 3)
                document.write(wep[i] + '(AP),  ' + wdsEncrypt[i] + '(WDS)');
        else if (wlanMode[i] == 4 || wlanMode[i] == 6)
                document.write(wep[i] + '(AP),  ' + meshEncrypt[i] + '(Mesh)');
            else if (wlanMode[i] == 5|| wlanMode[i] == 7)
                document.write( meshEncrypt[i] + '(Mesh)');

        document.write('</td>\
              </tr>\
              <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_bssid+'</b></td>\
                <td width=60%><font size=2>'+bssid_drv[i]+'</td>\
              </tr>');
        if (wlanMode[i]!=2) {    //2 means WDS mode
            document.write('<tr bgcolor="#EEEEEE">\n');
            if (wlanMode[i]==0 || wlanMode[i]==3 || wlanMode[i]==4) {
                document.write("<td width=40%%><font size=2><b>"+status_assoc_cli+"</b></td>\n");
                document.write("<td width=60%%><font size=2>"+clientnum[i]+"</td></tr>");
            }
            else {
                document.write("<td width=40%%><font size=2><b>"+status_state+"</b></td>\n");
                document.write('<td width=60%%><font size=2>'+state_drv[i]+'</td></tr>');
            }
            }

        /* mesh does not support virtual ap */
        if (!wlanDisabled[i] && (wlanMode[i]==0 || wlanMode[i]==3 )) {
            for (idx=0; idx<mssid_num; idx++) {
                if (!mssid_disable[i][idx]) {
                    document.write('\
                        <tr bgcolor="#EEEEEE">\
                        <td width=100% colspan="2" class=\"tbl_title\"><font color="#FFFFFF" size=2>\
                        <b>'+status_vir_ap+(idx+1)+" "+status_config+'</td>\
                        </tr>\
                        <tr bgcolor="#EEEEEE">\
                            <td width=40%><font size=2><b>'+status_band+'</b></td>\
                            <td width=60%><font size=2>');
                    if (mssid_band[i][idx] == 1)
                           document.write( "2.4 GHz (B)");
                    else if (mssid_band[i][idx] == 2)
                           document.write( "2.4 GHz (G)");
                    else if (mssid_band[i][idx] == 8){
                        if(channel_drv[i] > 14)
                            document.write( "5 GHz (N)");
                        else
                               document.write( "2.4 GHz (N)");
                       }
                    else if (mssid_band[i][idx] == 3)
                           document.write( "2.4 GHz (B+G)");
                    else if (mssid_band[i][idx] == 4)
                           document.write( "5 GHz (A)");
                    else if (mssid_band[i][idx] == 10)
                           document.write( "2.4 GHz (G+N)");
                    else if (mssid_band[i][idx] == 11)
                           document.write( "2.4 GHz (B+G+N)");
                    else if (mssid_band[i][idx] == 12)
                        document.write( "5 GHz (A+N)");
                    else if (mssid_band[i][idx] == 75)
                        document.write( "2.4 GHz (B+G+N+AC)");
                    else if (mssid_band[i][idx] == 76)
                        document.write( "5 GHz (A+N+AC)");
                    else if (mssid_band[i][idx] == 64)
                        document.write( "5 GHz (AC)");
                    else if (mssid_band[i][idx] == 68)
                        document.write( "5 GHz (A+AC)");
                    else if (mssid_band[i][idx] == 72)
                        document.write( "5 GHz (N+AC)");
    
                    document.write('</td></tr>\
                        <tr bgcolor="#DDDDDD">\
                            <td width=40%><font size=2><b>'+status_ssid+'</b></td>\
                            <td width=60%><font size=2>');
                    document.write(mssid_ssid_drv[i][idx]+'</td>');

                    document.write('</tr>\
                        <tr bgcolor="#EEEEEE">\
                        <td width=40%><font size=2><b>'+status_encrypt+'</b></td>\
                        <td width=60%><font size=2>'+mssid_wep[i][idx]+'</td>');
                    document.write('</tr>\
                            <tr bgcolor="#DDDDDD">\
                            <td width=40%><font size=2><b>Pre-Shared Key</b></td>\
                            <td width=60%><font size=2>'+mssid_psk[i][idx]+'</td>');//id ="Q40'+ i + idx +'"
                    document.write('</tr>\
                        <tr bgcolor="#EEEEEE">\
                        <td width=40%><font size=2><b>'+status_bssid+'</b></td>\
                        <td width=60%><font size=2>'+mssid_bssid_drv[i][idx]+'</td>');
                
                    document.write('</tr>\
                        <tr bgcolor="#DDDDDD">\
                        <td width=40%%><font size=2><b>'+status_assoc_cli+'</b></td>\
                        <td width=60%%><font size=2>'+mssid_clientnum[i][idx]+'</td></tr>');
                }
            }
        }

        //    document.write('</table>\
        if (rp_enabled[i])    // start of repeater
        {
            document.write('<tr bgcolor="#EEEEEE">\
                <td width=100% colspan="2" class=\"tbl_title\">');

            if (wlan_num > 1) {
                if(i==0)
                    var num = "(5G)";
                else
                    var num = "(2.4G)";

                document.write(status_wl + num + status_repater_config);
            }
            else
                document.write(status_wireless_repater_config);
            document.write('</td></tr>');

            document.write('<tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_mode+'</b></td>\
                <td width=60%><font size=2>' );
            /* mode */
            if(rp_mode[i] == 0)
                document.write("AP");
            else
                document.write( status_client_mode_inf);
            document.write('</td>\
                </tr>\
                <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_ssid+'</b></td>\
                <td width=60%><font size=2>'+rp_ssid[i] +'</td>\
                </tr>\
                <tr bgcolor="#EEEEEE">\
                <td width=40%><font size=2><b>'+status_encrypt+'</b></td>\
                <td width=60%><font size=2>'+rp_encrypt[i] +'</td>\
                </tr>\
                <tr bgcolor="#DDDDDD">\
                <td width=40%><font size=2><b>'+status_bssid+'</b></td>\
                <td width=60%><font size=2>'+rp_bssid[i] +'</td>\
                </tr>');
            document.write('<tr bgcolor="#EEEEEE">\n');
            if (rp_mode[i]==0 || rp_mode[i]==3) {
                document.write("<td width=40%%><font size=2><b>"+status_assoc_cli+"</b></td>\n");
                document.write("<td width=60%%><font size=2>"+rp_clientnum[i]+"</td></tr>");
            }
            else {
                document.write("<td width=40%%><font size=2><b>"+status_state+"</b></td>\n");
                document.write('<td width=60%%><font size=2>'+rp_state[i]+'</td></tr>');
            }
        }    // end of repeater
    }//end of wlan_num for
            </script>


            <tr>
                <td width=100% colspan="2" class="tbl_title">
                    <script>
                        dw(status_tcpip_config);
                    </script>
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td width=40%>
                    <font size=2><b><script>dw(status_attain_ip);</script></b>
                </td>
                <td width=60%>
                    <font size=2><span id="lan_current"><script>dw(fmget_ip)</script></span>
                </td>
            </tr>

            <tr bgcolor="#DDDDDD">
                <td width=40%>
                    <font size=2><b><script>dw(status_ip);</script></b>
                </td>
                <td width=60%>
                    <font size=2>
                        <script>
                            document.write(lan_ip);
                        </script>
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td width=40%>
                    <font size=2><b><script>dw(status_subnet_mask);</script></b>
                </td>
                <td width=60%>
                    <font size=2>
                        <script>
                            document.write(lan_mask);
                        </script>
                </td>
            </tr>
            <tr bgcolor="#DDDDDD">
                <td width=40%>
                    <font size=2><b><script>dw(status_default_gw)</script></b>
                </td>
                <td width=60%>
                    <font size=2>
                        <script>
                            document.write(lan_gateway);
                        </script>
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td width=40%>
                    <font size=2><b><script>dw(status_dhcp_server);</script></b>
                </td>
                <td width=60%>
                    <font size=2>
                        <SCRIPT>
                            if ( choice == 0 ) document.write(status_disabled);
            if ( choice == 2 ) document.write(status_enabled);
            if ( choice == 15 ) document.write(status_auto);
            if ( choice == 19 ) document.write(status_auto);
                        </SCRIPT>
                </td>
            </tr>

            <tr bgcolor="#DDDDDD">
                <td width=40%>
                    <font size=2><b><script>dw(status_mac)</script></b>
                </td>
                <td width=60%>
                    <font size=2>00:e0:4c:88:11:a1
                </td>
            </tr>



            <SCRIPT>
                if(isAP == 0)
        showWan();
            </SCRIPT>


        </table>
        <br>
</blockquote>
</body>

</html>
"""



//並聯裝置5
let tempWizardHtmData_3 = """
/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device1","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device22","ip_addr":"192.168.10.10","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45XXXX","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bYYYY","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bvvvv","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device44","ip_addr":"192.168.10.11","mac_address":"00e04c8844a4","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45ZZZZ","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bQQQQ","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device55","ip_addr":"192.168.10.13","mac_address":"00e04c8844a5","neighbor_devices":[],"station_info":[],"child_devices":[]}]},{"device_name":"EasyMesh_Device33","ip_addr":"192.168.10.12","mac_address":"00e04c8833a3","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45WWWW","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bAAAA","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""

//串聯裝置5
let tempWizardHtmData_4 = """
/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device3","ip_addr":"192.168.10.1","mac_address":"00e04c8833a3","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device44","ip_addr":"192.168.10.11","mac_address":"00e04c8844a4","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45ZZZZ","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bQQQQ","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device55","ip_addr":"192.168.10.13","mac_address":"00e04c8844a5","neighbor_devices":[],"station_info":[],"child_devices":[]}]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""


//裝置4
let tempWizardHtmData_7 = """
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bnnnn","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bcccc","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device44","ip_addr":"192.168.10.11","mac_address":"00e04c8844a4","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45ZZZZ","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bQQQQ","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device55","ip_addr":"192.168.10.13","mac_address":"00e04c8844a5","neighbor_devices":[],"station_info":[],"child_devices":[]},{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"}],"child_devices":[]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""


//裝置5 stationinfo 10
let tempWizardHtmData_6 = """
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637b392d","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637b39bd","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bqqqq","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bwwww","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bkkkk","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bpppp","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bnnnn","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bcccc","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device44","ip_addr":"192.168.10.11","mac_address":"00e04c8844a4","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45ZZZZ","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bQQQQ","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device55","ip_addr":"192.168.10.13","mac_address":"00e04c8844a5","neighbor_devices":[],"station_info":[],"child_devices":[]},{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device6","ip_addr":"192.168.10.99","mac_address":"00e04c8833a6","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""


//裝置5
let tempWizardHtmData_5 = """
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device3","ip_addr":"192.168.10.1","mac_address":"00e04c8833a3","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device44","ip_addr":"192.168.10.11","mac_address":"00e04c8844a4","neighbor_devices":[],"station_info":[{"station_mac":"787b8a45ZZZZ","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bQQQQ","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device55","ip_addr":"192.168.10.13","mac_address":"00e04c8844a5","neighbor_devices":[],"station_info":[],"child_devices":[]},{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device6","ip_addr":"192.168.10.99","mac_address":"00e04c8833a6","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""

//裝置2
let tempWizardHtmData_8 = """
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device11","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454bXX","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device22","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
"""

//裝置1
let tempWizardHtmData_0 = """
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device1","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454bXX","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"}],"child_devices":[]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
"""


//串聯
let tempWizardHtmData_2 = """
/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device1","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454bXX","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637bnnnn","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"},{"station_mac":"5499637bcccc","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device3","ip_addr":"192.168.10.1","mac_address":"00e04c8833a3","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
"""


//並聯
let tempWizardHtmData_1 = """

<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta HTTP-equiv="Cache-Control" content="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="Mon, 01 Jan 1990 00:00:01 GMT">

<title>Setup Wizard</title>
<script type="text/javascript" src="util_gw.js"></script>
<SCRIPT language=Javascript src="language_en.js"></SCRIPT>
<link href="style/style.css" rel="stylesheet" type="text/css">
<style>
p{
    color:#115ba6;
    font-weight: 600;
    font-size: 14px;
}
* {box-sizing: border-box;}
/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 165px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: #555;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<script>
var string_json = '{"device_name":"EasyMesh_Device","ip_addr":"192.168.10.1","mac_address":"00e04c8811a1","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[{"device_name":"EasyMesh_Device2","ip_addr":"192.168.10.1","mac_address":"00e04c8822a2","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]},{"device_name":"EasyMesh_Device3","ip_addr":"192.168.10.1","mac_address":"00e04c8833a3","neighbor_devices":[],"station_info":[{"station_mac":"787b8a454b3f","station_rssi":"52","station_connected_band":"5G","station_downlink":"780","station_uplink":"24"},{"station_mac":"5499637b3953","station_rssi":"76","station_connected_band":"2G","station_downlink":"144","station_uplink":"24"}],"child_devices":[]}]}';
var internetWanIdx = -1;
var ipmodeIdx = 0;
var wanName = "new WAN connection";
var pppUserName="";
var pppPassword="";
var pppServiceName="";
var connTypeIdx = 0;

internetWanIdx=1;
ipmodeIdx=0;
wanName="1_TR069_INTERNET_R_VID_";
connTypeIdx=0;

var WhatLanguage = 0; //TTI John 20170810 multilingual website
var SerialNum = '0000000000'; //TTI John 20190531 serial number for app greping
var GUIDNum = ''; //TTI John 20190531 GUID number for app greping
var moduleName="WAP-3516";
//for wlan1 page
var wlan_channel=new Array();
var wlan_idx=1 ;
var wlan_num=2 ;
var opmode=0 ;
var wispWanId = 0 ;
var WiFiTest=2 ;
var isMeshDefined = 0 ;
var isNewMeshUI =  0 ;
var isMultiWanSupport = 1;
var defPskLen=new Array();
var defPskFormat=new Array();
var init_bound=new Array();
var init_sideband=new Array();
var wlanBand2G5G=new Array();

//For wapi
var defWapiAuth=new Array();
var defWapiPskFormat=new Array();
var defWapiPskValue=new Array();
var defWapiASIP=new Array();
var defWapiCertSel=new Array();

var autoconf=new Array();
var regDomain=new Array();
var defaultChan=new Array();
var lastBand=new Array();
var lastRegDomain=new Array();
var usedBand=new Array();
var RFType=new Array();
var APMode=new Array();
var bandIdx=new Array();
var bandIdxAP=new Array();
var bandIdxClient=new Array();
var startChanIdx=new Array();
var disableSSID=new Array();
var wlanDisabled=new Array();
var networkType=new Array();
var keyinstall=new Array();
var ssid=new Array();
var encrypt=new Array();
var wep=new Array();
var defaultKeyId=new Array();
var wpaCipher=new Array();
var wpa2Cipher=new Array();
var pskValue=new Array();
var macClone=new Array();
var addProfile=new Array();

var keyType=new Array();
var autoCfgWlanMode=new Array();

var dot11k_enable=new Array();
var IEEE80211r_enable=new Array();
var IEEE80211v_enable=new Array(); //TTI John 20170919 add roaming wizard
var bandSecurity = "0" ; //TTI John 20171005
var mode_index = "0";
var nettype_index = "0";
/*
autoconf[wlan_idx]= 0;
*/
lastBand[wlan_idx] = 0;
lastRegDomain[wlan_idx] = 0;
bandIdxAP[wlan_idx] = -1;
bandIdxClient[wlan_idx] = -1;
startChanIdx[wlan_idx] = 0;
disableSSID[wlan_idx] = 0;
var dynamicWanIP=1;

// for WPS ------------------------------------------------>>
var wps_disabled=0;
var wps_ssid_old='CLARO_WIF_2.4G_M';
var wps_mode_old=0;
var wps_config_by_registrar=0;
var wps_encrypt_old=4;
var wps_wpaCipher_old=1;
var wps_wpa2Cipher_old=2;
var wps_psk_old='00000000';
var wps_psk_unmask_old='00000000';
var wps_type_old=0;
var wps_enable1x=0;
var wps_wpa_auth=2;
var wps_wizard=1;
//<<------------------------------------------------- for WPS

var isPocketRouter="0"*1;
var pocketRouter_Mode="0"*1;
var POCKETROUTER_GATEWAY = 3;
var POCKETROUTER_BRIDGE_AP = 2;
var POCKETROUTER_BRIDGE_CLIENT = 1;

var is_ulinker="0"*1;
var is_wan_link="1"*1;
var ulinker_opMode="2"*1; //0:AP; 1:Client; 2:Router; 3:RPT
var wlan_channelbound=new Array();
var isWDSDefined = 0 ;
var isAP = 0;
var wlan_support_92D =  1 ;
var wlan_support_92D_concurrent =  2 ; //0:no; 1:yes; 2:force concurrent
function wan_pptp_use_dynamic_carrier_selector(form, index, mode){
    if(mode == "dynamicIP") {
        form.wan_pptp_use_dynamic_carrier_radio[index].checked = true;
        form.pptpIpAddr.disabled = true;
        form.pptpSubnetMask.disabled = true;
        form.pptpDefGw.disabled = true;
        dynamicWanIP = 1;
    } else {
        form.wan_pptp_use_dynamic_carrier_radio[index].checked = true;
        form.pptpIpAddr.disabled = false;
        form.pptpSubnetMask.disabled = false;
        form.pptpDefGw.disabled = false;
        dynamicWanIP = 0;
    }
}

function wan_l2tp_use_dynamic_carrier_selector(form,index, mode){
    if(mode == "dynamicIP") {
        form.wan_l2tp_use_dynamic_carrier_radio[index].checked = true;
        form.l2tpIpAddr.disabled = true;
        form.l2tpSubnetMask.disabled = true;
        form.l2tpDefGw.disabled = true;
        dynamicWanIP =1;
    } else {
        form.wan_l2tp_use_dynamic_carrier_radio[index].checked = true;
        form.l2tpIpAddr.disabled = false;
        form.l2tpSubnetMask.disabled = false;
        form.l2tpDefGw.disabled = false;
        dynamicWanIP = 0 ;
    }
}
function includeSpace(str)
{
  for (var i=0; i<str.length; i++) {
      if ( str.charAt(i) == ' ' ) {
      return true;
    }
  }
  return false;
}
function checkString(str)
{
    for (var i=0; i<str.length; i++) {
        if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') ||
           (str.charAt(i) == '.') || (str.charAt(i) == ':') || (str.charAt(i) == '-') || (str.charAt(i) == '_') || (str.charAt(i) == ' ') || (str.charAt(i) == '/') || (str.charAt(i) == '@'))
            continue;
        return 0;
    }
    return 1;
}
function saveChanges_lan(form)
{
  if ( checkIpAddr(form.lan_ip, 'Invalid IP address value! ') == false )
        return false;
  if (checkIPMask(form.lan_mask) == false)
      return false ;
   return true;
}

function disable_all(form, wlan_id)
{
  disableTextField(form.elements["method"+wlan_id]);
  disableTextField(form.elements["pskFormat"+wlan_id]);
  disableTextField(form.elements["pskValue"+wlan_id]);
}

function checkState(wlan_id)
{
    form = document.wizard;
    if (disableSSID[wlan_id]) {
        disable_all(form,wlan_id);
    }
    else {
        if(form.elements["method"+wlan_id].selectedIndex==0){
            show_div(false,("wapi_div"+wlan_id));
            show_div(false,("wpa_div"+wlan_id));
            show_div(false,("wep_div"+wlan_id));
            //disableTextField(form.elements["wifi_roaming"+wlan_id]);
            document.getElementById(("wep_btn"+wlan_id)).style.top = "260px";
        }
        else if(form.elements["method"+wlan_id].selectedIndex==1){
            show_div(false,("wapi_div"+wlan_id));
            show_div(false,("wpa_div"+wlan_id));
            show_div(true,("wep_div"+wlan_id));
            //disableTextField(form.elements["wifi_roaming"+wlan_id]);
            document.getElementById(("wep_btn"+wlan_id)).style.top = "350px";
        }
        else if(form.elements["method"+wlan_id].selectedIndex>=2){
            if(form.elements["method"+wlan_id].selectedIndex==5){    //wapi
                show_div(true,("wapi_div"+wlan_id));
                show_div(false,("wpa_div"+wlan_id));
                show_div(false,("wep_div"+wlan_id));
                //disableTextField(form.elements["wifi_roaming"+wlan_id]);
                document.getElementById(("wep_btn"+wlan_id)).style.top = "350px";
            }
            else{
                show_div(false,("wapi_div"+wlan_id));
                show_div(true,("wpa_div"+wlan_id));
                show_div(false,("wep_div"+wlan_id));
                enableTextField(form.elements["wifi_roaming"+wlan_id]);
                document.getElementById(("wep_btn"+wlan_id)).style.top = "260px";
                if(form.elements["method"+wlan_id].value == 6){
                    form.elements["ciphersuite"+wlan_id].value="tkip+aes";
                    form.elements["wpa2ciphersuite"+wlan_id].value="tkip+aes";
                }
            }
        }
    }
}

function updateFormat(form, wlan_id)
{
  if (form.elements["length"+wlan_id].selectedIndex == 0) {
    form.elements["format"+wlan_id].options[0].text = 'ASCII (5 characters)';
    form.elements["format"+wlan_id].options[1].text = 'Hex (10 characters)';
  }
  else {
    form.elements["format"+wlan_id].options[0].text = 'ASCII (13 characters)';
    form.elements["format"+wlan_id].options[1].text = 'Hex (26 characters)';
  }
  form.elements["format"+wlan_id].selectedIndex =      keyType[wlan_id];
  
   //brad20070726
   //setDefaultKeyValue(form, wlan_id);
   setDefaultWEPKeyValue(form, wlan_id);
}
function setDhcpRange()
{
    // cal the bigger ip range
    form = document.wizard;
    //check ip
    val = getDigit(form.lan_ip.value,4) ;
    //check mask
    mask_end = (255 - getDigit(form.lan_mask.value,4)) + (getDigit(form.lan_mask.value,4)&val);
    mask_start = (getDigit(form.lan_mask.value,4)&val);
    // val will in range of mask start, end
    range1 = mask_end - val; // val+1 to mask_end
    range2 = val-mask_start;        // 1 to val-1
    if((mask_end - mask_start) < 3){
        form.dhcp.value = 0 ; //disable
        return false ;
    }
//    dhcpStart = (range1 > range2)? (val+1):(mask_start+1) ;
//    dhcpEnd = (range1 > range2)? (mask_end-1): (val-1);

//    form.dhcpRangeStart.value = getDigit(form.lan_ip.value,1) + "." + getDigit(form.lan_ip.value,2)
//    +"." + getDigit(form.lan_ip.value,3) + "."+ dhcpStart ;
    
//    form.dhcpRangeEnd.value = getDigit(form.lan_ip.value,1) + "." + getDigit(form.lan_ip.value,2) +"." + getDigit(form.lan_ip.value,3) + "." +dhcpEnd ;


}
/*-- keith: add l2tp support. 20080515  */
function wanTypeSelection(field)
{
      if(!document.getElementById)
      {
          alert('Error! Your browser must have CSS support !');
          return;
      }
      if(field.selectedIndex == 0)//static ip
      {
          wanShowDiv(0 ,1, 0, 0, 1, 0,0); //pptp, dns, dnsMode, pppoe, static (div), l2tp
        document.getElementById("wan_btn").style.top = "320px";
      }
      else if(field.selectedIndex == 1)//Dhcp
      {
          wanShowDiv(0 ,0, 0, 0, 0, 0, 0);
        document.getElementById("wan_btn").style.top = "260px";
      }
      else if(field.selectedIndex == 2)//pppoe
      {
          wanShowDiv(0 ,0, 0, 1, 0, 0, 0);
        document.getElementById("wan_btn").style.top = "260px";
      }
      else if(field.selectedIndex == 3)//pptp
      {
          wanShowDiv(1 ,0, 0, 0, 0, 0, 0);
        document.getElementById("wan_btn").style.top = "420px";
      }
      else if(field.selectedIndex == 4)//l2tp /*-- keith: add l2tp support. 20080515  */
      {
          wanShowDiv(0 ,0, 0, 0, 0, 1, 0);
        document.getElementById("wan_btn").style.top = "420px";
      }
      else if(field.selectedIndex == 5)//USB3G
      {
        wanShowDiv(0 ,0, 0, 0, 0, 0, 1);
        document.getElementById("wan_btn").style.top = "320px";
      }
}
function checkMsg(selValue){
    //Add by TTI John for Note number 2018-04-11
    //console.log(selValue);
    var count_note=1;
    var strNote = "";
    /*strNote=strNote.concat("<b>Note "+count_note+": </b>These Wi-Fi settings synchronize the 3514 Extender  when users click the Finished button. <br>") ;
    count_note++;*/ //for the mesh msg 20191211
    if(selValue == 0){
        if(WhatLanguage == 1){
                strNote=strNote.concat("<b>附註 "+count_note+": </b> 雙頻密碼同步設置已啟用，2.4GHz 和 5GHz 密碼將會設置成相同<br>") ;
            }else if(WhatLanguage == 3){
                strNote=strNote.concat("<b>注 ：</b> パスワードの同期を行うと、2.4GHz と 5GHz で同じパスワードを使用することになります。<br>")
            }else if(WhatLanguage == 4){
                strNote=strNote.concat("<b>Nota:</b> A configuração do Passphrase da banda dupla é a mesma, portanto você pode inserir a mesma Passphrase para Wi-Fi de 2,4 e 5 GHz.<br>")
            }else{
                strNote=strNote.concat("<b>Note "+count_note+": </b>The setup of Dual-band Passphrases is same ,so you can enter the same passphrase for both Wi-Fi 2.4 & 5 GHz. <br>") ;
            }

        count_note++;
        /*if(pskValue[0] != pskValue[1]){
            if(WhatLanguage == 1){
                strNote=strNote.concat("<b>附註 "+count_note+": </b> 當您按下啟用設定時，WizeLink Wi-Fi 2.4GHz 將會啟用<br>") ;
            }else{
                strNote=strNote.concat("<b>Note "+count_note+": </b>   Wi-Fi 2.4GHz and 5GHz Passphrase  are different ,so device will use same Passphrase for both Wi-Fi 2.4 & 5 GHz. <br>") ;
            }
            count_note++;
        }*/
    }else if(selValue == 1){
        if(WhatLanguage == 1){
                strNote=strNote.concat("<b>附註 "+count_note+": </b> 雙頻密碼同步設置已停用，2.4GHz 和 5GHz 密碼將會被分別設定<br>") ;
            }else if(WhatLanguage == 3){
                strNote=strNote.concat("<b>注: </b> パスワードの同期を行うと、2.4GHz と 5GHz で同じパスワードを使用することになります。同期を行わない場合は、それぞれでパスワードを設定します。 <br>") ;
            }else if(WhatLanguage == 4){
                strNote=strNote.concat("<b>Nota: </b> A configuração do Passphrase da banda dupla é separada, portanto você pode inserir passphrases do Wi-Fi de 2,4 e 5 GHz, respectivamente. <br>") ;
            }else{
                strNote=strNote.concat("<b>Note "+count_note+": </b> The setup of Dual-band Passphrases is separate ,so you can enter passphrases of Wi-Fi 2.4 & 5 GHz respectively. <br>") ;
            }
        count_note++;
        
    }
    if(wlanDisabled[0]){
        if(WhatLanguage == 1){
            strNote=strNote.concat("<b>附註 "+count_note+": </b> 當您按下啟用設定時，WizeLink Wi-Fi 5GHz 將會重新啟用 <br>") ;
        }else if(WhatLanguage == 4){
            strNote=strNote.concat("<b>Nota "+count_note+": </b> O rádio sem fio WizeLink 5GHz será reativado automaticamente quando você clicar no botão Salvar & Aplicar.<br>") ;
        }else{
            strNote=strNote.concat("<b>Note "+count_note+": </b> WizeLink 5GHz wireless radio will be re-enabled automatically when you click Finished button.<br>") ;
        }
        count_note++;
    }
    if(wlanDisabled[1]){

        if(WhatLanguage == 1){
            strNote=strNote.concat("<b>附註 "+count_note+": </b> 當您按下啟用設定時，WizeLink Wi-Fi 2.4GHz 將會被啟用<br>") ;
        }else if(WhatLanguage == 4){
            strNote=strNote.concat("<b>Nota "+count_note+": </b> O rádio sem fio WizeLink 2.4GHz será reativado automaticamente quando você clicar no botão Salvar & Aplicar.<br>") ;
        }else{
            strNote=strNote.concat("<b>Note "+count_note+": </b> WizeLink 2.4GHz wireless radio will be re-enabled automatically when you click Finished button.<br>") ;
        }
        count_note++;
    }
    document.getElementById("auto_msg_1").innerHTML =strNote;
}
function bandSecuritySelection(selValue){
    if(selValue == 0)//Same password
      {
          show_div(false , "wlan2_div0");
          show_div(true , "wlan2_div1");
      }
      else if(selValue == 1)//Different password
      {
          show_div(true , "wlan2_div0");
          show_div(true , "wlan2_div1");
      }
      checkMsg(selValue);
}
function wizardHideDiv()
{
    show_div(false, "opmode_div");
    show_div(false, "top_div");
    show_div(false, "lan_div");
    show_div(false, "ntp_div");
    show_div(false, "wan_div");
    show_div(false, "wlan_band_mode_div");

    show_div(false, "wlan1_div0");
    show_div(false, "wlan2_div0");
    if (wlan_support_92D)
    {
        show_div(false, "wlan1_div1");
        show_div(false, "wlan2_div1");
    }

    show_div(false, "pocket_wlan_band_mode_div");

    show_div(false, "pocket_wlan_name_div");
      show_div(false, "pocket_wlan1_ssid_div");
         
    show_div(false, "pocket_wlan_security_div");
    show_div(false, "pocket_wan_setting_div");

    show_div(false, "pocket_wlan1_security_div");
    show_div(false,("pocket_wpa_div1"));
    show_div(false,("pocket_wep_div1"));
    show_div(false,("otg_auto_div"));
}

function saveClick_opmode(next){
    form =  document.wizard;
    wizardHideDiv();
    if(next)
        show_div(true, ("ntp_div"));
    else
    {
        show_div(true, "top_div");
        if(is_ulinker == 1)
            show_div(true,("otg_auto_div"));
    }
}
function saveClick_ntp(next){
    form =  document.wizard;
    wizardHideDiv();
    if(next)
        show_div(true, ("lan_div"));
    else
    {
        if(isMultiWanSupport)
            show_div(true, "top_div");
        else
            show_div(true, "opmode_div");
    }

}

function saveClick_lan(next){
    form =  document.wizard;
    setDhcpRange();
    if(saveChanges_lan(form) ==false)
    {
        return false ;
    }
    else{
        wizardHideDiv();
        if(next)
        {
            if(isAP == 1)
            {
                if(wlan_num !=0)
                {
                       if(wlan_support_92D == 1)
                            show_div(true, ("wlan_band_mode_div"));
                       else
                            show_div(true, ("wlan1_div0"));
                }
                else
                {
                    alert("You have no wlan");
                    show_div(true,("top_div"));
                }
            }
            else
                show_div(true, ("wan_div"));
        }
        else
            show_div(true, "ntp_div");
    }
}
function saveClick_wan(next)
{
    if(next == 0)
    {
        wizardHideDiv();
        show_div(true, ("lan_div"));
    }
    else if(next == 1)
    {
        form = document.wizard;
        if(saveChanges_wan(form,0,dynamicWanIP) == false)
        {
            return false;
        }
        else
        {
            wizardHideDiv();
            //if(next)
            //{
                if(wlan_num !=0)
                {
                    if(wlan_support_92D == 1)
                    {
                        show_div(true, ("wlan_band_mode_div"));
                    }
                    else
                    {
                        show_div(true, ("wlan1_div0"));
                    }
                }
            //}
            //else
            //{
            //    show_div(true, ("lan_div"));
            //}
        }
    }
}
function saveClick_wlan1(next, wlan_id){
    form =  document.wizard;
    if(saveChanges_basic(form,wlan_id) ==false)
        return false ;
    else{
        wizardHideDiv();
        if(next)
            show_div(true, ("wlan2_div"+wlan_id));
        else
        {
            if(wlan_id == 0)
            {
                if(wlan_support_92D == 1)
                {
                    show_div(true, "wlan_band_mode_div");
                }
                else
                {
                if(isAP == 1)
                    show_div(true, "lan_div");
                else
                    show_div(true, "wan_div");
            }
            }
            else
            {
                var wizardForm=document.wizard;
                var wlBandMode = wizardForm.elements["wlBandMode"].value;
                var Band2G5GSupport = wlanBand2G5G[0];
            
                if(wlBandMode == 3) //3:single
                {
                    if(wlan_support_92D == 1)
                    {
                        show_div(true, "wlan_band_mode_div");
                    }
                    else
                    {
                        
                        if(isAP == 1)
                            show_div(true, "lan_div");
                        else
                            show_div(true, "wan_div");
                    }
                }
                else //2:both
                {
                    show_div(true, ("wlan2_div"+(wlan_id-1)));
        }
    }
        }
    }
    
        
}
function enableWan1(enable)
{
    if(enable)
        document.wizard.wanType.disabled=false;
    else
        document.wizard.wanType.disabled=true;
}

function Set_onChangeBand(form, wlan_id, band, index){
        var band;
        var auto;
         var txrate;
    var value;
    
     var checkid_bound=document.getElementById("channel_bounding"+wlan_id);
     var checkid_sideband = document.getElementById("control_sideband"+wlan_id);
     var mode_selected=0;
    var Type_selected=0;
    var index_channelbound=0;
    value =band.options[index].value;
    if(value ==9 || value ==10 || value ==7 || value ==11 || value==63|| value==71|| value==75){
        checkid_bound.style.display = "";
        checkid_sideband.style.display = "";
    }else{
        checkid_bound.style.display = "none";
        checkid_sideband.style.display = "none";
    }
    var channelbound_idx=form.elements["channelbound"+wlan_id].selectedIndex;
    wlan_channelbound[wlan_id]=form.elements["channelbound"+wlan_id].options[channelbound_idx].value;
    document.wizard.elements["channelbound"+wlan_id].length=0;
    showchannelbound_updated(document.wizard,value,wlan_id,2);
    
    updateChan_channebound(form, wlan_id);
    Type_selected = document.wizard.elements["type"+wlan_id].selectedIndex;
      mode_selected=document.wizard.elements["mode"+wlan_id].selectedIndex;
      //if client and infrastructure mode
      if(mode_selected ==1){
        if(Type_selected == 0){
            disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
            disableTextField(document.wizard.elements["channelbound"+wlan_id]);
        }else{
            enableTextField(document.wizard.elements["channelbound"+wlan_id]);
            index_channelbound=document.wizard.elements["channelbound"+wlan_id].selectedIndex;
        if(index_channelbound ==0)
            disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
        else if(index_channelbound ==2)
            disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
        else
            enableTextField(document.wizard.elements["controlsideband"+wlan_id]);
        }
    }else{
        enableTextField(document.wizard.elements["channelbound"+wlan_id]);
            index_channelbound=document.wizard.elements["channelbound"+wlan_id].selectedIndex;
        if(index_channelbound ==0)
            disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
        else if(index_channelbound ==2)
            disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
        else
            enableTextField(document.wizard.elements["controlsideband"+wlan_id]);
    }
    var chan_number_idx=form.elements["chan"+wlan_id].selectedIndex;
    var chan_number= form.elements["chan"+wlan_id].options[chan_number_idx].value;
    if(chan_number == 0)
        disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
    else{
        if(document.wizard.elements["channelbound"+wlan_id].selectedIndex == "0")
             disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
         else if(document.wizard.elements["channelbound"+wlan_id].selectedIndex == "2")
             disableTextField(document.wizard.elements["controlsideband"+wlan_id]);
         else
            enableTextField(document.wizard.elements["controlsideband"+wlan_id]);
    }
}


function cancelClick(){
    
    window.location="wizard.htm" ;
}
function saveClick_wlan2(next, wlan_id)
{
var tmp_wlan;
    form =  document.wizard;
  // P2P_SUPPORT
  
for(i=0; i< 2; i++){ //2
    //var mode =form.elements["mode"+ i] ;
    var ssid =form.elements["ssid"+ i] ;
    if ( ssid.value=="") {//mode.selectedIndex=4 means AP+MESH
      if(WhatLanguage==1){
          if(i == 0)
              alert("5G SSID不能是空白的");
          else if(i == 1)
              alert("2.4G SSID不能是空白的");
      }else if(WhatLanguage==3){
          if(i == 0)
              alert("5GHz SSIDは空白にできません");
          else if(i == 1)
              alert("2.4GHz SSIDは空白にできません");
      }
      else{
          if(i == 0)
              alert("5GHz SSID cannot be empty");
          else if(i == 1)
              alert("2.4GHz SSID cannot be empty");
      }

    ssid.value = ssid.defaultValue;
    ssid.focus();
    return false;
   }
   if(document.wizard.elements["wifi_roaming"+i].checked){
       if(form.elements["bandSecurity"].selectedIndex == 0){
           tmp_wlan=i;
           i=1;
       }

       if(form.elements["method"+i].selectedIndex < 2){
           if(WhatLanguage==1){
              if(tmp_wlan == 0)
                  alert("若您要開啟5GHz Wi-Fi智慧漫遊，請將Wi-Fi加密設為WPA2或WPA Mixed");
              else if(tmp_wlan == 1)
                  alert("若您要開啟2.4GHz Wi-Fi智慧漫遊，請將2.4G Wi-Fi加密設為WPA2或WPA Mixed");
          }else if(WhatLanguage==3){
              if(tmp_wlan == 0)
                  alert("5GHz Wi-Fiローミングを開く場合は、無線設定でWPA2またはWPA-Mixedの暗号化を選択してください。");
              else if(tmp_wlan == 1)
                  alert("2.4GHz Wi-Fiローミングを開く場合は、無線設定でWPA2またはWPA-Mixedの暗号化を選択してください。");
          }
          else{
              if(tmp_wlan == 0)
                  alert("When opening 5 GHz Wi-Fi roaming, please select WPA 2 or WPA-Mixed encryption in the wireless setting.");
              else if(tmp_wlan == 1)
                  alert("When opening 2.4 GHz Wi-Fi roaming, please select WPA 2 or WPA-Mixed encryption in the wireless setting.");
          }
        return false;
       }
   }
}
for(i=0; i< 2; i++){ //2
   if(form.elements["bandSecurity"].selectedIndex == 0){
       i=1;
   }
    if(form.elements["method"+i].selectedIndex == 2
    || form.elements["method"+i].selectedIndex == 3
    || form.elements["method"+i].selectedIndex == 4){
        if(saveChanges_wpa(form, i, WhatLanguage) == false)
            return false ;
    }else if(form.elements["method"+i].selectedIndex == 1){
    //brad20070726
    /*
        if(saveChanges_wep(form, i) == false)
            return false ;
    */
        if(saveChanges_wepkey(form, i) == false)
            return false;
    }
    if(form.elements["method"+i].selectedIndex == 0)
        form.elements["wepEnabled"+i].value =  "OFF" ;
    else
        form.elements["wepEnabled"+i].value =  "ON" ;
}
    //wizardHideDiv();
    //getTTIMeshNode(); //TTI John added for  mesh auto-configuration 20191203
    show_div(false, "body_block");
    show_div(true, "wait_div");//by TTI John for web rebooting a little late 2018-03-19
    document.wizard.submit();

    return true ;
}
function opModeClick(mode)
{
    form = document.wizard ;
    wlan_id = form.wispWanId.selectedIndex;
    if(mode == 2){ //WISP mode
        // infrastructure client mode
         form.elements["mode"+wlan_id].selectedIndex = 1;
         form.elements["type"+wlan_id].selectedIndex = 0;
    }
    else{    // reset default
         form.elements["mode"+wlan_id].selectedIndex =  form.elements["mode"+wlan_id].defaultSelected;
         form.elements["type"+wlan_id].selectedIndex =  form.elements["type"+wlan_id].defaultSelected;

    }
    updateMode(document.wizard, wlan_id);
    updateType(document.wizard, wlan_id);
    updateWISPWan();

}
function updateWISPWan()
{
    if(wlan_num > 1){
        if(form.opMode[2].checked)
            enableTextField(form.wispWanId);
        else
            disableTextField(form.wispWanId);
    }
}
//wizard wapi
function show_wizard_wapi_settings(wlan_id, index)
{
//    alert("wizard: wlan_id="+wlan_id+", index="+index);
    
    if(index == 2)
     {
        get_by_id("wapi_psk"+wlan_id).style.display = "";
        get_by_id("wapi_psk_"+wlan_id).style.display = "";
        get_by_id("wapi_as"+wlan_id).style.display = "none";
        get_by_id("wapi_as_"+wlan_id).style.display = "none";
        
       }
       else
       {
        get_by_id("wapi_psk"+wlan_id).style.display = "none";
        get_by_id("wapi_psk_"+wlan_id).style.display = "none";
        get_by_id("wapi_as"+wlan_id).style.display = "";
        get_by_id("wapi_as_"+wlan_id).style.display = "";
       }

}
function updateModeChangeSecurity(wlan_id)
{
    form =  document.wizard;
    if(form.elements["mode"+wlan_id].selectedIndex == 1){
        //form.elements["method"+wlan_id].options[2].text = "WPA(TKIP)";
        //form.elements["method"+wlan_id].options[2].value = 2;
        //form.elements["method"+wlan_id].options[3].text = "WPA2(AES)";
        //form.elements["method"+wlan_id].options[3].value = 4;
        form.elements["method"+wlan_id].options[2].text = "WPA Mixed";
        form.elements["method"+wlan_id].options[2].value = 6;
        form.elements["method"+wlan_id].options.length = 3;
    }
    else{
        form.elements["method"+wlan_id].options[2].text = "WPA2(AES)";
        form.elements["method"+wlan_id].options[2].value = 4;
        form.elements["method"+wlan_id].options[3].text = "WPA Mixed";
        form.elements["method"+wlan_id].options[3].value = 6;
    }
}
function Load_Setting()
{
    console.log(document.forms[0]);
    with ( document.forms[0] )
    {
        ipmode[ipmodeIdx].checked = true;
        username.value = password.value = "";
        serviceName.value = "";
        
        if(internetWanIdx >= 0)
        {
            wanIdx.value = internetWanIdx;
            if(ipmodeIdx == 1)
            {
                username.value = pppUserName;
                password.value = pppPassword;
                serviceName.value = pppServiceName;
            }
        }
        else
        {
            wanIdx.value = -1;
        }
        on_ctrlupdate();
    }
    var dF=document.forms[0];
    wpa_cipher="1";
    wpa2_cipher="2";
    //console.log(wpa_cipher);
    //console.log(wpa2_cipher);
    var dF=document.forms[0];
    if (wpa_cipher == 0)
        wpa_cipher = 1;
    if (wpa2_cipher == 0)
        wpa2_cipher = 2;
/*
    if (wpa_cipher & 1)
        dF.ciphersuite1[0].value = "tkip";
    if (wpa_cipher & 2)
        dF.ciphersuite1[1].value =  "aes";
    if (wpa2_cipher & 1)
        dF.wpa2ciphersuite1[0].value = "tkip";
    if (wpa2_cipher & 2)
        dF.wpa2ciphersuite1[1].value = "aes";
*/
    /* pocket router load setting*/
    var wlBandMode = 2 ;
    var wizardForm=document.wizard;
    var pocketForm=document.wizardPocketGW;
    
    var wlan1_phyband= "5GHz" ;
    var wlan2_phyband= "2.4GHz" ;

  if(get_by_id("wlan1_band_str0"))//TTI John 20170920
      get_by_id("wlan1_band_str0").innerHTML = "5GHz";
  if(get_by_id("wlan2_band_str0"))
    get_by_id("wlan2_band_str0").innerHTML = "5GHz";
    if(wlan_support_92D_concurrent)
    {
        if(wlan1_phyband.indexOf("2.4GHz") > -1) //find
            wlan2_phyband = "5GHz"
        else
            wlan2_phyband = "2.4GHz"

        if(get_by_id("wlan1_band_str1"))
            get_by_id("wlan1_band_str1").innerHTML = wlan2_phyband;
        if(get_by_id("wlan2_band_str1"))
            get_by_id("wlan2_band_str1").innerHTML = wlan2_phyband;
    }
    if(WhatLanguage==1){
        if(get_by_id("click_wlan2_next"+1)){
            get_by_id("click_wlan2_next"+0).style.display = "none";
            get_by_id("click_wlan2_next"+1).value = "啟用設定";
            get_by_id("click_wlan2_next"+1).name = "save_apply";
        }
    }else if(WhatLanguage==3){
        if(get_by_id("click_wlan2_next"+1)){
            get_by_id("click_wlan2_next"+0).style.display = "none";
            get_by_id("click_wlan2_next"+1).value = "設定";
            get_by_id("click_wlan2_next"+1).name = "save_apply";
        }
    }else if(WhatLanguage==4) {
        if(get_by_id("click_wlan2_next"+1)){
            get_by_id("click_wlan2_next"+0).style.display = "none";
            get_by_id("click_wlan2_next"+1).value = "Salvar & Aplicar";
            get_by_id("click_wlan2_next"+1).name = "save_apply";
        }
    }else {
        if(get_by_id("click_wlan2_next"+1)){
            get_by_id("click_wlan2_next"+0).style.display = "none";
            get_by_id("click_wlan2_next"+1).value = "Finished";
            get_by_id("click_wlan2_next"+1).name = "save_apply";
        }
    }
    
    if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
    {
        if(wlBandMode == 2) //2:BANDMODEBOTH
        {
            get_by_id("wlan0Ssid").innerHTML = wlan1_phyband+":<br>Wireless 1 Network Name(SSID):";
            get_by_id("wlan1Ssid").innerHTML = wlan2_phyband+":<br>Wireless 2 Network Name(SSID):";
            
            get_by_id("wlan0security").innerHTML = wlan1_phyband+":<br> Encryption";
            get_by_id("wlan1security").innerHTML = wlan2_phyband+":<br> Encryption";
        }
        else
        {
            get_by_id("wlan0Ssid").innerHTML = "Wireless Network Name(SSID):";
            show_div(false, "pocket_wlan1_ssid_div");
            
            get_by_id("wlan0security").innerHTML = "Encryption";
            show_div(false, "pocket_wlan1_security_div");
        }
    }
    else if(pocketRouter_Mode == POCKETROUTER_BRIDGE_CLIENT)
    {
        get_by_id("wlan0Ssid").innerHTML = "Wireless Network Name(SSID):";

        show_div(false, "pocket_wlan1_ssid_div");
        
        get_by_id("wlan0security").innerHTML = "Encryption";
        show_div(false, "pocket_wlan1_security_div");
    }
    else if(pocketRouter_Mode == POCKETROUTER_GATEWAY)
    {
        if(is_ulinker == 1)
        {
            get_by_id("wlan0Ssid").innerHTML = "Wireless Network Name(SSID):";
            get_by_id("wlan0security").innerHTML = "Encryption";
        }
        
    }
    if(wlan_num>0)
    {
    pocketForm.elements["pocket_ssid"].value = wizardForm.elements["ssid0"].value;
    pocketForm.elements["pocketAP_ssid"].value = wizardForm.elements["ssid0"].value;
    pocketForm.elements["pocketAP_realssid"].value = wizardForm.elements["ssid0"].value;


    pocketForm.elements["method0"].selectedIndex = wizardForm.elements["method0"].selectedIndex;
    pocketForm.elements["length0"].selectedIndex = wizardForm.elements["length0"].selectedIndex;
    pocketForm.elements["format0"].selectedIndex = wizardForm.elements["format0"].selectedIndex;
    pocketForm.elements["key0"].value = wizardForm.elements["key0"].value;
    
    pocketForm.elements["pskFormat0"].selectedIndex = wizardForm.elements["pskFormat0"].selectedIndex;
    pocketForm.elements["pskValue0"].value = wizardForm.elements["pskValue0"].value;

    pocketForm.elements["mode0"].value = APMode[0];

    if (wizardForm.elements["pskFormat0"].selectedIndex ==0){
        document.wizard.pskValue0.maxLength = "63";
        dF.pskFormat0.selectedIndex=0;
    }
    else{
        document.wizard.pskValue0.maxLength = "64";
        document.wizard.pskFormat0.selectedIndex=1;
    }
    }
    if(wlan_num == 2)
    {
        pocketForm.elements["mode1"].value = APMode[1];
        
        pocketForm.elements["pocket_ssid1"].value = wizardForm.elements["ssid1"].value;
        pocketForm.elements["pocketAP_ssid1"].value = wizardForm.elements["ssid1"].value;
        pocketForm.elements["method1"].selectedIndex = wizardForm.elements["method1"].selectedIndex;
        pocketForm.elements["length1"].selectedIndex = wizardForm.elements["length1"].selectedIndex;
        pocketForm.elements["format1"].selectedIndex = wizardForm.elements["format1"].selectedIndex;
        pocketForm.elements["key1"].value = wizardForm.elements["key1"].value;
        
        pocketForm.elements["pskFormat1"].selectedIndex = wizardForm.elements["pskFormat1"].selectedIndex;
        pocketForm.elements["pskValue1"].value = wizardForm.elements["pskValue1"].value;

        if (wizardForm.elements["pskFormat1"].selectedIndex ==0){
        document.wizard.pskValue1.maxLength = "63";
        dF.pskFormat1.selectedIndex=0;
        }
        else{
            document.wizard.pskValue1.maxLength = "64";
            document.wizard.pskFormat1.selectedIndex=1;
        }
    }
    if(wlan_num>0)
    {
    pocketForm.elements["wapiAuth0"][0].checked = wizardForm.elements["wapiAuth0"][0].checked;
    pocketForm.elements["wapiAuth0"][1].checked = wizardForm.elements["wapiAuth0"][1].checked;

    if(wizardForm.elements["wapiAuth0"][0].checked == true){
        show_wizard_wapi_settings(0, 1);
    }
    else{
        show_wizard_wapi_settings(0, 2);
    }

    if(pocketForm.elements["wapiAuth0"][0].checked == true){
        show_wapi_settings(1);
    }
    else{
        show_wapi_settings(2);
    }

    pocketForm.elements["wapiPskFormat0"].selectedIndex = wizardForm.elements["wapiPskFormat0"].selectedIndex;
    pocketForm.elements["wapiPskValue0"].value = wizardForm.elements["wapiPskValue0"].value;

    pocketForm.elements["wapiASIP0"].value = wizardForm.elements["wapiASIP0"].value;
    pocketForm.elements["wapiCertSel0"].selectedIndex = wizardForm.elements["wapiCertSel0"].selectedIndex;
    }
    if(isPocketRouter == 1)
    {
        var i;
        document.wizard.opMode[0].disabled =true;
        document.wizard.opMode[1].disabled =true;
        document.wizard.opMode[2].disabled =true;
        for(i=0;i<wlan_num;i++)
            disableTextField(document.wizard.elements["mode"+i]);
    }

    if(is_ulinker == 1)
    {
        show_div(true,("otg_auto_div"));
        if(parent.isOtg_Auto == 1)
        {
            get_by_id("otg_auto_span").innerHTML = "On";
            get_by_id("otg_auto_btn").value="Disable Auto Mode";
            get_by_id("otg_auto_val").value=1;
            disableTextField(document.wizardPocketGW.elements["wanType"]);
            
        }
        else
        {
            get_by_id("otg_auto_span").innerHTML = "Off";
            get_by_id("otg_auto_btn").value="Enable Auto Mode";
            get_by_id("otg_auto_val").value=0;
            enableTextField(document.wizardPocketGW.elements["wanType"]);
        }
        
        document.getElementById("opmode_wisp").style.display = "none";
        
    }

    
    var wlan_support_8812e=document.wizard.elements["wlan_support_8812e"].value;
    var idx_value= document.wizard.elements["band"+0].selectedIndex;
    var band_value= document.wizard.elements["band"+0].options[idx_value].value;
    var bound_idx = document.wizard.elements["channelbound"+0].selectedIndex;
    if(bound_idx == 0 || bound_idx == 2 || (wlan_support_8812e==1 && (band_value==11 || band_value==63 || band_value==71 || band_value==75 ||(band_value==7 && idx_value==1))))
        document.getElementById("control_sideband"+0).style.display = "none";
}

function wizardTopClick()
{
    wizardHideDiv();
    
    if(wlan_support_92D == 1 && pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
    {
        show_div(true, "pocket_wlan_band_mode_div");
    }
    else if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP || pocketRouter_Mode == POCKETROUTER_BRIDGE_CLIENT)
    {
            show_div(true, "pocket_wlan_name_div");
    }
    else if(pocketRouter_Mode == POCKETROUTER_GATEWAY)
    {
            var pocketForm=document.wizardPocketGW;
            show_div(true, "pocket_wan_setting_div");
            //pocketForm.elements["pppUserName"].focus();
            
            if(is_ulinker == 1)
            {
                get_by_id("pocket_wan_setting_div_next_btn").value = " Next>>";
            }
    }
    else
    {
        if(isAP == 1)
            show_div(true, "lan_div");
        else
        {
            if(isMultiWanSupport)
                show_div(true, "ntp_div");
            else
                show_div(true, "opmode_div");
        }
    }
    
    
}
function saveClick_bandMode(next){

    wizardHideDiv();
    if(next)
    {
        var wizardForm=document.wizard;
        
        var selValue = wizardForm.elements["wlBandModeSel"].value;
        //### edit by sen_liu 2011.4.8 when 92C + 92D, it must be "2.4G+5G  Concurrent"
        if(wlan_support_92D_concurrent == 2)//92C + 92D
        {
            wizardForm.elements["wlBandMode"].value = 2;
            selValue = 2;
        }
        //### end
        wizardForm.elements["wlBandMode"].value = selValue;
        if(selValue == 0) //0:2g
        {
            wizardForm.elements["wlBandMode"].value = 3; //3:single
            wizardForm.elements["Band2G5GSupport"].value = 1;//1:PHYBAND_2G
            
            if(wlan_num !=0)
            {
                if(wlanBand2G5G[0] == 1) //1:2g
                {
                    show_div(true, ("wlan1_div0"));
                }
                else
                {
                    show_div(true, ("wlan1_div1"));
                }
            }
        }
        else if(selValue == 1) //1:5g
        {
            wizardForm.elements["wlBandMode"].value = 3; //3:single
            wizardForm.elements["Band2G5GSupport"].value = 2;//1:PHYBAND_5G
            
            if(wlan_num !=0)
            {
                if(wlanBand2G5G[0] == 2) //2:5g
                {
                    show_div(true, ("wlan1_div0"));
                }
                else
                {
                    show_div(true, ("wlan1_div1"));
                }
            }
        }
        else
        {
            wizardForm.elements["wlBandMode"].value = 2; //2:both
            
            if(wlan_num !=0)
                show_div(true, ("wlan1_div0"));
        }
        
        if(selValue == 2) //2: both
        {
            wlan_num = 2;
            if(WhatLanguage==1){
                get_by_id("click_wlan2_next"+0).value = "下一步";
                get_by_id("click_wlan2_next"+1).value = "啟用設定";
                get_by_id("click_wlan2_next"+1).name = "save_apply";
            }else if(WhatLanguage==3){
                console.log(WhatLanguage);
                get_by_id("click_wlan2_next"+0).value = "  Next>>";
                get_by_id("click_wlan2_next"+1).value = " 設定 ";
                get_by_id("click_wlan2_next"+1).name = "save_apply";
            }else {
                get_by_id("click_wlan2_next"+0).value = "  Next>>";
                get_by_id("click_wlan2_next"+1).value = "Finished";
                get_by_id("click_wlan2_next"+1).name = "save_apply";
            }
        }
        else
        {
            wlan_num = 1;
            get_by_id("click_wlan2_next"+0).value = "Finished";
            get_by_id("click_wlan2_next"+1).value = "Finished";
        }

    }
    else
    {
        if(isAP == 1)
            show_div(true, "lan_div");
        else
            show_div(true, "wan_div");
    }
                
    
    
}
function wlanBandModeSelection(selValue)
{
    var wizardForm=document.wizard;
    wizardForm.elements["wlBandMode"].value = selValue;
    if(selValue == 0) //0:2g
    {
        wizardForm.elements["wlBandMode"].value = 3; //3:single
                wizardForm.elements["Band2G5GSupport"].value = 1;//1:PHYBAND_2G
            }
    else if(selValue == 1) //1:5g
            {
        wizardForm.elements["wlBandMode"].value = 3; //3:single
        wizardForm.elements["Band2G5GSupport"].value = 2;//1:PHYBAND_5G
        }
    else
        {
        wizardForm.elements["wlBandMode"].value = 2; //2:both
    }
}
/*var visible_a=1;
var visible_b=1;
var visible_c=1;
function show_a(){
    if(visible_a==1)
        show_div(true, "opmode_1");
    else
        show_div(false, "opmode_1");
    visible_a=!visible_a;
}
function show_b(){
    if(visible_b==1)
        show_div(true, "opmode_2");
    else
        show_div(false, "opmode_2");
    visible_b=!visible_b;
}
function show_c(){
    if(visible_c==1)
        show_div(true, "opmode_3");
    else
        show_div(false, "opmode_3");
    visible_c=!visible_c;
}*/
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}

function myFunction_1(wlan_id) {
    var popup = document.getElementById("myPopup_"+wlan_id);
    popup.classList.toggle("show");
}
function myFunction_2() {
    var popup = document.getElementById("myPopup_2");
    popup.classList.toggle("show");
}
function on_ctrlupdate()
{
    var wizardForm=document.wizard;
    
    
    if (wizardForm.ipmode[0].checked == true)  //DHCP, IPv4 only, or IPv4/IPv6
    {
        show_div(false, ("pppoe_div"));
    }else if (wizardForm.ipmode[1].checked == true) //PPPoE, IPv4 only, or IPv4/IPv6, or IPv6 only
    {
        show_div(true, ("pppoe_div"));
    }
    
}
/*function getTTIMeshNode(){
    var buffer =' showClients()';
    var res = buffer.split(">>>");
   // document.getElementById("demo").innerHTML = res;
    var count;
    var mesh_mac="";
    for(count=0;count<res.length;count++){
        
          var arr_buffer =res[count].split(" ");
          var brand=arr_buffer[arr_buffer.length-1];
          console.log(brand);
          if(brand=="TTI"){
                  mesh_mac=mesh_mac +" "+ arr_buffer[1];
          }
       }
       document.wizard.elements["meshNode"].value = mesh_mac;
       console.log(mesh_mac);
  }*/
</script>
</head>
<body onload="Load_Setting();" >

<blockquote id="body_block">
<form action=/boafrm/formWizard method=POST name="wizard">
<INPUT type=hidden name="meshNode" id="meshNode" value="">
<!-- for WPS -->
<INPUT type=hidden name=wps_clear_configure_by_reg0 value=0>
<INPUT type=hidden name=wps_clear_configure_by_reg0 value=0>
<INPUT type=hidden name=wps_clear_configure_by_reg0 value=0>
<INPUT type=hidden name=wps_clear_configure_by_reg1 value=0>
<INPUT type=hidden name=Band2G5GSupport value=1>
<INPUT type=hidden name=dsf_enable value=1>
<INPUT type=hidden name=wlBandMode value=2>
<INPUT type=hidden name=wlan1_phyband value="5GHz">
<INPUT type=hidden name=wlan2_phyband value="2.4GHz">
<INPUT type=hidden name=wlan_support_8812e value=1>
<INPUT type=hidden name=wlan_support_ac2g value=0>
<!-- for lte4g -->
<input type="hidden" id="lte4g_build" value="0" name="lte4g_build">
<input type="hidden" id="lte4g_enable" value="0" name="lte4g_enable">

<span id = "top_div" class = "on" >
<br>
<table  border=0 width="500" cellspacing=4 cellpadding=0>
<tr><td><h2 ID="A4">Easy Setup</h2></td> </tr><!--A4 replace select operation mode-->
<tr><td><font size=2><span id="A63"><!--A63 it will disappear in chinese-->
 The Easy Setup will guide  you to configure access point for first time.
</span></font></td></tr>

<tr><td><hr size=1 noshade align=top></td></tr>

</table>
<div>
<table border=0 width="350" cellspacing=4 cellpadding=0 class="off">
<tr>
  <td colspan="2" align=right>

    <input type="button" value="  Next>>" name="next" onClick='wizardTopClick();'>

  </td>
</tr>
</table>
</div>
</span>


<!-- opmode page -->

<!-- ntp page -->
<span id = "ntp_div" class = "on" >
<br>
<table  border=0 width="500" cellspacing=4 cellpadding=0>
  <tr><td colspan="2"><p id="A20">Time Zone Setting</p></td></tr>
  <!--<tr><td colspan="2"><hr size=1 noshade align=top></td></tr>-->
</table>
<span id = "ntp_enable_div" class = "on" >
<input type="hidden" name="enabled" value="ON">
<input type="checkbox" name="dlenabled" class="off" value="ON" >
<table  border="0" width=600 cellspacing=4 cellpadding=0>
    <tr ><td height=10> </td> </tr>

    <tr><td width="28%"><font size=2> <b id="A24">Time Zone Select :</b> </font></td>
        <td width="72%">
            <select name="timeZone">
                <script language="javascript">
                var i;
                for(i=0;i<ntp_zone_array.length;i++){
            if (i == ntp_zone_index)
                document.write('<option value="',ntp_zone_array[i].value,'" selected>',ntp_zone_array[i].name,'</option>');
            else
                document.write('<option value="',ntp_zone_array[i].value,'">',ntp_zone_array[i].name,'</option>');
                }
                </script>
            </select>
        </td>
    </tr></table>
<select name="ntpServerIp1" class="off">
            <option value="131.188.3.220">131.188.3.220  - Europe</option>
            <option value="130.149.17.8">130.149.17.8   - Europe</option>
            <option value="203.117.180.36"> 203.117.180.36 - Asia Pacific</option>
</select>
<script>
            form = document.wizard ;

            if ( 1 )
                form.enabled.checked = true;
            else
                form.enabled.checked = false;

            if ( 0 )
                form.dlenabled.checked = true;
            else
                form.dlenabled.checked = false;
            setTimeZone(form.timeZone, "5 1");
            setNtpServer(form.ntpServerIp1, "131.188.3.220");
            //updateState_ntp(form);

</script>
     <input type="hidden" value="0" name="ntpServerId">

</span>
<!-- lan page -->
<input type="hidden" name="lan_ip"  value=192.168.10.1>
<input type="hidden" name="lan_mask" value="255.255.255.0">
<!-- wan page -->
<span id = "wan_div" class = "on" >
    <table  border=0 width="500" cellspacing=4 cellpadding=0>
        <br>
          <tr>
              <td colspan="2">
                  <p ><span id="A33">WAN Interface Setup</span> </p>

              </td>
          </tr>

    </table>
    <input type="hidden" name="wanIdx" value="">
    <input type="hidden" value="pppoeNumber" name="pppoeNumber">
      <table cellpadding="0px" cellspacing="2px">
            <tr nowrap>
                <td ><b id="A35">WAN Access Type:</b>&nbsp;&nbsp;<a class="popup" href="#" onmouseover="myFunction()" onmouseout="myFunction()"><img src="Qmark.jpg" width="15" height="15" border="0" alt="" align="middle"><div class="popuptext" id="myPopup"><font size=2 id="A18">The default setting is Dynamic IP,which is the most common setting and with most cable modems.<br>DSL users may need to use PPPoE,which requires a unique DSL username and password.</font></div></a></td>
            </tr>
        </table>
      <table cellpadding="0px" cellspacing="2px">
            <tr nowrap><td width="150px"><input type="radio" id="ipmode"  name="ipmode" value="0" disabled="disabled">DHCP</td><td><font size=2><script>dw(wizard_dhcp_description);</script> </font></td></tr>
        </table>

        <table  cellpadding="0px" cellspacing="2px">
            <tr nowrap><font size=2><td width="150px"><input type="radio" id="ipmode"  name="ipmode" value="2" disabled="disabled">PPPoE </td><td><font size=2><script>dw(wizard_PPPoE_description);</script></font></td></tr>
        </table>
      <span id = "static_div" class = "off" >
          <table border="0" width=500>
            <tr>
                   <td width="35%"><font size=2><b id="A38">IP Address:</b></td>
                   <td width="65%"><font size=2>
                    <input type="text" name="wan_ip" size="18" maxlength="15" value="172.1.1.1">
                  </td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A39">Subnet Mask:</b></td>
                  <td width="65%"><font size=2><input type="text" name="wan_mask" size="18" maxlength="15" value="255.255.255.0"></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A40">Default Gateway:</b></td>
                  <td width="65%"><font size=2><input type="text" name="wan_gateway" size="18" maxlength="15" value="172.1.1.254"></td>
            </tr>
          </table>
      </span>
      
      <span id = "dhcp_div" class = "off" >
      </span>
      
      <span id = "pppoe_div" class = "off" >
          <table border="0" width=500>
            <tr nowrap>
                <td width="150px"><font size=2><b id="A41">User name：</b></font></td>
                <td><input type="text" name="username" maxlength="30" size="16" style="width:150px "></td>
            </tr>
            <tr nowrap>
                <td><font size=2><b id="A42">Password：</b></font></td>
                <td><input type="password" name="password" maxlength="30" size="16" style="width:150px "></td>
            </tr>
            <tr nowrap>
                <td><font size=2><b>Service Name:</b></font></td>
                <td><input type="text" name="serviceName" maxlength="20" size="16" style="width:150px "></td>
            </tr>
        </table>
      </span>
      
      <span id = "pptp_div" class = "off" >
          <table border="0" width=500>
          <tr>
      <td width="30%"><font size=2><b id="A16">Address Mode:</b></td>
      <td width="70%"><font size=2><input type="radio" value="dynamicIP" name="wan_pptp_use_dynamic_carrier_radio"  onClick="wan_pptp_use_dynamic_carrier_selector(this.form,0, this.value);"><b id="A43">Dynamic</b>&nbsp;<input type="radio" value="staticIP" name="wan_pptp_use_dynamic_carrier_radio"  onClick="wan_pptp_use_dynamic_carrier_selector(this.form,1, this.value);"><b id="A44">Static</b></td>
    </tr>
            
            <tr>
                  <td width="35%"><font size=2><b id="A45">IP Address:</b></td>
                  <td width="65%"><font size=2><input type="text" name="pptpIpAddr" size="18" maxlength="30" value="172.1.1.2" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A46">Subnet Mask:</b></td>
                  <td width="65%"><font size=2><input type="text" name="pptpSubnetMask" size="18" maxlength="30" value="255.255.255.0" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A47">Default Gateway:</b></td>
                  <td width="65%"><font size=2><input type="text" name="pptpDefGw" size="18" maxlength="30" value="172.1.1.254" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A48">Server IP Address:</b></td>
                  <td width="65%"><font size=2><input type="text" name="pptpServerIpAddr" size="18" maxlength="30" value="172.1.1.1"></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A49">User Name:</b></td>
                  <td width="65%"><font size=2><input type="text" name="pptpUserName" size="18" maxlength="128" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A50">Password:</b></td>
                  <td width="65%"><font size=2><input type="password" name="pptpPassword" size="18" maxlength="128" value=""></td>
            </tr>
        </table>
    </span>
    
<!-- keith: add l2tp support. 20080515  -->
    <span id = "l2tp_div" class = "off" >
        <table border="0" width=500>
        <tr>
      <td width="30%"><font size=2><b id="A17">Address Mode:</b></td>
      <td width="70%"><font size=2><input type="radio" value="dynamicIP" name="wan_l2tp_use_dynamic_carrier_radio"  onClick="wan_l2tp_use_dynamic_carrier_selector(this.form,0, this.value);"><b id="A51">Dynamic</b>&nbsp;<input type="radio" value="staticIP" name="wan_l2tp_use_dynamic_carrier_radio"  onClick="wan_l2tp_use_dynamic_carrier_selector(this.form,1, this.value);"><b id="A52">Static</b></td>
    </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A53">IP Address:</b></td>
                  <td width="65%"><font size=2><input type="text" name="l2tpIpAddr" size="18" maxlength="30" value="172.1.1.2" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A54">Subnet Mask:</b></td>
                  <td width="65%"><font size=2><input type="text" name="l2tpSubnetMask" size="18" maxlength="30" value="255.255.255.0" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A55">Default Gateway:</b></td>
                  <td width="65%"><font size=2><input type="text" name="l2tpDefGw" size="18" maxlength="30" value="172.1.1.254" 0></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A56">Server IP Address:</b></td>
                  <td width="65%"><font size=2><input type="text" name="l2tpServerIpAddr" size="18" maxlength="30" value="172.1.1.1"></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A57">User Name:</b></td>
                  <td width="65%"><font size=2><input type="text" name="l2tpUserName" size="18" maxlength="128" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A58">Password:</b></td>
                  <td width="65%"><font size=2><input type="password" name="l2tpPassword" size="18" maxlength="128" value=""></td>
            </tr>
        </table>
    </span>

    <span id = "USB3G_div" class = "off" >
        <table border="0" width=500>
            <tr>
                  <td width="35%"><font size=2><b id="A95">User Name:</b></td>
                  <td width="65%"><font size=2><input type="text" name="USB3G_USER" size="18" maxlength="30" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A96">Password:</b></td>
                  <td width="65%"><font size=2><input type="password" name="USB3G_PASS" size="18" maxlength="30" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b>PIN:</b></td>
                  <td width="65%"><font size=2><input type="password" name="USB3G_PIN" size="18" maxlength="30" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b>APN:</b></td>
                  <td width="65%"><font size=2><input type="text" name="USB3G_APN" size="18" maxlength="128" value=""></td>
            </tr>
            <tr>
                  <td width="35%"><font size=2><b id="A97">Dial Number:</b></td>
                  <td width="65%"><font size=2><input type="text" name="USB3G_DIALNUM" size="18" maxlength="128" value=""></td>
            </tr>
        </table>
    </span>
    
    <span id = "dnsMode_div" class = "off" >
        <table border="0" width=500>
             <tr>
                  <td width="100%" colspan="2"><font size=2>
                    
                        <input type="radio" value="dnsAuto" name="dnsMode" onClick="autoDNSclicked()"><b id="A59">Attain DNS Automatically
                    </b>
                  </td>
            </tr>
            <tr>
                  <td width="100%" colspan="2"><font size=2>
                        <input type="radio" value="dnsManual" name="dnsMode" onClick="manualDNSclicked()"><b id="A60">Set DNS Manually
                    </b>
                  </td>
            </tr>
       <!--<tr>-->
        </table>
    </span>
    
    <span id = "dns_div" class = "off" >
        <table border="0" width=500>
            <tr>
                   <td width="35%"><font size=2><b>DNS :</b></td>
                   <td width="65%"><font size=2><input type="text" name="dns1" size="18" maxlength="15" value=></td>
            </tr>
        </table>
    </span>
    
    <span id="wan_btn">  <!--TTI John 20170828
        <table border="0" width=500>
            <tr>
                <td colspan="2" align=right>
                      <input type="button" value="  Cancel  " name="cancel" onClick='cancelClick();'>
                      <input type="button" value="<<Back  " name="back" onClick='return saveClick_wan(0)' >
                    <script>
                        if(wlan_num==0)
                        {
                            document.write('<input type="submit" value="Finished" name="next" ');
                        }
                        else
                        {
                            document.write('<input type="button" value="  Next>>" name="next" ');
                        }
                        document.write(' onClick="return saveClick_wan(1)">');
                    </script>
                  </td>
              </tr>
        </table>   -->
    </span>
    <script>
        //wanTypeSelection(form.wanType); TTI John 20171030 Mask
    </script>
</span>

<!-- wireless band mode -->

<script>
wlanDisabled[0]=0;
RFType[0]=10;
APMode[0]=0;
bandIdx[0]=75;
bandIdxClient[0]=75;
networkType[0]=0;
regDomain[0]=1;
defaultChan[0]=44;
usedBand[0]=76;
ssid[0]='CLARO_WIF_5G_M';
dot11k_enable[0]=0;
IEEE80211r_enable[0]=0;
IEEE80211v_enable[0]=0;
encrypt[0]=4;
wep[0]=0;
defaultKeyId[0]=1;
defPskFormat[0]=0;
macClone[0]=0;
wpaCipher[0]='1';
wpa2Cipher[0]='2';
pskValue[0]='00000000';
keyType[0]=1;
defWapiAuth[0]=0;
defWapiPskFormat[0]=0;
defWapiPskValue[0]="";
defWapiASIP[0]="";
defWapiCertSel[0]="";
init_bound[0]=2;
init_sideband[0]=1;
wlanBand2G5G[0]=2;
wlanDisabled[1]=0;
RFType[1]=10;
APMode[1]=0;
bandIdx[1]=10;
bandIdxClient[1]=10;
networkType[1]=0;
regDomain[1]=1;
defaultChan[1]=6;
usedBand[1]=11;
ssid[1]='CLARO_WIF_2.4G_M';
dot11k_enable[1]=1;
IEEE80211r_enable[1]=0;
IEEE80211v_enable[1]=0;
encrypt[1]=4;
wep[1]=0;
defaultKeyId[1]=1;
defPskFormat[1]=0;
macClone[1]=0;
wpaCipher[1]='1';
wpa2Cipher[1]='2';
pskValue[1]='00000000';
keyType[1]=1;
defWapiAuth[1]=0;
defWapiPskFormat[1]=0;
defWapiPskValue[1]="";
defWapiASIP[1]="";
defWapiCertSel[1]="";
init_bound[1]=1;
init_sideband[1]=0;
wlanBand2G5G[1]=1;

</script>
<br>
<table  border=0 width="600" cellspacing=4 cellpadding=0>
    <tr><td  colspan="2"> <p><span id="A910">Wireless </span><span id="A650">Configuration</span> <span  id="wlBandModeSel" name="wlBandModeSel"> </span></p> </td></tr>
</table>
<script>
/*document.write('\
<table border="0" width=700 cellspacing=4 cellpadding=0>\
    <tr>\
       <td width="25%" ><font size=2><b id="A61">Wireless Band:</b></td>\
       <td width="75%"><font size=2>\
       <span size="1" id="wlBandModeSel" name="wlBandModeSel">\
    </span></td></tr>\
    </table>');*/
//<select size="1" name="wlBandModeSel" onChange="wlanBandModeSelection(this.value)" ></select>
    //var bandSecurity = 0 ; //TTI John 20171005
    //document.wizard.bandSecurity.selectedIndex = bandSecurity;
    var wizardForm=document.wizard;
    var band2G5GSupport = wlanBand2G5G[0];
    var wlBandModeSelect = wizardForm.elements["wlBandMode"].value;
    var idx=0;
    //### edit by sen_liu 2011.4.7 when 92C + 92D, it must be "2.4G+5G  Concurrent"
    /*if(wlan_support_92D_concurrent == 0)//92C
    {
        wizardForm.elements["wlBandModeSel"].options[idx++] = new Option("2.4G ", "0", false, false);
        wizardForm.elements["wlBandModeSel"].selectedIndex = 0;
        wizardForm.elements["wlBandMode"].value = 0;
    }else if(wlan_support_92D_concurrent == 1)//92D
    {
        wizardForm.elements["wlBandModeSel"].options[idx++] = new Option("2.4G ", "0", false, false);
        wizardForm.elements["wlBandModeSel"].options[idx++] = new Option("5G ", "1", false, false);
        wizardForm.elements["wlBandModeSel"].options[idx++] = new Option("2.4G+5G Concurrent(support 2.4G and 5G Concurrent mode by 2T2R)", "2", false, false);
        if(wlBandModeSelect == 0) //0:2G only
        {
            wizardForm.elements["wlBandModeSel"].selectedIndex = 0;
        }
        else if(wlBandModeSelect == 1) //1:5G only
        {
            wizardForm.elements["wlBandModeSel"].selectedIndex = 1;
        }
        else if(wlBandModeSelect == 2) //2:both
        {
            wizardForm.elements["wlBandModeSel"].selectedIndex = 2;
        }
        else if(wlBandModeSelect == 3) //3:single
        {
            if(band2G5GSupport == 1) //1:2g
                wizardForm.elements["wlBandModeSel"].selectedIndex = 0;
            else
                wizardForm.elements["wlBandModeSel"].selectedIndex = 1;
        }
    }else if(wlan_support_92D_concurrent == 2)//92C + 92D
    {*/
        if(WhatLanguage==1){
            get_by_id("wlBandModeSel").innerHTML ="[雙頻並行模式(同時使用2.4GHz和5GHz)]";
        }else if(WhatLanguage==3){
            get_by_id("wlBandModeSel").innerHTML = "(2.4GHz+5GHz コンカレント)";
        }else{
            get_by_id("wlBandModeSel").innerHTML = "(2.4GHz+5GHz Concurrent)";
        }
        //(support 2.4G and 5G Concurrent mode by 2T2R)
        //wizardForm.elements["wlBandModeSel"].selectedIndex = 0;
    //}
    //### end

</script>
<table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr>
    <td colspan="2" ><font size=2><b id="A19">Dual-band Passphrase Setup: &nbsp;&nbsp;&nbsp;&nbsp;</b></font> <select name="bandSecurity" onChange="bandSecuritySelection(this.value)" >
            <option value="0" id="A11">Same</option>
            <option value="1" id="A12">Separate</option>
    </select></td></tr>
    </table>
<!-- wlan1 page -->
<script>

wlan_channel[wlan_idx] = defaultChan[wlan_idx];
for(i=0; i< 2; i++){
/**********************************************************/
document.write('<input type="hidden" name="wlanDisabled'+i+'" value="">');// keep wlanDisabled off 2018-01-11
document.write('<input type="checkbox" name="wlanMacClone'+i+'" class="off" value="ON">');
document.write('<input type="checkbox" name="wizardAddProfile'+i+'" class="off" value="ON">');
document.write('<input type="hidden" name="basicrates'+i+'" value=0>');
document.write('<input type="hidden" name="operrates'+i+'" value=0>');
document.write('<input type="hidden" value="ON" name="wepEnabled'+i+'">\
    <input type="hidden" value="OFF" name="use1x'+i+'">\
    <input type="hidden" value="psk" name="wpaAuth'+i+'">\
    <input type="hidden" value="none" name="wpa11w'+i+'">\
    <input type="hidden" value="disable" name="wpa2EnableSHA256'+i+'">\
    <input type="hidden" value="tkip" name="ciphersuite'+i+'">\
    <input type="hidden" value="aes" name="wpa2ciphersuite'+i+'">\
    <input type="hidden" value="1812" name="radiusPort'+i+'">\
    <input type="hidden" value="" name="radiusIP'+i+'">\
    <input type="hidden" value="" name="radiusPass'+i+'">');
 /*if(wlanDisabled[i])
    document.wizard.elements["wlanDisabled"+i].checked = true;
 else
     document.wizard.elements["wlanDisabled"+i].checked = false;*/
document.write('<table border=0 width="500" cellspacing=4 cellpadding=0>\
    <tr><td colspan="2"><a style="text-decoration:underline; color: #000000;font-weight: 600;font-size: 13px;">Wi-Fi&nbsp;<span id=wlan2_band_str'+i+'></span></a></td></tr>\
<tr><td width="35%"><font size=2><b><span id="A93'+i+'">Wi-Fi Roaming:</span></b></td>\
    <td width="65%">\
    <label class="switch" style="margin:5px;">\
    <input type="checkbox" name="wifi_roaming'+i+'" class="switch__input" value="ON">\
    <div class="switch__toggle"></div></label>\
    </td></tr>\
  <tr>\
      <td width="35%"><font size=2><b><SPAN id=wlan1_band_str'+i+'></SPAN>&nbsp;<span>SSID:</span>\
      </b></td>\
      <td width="65%"><font size=2><input type="text" name="ssid'+i+'" size="33" maxlength="32" value="'+ssid[i]+'">\
      </td>\
  </tr></table>');

/**********************************************************/
/*<!-- wlan2 page --> */
/**********************************************************/
document.write('\
<span id = "wlan2_div'+i+'" class = "off" >\
<table border=0 width="500" cellspacing=4 cellpadding=0>\
    <tr height="7px"></tr>\
   <tr><td width="35%"><font size="2"><b id="A79'+i+'">Encryption:</b>&nbsp;\
   </b></td>\
      <td width="65%"><select size="1" name="method'+i+'" onChange="checkState('+i+')">\
          <option id="A92'+i+'" value="0">None</option>\
          <option value="1">WEP</option>');
            if (APMode[i] != 1) //client =1
                document.write('<option value=\"4\"> WPA2</option>');//WPA2(AES)
              document.write('<option value=\"6\">WPA Mixed</option>');
       document.write('');
         document.write('</select>&nbsp;<a class="popup" href="#" onmouseover="myFunction_1('+i+')" onmouseout="myFunction_1('+i+')"><img src="Qmark.jpg" width="15" height="15" border="0" alt="" align="middle"><div class="popuptext" id="myPopup_'+i+'"><span id="A94'+i+'">Change the "Encryption" type to your preferred form of security. WPA2(AES) will set the password that will be needed to log into your wireless network. The password must have at least 8 characters.</span></div></a>');
       document.write('</font></td></tr>\
</table>\
<span id = "wep_div'+i+'" class = "off" >  \
<table border=0 width="500" cellspacing=4  cellpadding=0>\
\
  <tr>\
      <td width="35%"><font size=2><b id="A80'+i+'">Key Length:</b></td>\
      <td width="65%"><font size=2><select size="1" name="length'+i+'" ONCHANGE="lengthClick(document.wizard, '+i+')">\
                  <option value=1 >64-bit</option>\
            <option value=2 >128-bit</option>\
      </select></td>\
  </tr>\
\
  <tr>\
      <td width="35%"><font size=2><b id="A81'+i+'">Key Format:</b></td>\
      <td width="65%"><font size=2><select size="1" name="format'+i+'" ONCHANGE="setDefaultWEPKeyValue(document.wizard, '+i+')">\
         <option value=1>ASCII</option>\
    <option value=2>Hex</option>\
       </select></td>\
  </tr>\
  <tr>\
     <td width="35%"><font size=2><b id="A82'+i+'">Key Setting:</b></td>\
     <td width="65%"><font size=2>\
         <input type="text" name="key'+i+'" size="26" maxlength="26">\
     </td>\
  </tr>\
</table>\
</span>');
/**********************************************************/

document.write('\
<span id = "wpa_div'+i+'" class = "on" >  \
   <table border=0 width="500" cellspacing=4 cellpadding=0>');
/**********************************************************/
/*
<!--
    <tr>
      <td width="35%"><font size="2"><b>WPA Unicast Cipher Suite:</b></font> </td>
      <td width="65%"><font size="2">
      <input type="radio" name="ciphersuite" value="tkip" 1>TKIP&nbsp;
      <input type="radio" name="ciphersuite" value="aes"  1>AES
       </font></td>
    </tr>

-->  */
/**********************************************************/
document.write('\
    <tr class="off">\
      <td width="35%"><font size="2"><b id="A83'+i+'">Pre-Shared Key Format:</b></font> </td>\
      <td width="65%"><font size="2"><select size="1" name="pskFormat'+i+'" onChange="pskFormatChange(document.wizard,'+i+')">\
          <option value="0" id="A84'+i+'">Passphrase</option>\
          <option value="1" >Hex (64 characters)</option>\
        </select></font></td>\
    </tr>\
    <tr>\
      <td width="35%"><font size="2"><b id="A85'+i+'">Passphrase:</b></font> </td>\
      <td width="65%"><font size="2"><input type="text" name="pskValue'+i+'" size="32" value='+pskValue[i]+'></font></td>\
    </tr>\
     <tr></tr><tr></tr><tr></tr>\
  </table>\
</span>');

/********************wapi begin*********************************/
document.write('\
<span id = "wapi_div'+i+'" class = "on" >\
   <table border=0 width="500" cellspacing=4 cellpadding=0>\
    <tr>\
      <td width="35%" class="bgblue"><font size="2"><b id="A86'+i+'">Authentication Mode:</b></font></td>\
      <td width="65%" class="bggrey"><font size="2">\
          <input  name="wapiAuth'+i+'" type="radio" value="eap" onClick="show_wizard_wapi_settings('+i+',1)">Enterprise (AS Server)\
          <input name="wapiAuth'+i+'" type="radio" value="psk" onClick="show_wizard_wapi_settings('+i+', 2)">Personal (Pre-Shared Key)\
          </font>\
      </td>\
   </tr>\
    <tr id="wapi_psk'+i+'">\
     <td width="35%" class="bgblue"><font size="2"><b id="A87'+i+'">Pre-Shared Key Format:</b></font></td>\
      <td width="65%" class="bggrey">\
         <select name="wapiPskFormat'+i+'" onChange="">\
             <option value="0">Passphrase</option>\
             <option value="1">HEX (64 characters)</option>\
         </select>\
     </td></tr>\
    <tr id="wapi_psk_'+i+'">\
    <td width="35%" class="bgblue"><font size="2"><b id="A88'+i+'">Pre-Shared&nbsp;Key:</b></font></td>\
    <td width="65%" class="bggrey"><input type="password" name="wapiPskValue'+i+'" size="32" maxlength="64" value="">\
    </td>\
   </tr>\
    <tr id="wapi_as'+i+'">\
   <td width="35%"  class="bgblue"><font size="2"><b id="A89'+i+'">AS&nbsp;Server&nbsp;IP&nbsp;Address:</b></font></td>\
   <td width="65%"  class="bggrey"><input name="wapiASIP'+i+'" size="16" maxlength="15" value="0.0.0.0"></td>\
   </tr>\
    <tr id="wapi_as_'+i+'">\
         <td width="35%" class="bgblue"><font size="2"><b id="A90'+i+'">Select WAPI certificate:</b></font></td>\
          <td width="65%" class="bggrey"><font size="2">\
         <select size="1"  id="wapi_cert_sel" name="wapiCertSel'+i+'">\
            <option value="0"> Use Cert from Remote AS0 </option>\
            <option value="1"> Use Cert from Remote AS1 </option>\
            \
        </select> </font></td>\
    </tr>\
  </table>\
</span>\
<p id="auto_msg_'+i+'" style="width:600px;"></p>\
<span id="wep_btn'+i+'" >  \
    <table border=0 width="500" >  \
    <tr> <td colspan="2" align=right>\
          <input type="button" value="" id="click_wlan2_next'+i+'" onClick="return saveClick_wlan2(1,'+i+')">\
      </td> </tr>\
</table> \
</span>\
</span>');

    

/********************wapi end*********************************/

/**********************************************************/
    form = document.wizard ;
    // set  ecrypt
    var wpa2_idx;
    //if(ap_mode != 1)//AP
        wpa2_idx=2;
    //else//client
    //    wpa2_idx=3;
    if(encrypt[i]==0)
        form.elements["method"+i].selectedIndex = 0;
    else if(encrypt[i]==1)
        form.elements["method"+i].selectedIndex = 1;
    else if(encrypt[i]==2)
        form.elements["method"+i].selectedIndex = 2;
    else if(encrypt[i]==4 && APMode[i] != 1)
        form.elements["method"+i].selectedIndex = wpa2_idx;
    else if(encrypt[i]==4 && APMode[i] == 1)
        form.elements["method"+i].selectedIndex = wpa2_idx;
    else if(encrypt[i]==6 && APMode[i] == 1)
        form.elements["method"+i].selectedIndex = 2;
    else if(encrypt[i]==6 && APMode[i] != 1)
        form.elements["method"+i].selectedIndex = 3;
    else if(encrypt[i]==7){
        if(pocketRouter_Mode != POCKETROUTER_BRIDGE_CLIENT){
            form.elements["method"+i].selectedIndex = 5;
        }
        else{
            form.elements["method"+i].selectedIndex = 4;
        }
    }
    
    //set wep   key length
    if(wep[i] ==2 )
        form.elements["length"+i].selectedIndex = 1;
    else
        form.elements["length"+i].selectedIndex = 0;
    //set wep default key
    //form.elements["defaultTxKeyId"+i].selectedIndex = defaultKeyId[i]-1;
    
    //set pskFormat
    if(defPskFormat[i]==0)
        form.elements["pskFormat"+i].selectedIndex= 0;
    else
        form.elements["pskFormat"+i].selectedIndex= 1;

    //set wapi auth type
    if(defWapiAuth[i]==1){
        form.elements["wapiAuth"+i][0].checked=true;
        form.elements["wapiAuth"+i][1].checked=false;

        show_wizard_wapi_settings(i, 1);
    }
    else{
        form.elements["wapiAuth"+i][0].checked=false;
        form.elements["wapiAuth"+i][1].checked=true;
        show_wizard_wapi_settings(i, 2);
    }

    //set wapi-psk
    if(defWapiPskFormat[i]==0){
        form.elements["wapiPskFormat"+i].selectedIndex= 0;
    }
    else{
        form.elements["wapiPskFormat"+i].selectedIndex= 1;
    }

    form.elements["wapiPskValue"+i].value=defWapiPskValue[i];

    //set wapi-cert as
    form.elements["wapiASIP"+i].value=defWapiASIP[i];
    form.elements["wapiCertSel"+i].selectedIndex=defWapiCertSel[i];

    if(macClone[i])
        form.elements["wlanMacClone"+i].checked = true;

    if(addProfile[i])
        form.elements["wizardAddProfile"+i].checked = true;

    updateFormat(form, i);

    checkState(i);
    defPskLen[i] = form.elements["pskValue"+i].value.length;
    //set Wi-Fi roaming 20170921
    if(APMode[i]==0 || APMode[i]==3 || APMode[i]==4){
        if(encrypt[i]==4 || encrypt[i]==6){
            enableTextField(form.elements["wifi_roaming"+i]);
            if(dot11k_enable[i]==1 && IEEE80211r_enable[i]==1 && IEEE80211v_enable[i]==1){
                document.wizard.elements["wifi_roaming"+i].checked = true;
            }else{
                document.wizard.elements["wifi_roaming"+i].checked = false;
            }
        }else{
            document.wizard.elements["wifi_roaming"+i].checked = false;
            disableTextField(form.elements["wifi_roaming"+i]);
        }
    }else{
        document.wizard.elements["wifi_roaming"+i].checked = false;
        disableTextField(form.elements["wifi_roaming"+i]);
    }

    
} // end of for loop
    //set default div
    show_div(true , "top_div");
    //show_div(true , "wlan2_div1");
    form.bandSecurity.selectedIndex = bandSecurity;
    bandSecuritySelection(form.bandSecurity.value); //ADD BY TTI John 2018-04-10
    //ADD BY TTI John for checking some message 2018-04-16
    //checkMsg(form);// combine to  bandSecuritySelection function

</script>
    <script>
        var alias = 'A';
        if(typeof chg_lan != 'undefined' && chg_lan instanceof Function){
            chg_lan(WhatLanguage,alias); //TTI John 20170313 language version
        }
    </script>

    <!-- lan page -->
<!--
    <input type="hidden" value="0.0.0.0" name="lan_gateway">
    <input type="hidden" value="2" name="dhcp">
    <input type="hidden" value="0.0.0.0" name="dhcpRangeStart">
    <input type="hidden" value="0.0.0.0" name="dhcpRangeEnd">
    <input type="hidden" value="0" name="stp">
    <input type="hidden" value="000000000000" name="lan_macAddr">
-->
    <!-- wan page -->
<!--
    <input type="hidden"   value="0" name="pppConnectType">
    <input type="hidden" value="5" name="pppIdleTime">
    <input type="hidden" value="0.0.0.0" name="dsn2">
    <input type="hidden" value="0.0.0.0" name="dsn3">
    <input type="hidden" value="000000000000" name="wan_macAddr">
-->
<!--    <input type="hidden" value="0" name="upnpEnabled"> -->
    <!-- wlan1 page -->
    <span id = "wpa_auth" class = "off" >
    <table>
    <!--<input type="checkbox" name="ciphersuite1" value="tkip">TKIP
    <input type="checkbox" name="ciphersuite1" value="aes" >AES
    <input type="checkbox" name="wpa2ciphersuite1" value="tkip">TKIP
    <input type="checkbox" name="wpa2ciphersuite1" value="aes" >AES --> <!--Hidden by TTI John 2018/04/10-->
    </table>
    </span>
<!--
    <input type="hidden" value="" name="preAuth">
-->

    <!-- wlan2 page -->
</form>

</blockquote>
 <span id = "wait_div" class = "off" >
    <blockquote style="padding: 19px 32px 5px 91px;">
    <h4 id="A98">System is configuring.</h4>
            <h4 id="A99">Please wait ...</h4>
    </blockquote>
   </span>
<script>
    
function saveClickPocket_bandMode(next){

    wizardHideDiv();
    if(next)
    {
        var pocketForm=document.wizardPocketGW;
        
        var selValue = pocketForm.elements["wlBandModeSel"].value;
        
        pocketForm.elements["wlBandMode"].value = selValue;
        if(selValue == 0) //0:2g
        {
            pocketForm.elements["wlBandMode"].value = 3; //3:single
            pocketForm.elements["Band2G5GSupport"].value = 1;//1:PHYBAND_2G
        }
        else if(selValue == 1) //1:5g
        {
            pocketForm.elements["wlBandMode"].value = 3; //3:single
            pocketForm.elements["Band2G5GSupport"].value = 2;//1:PHYBAND_5G
        }
        else
        {
            pocketForm.elements["wlBandMode"].value = 2; //2:both
        }
        
        if(selValue == 2) //2: both
        {
            get_by_id("wlan0Ssid").innerHTML = "5GHz"+":<br>Wireless 1 Network Name(SSID):";
            get_by_id("wlan1Ssid").innerHTML = "2.4GHz"+":<br>Wireless 2 Network Name(SSID):";
            
            get_by_id("wlan0security").innerHTML = "5GHz"+":<br> Encryption";
            get_by_id("wlan1security").innerHTML = "2.4GHz"+":<br> Encryption";
            wlan_num = 2;
        }
        else
        {
            get_by_id("wlan0Ssid").innerHTML = "Wireless Network Name(SSID):";
            show_div(false, "pocket_wlan1_ssid_div");
            
            get_by_id("wlan0security").innerHTML = "Encryption";
            show_div(false, "pocket_wlan1_security_div");
            wlan_num = 1;
        }

        show_div(true, "pocket_wlan_name_div");
        pocketForm.elements["pocket_ssid"].focus();
        
        if(wlan_num == 2 && pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
        {
            show_div(true, "pocket_wlan1_ssid_div");
        }
    }
    else
    {
        show_div(true, "top_div");
    }
                
    
    
}

function saveClickPocket_ssid(next){
    var wizardForm=document.wizard;
    var pocketForm=document.wizardPocketGW;

    

    
    if(next)
    {
        if(pocketForm.elements["pocket_ssid"].value == "")
        {
            alert("SSID can not be empty!");
                pocketForm.elements["pocket_ssid"].focus();
            return false;
        }
        
        if(wlan_num ==2)
        {
            if(pocketForm.elements["pocket_ssid1"].value == "")
            {
                alert("SSID can not be empty!");
                pocketForm.elements["pocket_ssid1"].focus();
                return false;
            }
            
        }
        
        wizardHideDiv();
        show_div(true, ("pocket_wlan_security_div"));

            //if(pocketForm.elements["pocket_ssid"].value != pocketForm.elements["pocketAP_ssid"].value) //pocketAP_ssid is MAC
            if(pocketForm.elements["pocket_ssid"].value != pocketForm.elements["pocketAP_realssid"].value)
            {
                document.getElementById("pocket_encrypt").value = "no";
                pocketForm.elements["ciphersuite0"].value = "aes";
                pocketForm.elements["wpa2ciphersuite0"].value = "aes";
            }

            if(document.getElementById("pocket_encrypt").value != "")
            {
                if(document.getElementById("pocket_encrypt").value == "no")
                    pocketForm.elements["method0"].selectedIndex = 0;
                else if(document.getElementById("pocket_encrypt").value == "WEP")
                    pocketForm.elements["method0"].selectedIndex = 1;
                else if(document.getElementById("pocket_encrypt").value == "WPA-PSK")
                    pocketForm.elements["method0"].selectedIndex = 2;
                else
                    pocketForm.elements["method0"].selectedIndex = 3;
            }

            if(wlan_num == 2)
            {
                if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
                {
                    show_div(true, ("pocket_wlan1_security_div"));
                    if(pocketForm.elements["pocket_ssid1"].value != pocketForm.elements["pocketAP_ssid1"].value)
                    {
                        document.getElementById("pocket_encrypt1").value = "no";
                        pocketForm.elements["ciphersuite1"].value = "aes";
                        pocketForm.elements["wpa2ciphersuite1"].value = "aes";
                    }
        
                    if(document.getElementById("pocket_encrypt1").value != "")
                    {
                        var pocket_encrypt1 = document.getElementById("pocket_encrypt1").value;
                        if(pocket_encrypt1 == "no")
                            pocketForm.elements["method1"].selectedIndex = 0;
                        else if(pocket_encrypt1 == "WEP")
                            pocketForm.elements["method1"].selectedIndex = 1;
                        else if(pocket_encrypt1 == "WPA-PSK")
                            pocketForm.elements["method1"].selectedIndex = 2;
                        else
                            pocketForm.elements["method1"].selectedIndex = 3;
                    }
                }
                
            }

        pocket_checkState(0);
        pocket_checkState(1);
    }
    else
    {
        wizardHideDiv();
        if(wlan_support_92D == 1 && pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
            show_div(true, "pocket_wlan_band_mode_div");
        else
        {
            if(is_ulinker == 1)
            {
                if(wlan_support_92D == 1)
                    show_div(true, "pocket_wlan_band_mode_div");
                else
                {
                    if(pocketRouter_Mode != POCKETROUTER_GATEWAY)
                    {
                        show_div(true, "top_div");
                        show_div(true,("otg_auto_div"));
                    }
                    else
                    show_div(true, "pocket_wan_setting_div");
            }
            }
            else
            show_div(true, "top_div");
}
}
}

function saveClickPocket_Security(next){
    wizardHideDiv();
    if(next)
    {
        var pocketForm=document.wizardPocketGW;
        
        for(i=0; i<wlan_num; i++)
        {
            if(pocketForm.elements["method"+i].selectedIndex == 0)
                pocketForm.elements["wepEnabled"+i].value =  "OFF" ;
        else
                pocketForm.elements["wepEnabled"+i].value =  "ON" ;
        
        if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
        {
                if(pocketForm.elements["method"+i].selectedIndex == 4)
            {
                    pocketForm.elements["ciphersuite"+i].value = "aes/tkip";
                    pocketForm.elements["wpa2ciphersuite"+i].value = "aes/tkip";
                }
            }
        }

        document.wizardPocketGW.submit();
    }
    else
    {
        show_div(true, "pocket_wlan_name_div");
        if(wlan_num == 2)
            show_div(true, "pocket_wlan1_ssid_div");
            
    }
}

function pocket_checkState(wlanIdx)
{
    var wizardForm=document.wizard;
    var pocketForm=document.wizardPocketGW;
    
  if(pocketRouter_Mode != POCKETROUTER_BRIDGE_CLIENT){
        if(wlanIdx == 0)
        {
            var methodSelIdx = pocketForm.elements["method0"].selectedIndex;
            if(methodSelIdx == 5){ //wapi
              show_div(false,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
            show_div(true,("pocket_wapi_div"));
      }
            else if(methodSelIdx == 0){ //none
              show_div(false,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
            else if(methodSelIdx == 1){ //wep
              show_div(false,("pocket_wpa_div"));
              show_div(true,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
            else if(methodSelIdx >= 2){ //wpa~
              show_div(true,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
  }
        else if(wlanIdx == 1)
        {
            var methodSelIdx = pocketForm.elements["method1"].selectedIndex;
            if(methodSelIdx == 0){ //none
                show_div(false,("pocket_wpa_div1"));
                show_div(false,("pocket_wep_div1"));
            }
            else if(methodSelIdx == 1){ //wep
                show_div(false,("pocket_wpa_div1"));
                show_div(true,("pocket_wep_div1"));
            }
            else if(methodSelIdx >= 2){ //wpa~
                show_div(true,("pocket_wpa_div1"));
                show_div(false,("pocket_wep_div1"));
            }
        }
    }
  else
  {
      if(pocketForm.elements["method0"].selectedIndex == 4){ //wapi
              show_div(false,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
            show_div(true,("pocket_wapi_div"));
      }
      else if(pocketForm.elements["method0"].selectedIndex == 0){ //none
              show_div(false,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
      else if(pocketForm.elements["method0"].selectedIndex == 1){ //wep
              show_div(false,("pocket_wpa_div"));
              show_div(true,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
      else if(pocketForm.elements["method0"].selectedIndex >= 2){ //wpa~
              show_div(true,("pocket_wpa_div"));
              show_div(false,("pocket_wep_div"));
              show_div(false,("pocket_wapi_div"));
      }
  }

}
function show_wapi_settings(index)
{
//    alert("index="+index);
    
    if(index == 2)
     {
        get_by_id("show_wapi_psk1").style.display = "";
        get_by_id("show_wapi_psk2").style.display = "";
        get_by_id("show_wapi_as").style.display = "none";
        get_by_id("show_wapi_as2").style.display = "none";
        
       }
       else
       {
        get_by_id("show_wapi_psk1").style.display = "none";
        get_by_id("show_wapi_psk2").style.display = "none";
        get_by_id("show_wapi_as").style.display = "";
        get_by_id("show_wapi_as2").style.display = "";
       }

}

var getFFVersion=navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]
//extra height in px to add to iframe in FireFox 1.0+ browsers
var FFextraHeight=getFFVersion>=0.1? 16 : 0

function dyniframesize() {
    var iframename ="SSIDSiteSurvey";
  var pTar = null;
  if (document.getElementById){
    pTar = document.getElementById(iframename);
  }
  else{
    eval('pTar = ' + iframename + ';');
  }
  if (pTar && !window.opera){
    //begin resizing iframe
    pTar.style.display="block"
    
    if (pTar.contentDocument && pTar.contentDocument.body.offsetHeight){
      //ns6 syntax
      pTar.height = pTar.contentDocument.body.offsetHeight+FFextraHeight;
    }
    else if (pTar.Document && pTar.Document.body.scrollHeight){
      //ie5+ syntax
      pTar.height = pTar.Document.body.scrollHeight;
    }
  }
}

function pocketWlanBandModeSelection(selValue)
{
    var pocketForm=document.wizardPocketGW;
    
    pocketForm.elements["wlBandMode"].value = selValue;
    if(selValue == 0) //0:2g
    {
        pocketForm.elements["wlBandMode"].value = 3; //3:single
                pocketForm.elements["Band2G5GSupport"].value = 1;//1:PHYBAND_2G
            }
    else if(selValue == 1) //1:5g
            {
        pocketForm.elements["wlBandMode"].value = 3; //3:single
        pocketForm.elements["Band2G5GSupport"].value = 2;//1:PHYBAND_5G
        }
    else
        {
        pocketForm.elements["wlBandMode"].value = 2; //2:both
    }
}

function saveClickPocket_Wan(next)
{
    
    if(next)
    {
        form =  document.wizardPocketGW;
        if(saveChanges_wan(form) == false)
        {
            return false ;
        }
        else
        {
            if(is_ulinker == 1)
            {
                wizardHideDiv();
                if(wlan_num !=0)
                {
                    if(wlan_support_92D == 1)
                        show_div(true, ("pocket_wlan_band_mode_div"));
                    else
                        show_div(true, ("pocket_wlan_name_div"));
                }
            }
            else
                document.wizardPocketGW.submit();
        }
    }
    else
    {
        wizardHideDiv();
        show_div(true, "top_div");
        if(is_ulinker == 1)
        {
            show_div(true,("otg_auto_div"));
        }
    }
}

function pocketWanTypeInit()
{
    var wan_type=1;
    
    var pocketForm=document.wizardPocketGW;
    var wanTypeValue;
    show_div(false,"pocket_pptp_div");
  show_div(false,"pocket_dns_div");
  show_div(false,"pocket_dnsMode_div");
  show_div(false,"pocket_dhcp_div");
  show_div(false,"pocket_static_div");
    show_div(false,"pocket_l2tp_div");
    show_div(false,"pocket_pppoe_div");
    
    if(wan_type == 0)
    {
        wanTypeValue="fixedIp";
        show_div(true,"pocket_static_div");
        show_div(true,"pocket_dns_div");
        wizardPocketGW.elements["dnsMode"].value = "dnsManual";
    }
    else if(wan_type == 1)
    {
        wanTypeValue="autoIp";
        show_div(true,"pocket_dhcp_div");
    }
    else if(wan_type == 3)
    {
        wanTypeValue="ppp";
        show_div(true,"pocket_pppoe_div");
    }
    else if(wan_type == 4)
    {
        wanTypeValue="pptp";
        show_div(true,"pocket_pptp_div");
    }
    else if(wan_type == 6)
    {
        wanTypeValue="l2tp";
        show_div(true,"pocket_l2tp_div");
    }
    
    var index;
    for (index=0; index<pocketForm.elements["wanType"].length; index++)
    {

        if(wanTypeValue == pocketForm.elements["wanType"][index].value)
        {
            pocketForm.elements["wanType"].selectedIndex = index;
            pocketForm.elements["otg_wan_type"].value = wanTypeValue;
        }
    }
    
}
    
function pocketWanTypeSelection(selValue)
{

    show_div(false,"pocket_pptp_div");
  show_div(false,"pocket_dns_div");
  show_div(false,"pocket_dnsMode_div");
  show_div(false,"pocket_dhcp_div");
  show_div(false,"pocket_static_div");
    show_div(false,"pocket_l2tp_div");
    show_div(false,"pocket_pppoe_div");
    
    wizardPocketGW.elements["dnsMode"].value = "dnsAuto";
    wizardPocketGW.elements["otg_wan_type"].value = selValue;
    
    if(selValue == "fixedIp")
    {
        show_div(true,"pocket_static_div");
        show_div(true,"pocket_dns_div");
        wizardPocketGW.elements["dnsMode"].value = "dnsManual";
    }
    else if(selValue == "autoIp")
    {
        show_div(true,"pocket_dhcp_div");
    }
    else if(selValue == "ppp")
    {
        show_div(true,"pocket_pppoe_div");
    }
    else if(selValue == "pptp")
    {
        show_div(true,"pocket_pptp_div");
    }
    else if(selValue == "l2tp")
    {
        show_div(true,"pocket_l2tp_div");
    }
    
    
}

</script>
<blockquote>
<form action=/boafrm/formPocketWizard method=POST name="wizardPocketGW">
<input type=hidden id="pocket_encrypt" name="pocket_encrypt" value="">
<input type=hidden id="pocket_encrypt1" name="pocket_encrypt1" value="">
<input type=hidden id="pocketAP_ssid" name="pocketAP_ssid" value="">
<input type=hidden id="pocketAP_ssid1" name="pocketAP_ssid1" value="">
<input type=hidden id="pocketAP_realssid" name="pocketAP_realssid" value="">

<input type=hidden id="pocket_channel" name="pocket_channel" value="">
<input type=hidden id="mode0" name="mode0" value="0"*1>
<input type=hidden id="mode1" name="mode1" value="0"*1>
<input type=hidden id="wepEnabled0" name="wepEnabled0" value="OFF">
<input type=hidden id="wepEnabled1" name="wepEnabled1" value="OFF">
<input type=hidden name="wps_clear_configure_by_reg0" value=0>
<input type=hidden name="wps_clear_configure_by_reg1" value=0>
<input type=hidden name="wpaAuth0" value="psk">
<input type=hidden name="wpaAuth1" value="psk">
<input type=hidden name="ciphersuite0" value="aes">
<input type=hidden name="ciphersuite1" value="aes">
<input type=hidden name="wpa2ciphersuite0" value="aes">
<input type=hidden name="wpa2ciphersuite1" value="aes">

<INPUT type=hidden name=wlBandMode value=2>
<INPUT type=hidden name=Band2G5GSupport value=1>

<input type=hidden id="mode0" name="mode0" value="0"*1>
<input type="hidden" id="hiwanType" name="hiwanType" value="ppp">
<input type="hidden" name="pppServiceName" value="">
<input type="hidden" name="pppConnectType" value="0"> <!-- 0:Continuous -->
<input type="hidden" name="pppMtuSize" value="1492">
<input type="hidden" name="dnsMode" value="dnsAuto">
<input type="hidden" name="isPocketWizard" value="1">
<INPUT type=hidden name=wlBandMode value=2>
<input type=hidden value="0" name="otg_auto_val" id="otg_auto_val">
<input type=hidden value="0" name="otg_wan_type" id="otg_wan_type">
<INPUT type=hidden name="ulinker_opMode" value="2">

<!-- pocketRouter wan -->
<span id = "pocket_wan_setting_div" class = "off" >
<br>
<table  border=0 width="500" cellspacing=4 cellpadding=0>
  <tr><td colspan="2"><h2> WAN Interface Setup</h2></td></tr>
  <tr><td colspan="2"><font size=2>
    This page is used to configure the parameters for Internet network which
    connects to the WAN port of your Access Point.
    </td>
  </tr>
  <tr><td colspan="2"><hr size=1 noshade align=top></td></tr>
  <tr>
</table>
  

  <table border="0" width=500>
    <tr>
       <td width="30%" ><font size=2><b>WAN Access Type:</b></td>
       <td width="70%"><font size=2>
           <select size="1" name="wanType" onChange="pocketWanTypeSelection(this.value)" disable=false>
     <option value="fixedIp">Static IP</option>
    <option value="autoIp">DHCP Client</option>
     <option value="ppp">PPPoE</option>
    <option value="pptp">PPTP</option>
    <option value="l2tp">L2TP</option>
    </select>
    </td>
    <tr>
  </table>
  <span id = "pocket_static_div" class = "off" >
  <table border="0" width=500>
    <tr>
       <td width="30%"><font size=2><b>IP Address:</b></td>
       <td width="70%"><font size=2>
        <input type="text" name="wan_ip" size="18" maxlength="15" value="172.1.1.1">
      </td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Subnet Mask:</b></td>
      <td width="70%"><font size=2><input type="text" name="wan_mask" size="18" maxlength="15" value="255.255.255.0"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Default Gateway:</b></td>
      <td width="70%"><font size=2><input type="text" name="wan_gateway" size="18" maxlength="15" value="172.1.1.254"></td>
    </tr>
  </table>
  </span>
  <span id = "pocket_dhcp_div" class = "off" >
  </span>
  <span id = "pocket_pppoe_div" class = "off" >
  <table border="0" width=500>
     <tr>
      <td width="30%"><font size=2><b>User Name:</b></td>
      <td width="70%"><font size=2><input type="text" id="pppUserName" name="pppUserName" size="18" maxlength="128" value=""></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Password:</b></td>
      <td width="70%"><font size=2><input type="password" name="pppPassword" size="18" maxlength="128" value=""></td>
    </tr>
      </table>
  </span>
      
  <span id = "pocket_pptp_div" class = "off" >
  <table border="0" width=500>
    <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="dynamicIP" name="wan_pptp_use_dynamic_carrier_radio"  onClick="wan_pptp_use_dynamic_carrier_selector(this.form,0, this.value);">Dynamic IP (DHCP)</b>
      </td>
    </tr>
    <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="staticIP" name="wan_pptp_use_dynamic_carrier_radio"  onClick="wan_pptp_use_dynamic_carrier_selector(this.form, 1, this.value);">Static IP</b>
      </td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>IP Address:</b></td>
      <td width="70%"><font size=2><input type="text" name="pptpIpAddr" size="18" maxlength="30" value="172.1.1.2"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Subnet Mask:</b></td>
      <td width="70%"><font size=2><input type="text" name="pptpSubnetMask" size="18" maxlength="30" value="255.255.255.0"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Default Gateway:</b></td>
      <td width="70%"><font size=2><input type="text" name="pptpDefGw" size="18" maxlength="30" value="172.1.1.254" 0></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Server IP Address:</b></td>
      <td width="70%"><font size=2><input type="text" name="pptpServerIpAddr" size="18" maxlength="30" value="172.1.1.1"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>User Name:</b></td>
      <td width="70%"><font size=2><input type="text" name="pptpUserName" size="18" maxlength="128" value=""></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Password:</b></td>
      <td width="70%"><font size=2><input type="password" name="pptpPassword" size="18" maxlength="128" value=""></td>
    </tr>
        </table>
    </span>
    
<!-- keith: add l2tp support. 20080515  -->
    <span id = "pocket_l2tp_div" class = "off" >
    <table border="0" width=500>
    <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="dynamicIP" name="wan_l2tp_use_dynamic_carrier_radio"  onClick="wan_l2tp_use_dynamic_carrier_selector(this.form,0, this.value);">Dynamic IP (DHCP)</b>
      </td>
    </tr>
    <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="staticIP" name="wan_l2tp_use_dynamic_carrier_radio"  onClick="wan_l2tp_use_dynamic_carrier_selector(this.form,1, this.value);">Static IP</b>
      </td>
    </tr>
    <tr>

      <td width="30%"><font size=2><b>IP Address:</b></td>
      <td width="70%"><font size=2><input type="text" name="l2tpIpAddr" size="18" maxlength="30" value="172.1.1.2"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Subnet Mask:</b></td>
      <td width="70%"><font size=2><input type="text" name="l2tpSubnetMask" size="18" maxlength="30" value="255.255.255.0"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Default Gateway:</b></td>
      <td width="70%"><font size=2><input type="text" name="l2tpDefGw" size="18" maxlength="30" value="172.1.1.254" 0></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Server IP Address:</b></td>
      <td width="70%"><font size=2><input type="text" name="l2tpServerIpAddr" size="18" maxlength="30" value="172.1.1.1"></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>User Name:</b></td>
      <td width="70%"><font size=2><input type="text" name="l2tpUserName" size="18" maxlength="128" value=""></td>
    </tr>
    <tr>
      <td width="30%"><font size=2><b>Password:</b></td>
      <td width="70%"><font size=2><input type="password" name="l2tpPassword" size="18" maxlength="128" value=""></td>
    </tr>
        </table>
    </span>
    
    <span id = "pocket_dnsMode_div" class = "off" >
<!--
    <table border="0" width=500>

     <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="dnsAuto" name="dnsMode" 0 onClick="autoDNSclicked()">Attain DNS Automatically</b>
      </td>
    </tr>
    <tr>
      <td width="100%" colspan="2"><font size=2>
        <b><input type="radio" value="dnsManual" name="dnsMode" 0 onClick="manualDNSclicked()">Set DNS Manually</b>
      </td>
    </tr>
    <tr>
    </table>
-->
    </span>
    
    <span id = "pocket_dns_div" class = "off" >
    <table border="0" width=500>
    <tr>
       <td width="30%"><font size=2><b>DNS :</b></td>
       <td width="70%"><font size=2><input type="text" name="dns1" size="18" maxlength="15" value=></td>
    </tr>
    
    </table>
    </span>
<table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr> <td colspan="2" align=right>
          <input type="button" value="  Cancel  " name="cancel" onClick='cancelClick();'>
          <input type="button" value="<<Back  " name="back" onClick='saveClickPocket_Wan(0)' >
          <input type="button" value="  Finished  " name="pocket_wan_setting_div_next_btn" id="pocket_wan_setting_div_next_btn" onClick='saveClickPocket_Wan(1)'>
  </td> </tr>
</table>
<script>
            pocketWanTypeInit();
</script>
</span>

<!-- pocketRouter band mode -->
<span id = "pocket_wlan_band_mode_div" class = "off" >
<br>
<table border=0 width="500" cellspacing=0 cellpadding=0>
<tr><td><h2>Select Wireless Band</h2></td> </tr>

  <tr>
      <td><font size=2>You can select Wireless Band.</td>
  </tr>
  <tr><td><hr size=1 noshade align=top></td></tr>
</table>

<br>


<table border="0" width=500>
    <tr>
       <td width="30%" ><font size=2><b>Wireless Band:</b></td>
       <td width="70%"><font size=2>
       <select size="1" name="wlBandModeSel" onChange="pocketWlanBandModeSelection(this.value)" >
    </select>
    </td>
    <tr>
  </table>

<script>
if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP)
{
    var pocketForm=document.wizardPocketGW;
    var band2G5GSupport = wlanBand2G5G[0];
    var wlBandModeSelect = pocketForm.elements["wlBandMode"].value;
    var idx=0;
    
    if(wlan_support_92D_concurrent != 2)
    {
        pocketForm.elements["wlBandModeSel"].options[idx++] = new Option("2.4G", "0", false, false);
        pocketForm.elements["wlBandModeSel"].options[idx++] = new Option("5G", "1", false, false);
    }
    if(wlan_support_92D_concurrent == 1)
        pocketForm.elements["wlBandModeSel"].options[idx++] = new Option("2.4G/5G Concurrent", "2", false, false);
        
    if(wlBandModeSelect == 0) //0:2G only
    {
        pocketForm.elements["wlBandModeSel"].selectedIndex = 0;
    }
    else if(wlBandModeSelect == 1) //1:5G only
    {
        pocketForm.elements["wlBandModeSel"].selectedIndex = 1;
    }
    else if(wlBandModeSelect == 2) //2:both
    {
        pocketForm.elements["wlBandModeSel"].selectedIndex = 2;
    }
    else if(wlBandModeSelect == 3) //3:single
    {
        if(band2G5GSupport == 1) //1:2g
            pocketForm.elements["wlBandModeSel"].selectedIndex = 0;
        else
            pocketForm.elements["wlBandModeSel"].selectedIndex = 1;
    }
    
}
</script>

<br>
<table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr> <td colspan="2" align=right>
          <input type="button" value="  Cancel  " name="cancel" onClick='cancelClick();'>
          <input type="button" value="<<Back  " name="back" onClick='saveClickPocket_bandMode(0)' >
          <input type="button" value="  Next>>" name="next" onClick='saveClickPocket_bandMode(1)'>
  </td> </tr>
</table>

</span>
<!-- pocketRouter ssid -->
<span id = "pocket_wlan_name_div" class = "off" >
<br>
<table border=0 width="500" cellspacing=0 cellpadding=0>
<tr><td><h2>Set Wireless Network Name </h2></td> </tr>

  <tr>
      <td><font size=2>You can enter the Wireless Network Name of AP.</td>
  </tr>
  <tr><td><hr size=1 noshade align=top></td></tr>
</table>

<br>


<table border="0" width=500>
    <tr>
        <td width ="60%" valign="top">
            <font size=2> <b><SPAN id=wlan0Ssid></SPAN></b> </font>
        </td>
        <td width="70%"><input type="text" id="pocket_ssid" name="pocket_ssid" size="15" maxlength="32" value=""></td>
    </tr>
<tr><td colspan="2"><hr size=1 noshade align=top></td></tr>

<script>
if(pocketRouter_Mode == POCKETROUTER_BRIDGE_CLIENT)
    document.write('<tr><td><input type="button" value="Site Survey" name="refresh" onclick="SSIDSiteSurvey.window.siteSurvey(0);"></td></tr>');
else if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP && ulinker_opMode == 3) //ulinker_opMode //0:AP; 1:Client; 2:Router; 3:RPT
    document.write('<tr><td><input type="button" value="Site Survey" name="refresh" onclick="SSIDSiteSurvey.window.siteSurvey(0);"></td></tr>');
        
</script>



</table>

<script>
if(pocketRouter_Mode == POCKETROUTER_BRIDGE_CLIENT)
{
//document.write('<iframe width="800" height="400" id="SSIDSiteSurvey" name="SSIDSiteSurvey" frameborder="0" marginheight="0" scrolling="yes" src="pocket_sitesurvey_tmp.htm"></iframe>');
document.write('<iframe id="SSIDSiteSurvey" name="SSIDSiteSurvey" onload="javascript:{dyniframesize();}" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" src="pocket_sitesurvey.htm" width=800 height=0></iframe>');
}
else if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP && ulinker_opMode == 3) //ulinker_opMode //0:AP; 1:Client; 2:Router; 3:RPT
{
document.write('<iframe id="SSIDSiteSurvey" name="SSIDSiteSurvey" onload="javascript:{dyniframesize();}" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" src="pocket_sitesurvey.htm" width=800 height=0></iframe>');
}

    if(wpaCipher[0]*1 == 1)
        document.wizardPocketGW.elements["ciphersuite0"].value = "tkip";
    else if(wpaCipher[0]*1 == 2)
        document.wizardPocketGW.elements["ciphersuite0"].value = "aes";
    else
        document.wizardPocketGW.elements["ciphersuite0"].value = "aes/tkip";
        
    if(wpa2Cipher[0]*1 == 1)
        document.wizardPocketGW.elements["wpa2ciphersuite0"].value = "tkip";
    else if(wpa2Cipher[0]*1 == 2)
        document.wizardPocketGW.elements["wpa2ciphersuite0"].value = "aes";
    else
        document.wizardPocketGW.elements["wpa2ciphersuite0"].value = "aes/tkip";
        
    if(wlan_num == 2)
    {
        if(wpaCipher[1]*1 == 1)
            document.wizardPocketGW.elements["ciphersuite1"].value = "tkip";
        else if(wpaCipher[1]*1 == 2)
            document.wizardPocketGW.elements["ciphersuite1"].value = "aes";
        else
            document.wizardPocketGW.elements["ciphersuite1"].value = "aes/tkip";
            
        if(wpa2Cipher[1] == 1)
            document.wizardPocketGW.elements["wpa2ciphersuite1"].value = "tkip";
        else if(wpa2Cipher[1] == 2)
            document.wizardPocketGW.elements["wpa2ciphersuite1"].value = "aes";
        else
            document.wizardPocketGW.elements["wpa2ciphersuite1"].value = "aes/tkip";
    }
            
    
            
</script>

<span id = "pocket_wlan1_ssid_div" class = "off" >
    <table border="0" width=500>
        <tr>
            <tr><td style=" height:20px;"></td></tr>
            <td width ="60%" valign="top">
                <font size=2> <b><SPAN id=wlan1Ssid>ssss</SPAN></b> </font>
            </td>
            <td width="70%"><input type="text" id="pocket_ssid1" name="pocket_ssid1" size="15" maxlength="32" value=""></td>
        </tr>
    </table>
</span>


<br>
<table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr> <td colspan="2" align=right>
          <input type="button" value="  Cancel  " name="cancel" onClick='cancelClick();'>
          <input type="button" value="<<Back  " name="back" onClick='saveClickPocket_ssid(0)' >
          <input type="button" value="  Next>>" name="next" onClick='saveClickPocket_ssid(1)'>
  </td> </tr>
</table>

</span>

<!-- pocketRouter Security -->
<span id = "pocket_wlan_security_div" class = "off" >
<br>
<table border=0 width="500" cellspacing=0 cellpadding=0>
    <tr><td><h2>Select Wireless Security Mode </h2></td> </tr>

  <tr>
      <td><font size=2>This page allows you setup the wireless security. Turn on WEP or WPA by using Encryption Keys could prevent any unauthorized access to your wireless network.</td>
  </tr>
  <tr><td><hr size=1 noshade align=top></td></tr>
</table>

<br>

<table border=0 width="500" cellspacing=4 cellpadding=0>
        <td width="35%"><font size="2"><b><SPAN id=wlan0security></SPAN>:&nbsp;</b>
            <select size="1" name="method0" onChange="pocket_checkState(0)">
          <option  value="0">None</option>
          <option value="1">WEP</option>
<script>
//document.write('<option value="2">WPA</option>');
if(pocketRouter_Mode != POCKETROUTER_BRIDGE_CLIENT)
    document.write('<option value="4">WPA2</option>');
document.write('<option value="6">WPA2 Mixed</option>');
</script>
     

        </select></font></td>
</table>

<span id = "pocket_wep_div" class = "off" >
<table border=0 width="500" cellspacing=4  cellpadding=0>
  <tr>
      <td width="15%"><font size=2><b>Key Length:</b></td>
      <td width="60%"><font size=2><select size="1" name="length0" ONCHANGE="lengthClick(document.wizardPocketGW,0)">
                  <option value=1 >64-bit</option>
            <option value=2 >128-bit</option>
      </select></td>
  </tr>
  <tr>
      <td width="15%"><font size=2><b>Key Format:</b></td>
      <td width="40%"><font size=2><select size="1" name="format0" ONCHANGE="setDefaultWEPKeyValue(document.wizardPocketGW,0)">
         <option value=1>ASCII</option>
    <option value=2>Hex</option>
       </select></td>
  </tr>
  <tr>
     <td width="15%"><font size=2><b>Key Setting:</b></td>
     <td width="40%"><font size=2>
         <input type="text" name="key0" size="26" maxlength="26">
     </td>
  </tr>
</table>
</span>


<span id = "pocket_wpa_div" class = "off" >
   <table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr>
      <td width="35%"><font size="2"><b>Pre-Shared Key Format:</b></font> </td>
      <td width="65%"><font size="2"><select size="1" name="pskFormat0" onChange="pskFormatChange(wizardPocketGW,0)">
          <option value="0" >Passphrase</option>
          <option value="1" >Hex (64 characters)</option>
        </select></font></td>
    </tr>
    <tr>
      <td width="35%"><font size="2"><b>Pre-Shared Key:</b></font> </td>
      <td width="65%"><font size="2"><input type="text" name="pskValue0" size="32"  value=""></font></td>
    </tr>
     <tr></tr><tr></tr><tr></tr>
  </table>

</span>

 <script>
    if(pocketRouter_Mode == POCKETROUTER_BRIDGE_CLIENT)
        document.write('<tr><td width="100%" colspan=2><font size=2><b><input type="checkbox" name="wizardAddProfile0" value="ON">&nbsp;&nbsp; Add to Wireless Profile</b></td></tr>');
    else if(pocketRouter_Mode == POCKETROUTER_BRIDGE_AP && ulinker_opMode == 3) //ulinker_opMode //0:AP; 1:Client; 2:Router; 3:RPT
        document.write('<tr><td width="100%" colspan=2><font size=2><b><input type="checkbox" name="wizardAddProfile0" value="ON">&nbsp;&nbsp; Add to Wireless Profile</b></td></tr>');
            
</script>
      
<span id = "pocket_wapi_div" class = "off" >
   <table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr>
      <td width="30%" class="bgblue"><font size="2"><b>Authentication Mode:</b></font></td>
      <td width="70%" class="bggrey"><font size="2">
          <input  name="wapiAuth0" type="radio" value="eap" onClick="show_wapi_settings(1)">Enterprise (AS Server)
          <input name="wapiAuth0" type="radio" value="psk" onClick="show_wapi_settings(2)">Personal (Pre-Shared Key)
          </font>
      </td>
   </tr>
    <tr id="show_wapi_psk1">
     <td width="30%" class="bgblue"><font size="2"><b>Pre-Shared&nbsp;Key&nbsp;Format:</b></font></td>
      <td width="70%" class="bggrey">
         <select id="wapi_psk_fmt" name="wapiPskFormat0" onChange="">
             <option value="0">Passphrase</option>
             <option value="1">HEX (64 characters)</option>
         </select>
     </td></tr>
    <tr id="show_wapi_psk2">
    <td width="30%" class="bgblue"><font size="2"><b>Pre-Shared&nbsp;Key:</b></font></td>
    <td width="70%" class="bggrey"><input type="password" name="wapiPskValue0" id="wapipsk" size="32" maxlength="64" value="">
    </td>
   </tr>

    <tr id="show_wapi_as">
   <td width="30%" class="bgblue"><font size="2"><b>AS&nbsp;Server&nbsp;IP&nbsp;Address:</b></font></td>
   <td width="70%" class="bggrey"><input id="wapiAS_ip" name="wapiASIP0" size="16" maxlength="15" value="0.0.0.0"></td></tr>

    <tr id="show_wapi_as2">
         <td width="30%" class="bgblue"><font size="2"><b>Select WAPI certificate:</b></font></td>
          <td width="70%" class="bggrey"><font size="2">
         <select size="1"  id="wapi_cert_sel" name="wapiCertSel0">
            <option value="0"> Use Cert from Remote AS0 </option>
            <option value="1"> Use Cert from Remote AS1 </option>
            
        </select> </font></td>
    </tr>
   
   </tr>
</span>

<span id = "pocket_wlan1_security_div" class = "off" >
    <table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr><td colspan="2"><hr size=1 noshade align=top></td></tr>
   <td width="35%"><font size="2"><b><SPAN id=wlan1security></SPAN>:&nbsp;</b>
        <select size="1" name="method1" onChange="pocket_checkState(1)">
          <option  value="0">None</option>
          <option value="1">WEP</option>
<script>
    //document.write('<option value="2">WPA</option>');
if(pocketRouter_Mode != POCKETROUTER_BRIDGE_CLIENT)
        document.write('<option value="4">WPA2</option>');
document.write('<option value="6">WPA2 Mixed</option>');
</script>
     

        </select></font></td>
</table>

<span id = "pocket_wep_div1" class = "off" >
<table border=0 width="500" cellspacing=4  cellpadding=0>
  <tr>
      <td width="15%"><font size=2><b>Key Length:</b></td>
      <td width="60%"><font size=2><select size="1" name="length1" ONCHANGE="lengthClick(document.wizardPocketGW,0)">
                  <option value=1 >64-bit</option>
            <option value=2 >128-bit</option>
      </select></td>
  </tr>
  <tr>
      <td width="15%"><font size=2><b>Key Format:</b></td>
      <td width="40%"><font size=2><select size="1" name="format1" ONCHANGE="setDefaultWEPKeyValue(document.wizardPocketGW,0)">
         <option value=1>ASCII</option>
    <option value=2>Hex</option>
       </select></td>
  </tr>
  <tr>
     <td width="15%"><font size=2><b>Key Setting:</b></td>
     <td width="40%"><font size=2>
         <input type="text" name="key1" size="26" maxlength="26">
     </td>
  </tr>
</table>
</span>


<span id = "pocket_wpa_div1" class = "off" >
   <table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr>
      <td width="35%"><font size="2"><b>Pre-Shared Key Format:</b></font> </td>
      <td width="65%"><font size="2"><select size="1" name="pskFormat1" onChange="pskFormatChange(wizardPocketGW,1)">
          <option value="0" >Passphrase</option>
          <option value="1" >Hex (64 characters)</option>
        </select></font></td>
    </tr>
    <tr>
      <td width="35%"><font size="2"><b>Pre-Shared Key:</b></font> </td>
      <td width="65%"><font size="2"><input type="text" name="pskValue1" size="32"  value=""></font></td>
    </tr>
     <tr></tr><tr></tr><tr></tr>
  </table>

</span>
</span> <!--pocket_wlan1_security_div -->


<br>
<table border=0 width="500" cellspacing=4 cellpadding=0>
    <tr> <td colspan="2" align=right>
          <input type="button" value="  Cancel  " name="cancel" onClick='cancelClick();'>
          <input type="button" value="<<Back  " name="back" onClick='saveClickPocket_Security(0)' >
          <input type="button" value="  Finished  " name="next" onClick='saveClickPocket_Security(1)'>
  </td> </tr>
</table>

</span>

</form>


</blockquote>

</body>

</html>

"""
