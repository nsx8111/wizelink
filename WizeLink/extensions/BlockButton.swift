//
//  BlockButton.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/7/20.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import ObjectiveC
/**
 客製化按鈕樣式(目前主要是親子鎖使用)
 - Date: 08/25 Yang
*/
@IBDesignable
class HighlightButton: UIButton {
    
    @IBInspectable var highlightedTitleColor: UIColor?
    
    private func animateTitle(to color: UIColor?, duration: TimeInterval) {
        guard let color = highlightedTitleColor else { return }
        UIView.animate(withDuration: highlightDuration) {
            self.setTitleColor(color, for: .highlighted)
        }
    }
    
    @IBInspectable var normalBackgroundColor: UIColor? {
        didSet {
            backgroundColor = normalBackgroundColor
        }
    }
    
    @IBInspectable var highlightedBackgroundColor: UIColor?

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            if cornerRadius < 0 {
                layer.cornerRadius = frame.height / 2
            } else {
                layer.cornerRadius = cornerRadius
            }
        }
    }

    override var isHighlighted: Bool {
        didSet {
            if oldValue == false && isHighlighted {
                highlight()
            } else if oldValue == true && !isHighlighted {
                unHighlight()
            }
        }
    }

    var highlightDuration: TimeInterval = 0.25
    
    private func animateBackground(to color: UIColor?, duration: TimeInterval) {
        guard let color = color else { return }
        UIView.animate(withDuration: highlightDuration) {
            self.backgroundColor = color
        }
    }
    
    func highlight() {
        animateTitle(to: highlightedTitleColor, duration: highlightDuration)
        animateBackground(to: highlightedBackgroundColor, duration: highlightDuration)
    }

    func unHighlight() {
        animateBackground(to: normalBackgroundColor, duration: highlightDuration)
    }
    
}
