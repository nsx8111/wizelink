//
//  AES128CBC.swift
//  ProjectSupportingIOS12n13
//
//  Created by 蒼月喵 on 2020/5/13.
//  Copyright © 2020 Natalie. All rights reserved.
//

import Foundation
import CryptoSwift


var newPwdStr : String = ""
var keyStr : String = ""
var ivStr : String = ""
var AuthKeyStr : String = ""
var AuthKeyAESStr : String = ""

extension String {
    
    /// AES加密
    ///
    /// - Parameters:
    ///   - key: key
    ///   - iv: iv
    /// - Returns: String
    func aesEncrypt(key: String, iv: String) -> String? {
        var result: String?
        do {
            // 用UTF8的编碼方式將字串轉成Data
            let data: Data = self.data(using: String.Encoding.utf8, allowLossyConversion: true)!
            
            // 用AES的方式將Data加密
            let aecEnc: AES = try AES(key: key, iv: iv)
            let enc = try aecEnc.encrypt(data.bytes)
            
            // 使用Base64編碼方式將Data轉回字串
            let encData: Data = Data(bytes: enc, count: enc.count)
            result = encData.base64EncodedString()
        } catch {
            print("\(error.localizedDescription)")
        }
        if result != nil{
            let end = result!.index(result!.endIndex, offsetBy: -2)
            return String(result![..<end])
        }
        
        return result
    }
    
}
