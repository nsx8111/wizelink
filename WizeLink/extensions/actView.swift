//
//  actView.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/5/4.
//  Copyright © 2020 YangYang. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension UIViewController {
    func setActView(actView:UIView){
        /** 環形進度圈，用來表示讀取資料中 */
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        actView.addSubview(activityIndicator)
        //轉圈圈 loading 圖示的設定
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style  = UIActivityIndicatorView.Style.whiteLarge
//        activityIndicator.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        activityIndicator.center = view.center
//        activityIndicator.snp.makeConstraints { (makes) in
//            makes.center.equalToSuperview()
//            makes.height.width.equalTo(100)
//        }
        activityIndicator.startAnimating()
//        activityIndicator.size
        self.view.addSubview(actView)
        actView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
        actView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
    }
}
