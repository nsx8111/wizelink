//
//  cloudFuncs.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/5/20.
//  Copyright © 2020 YangYang. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

var cloudState : Bool = false
//let cloudServerUrl : String = "https://wizelink-api.appspot.com/"
let cloudServerUrl : String = "https://tti-ep.tti.tv:8081/"

var emailStr : String = ""
var passStr : String = ""

func cloudLogin(completion : @escaping ()->()){
    let parameters: [String: String] = [
        "Email": emailStr,
        "Password": passStr
    ]
    print("request : \(cloudServerUrl)api/v1/User/Login")
    AF.request("\(cloudServerUrl)api/v1/User/Login",method: .post, parameters: parameters).responseJSON { (response) in
        //TODO 連線失敗會當機 要修正為跳提示訊息 ,0615 done
        if response.value == nil{
            print("cloud login error")
//            break
            cloudState = false
            completion()
        }else{
            print("response.value != nil")
            let swiftyJsonVar = JSON(response.value!)
                    print(swiftyJsonVar)
                    print(swiftyJsonVar["Status"])

                    if swiftyJsonVar["Status"] == "SUCCESS"{
                        print("Login success")
                        let dataJson = JSON(swiftyJsonVar["Data"])
                        print("AuthKey:\(dataJson["AuthKey"])")
                        AuthKeyStr = dataJson["AuthKey"].string!
                        //                emailStr = dataJson["Email"].string!
                        keyStr = dataJson["SaltKey1"].string!
                        ivStr = dataJson["SaltKey2"].string!
                        AuthKeyAESStr = AuthKeyStr.aesEncrypt(key: keyStr, iv: ivStr)!
                        print("AuthKeyAESStr:\(AuthKeyAESStr)")

                        //                self.sOrELabel.text = "Success"
            //            self.changeLabel(true)
                        cloudState = true
                        completion()
                    }else{
                        print("Login error")
                        print("Msg:\(swiftyJsonVar["Msg"])")

                        //                self.sOrELabel.text = "Error"
            //            self.changeLabel(false)
                        cloudState = false
                        completion()
                    }
            
        }
        
    }
}

func cloudGetDevice (completion : @escaping ()->()){
    let parameters: [String: String] = [
        "Email": emailStr,
        "AuthKey": AuthKeyAESStr
    ]
    
    AF.request("\(cloudServerUrl)api/v1/User/GetDevices",method: .post, parameters: parameters).responseJSON { (response) in
        let swiftyJsonVar = JSON(response.value!)
        print(swiftyJsonVar)
        
        print(swiftyJsonVar["Status"])
        
        if swiftyJsonVar["Status"] == "SUCCESS"{
            print("GetDevice success")
            let dataJson = JSON(swiftyJsonVar["Data"])
            print("dataJson.count:\(dataJson.count)")
            let devicesArray = dataJson["Devices"].arrayObject
            print("devicesArray?.count:\(String(describing: devicesArray?.count))")
            
            // 這邊不知道為什麼要用 Array 來存放 devices 資料，我先設定取資料目標為第一組資料
            for device in devicesArray!{
                let deviceJSON = JSON(device)
                print("deviceJSON[\"DeviceAddr\"].string :")
                print(deviceJSON["DeviceAddr"].string ?? "error")
                let dvaddr = deviceJSON["DeviceAddr"].string ?? "error"
                serverUrlDict["cloud"] = "http://\(dvaddr):8080/"
//                print(serverUrlDict["cloud"])
                serverType = .cloud
                changeHtms()
            }
            completion()

        }else{
            print("GetDevice error")
            print("Msg:\(swiftyJsonVar["Msg"])")
            completion()
        }
    }
}

var cloudRegResult : Bool = false
var cloundErrorMsg : String = ""

func cloudRegister(completion : @escaping ()->()){
    let parameters: [String: String] = [
        "Email": emailStr,
        "Password": passStr
    ]
//    https://tti-ep.tti.tv:8080/api/v1/User/Register
    print("\(cloudServerUrl)api/v1/User/Register")
    AF.request("\(cloudServerUrl)api/v1/User/Register",method: .post, parameters: parameters).responseJSON { (response) in
        let swiftyJsonVar = JSON(response.value!)
        print(swiftyJsonVar)
        
        print(swiftyJsonVar["Status"])
        
        if swiftyJsonVar["Status"] == "SUCCESS"{
            print("Register success")
            
            let dataJson = JSON(swiftyJsonVar["Data"])
            print("AuthKey:\(dataJson["AuthKey"])")
            AuthKeyStr = dataJson["AuthKey"].string!
            //                emailStr = dataJson["Email"].string!
            keyStr = dataJson["SaltKey1"].string!
            ivStr = dataJson["SaltKey2"].string!
            AuthKeyAESStr = AuthKeyStr.aesEncrypt(key: keyStr, iv: ivStr)!
            print("AuthKeyAESStr:\(AuthKeyAESStr)")
            
            cloudRegResult = true
            completion()
        }else{
            print("Register error")
            print("Msg:\(swiftyJsonVar["Msg"])")
            cloundErrorMsg = swiftyJsonVar["Msg"].string ?? "Register error"
            cloudRegResult = false
            completion()
        }
    }
}

func cloudActiveAccount(completion : @escaping ()->()){
    let parameters: [String: String] = [
        "Email": emailStr,
        "AuthKey": AuthKeyAESStr
    ]
    print("\(cloudServerUrl)api/v1/User/ActiveAccount")
    AF.request("\(cloudServerUrl)api/v1/User/ActiveAccount",method: .post, parameters: parameters).responseJSON { (response) in
        let swiftyJsonVar = JSON(response.value!)
        print(swiftyJsonVar)
        
        print(swiftyJsonVar["Status"])
        
        if swiftyJsonVar["Status"] == "SUCCESS"{
            print("ActiveAccount success")
            
            cloudRegResult = true
            completion()
        }else{
            print("ActiveAccount error")
            print("Msg:\(swiftyJsonVar["Msg"])")
            cloundErrorMsg = swiftyJsonVar["Msg"].string ?? "ActiveAccount error"
            cloudRegResult = false
            completion()
        }
    }
}

func cloudCreateDevices(completion : @escaping ()->()){
    // get DeviceGuid, DeviceSn, DeviceAddr
    let deviceName : String = searchFunc(src: wizardHtmData, str1: "var moduleName=\"", str2: "\";")
    let deviceGUID : String = searchFunc(src: wizardHtmData, str1: "var GUIDNum = '", str2: "';")
    let deviceSN : String = searchFunc(src: wizardHtmData, str1: "var SerialNum = '", str2: "';")
    let deviceWanIP : String = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
    
    let parameters: [String: String] = [
        "Email": emailStr,
        "AuthKey": AuthKeyAESStr,
        "DeviceName": deviceName,
        "DeviceType":"Router",
        "DeviceGuid": deviceGUID,
        "DeviceSn": deviceSN,
        "DeviceAddr": deviceWanIP
    ]
    
    print("parameters:\(parameters)")
    //    //TODO:
    //    //DeviceName, DeviceType, DeviceGuid, DeviceSn 這些應該要從 server 撈取，測試方便先寫死
    //    //Guid & SN 測試方便先微調
    //    let parameters: [String: String] = [
    //        "Email": emailStr,
    //        "AuthKey": AuthKeyAESStr,
    //        "DeviceName":"WAP-3516",
    //        "DeviceType":"Router",
    //        "DeviceGuid":"9292024C-0809-49FB-9908-083E275B9EDE",
    //        "DeviceSn":"01D047S35165",
    //        "DeviceAddr":"10.118.209.125"
    //    ]
    print("\(cloudServerUrl)api/v1/User/CreateDevices")
    AF.request("\(cloudServerUrl)api/v1/User/CreateDevices",method: .post, parameters: parameters).responseJSON { (response) in
        let swiftyJsonVar = JSON(response.value!)
        print(swiftyJsonVar)
        
        print(swiftyJsonVar["Status"])
        
        if swiftyJsonVar["Status"] == "SUCCESS"{
            print("cloudCreateDevices success")
            
            cloudRegResult = true
            completion()
        }else{
            print("cloudCreateDevices error")
            print("Msg:\(swiftyJsonVar["Msg"])")
            cloundErrorMsg = swiftyJsonVar["Msg"].string ?? "ActiveAccount error"
            cloudRegResult = false
            completion()
        }
    }
    
    
}
