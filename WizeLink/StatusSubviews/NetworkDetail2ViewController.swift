//import UIKit
//import Foundation
//
//struct NetworkConfiguration2 {
//    var support_type: String = ""
//    var version_type: String = ""
//}
//
//class NetworkDetail2ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
//
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            return types.count
//        }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "NetworkConfigurationCell2")
//
//        cell.selectionStyle = .none
//        cell.textLabel?.text = types[indexPath.row].support_type
//        cell.textLabel?.textAlignment = .left
//        cell.textLabel?.textColor = PrimaryColor2
//        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
//
//        cell.detailTextLabel?.text = types[indexPath.row].version_type
//        cell.detailTextLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
//        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
//        cell.detailTextLabel?.textColor = .white
//
//        if (indexPath.row) % 2 == 0{
//            cell.backgroundColor = LightSecondaryColor2
//        }else{
//            cell.backgroundColor = PrimaryColor1
//        }
//        return cell
//    }
//
//    func setTableView(){
//        NetworkConfigurationTableView = UITableView()
//        NetworkConfigurationTableView.register(UITableViewCell.self, forCellReuseIdentifier: "NetworkConfigurationCell2")
//        NetworkConfigurationTableView.delegate = self
//        NetworkConfigurationTableView.dataSource = self
//        NetworkConfigurationTableView.bounces = false
//        NetworkConfigurationTableView.allowsSelection = true
//        NetworkConfigurationTableView.backgroundColor = LightSecondaryColor2
//        NetworkConfigurationTableView.rowHeight = fullScreenSize.height*0.067
//        self.view.addSubview(NetworkConfigurationTableView)
//
//        NetworkConfigurationTableView.snp.makeConstraints { (make) in
//            make.top.equalTo(NetworkConfigurationLabel.snp.bottom).offset(spacing2)
//            make.width.equalTo(fullScreenSize.width)
//            make.height.equalTo(NetworkConfigurationTableView.rowHeight*5) //預設先七列
//            make.centerX.equalToSuperview()
//        }
//
//    }
//
//    var types = [
//        NetworkConfiguration2(support_type: "IP位址", version_type: "192.168.10.1"),
//        NetworkConfiguration2(support_type: "子網路遮罩", version_type: "255.255.255.0"),
//           NetworkConfiguration2(support_type: "預設閘道", version_type: "192.168.10.1"),
//        NetworkConfiguration2(support_type: "DHCP伺服器", version_type: "Enabled"),
//        NetworkConfiguration2(support_type: "MAC位址", version_type: "AC:6F:BB:1F:03:A7")
//    ]
//
//    let backButton = UIButton()
//    let reloadButton = UIButton()
//    let bkgImageView2 = UIImageView()
//    let NetworkConfigurationLabel = UILabel()
//    private var NetworkConfigurationTableView: UITableView!
//    var NetworkConfigurations: [NetworkConfiguration] = []
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        bkgImageView2.backgroundColor = PrimaryColor1
//        self.view.addSubview(bkgImageView2)
//        bkgImageView2.snp.makeConstraints{ (makes) in
//            makes.edges.equalToSuperview()
//        }
//
//        backButton.setImage(UIImage(named: "go-back"), for: .normal)
//        backButton.isEnabled = true
//        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
//        self.view.addSubview(backButton)
//
//        backButton.snp.makeConstraints { (makes) in
//            makes.top.equalToSuperview().offset(loginLabelHeight)
//            makes.width.height.equalTo(loginLabelHeight/1.5)
//            makes.left.equalToSuperview().offset(spacing2)
//        }
//
//        reloadButton.setImage(UIImage(named: "start"), for: .normal)
//        reloadButton.backgroundColor = UIColor.clear
//        reloadButton.isEnabled = true
//        reloadButton.addTarget(self,action: #selector(self.reloadVC),for: .touchUpInside)
//        self.view.addSubview(reloadButton)
//
//        reloadButton.snp.makeConstraints { (makes) in
//            makes.centerY.equalTo(backButton)
//            makes.width.height.equalTo(loginLabelHeight/1.5)
//            makes.right.equalToSuperview().offset(-spacing2)
//        }
//
//        NetworkConfigurationLabel.backgroundColor = .clear
//        NetworkConfigurationLabel.attributedText = NSAttributedString(string: networkNameLabel, attributes:
//            nil)
//        NetworkConfigurationLabel.font = UIFont.boldSystemFont(ofSize: 18)
//        NetworkConfigurationLabel.textColor = .white
//        NetworkConfigurationLabel.textAlignment = .left
//        self.view.addSubview(NetworkConfigurationLabel)
//        NetworkConfigurationLabel.snp.makeConstraints { (makes) in
//            makes.top.equalTo(backButton.snp.bottom).offset(loginLabelHeight-5)
//            makes.width.equalTo(loginLabelWidth*5)
//            makes.height.equalTo(loginLabelHeight)
//            makes.left.equalToSuperview().offset(spacing)
//        }
//        
//        setTableView()
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
////           self.navigationController?.setNavigationBarHidden(true, animated: false)
//       }
//
//    @objc func backVC(){
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    @objc func reloadVC(){
//
//    }
//
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
