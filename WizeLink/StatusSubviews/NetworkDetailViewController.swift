//
//  NetworkDetailViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import Foundation
import SnapKit
/**
 狀態資訊detail頁
 - Date: 08/25 Yang
*/
struct NetworkConfiguration {
    var titleLabel: String = ""
    var detailLabel: String = ""
}

class NetworkDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if NetworkConfigurationTableView == tableView{
            return types.count
        }else{
            return types2.count
        }
    }
    /**
     顯示上下兩個表格 網路配置跟GuestNetwork配置
     - Date: 08/25 Yang
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "NetworkConfigurationCell")
        let cell2 = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "GuestNetworkCell")
        
        if NetworkConfigurationTableView == tableView{
            cell.selectionStyle = .none
            cell.textLabel?.text = types[indexPath.row].titleLabel
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.textColor = PrimaryColor2
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.text = types[indexPath.row].detailLabel
            cell.detailTextLabel?.textAlignment = NSTextAlignment.center
            cell.detailTextLabel?.textColor = .white
            
            ///currentDevice.deviceType 調整各裝置button跟Label的圓角跟字體大小
            switch currentDevice.deviceType {
            case .iPhone40:
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12)
            case .iPhone47:
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
            case .iPhone55:
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 16)
            case .iPad:
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 21)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 21)
            default:
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 13.5)
                cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.5)
            }
            
            if (indexPath.row) % 2 == 0{
                cell.backgroundColor = LightSecondaryColor2
            }else{
                cell.backgroundColor = PrimaryColor1
            }
            return cell
        }else{
            cell2.selectionStyle = .none
            cell2.textLabel?.text = types2[indexPath.row].titleLabel
            cell2.textLabel?.textAlignment = .left
            cell2.textLabel?.textColor = PrimaryColor2
            cell2.detailTextLabel?.text = types2[indexPath.row].detailLabel
            cell2.detailTextLabel?.textAlignment = NSTextAlignment.center
            cell2.detailTextLabel?.textColor = .white
            
            switch currentDevice.deviceType {
            case .iPhone40:
                cell2.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 12)
            case .iPhone47:
                cell2.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
            case .iPhone55:
                cell2.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 16)
            case .iPad:
                cell2.textLabel?.font = UIFont.boldSystemFont(ofSize: 21)
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 21)
            default:
                cell2.textLabel?.font = UIFont.boldSystemFont(ofSize: 13.5)
                cell2.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.5)
            }
            
            if (indexPath.row) % 2 == 0{
                cell2.backgroundColor = LightSecondaryColor2
            }else{
                cell2.backgroundColor = PrimaryColor1
            }
            return cell2
        }
    }
   
    let RefreshScrollView = UIScrollView()
    let ContentView = UIView()
    let actView : UIView = UIView()
    let backButton = UIButton()
    let reloadButton = UIButton(type: .custom)
    let reloadImage = UIImage(named: "reload-outline")?.withRenderingMode(.alwaysTemplate)
    let bkView = UIView()
    let NetworkConfigurationLabel = UILabel()
    let GuestNetworkLabel = UILabel()
    private var NetworkConfigurationTableView: UITableView!
    private var GuestNetworkTableView: UITableView!
    var types : [NetworkConfiguration] = []
    var types2 : [NetworkConfiguration] = []
    var typesFor24G_GN:[NetworkConfiguration] = []
    var typesFor5G_GN:[NetworkConfiguration] = []
    var typesFor24G:[NetworkConfiguration] = []
    var typesFor5G:[NetworkConfiguration] = []
    var typesForLan:[NetworkConfiguration] = []
    var typesForWan:[NetworkConfiguration] = []
    var constraintForBottom1 : Constraint?
    var constraintForBottom2 : Constraint?
    
    /**
     根據無線網路頁GuestNetwork是否開啟來決定是否顯示GuestNetworkLabel跟GuestNetworkTableView
     - Date: 08/25 Yang
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        typesFor24G_GN = types24G
        typesFor5G_GN = types5G
        searchInfo()
        if typesNum == 0{
            if GNisOn == true{
                types2 = typesFor24G_GN
                GuestNetworkLabel.isHidden = false
            }else{
                types2 = []
                GuestNetworkLabel.isHidden = true
            }
        }else if typesNum == 1{
            if GNisOn2 == true{
                types2 = typesFor5G_GN
                GuestNetworkLabel.isHidden = false
            }else{
                types2 = []
                GuestNetworkLabel.isHidden = true
            }
        }
        setViews()
    }
    /**
     刷新此頁面 並同步刷新其他頁相關資料
     - Date: 08/25 Yang
    */
    func RefreshData(){
        self.setActView(actView: self.actView)
        getData{
            let IpValue = searchFunc(src: statusHtmData, str1: "var wan_ip='", str2: "';")
            ///判斷是否離線 離線則彈出alert提示
            if IpValue == "" || IpValue == "0.0.0.0" {
                self.alertMsg(title: localString(key: "ERR_CONNECTION", table: .alertTitle), msg: localString(key: "ERR_CONNECTION", table: .alertMsg))
                self.actView.removeFromSuperview()
                return
            }
            ///同時刷新WirelessVC
            let name2 = Notification.Name("channelNumNow")
            NotificationCenter.default.post(name: name2, object: nil, userInfo:
                nil)
            let name3 = Notification.Name("channelNumNow2")
            NotificationCenter.default.post(name: name3, object: nil, userInfo:
                nil)
            ///同時刷新ConnectVC
            let name4 = Notification.Name("Refresh_ConnectVC")
            NotificationCenter.default.post(name: name4, object: nil, userInfo:
                nil)
            ///同時刷新StatusVC searchInfo()
            let name = Notification.Name("Refresh_StatusVC")
            NotificationCenter.default.post(name: name, object: nil, userInfo:
                nil)
            self.searchInfo()
            if typesNum == 0{
                if GNisOn == true{
                    self.types2 = self.typesFor24G_GN
                    types24G = self.typesFor24G_GN
                    self.GuestNetworkLabel.isHidden = false
                    self.constraintForBottom1?.activate()
                    self.constraintForBottom2?.deactivate()
                }else{
                    self.types2 = []
                    types24G = self.types2
                    self.GuestNetworkLabel.isHidden = true
                    self.constraintForBottom1?.deactivate()
                    self.constraintForBottom2?.activate()
                }
                self.types.removeAll()
                self.types.append(contentsOf: self.typesFor24G)
            }else if typesNum == 1{
                if GNisOn2 == true{
                    self.types2 = self.typesFor5G_GN
                    types5G = self.typesFor5G_GN
                    self.GuestNetworkLabel.isHidden = false
                    self.constraintForBottom1?.activate()
                    self.constraintForBottom2?.deactivate()
                }else{
                    self.types2 = []
                    types5G = self.types2
                    self.GuestNetworkLabel.isHidden = true
                    self.constraintForBottom1?.deactivate()
                    self.constraintForBottom2?.activate()
                }
                self.types = self.typesFor5G
            }else if typesNum == 2{
                self.types = self.typesForLan
                self.constraintForBottom1?.deactivate()
                self.constraintForBottom2?.activate()
            }else if typesNum == 3{
                self.types = self.typesForWan
                self.constraintForBottom1?.deactivate()
                self.constraintForBottom2?.activate()
            }
            ///必須重新updateConstraint 否則無法顯示更新資料後的tableView
            self.NetworkConfigurationTableView.snp.updateConstraints { (makes) in
                makes.height.equalTo(self.NetworkConfigurationTableView.rowHeight * CGFloat(self.types.count))
            }
            self.GuestNetworkTableView.snp.updateConstraints { (makes) in
                makes.height.equalTo(self.GuestNetworkTableView.rowHeight * CGFloat(self.types2.count))
            }
            DispatchQueue.main.async {
                self.NetworkConfigurationTableView.reloadData()
                self.GuestNetworkTableView.reloadData()
            }
            self.actView.removeFromSuperview()
        }
    }
    /**
     下滑頁面會刷新資料
     - Date: 08/25 Yang
    */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.contentOffset.y < -labelWidth*1.2*HeightForScroll{
            RefreshData()
        }
    }
    /**
     初始畫面
     - Date: 08/25 Yang
    */
    func setViews(){
        bkView.backgroundColor = PrimaryColor1
        self.view.addSubview(bkView)
        bkView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset((statusBarHeight+spacing1)/1.1)
            makes.width.height.equalTo(loginLabelHeight/1.1)
            makes.left.equalToSuperview().offset(spacing3)
        }
        
        reloadButton.setImage(reloadImage, for: .normal)
        reloadButton.tintColor = SecondaryColor3
        reloadButton.backgroundColor = UIColor.clear
        reloadButton.isEnabled = true
        reloadButton.addTarget(self,action: #selector(self.reloadVC),for: .touchUpInside)
        self.view.addSubview(reloadButton)
        reloadButton.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(backButton)
            makes.width.height.equalTo(loginLabelHeight/1.2)
            makes.right.equalToSuperview().offset(-spacing1)
        }
        ///下滑刷新資料用
        RefreshScrollView.bounces = true
        RefreshScrollView.delegate = self
        bkView.addSubview(RefreshScrollView)
        RefreshScrollView.snp.makeConstraints { (makes) in
            makes.left.right.bottom.equalTo(bkView)
            makes.top.equalTo(backButton.snp.bottom).offset(spacing2)
        }
        ///元件需置於ContentView上方能滑動
        RefreshScrollView.addSubview(ContentView)
        ContentView.snp.makeConstraints { (makes) in
            makes.top.bottom.equalTo(RefreshScrollView)
            makes.left.right.equalTo(bkView)
        }
        
        NetworkConfigurationLabel.backgroundColor = .clear
        NetworkConfigurationLabel.attributedText = NSAttributedString(string: networkNameLabel, attributes:
            nil)
        NetworkConfigurationLabel.textColor = .white
        NetworkConfigurationLabel.textAlignment = .left
        ContentView.addSubview(NetworkConfigurationLabel)
        NetworkConfigurationLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(RefreshScrollView).offset(0)
            makes.centerX.equalToSuperview()
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }
        ///無線網路或區域網路配置表格
        NetworkConfigurationTableView = UITableView()
        NetworkConfigurationTableView.register(UITableViewCell.self, forCellReuseIdentifier: "NetworkConfigurationCell")
        NetworkConfigurationTableView.delegate = self
        NetworkConfigurationTableView.dataSource = self
        NetworkConfigurationTableView.bounces = false
        NetworkConfigurationTableView.allowsSelection = true
        NetworkConfigurationTableView.separatorStyle = .none
        NetworkConfigurationTableView.backgroundColor = LightSecondaryColor2
        NetworkConfigurationTableView.rowHeight = fullScreenSize.height*0.067
        ContentView.addSubview(NetworkConfigurationTableView)
        NetworkConfigurationTableView.snp.makeConstraints { (make) in
            make.top.equalTo(NetworkConfigurationLabel.snp.bottom).offset(spacing1)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(NetworkConfigurationTableView.rowHeight * CGFloat(types.count))
            make.centerX.equalToSuperview()
        }
        
        GuestNetworkLabel.backgroundColor = .clear
        GuestNetworkLabel.attributedText = NSAttributedString(string: GNLabel, attributes:
        nil)
        GuestNetworkLabel.textColor = .white
        GuestNetworkLabel.textAlignment = .left
        GuestNetworkLabel.numberOfLines = 0
        ContentView.addSubview(GuestNetworkLabel)
        GuestNetworkLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(NetworkConfigurationTableView.snp.bottom).offset(spacing3)
            makes.centerX.equalToSuperview()
            makes.left.equalToSuperview().offset(spacing2)
        }
        ///GuestNetwork配置表格
        GuestNetworkTableView = UITableView()
        GuestNetworkTableView.register(UITableViewCell.self, forCellReuseIdentifier: "GuestNetworkCell")
        GuestNetworkTableView.delegate = self
        GuestNetworkTableView.dataSource = self
        GuestNetworkTableView.bounces = false
        GuestNetworkTableView.allowsSelection = true
        GuestNetworkTableView.separatorStyle = .none
        GuestNetworkTableView.backgroundColor = LightSecondaryColor2
        GuestNetworkTableView.rowHeight = fullScreenSize.height*0.067
        ContentView.addSubview(GuestNetworkTableView)
        GuestNetworkTableView.snp.makeConstraints { (make) in
            make.top.equalTo(GuestNetworkLabel.snp.bottom).offset(spacing1)
            make.width.equalTo(fullScreenSize.width)
            make.height.equalTo(GuestNetworkTableView.rowHeight * CGFloat(types2.count))
            make.centerX.equalToSuperview()
            ///判斷不同裝置時 下滑的幅度佈局有不同的constraint
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                constraintForBottom1 = make.bottom.equalToSuperview().offset(-spacing4*4*HeightForScroll).constraint
            case .phone:
                constraintForBottom1 = make.bottom.equalToSuperview().offset(-spacing4*2*HeightForScroll).constraint
            default:
                print("default")
            }
            constraintForBottom2 = make.bottom.equalToSuperview().offset(-(spacing4+spacing1+spacing3+loginLabelHeight+fullScreenSize.height*0.067*5.0)*HeightForScroll).constraint
        }
        if typesNum == 0{
            if GNisOn == true{
                constraintForBottom1?.activate()
                constraintForBottom2?.deactivate()
            }else{
                constraintForBottom1?.deactivate()
                constraintForBottom2?.activate()
            }
        }else if typesNum == 1{
            if GNisOn2 == true{
                constraintForBottom1?.activate()
                constraintForBottom2?.deactivate()
            }else{
                constraintForBottom1?.deactivate()
                constraintForBottom2?.activate()
            }
        }else{
            constraintForBottom1?.deactivate()
            constraintForBottom2?.activate()
        }
        switch currentDevice.deviceType {
        case .iPad:
            NetworkConfigurationLabel.font = UIFont.boldSystemFont(ofSize: 24)
            GuestNetworkLabel.font = UIFont.boldSystemFont(ofSize: 24)
        default:
            NetworkConfigurationLabel.font = UIFont.boldSystemFont(ofSize: 18)
            GuestNetworkLabel.font = UIFont.boldSystemFont(ofSize: 18)
        }
    }
    /**
     返回上一頁
     - Date: 08/25 Yang
    */
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    /**
     刷新資料
     - Date: 08/25 Yang
    */
    @objc func reloadVC(){
        RefreshData()
    }
    /**
     撈資料
     - Date: 08/25 Yang
    */
    func searchInfo(){
        //for test
        //        statusHtmData = tempStatusHtmData
        //Mike debug
        // Mike mark
        //        print("""
        //            ==statusHtmData @ StatusVC.searchInfo()==
        //            \(statusHtmData)
        //            =========================================
        //            """)
        //record band
        
        let aa = statusHtmData.components(separatedBy: "band[0] =")
        if aa.count < 2 {
            self.actView.removeFromSuperview()
            return
        }
        
        var tempValueString : String = ""
        //        var tempValueString2 : String = ""
        typesFor5G_GN = []
        typesFor5G = []
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "MODE", table: .dataTitle), detailLabel: "AP"))

        let aa2 = aa[1].components(separatedBy: ";")
        tempValueString = aa2[0]
        print("test_statusHtmData")
        //        let rangeA1:Range = statusHtmData.range(of: "band[0] =")!
        //        let rangeA2:Range = statusHtmData.range(of: ";\n        ssid_drv[0] ='")!
        //        tempValueString  = String(statusHtmData[rangeA1.upperBound ..< rangeA2.lowerBound ])
        
        let bandStringArray = statusHtmData.components(separatedBy: "if (band[i] == ")
        //        print("bandStringArray.count:\(bandStringArray.count)")
        for bandString in bandStringArray{
            let bbbb = bandString.split(separator: ")")
            //            print(bbbb[0])
            //要找到的頻段
            if bbbb[0] == tempValueString{
                let bbbb2 = bbbb[1].components(separatedBy: ".write( \"")
                let bbbb3 = bbbb2[1].components(separatedBy: "\");")
                //                print("bbbb3[0]:\(bbbb3[0]))")
                typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
                typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
            }
        }
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: SSID2))
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: wep2))
        typesFor5G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: clientNum2))
        types5G = typesFor5G_GN

        var cccc = statusHtmData.components(separatedBy: "ssid_drv[0] ='")
        var cccc2 = cccc[1].components(separatedBy: "';")
        
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "channel_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "CHANNEL", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "wep[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: "BSSID", detailLabel: "\(cccc2[0].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "clientnum[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor5G.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        
        typesFor24G_GN = []
        typesFor24G = []
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "MODE", table: .dataTitle), detailLabel: "AP"))
        
        cccc = statusHtmData.components(separatedBy: "band[1] =")
        tempValueString = String(cccc[1].split(separator: ";")[0])
        //        bandStringArray = statusHtmData.components(separatedBy: "if (band[i] == ")
        //        print("bandStringArray.count:\(bandStringArray.count)")
        for bandString in bandStringArray{
            let bbbb = bandString.split(separator: ")")
            //            print(bbbb[0])
            //要找到的頻段
            if bbbb[0] == tempValueString{
                let bbbb2 = bbbb[1].components(separatedBy: ".write( \"")
                let bbbb3 = bbbb2[1].components(separatedBy: "\");")
                //                print("bbbb3[0]:\(bbbb3[0]))")
                typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
                typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "BAND", table: .dataTitle), detailLabel: "\(bbbb3[0]))"))
            }
        }
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: SSID))
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: wep))
        typesFor24G_GN.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: clientNum))
        types24G = typesFor24G_GN

        cccc = statusHtmData.components(separatedBy: "ssid_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        //        print("")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "WIFI_NAME", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "channel_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "CHANNEL", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "wep[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "ENCRYPTION", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: "BSSID", detailLabel: "\(cccc2[0].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "clientnum[1] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesFor24G.append(NetworkConfiguration(titleLabel: localString(key: "ASSOCIATED_CLIENTS", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        
        typesForLan = []
        cccc = statusHtmData.components(separatedBy: "var lan_ip='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var lan_mask='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "MASK", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var lan_gateway='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "GATEWAY", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        // dhcp be care
        print("DHCP")
        cccc = statusHtmData.components(separatedBy: "var choice=")
        //        print("cccc.count:\(cccc.count)")
        //        for c in cccc{
        //            print("c@cccc:\(c)")
        //        }
        cccc2 = cccc[1].components(separatedBy: ";")
        let choice = cccc2[0]
        
        cccc = statusHtmData.components(separatedBy: "if ( choice == \(choice) ) document.write(status_")
        cccc2 = cccc[1].components(separatedBy: ");")
        // mike mark 0602
        //        print("cccc[1]:\(cccc[1])")
        let indices = [0]
        print(cccc2[0].uppercased(at: indices))
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "DHCP_SERVER", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased(at: indices))"))
        
        cccc = statusHtmData.components(separatedBy: "bssid_drv[0] ='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForLan.append(NetworkConfiguration(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased())"))
        
        //var wan_current='
        typesForWan = []
        cccc = statusHtmData.components(separatedBy: "var wan_current='")
        cccc2 = cccc[1].components(separatedBy: "';")
        cccc = cccc2[0].components(separatedBy: "_")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "AIPP", table: .dataTitle), detailLabel: "\(cccc[1].uppercased())"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_ip='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "IP", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_subMask='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "MASK", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_gateway='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "GATEWAY", table: .dataTitle), detailLabel: "\(cccc2[0])"))
        
        cccc = statusHtmData.components(separatedBy: "var wan_mac='")
        cccc2 = cccc[1].components(separatedBy: "';")
        typesForWan.append(NetworkConfiguration(titleLabel: localString(key: "MAC", table: .dataTitle), detailLabel: "\(cccc2[0].uppercased())"))
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
