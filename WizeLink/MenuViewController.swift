//
//  MenuViewController.swift
//  WizeLink
//
//  Created by 蒼月喵 on 2020/4/6.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire

class MenuViewController: UIViewController {

    @IBOutlet weak var LeftMenu: UIView!

    var LeftMenuViewController: LeftMenuViewController?

    var ConnectViewController: ConnectViewController?

    @IBOutlet var ConnectView: UIView!
    @IBOutlet var WirelessView: UIView!
    @IBOutlet var StatusView: UIView!
    @IBOutlet var ParentsView: UIView!
    @IBOutlet var OtherView: UIView!

    var tempView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("=== Start Menu ===")
        self.getStatusHtm(){

            // load temp data for server error
            if serverStatus == nil {
                print("serverStatus:\(String(describing: serverStatus))")
                statusHtmData = tempStatusHtmData
                clientsHtmData = tempClientsHtmData
            }else{
                print("statusHtmData:\(statusHtmData)")
                print("clientsHtmData:\(clientsHtmData)")
            }
        }

        setViews()

    }

    func setViews(){
        LeftMenu.snp.makeConstraints { (makes) in
            makes.width.equalTo(fullScreenSize.width * MenuWidth)
            makes.height.equalTo(fullScreenSize.height)
            makes.top.left.equalToSuperview()
        }


        view.addSubview(WirelessView)
        WirelessView.snp.makeConstraints { (makes) in
            makes.edges.equalToSuperview()
        }
    }

    func getStatusHtm(completion : @escaping ()->()){
        let user = "admin"
        let password = "admin"
        let headers: HTTPHeaders = [.authorization(username: user, password: password)]
        print("In getStatusHtm @ MenuVC")
        AF.request("http://192.168.10.1/status.htm",headers: headers)
            .responseJSON { response in
                let res = String(response.debugDescription)
                statusHtmData = res

        }

        AF.request("http://192.168.10.1/clients.htm",headers: headers)
            .responseJSON { response in
                let res = String(response.debugDescription)
                clientsHtmData = res
                print("test1111111")
                if response.response == nil{
                    serverStatus = false
                }else{
                    serverStatus = true
                }


        }


        print("Leave getStatusHtm @ MenuVC")
        completion()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        print(segue.identifier)

        if let vc1 = segue.destination as? LeftMenuViewController {
            LeftMenuViewController = vc1
            LeftMenuViewController?.MenuDelegate = self
            //            print("hihihi")
        }
        if segue.identifier == "WirelessSegue" {
            let vc = segue.destination.children[0] as? WirelessViewController
            //            WirelessViewController = vc
            vc?.ButtonDelegate = self
            //            print("hihihi2")
        }
        else if segue.identifier == "ConnectSegue" {
            let vc = segue.destination.children[0] as? ConnectViewController
            vc?.ButtonDelegate = self
            ConnectViewController = vc
            //            print("hihihi3")
        }else if segue.identifier == "StatusSegue"{
            let vc = segue.destination.children[0] as? StatusViewController
            //            WirelessViewController = vc
            vc?.ButtonDelegate = self
        }

    }

}

extension  MenuViewController: MenuDelegate {

    func reloadAfterLoad(){
        print("reloadAfterLoad")
        let workingView = view.subviews.last

        let tmpView = ConnectView
        if workingView != tmpView {
            self.view.addSubview(tmpView!)
            tmpView!.snp.makeConstraints { (makes) in
                makes.edges.equalToSuperview()
            }

            workingView?.removeFromSuperview()
        }else{
            tmpView!.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()

            self.view.addSubview(tmpView!)

            //            CatalogMenuViewController?.viewDidLoad()
            tmpView!.subviews.last?.menu()
        }
    }

    func changeUserMenuView(_ viewNum: Int) {
        print("viewNum:\(viewNum)")
        var tmpView: UIView!
        let workingView = view.subviews.last

        if viewNum == 0 {
            //            tmpView = ConnectView
            tmpView = WirelessView
            if workingView != tmpView {
                print("enter test")
                workingView?.removeFromSuperview()
                view.addSubview(WirelessView)
                WirelessView.snp.makeConstraints { (makes) in
                    makes.top.bottom.width.equalToSuperview()
                    makes.left.equalToSuperview().offset(fullScreenSize.width * MenuWidth)
                }
                //                changeUserMenuView(0)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                    //                    print("时间2：", Date())
                    self.changeUserMenuView(0)
                }
                return
            }

        }
        else if viewNum == 1 {
            tmpView = ConnectView
            //            tmpView = WirelessView
        }
        else if viewNum == 2 {
            tmpView = StatusView
        }
        else if viewNum == 3 {
            tmpView = ParentsView
        }
        else if viewNum == 4 {
            tmpView = OtherView
        }
        else {
            return
        }

        //        print("tmpView.tag:\(tmpView.tag)")
        //        print("workingView?.frame\(workingView?.frame)")
        //        print("workingView.tag:\(workingView?.tag)")

        if workingView != tmpView {
            print("workingView != tmpView")
            //            print("removeFromSuperview :\(workingView)")
            tmpView.frame = (workingView?.frame)!
            workingView?.removeFromSuperview()

            self.view.addSubview(tmpView)
            print("tmpView.frame:\(tmpView.frame)")
            tmpView.subviews.last?.menu()
            print("tmpView.frame After menu():\(tmpView.frame)")
        }else{
            print("workingView == tmpView")
            tmpView.subviews.last?.menu()
        }


        //        UIView.animate(withDuration: 0.5) {
        //            self.superview?.frame = frame!
        //        }
    }

    //    @IBAction func handleEdgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
    //        let location = sender.location(in: nil)
    //        navigationController?.view.slideByFinger(location: location, state: sender.state)
    //        ButtonDelegate?.clickLeft()
    //    }

    @IBAction func handleTapGesture(_ sender: Any) {
        if navigationController?.view.superview?.frame.origin.x != 0 {
            navigationController?.view.menu()
        }
    }
}

extension  MenuViewController: ButtonDelegate{
    func clickLeft() {
        print("clickLeft")
        self.LeftMenu.isHidden = false

    }
    func clickRight() {
        self.LeftMenu.isHidden = true
    }
}


