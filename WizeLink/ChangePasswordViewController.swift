//
//  ChangePasswordViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/4/7.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    
    let navBarLabel = UILabel()
    let navigationBarView = UIImageView()
    let bkgImageView2 = UIImageView()
    let backButton = UIButton()
    let passwordLabel = UILabel()
    let passwordTextField = UITextField()
    let passwordLabel2 = UILabel()
    let passwordTextField2 = UITextField()
    let changeButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bkgImageView2.backgroundColor = .white
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }
        
        let navigationBarHeight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        
        navigationBarView.backgroundColor = PrimaryColor1
        self.view.addSubview(navigationBarView)
        navigationBarView.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(0)
            makes.width.equalTo(fullScreenSize.width)
            makes.height.equalTo(navigationBarHeight)
        }
        
        navBarLabel.backgroundColor = .clear
        navBarLabel.attributedText = NSAttributedString(string: "變更登入密碼", attributes:
            nil)
        navBarLabel.font = UIFont.boldSystemFont(ofSize: 17)
        navBarLabel.textColor = .white
        navBarLabel.textAlignment = .center
        self.view.addSubview(navBarLabel)
        
        navBarLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }
        
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.backVC),for: .touchUpInside)
        self.view.addSubview(backButton)
        
        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight/1.5)
            makes.left.equalToSuperview().offset(spacing2)
        }
        
        passwordLabel.backgroundColor = .clear
        passwordLabel.attributedText = NSAttributedString(string: "新登入密碼", attributes:
            nil)
        passwordLabel.font = UIFont.boldSystemFont(ofSize: labelFontSize+2)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(navigationBarHeight*1.5)
            makes.width.equalTo(labelWidth*4)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing*2)
        }
        
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = TextColor2
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        passwordTextField.delegate = self
        self.view.addSubview(passwordTextField)
        
        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*4)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        passwordLabel2.backgroundColor = .clear
        passwordLabel2.attributedText = NSAttributedString(string: "再次輸入密碼", attributes:
            nil)
        passwordLabel2.font = UIFont.boldSystemFont(ofSize: labelFontSize+2)
        passwordLabel2.textColor = PrimaryColor2
        passwordLabel2.textAlignment = .left
        self.view.addSubview(passwordLabel2)
        passwordLabel2.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing2*2)
            makes.width.equalTo(labelWidth*4)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing*2)
        }
        
        passwordTextField2.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField2.textAlignment = .left
        passwordTextField2.clearButtonMode = .whileEditing
        passwordTextField2.returnKeyType = .done
        passwordTextField2.textColor = .white
        passwordTextField2.backgroundColor = TextColor2
        passwordTextField2.layer.cornerRadius = 1.0
        passwordTextField2.keyboardType = .namePhonePad
        passwordTextField2.delegate = self
        self.view.addSubview(passwordTextField2)
        
        passwordTextField2.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel2.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*4)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        
        changeButton.backgroundColor = SecondaryColor3
               changeButton.layer.cornerRadius = 23
               changeButton.setTitle("確認變更", for: [])
               changeButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: labelFontSize+2)
               changeButton.titleLabel?.textColor = .white
               changeButton.isEnabled = true
               changeButton.addTarget(self,action: #selector(self.clickChangeButton),for: .touchUpInside)
               self.view.addSubview(changeButton)
               
               changeButton.snp.makeConstraints { (makes) in
                   makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordTextField2.snp.bottom).offset(fullScreenSize.height*0.07)
                   makes.width.equalTo(fullScreenSize.width*0.45)
                   makes.height.equalTo(fullScreenSize.height*0.06)
               }
        
            hideKeyboardWhenTappedAround()
            
        }
        
        func hideKeyboardWhenTappedAround() {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            view.addGestureRecognizer(tap)
        }
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textFieldResignFirstResponder()
            self.view.endEditing(true)
            return true
        }
        func textFieldResignFirstResponder() {
            passwordTextField.resignFirstResponder()
            passwordTextField2.resignFirstResponder()
        }
        
        @objc func clickChangeButton(){
            let controller = UIAlertController(title: "無效", message: "登入密碼不可為空白", preferredStyle: .alert)
                 let cancelAction = UIAlertAction(title: "確認", style: .cancel, handler: nil)
                 controller.addAction(cancelAction)
                 present(controller, animated: true, completion: nil)
        }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
