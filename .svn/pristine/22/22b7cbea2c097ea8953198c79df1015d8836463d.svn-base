//
//  LoginViewController.swift
//  WizeLink
//
//  Created by 洋洋 on 2020/3/26.
//  Copyright © 2020 YangYang. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate {

    let backButton = UIButton()
    let addAccountButton = UIButton()
    let compatibilityButton = UIButton()
    let loginButton = UIButton()
    let wifiLabel = UILabel()
    let bkgImageView = UIImageView()
    let bkgImageView2 = UIImageView()
    let loginLabel = UILabel()
    let WizeLinkLOGO = UIImageView()
    //    let general_Login = UIButton()
    //    let cloud_Login = UIButton()
    let loginSegmentedControl = UISegmentedControl(
        items: ["一般登入","雲端登入"])
    let accountLabel = UILabel()
    let passwordLabel = UILabel()
    var accountTextField = UITextField()
    var passwordTextField = UITextField()
    let moduleLabel = UILabel()
    let routerMode = UIButton()
    let inputSwitch = UISwitch()
    let inputLabel = UILabel()
    let emailLabel = UILabel()
    var emailTextField = UITextField()
    let cloudPasswordLabel = UILabel()
    var cloudPasswordTextField = UITextField()

    var constraint1 : Constraint?
    var constraint2 : Constraint?
    var constraint3 : Constraint?


    override func viewDidLoad() {
        super.viewDidLoad()
    
        setBackGroundView()
        setFirstLayout()
        setLoginLayout()
        hideKeyboardWhenTappedAround()
        
        let accountText =
            UserDefaults.standard.string(forKey: "accountText")
        accountTextField.text = accountText
        let passwordText =
            UserDefaults.standard.string(forKey: "passwordText")
        passwordTextField.text = passwordText
        
    }

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldResignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldResignFirstResponder() {
        accountTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        cloudPasswordTextField.resignFirstResponder()
    }

    func setBackGroundView(){
        //UIColorbkgImageView
        bkgImageView.image = UIImage(named: "所有產品線稿")
        bkgImageView.alpha = 1
        self.view.addSubview(bkgImageView)
        bkgImageView.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }

        bkgImageView2.backgroundColor = PrimaryColor1
        bkgImageView2.alpha = 0
        self.view.addSubview(bkgImageView2)
        bkgImageView2.snp.makeConstraints{ (makes) in
            makes.edges.equalToSuperview()
        }

    }

    func setFirstLayout(){
        WizeLinkLOGO.alpha = 1
        WizeLinkLOGO.backgroundColor = .clear
        WizeLinkLOGO.image = UIImage(named: "WizeLink-LOGO")
        self.view.addSubview(WizeLinkLOGO)

        WizeLinkLOGO.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.24)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.075)
        }

        loginButton.backgroundColor = SecondaryColor3
        loginButton.layer.cornerRadius = 23
        loginButton.setTitle("登入", for: [])
        loginButton.titleLabel?.font = UIFont(name: "Helvetica-Light", size: fontSize)
        loginButton.titleLabel?.textColor = .white
        loginButton.isEnabled = true
        loginButton.addTarget(self,action: #selector(self.clickLoginButton),for: .touchUpInside)
        self.view.addSubview(loginButton)

        loginButton.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.75)
            makes.width.equalTo(fullScreenSize.width*0.54)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }

        wifiLabel.backgroundColor = .clear
        wifiLabel.attributedText = NSAttributedString(string: "請先確認裝置已連接WiFi", attributes:
            nil)
        wifiLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        wifiLabel.textColor = .black
        wifiLabel.textAlignment = .center
        //        wifiLabel.layer.masksToBounds = true
        self.view.addSubview(wifiLabel)

        wifiLabel.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.81+20)
            makes.width.equalTo(fullScreenSize.width*0.6)
        }

    }

    func setLoginLayout(){

        backButton.alpha = 0
        backButton.isHidden = true
        backButton.setImage(UIImage(named: "go-back"), for: .normal)
        backButton.isEnabled = true
        backButton.addTarget(self,action: #selector(self.clickBackButton),for: .touchUpInside)
        self.view.addSubview(backButton)

        backButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing2)
        }

        //        compatibilityButton.backgroundColor = .red
        compatibilityButton.alpha = 0
        compatibilityButton.isHidden = true
        compatibilityButton.setImage(UIImage(named: "S__32505862"), for: .normal)
        compatibilityButton.isEnabled = true
        compatibilityButton.addTarget(self,action: #selector(self.clickCompatibility),for: .touchUpInside)
        self.view.addSubview(compatibilityButton)

        compatibilityButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight)
            makes.right.equalToSuperview().offset(-spacing2)
        }

        //        addAccountButton.backgroundColor = .black
        addAccountButton.alpha = 0
        addAccountButton.isHidden = true
        addAccountButton.setImage(UIImage(named: "S__32505860"), for: .normal)
        addAccountButton.isEnabled = true
        addAccountButton.addTarget(self,action: #selector(self.clickAddAccount),for: .touchUpInside)
        self.view.addSubview(addAccountButton)

        addAccountButton.snp.makeConstraints { (makes) in
            makes.top.equalToSuperview().offset(loginLabelHeight)
            makes.width.height.equalTo(loginLabelHeight+2)
            makes.right.equalTo(compatibilityButton.snp.left).offset(-spacing2)
        }

        loginLabel.alpha = 0
        loginLabel.backgroundColor = .clear
        //Mike
              loginLabel.tappable = true
              loginLabel.callback = {
                  self.forTest()
              }
        loginLabel.attributedText = NSAttributedString(string: "登入", attributes:
            nil)
        loginLabel.font = UIFont(name: "Helvetica-Light", size: fontSize+2)
        loginLabel.textColor = .white
        loginLabel.textAlignment = .center
        self.view.addSubview(loginLabel)
        loginLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(backButton.snp.bottom).offset(spacing)
            makes.width.equalTo(loginLabelWidth)
            makes.height.equalTo(loginLabelHeight)
            makes.left.equalToSuperview().offset(spacing)
        }

        loginSegmentedControl.isHidden = true
        loginSegmentedControl.alpha = 0
        loginSegmentedControl.selectedSegmentIndex = 0
        // 設置外觀顏色 預設為藍色
        loginSegmentedControl.tintColor = .white
        // 設置底色 沒有預設的顏色
        loginSegmentedControl.layer.cornerRadius = 2
        loginSegmentedControl.layer.borderWidth = 1.5
        loginSegmentedControl.layer.borderColor = SecondaryColor3.cgColor
        loginSegmentedControl.backgroundColor = SecondaryColor3

        loginSegmentedControl.addTarget(
            self,
            action:
            #selector(self.onChange2),
            for: .valueChanged)
        self.view.addSubview(loginSegmentedControl)

        loginSegmentedControl.snp.makeConstraints { (makes) in
            makes.top.equalTo(loginLabel.snp.bottom).offset(spacing)
            makes.left.equalToSuperview().offset(spacing)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.047)
        }

        //        general_Login.alpha = 0
        //        general_Login.isHidden = true
        //        general_Login.isEnabled = true
        //        general_Login.setTitle("一般登入", for:.normal) //普通状态下的文字
        //        general_Login.setTitle("一般登入", for:.highlighted) //触摸状态下的文字
        //        general_Login.setTitleColor(UIColor.white, for: .normal) //普通状态下文字的颜色
        //        general_Login.setTitleColor(UIColor.white, for: .highlighted) //触摸状态下文字的颜色
        //        general_Login.backgroundColor = SecondaryColor3
        //        general_Login.addTarget(self,action: #selector(self.clickGeneralLogin),for: .touchUpInside)
        //        self.view.addSubview(general_Login)
        //
        //        general_Login.snp.makeConstraints { (makes) in
        //            makes.top.equalTo(loginLabel.snp.bottom).offset(spacing)
        //            makes.left.equalToSuperview().offset(spacing)
        //            makes.width.equalTo((fullScreenSize.width-(spacing)*2)/2)
        //            makes.height.equalTo(fullScreenSize.height*0.043)
        //        }
        //
        //        cloud_Login.alpha = 0
        //        cloud_Login.isHidden = true
        //        cloud_Login.isEnabled = true
        //        cloud_Login.setTitle("雲端登入", for:.normal) //普通状态下的文字
        //        cloud_Login.setTitle("雲端登入", for:.highlighted) //触摸状态下的文字
        //        cloud_Login.setTitleColor(SecondaryColor3, for: .normal) //普通状态下文字的颜色
        //        cloud_Login.setTitleColor(UIColor.white, for: .highlighted) //触摸状态下文字的颜色
        //        cloud_Login.backgroundColor = UIColor.clear
        //        cloud_Login.addTarget(self,action: #selector(self.clickCloudLogin),for: .touchUpInside)
        //        self.view.addSubview(cloud_Login)
        //
        //        cloud_Login.snp.makeConstraints { (makes) in
        //            makes.top.equalTo(loginLabel.snp.bottom).offset(spacing)
        //            makes.left.equalTo(general_Login.snp.right).offset(0)
        //            makes.width.equalTo((fullScreenSize.width-(spacing)*2)/2)
        //            makes.height.equalTo(fullScreenSize.height*0.043)
        //        }

        inputSwitch.alpha = 0
        inputSwitch.isHidden = true
        inputSwitch.isOn = false
        // 設置滑桿鈕的顏色
        inputSwitch.thumbTintColor = PrimaryColor2
        // 設置未選取時( off )的外觀顏色
        inputSwitch.tintColor = .lightGray
        // 設置選取時( on )的外觀顏色
        inputSwitch.onTintColor = TextColor2

        inputSwitch.addTarget(
            self,
            action:
            #selector(self.onChange),
            for: .valueChanged)
        self.view.addSubview(inputSwitch)

        inputSwitch.snp.makeConstraints { (makes) in
            //            makes.top.equalTo(cloud_Login.snp.bottom).offset(spacing)
            //            makes.right.equalTo(cloud_Login.snp.right).offset(-spacing)
            makes.top.equalTo(loginSegmentedControl.snp.bottom).offset(spacing)
            makes.right.equalTo(loginSegmentedControl.snp.right).offset(-spacing)
        }

        inputLabel.alpha = 0
        inputLabel.backgroundColor = .clear
        inputLabel.attributedText = NSAttributedString(string: "手動輸入", attributes:
            nil)
        inputLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        inputLabel.textColor = SecondaryColor3
        inputLabel.textAlignment = .center
        self.view.addSubview(inputLabel)
        inputLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(inputSwitch.snp.top)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalTo(loginSegmentedControl.snp.left).offset(spacing)
            //           makes.left.equalTo(general_Login.snp.left).offset(spacing)
        }

        emailLabel.alpha = 0
        emailLabel.backgroundColor = .clear
        emailLabel.attributedText = NSAttributedString(string: "電子郵件地址", attributes:
            nil)
        emailLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        emailLabel.textColor = PrimaryColor2
        emailLabel.textAlignment = .left
        self.view.addSubview(emailLabel)

        emailLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(inputSwitch.snp.bottom).offset(spacing)
            makes.width.equalTo(labelWidth*2)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }

        emailTextField.alpha = 0
        emailTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        emailTextField.textAlignment = .left
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.returnKeyType = .done
        emailTextField.textColor = .white
        emailTextField.backgroundColor = LightSecondaryColor
        emailTextField.layer.cornerRadius = 1.0
        emailTextField.keyboardType = .namePhonePad
        emailTextField.delegate = self
        self.view.addSubview(emailTextField)

        emailTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(emailLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }

        cloudPasswordLabel.alpha = 0
        cloudPasswordLabel.backgroundColor = .clear
        cloudPasswordLabel.attributedText = NSAttributedString(string: "雲端密碼", attributes:
            nil)
        cloudPasswordLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        cloudPasswordLabel.textColor = PrimaryColor2
        cloudPasswordLabel.textAlignment = .left
        self.view.addSubview(cloudPasswordLabel)

        cloudPasswordLabel.snp.makeConstraints { (makes) in
            //mike
            makes.top.equalTo(emailTextField.snp.bottom).offset(spacing)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }

        cloudPasswordTextField.alpha = 0
        cloudPasswordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        cloudPasswordTextField.textAlignment = .left
        cloudPasswordTextField.clearButtonMode = .whileEditing
        cloudPasswordTextField.returnKeyType = .done
        cloudPasswordTextField.textColor = .white
        cloudPasswordTextField.backgroundColor = LightSecondaryColor
        cloudPasswordTextField.layer.cornerRadius = 1.0
        cloudPasswordTextField.keyboardType = .namePhonePad
        cloudPasswordTextField.delegate = self
        cloudPasswordTextField.isSecureTextEntry = true
        self.view.addSubview(cloudPasswordTextField)

        cloudPasswordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(cloudPasswordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }


        accountLabel.alpha = 0
        accountLabel.backgroundColor = .clear
        accountLabel.attributedText = NSAttributedString(string: "登入名稱", attributes:
            nil)
        accountLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        accountLabel.textColor = PrimaryColor2
        accountLabel.textAlignment = .left
        self.view.addSubview(accountLabel)

        accountLabel.snp.makeConstraints { (makes) in
            constraint1 = makes.top.equalTo(inputSwitch.snp.bottom).offset(spacing).constraint
            constraint2 = makes.top.equalTo(cloudPasswordTextField.snp.bottom).offset(spacing2).constraint
            constraint3 = makes.top.equalTo(inputLabel.snp.bottom).offset(0).constraint

            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }

        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()

        accountTextField.alpha = 0
        accountTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        accountTextField.textAlignment = .left
        accountTextField.clearButtonMode = .whileEditing
        accountTextField.returnKeyType = .done
        accountTextField.textColor = .white
        accountTextField.backgroundColor = LightSecondaryColor
        accountTextField.layer.cornerRadius = 1.0
        accountTextField.keyboardType = .namePhonePad
        //        accountTextField.addTarget(self, action: #selector(tapAccTextField), for: UIControl.Event.touchDown)
        accountTextField.delegate = self
        self.view.addSubview(accountTextField)

        accountTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }

        passwordLabel.alpha = 0
        passwordLabel.backgroundColor = .clear
        passwordLabel.attributedText = NSAttributedString(string: "登入密碼", attributes:
            nil)
        passwordLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        passwordLabel.textColor = PrimaryColor2
        passwordLabel.textAlignment = .left
        self.view.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }

        passwordTextField.alpha = 0
        passwordTextField.font = UIFont(name: "Helvetica-Light", size: fontSize)
        passwordTextField.textAlignment = .left
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.returnKeyType = .done
        passwordTextField.textColor = .white
        passwordTextField.backgroundColor = LightSecondaryColor
        passwordTextField.layer.cornerRadius = 1.0
        passwordTextField.keyboardType = .namePhonePad
        //        accountTextField.addTarget(self, action: #selector(tapAccTextField), for: UIControl.Event.touchDown)
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        self.view.addSubview(passwordTextField)

        passwordTextField.snp.makeConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }

        moduleLabel.alpha = 0
        moduleLabel.backgroundColor = .clear
        moduleLabel.attributedText = NSAttributedString(string: "模組", attributes:
            nil)
        moduleLabel.font = UIFont(name: "Helvetica-Light", size: labelFontSize)
        moduleLabel.textColor = PrimaryColor2
        moduleLabel.textAlignment = .left
        self.view.addSubview(moduleLabel)
        moduleLabel.snp.makeConstraints { (makes) in
            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }

        routerMode.alpha = 1
        routerMode.isHidden = true
        routerMode.isEnabled = true
        routerMode.setTitle("路由器模式", for: .normal)
        routerMode.addTarget(self,action: #selector(self.clickRouter),for: .touchUpInside)
        self.view.addSubview(routerMode)
        routerMode.snp.makeConstraints { (makes) in
            makes.centerY.equalTo(moduleLabel)
            makes.width.equalTo(labelWidth*1.5)
            makes.height.equalTo(loginLabelHeight)
            makes.right.equalTo(passwordTextField.snp.right).offset(0)
        }

    }

    func layout_isOn() {
        moduleLabel.alpha = 0
        routerMode.isHidden = true
        emailLabel.alpha = 1
        emailTextField.alpha = 1
        cloudPasswordLabel.alpha = 1
        cloudPasswordTextField.alpha = 1
        emailTextField.isHidden = false
        cloudPasswordTextField.isHidden = false

        constraint1?.deactivate()
        constraint2?.activate()
        constraint3?.deactivate()

        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
        }

        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.85)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }

    }

    func layout_isOff() {
        moduleLabel.alpha = 0
        routerMode.isHidden = true
        emailLabel.alpha = 0
        emailTextField.alpha = 0
        cloudPasswordLabel.alpha = 0
        cloudPasswordTextField.alpha = 0
        emailTextField.isHidden = true
        cloudPasswordTextField.isHidden = true

        constraint1?.activate()
        constraint2?.deactivate()
        constraint3?.deactivate()

        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            //            makes.top.equalTo(passwordTextField.snp.bottom).offset(spacing)
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.6)
            makes.width.equalTo(fullScreenSize.width*0.4)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }

    }

    @objc func onChange2(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            clickGeneralLogin()
        }else{
            clickCloudLogin()
        }
        // 印出這個選項的文字
//        print(sender.titleForSegment(at: sender.selectedSegmentIndex))
    }

    @objc func onChange(sender: UISwitch) {
        if sender.isOn == true{
            layout_isOn()
        } else {
            layout_isOff()
        }
    }

    @objc func clickRouter(){
        alertMsg5(title: "" , msg: "模組" , titleAction1: "路由器模式" , titleAction2: "橋接器模式")
    }

    @objc func clickGeneralLogin(){
        //        cloud_Login.setTitleColor(SecondaryColor3, for: .normal) //普通状态下文字的颜色
        //        cloud_Login.backgroundColor = UIColor.clear
        //        general_Login.setTitleColor(UIColor.white, for: .normal) //普通状态下文字的颜色
        //        general_Login.backgroundColor = SecondaryColor3

        emailLabel.alpha = 0
        emailTextField.alpha = 0
        cloudPasswordLabel.alpha = 0
        cloudPasswordTextField.alpha = 0
        emailTextField.isHidden = true
        cloudPasswordTextField.isHidden = true
        inputLabel.alpha = 0
        inputSwitch.alpha = 0
        inputSwitch.isHidden = true

        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()

        accountTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        passwordLabel.snp.updateConstraints { (makes) in
            makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
            makes.width.equalTo(labelWidth)
            makes.height.equalTo(spacing)
            makes.left.equalToSuperview().offset(spacing)
        }
        passwordTextField.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
            makes.width.equalTo(fullScreenSize.width-(spacing)*2)
            makes.height.equalTo(fullScreenSize.height*0.07)
        }
        self.loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.6)
            makes.width.equalTo(fullScreenSize.width*0.72)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }

        backButton.isHidden = false
        backButton.alpha = 1
        wifiLabel.alpha = 0
        bkgImageView.alpha = 0
        bkgImageView2.alpha = 1
        loginLabel.alpha = 1
        WizeLinkLOGO.alpha = 0
        //        general_Login.alpha = 1
        //        general_Login.isHidden = false
        //        cloud_Login.alpha = 1
        //        cloud_Login.isHidden = false
        accountLabel.alpha = 1
        passwordLabel.alpha = 1
        accountTextField.alpha = 1
        accountTextField.isHidden = false
        passwordTextField.alpha = 1
        passwordTextField.isHidden = false
        moduleLabel.alpha = 1
        routerMode.alpha = 1
        routerMode.isHidden = false

    }

    @objc func clickCloudLogin(){
        //        general_Login.setTitleColor(SecondaryColor3, for: .normal) //普通状态下文字的颜色
        //        general_Login.backgroundColor = UIColor.clear
        //        cloud_Login.setTitleColor(UIColor.white, for: .normal) //普通状态下文字的颜色
        //        cloud_Login.backgroundColor = SecondaryColor3

        inputLabel.alpha = 1
        inputSwitch.alpha = 1
        inputSwitch.isHidden = false

        if inputSwitch.isOn == true{
            layout_isOn()
        }else{
            layout_isOff()
        }

    }

    @objc func clickCompatibility(){
        let vc = CompatibilityViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

    @objc func clickAddAccount(){
        let vc = AddAccountViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

    @objc func clickBackButton(){

        constraint3?.activate()
        constraint1?.deactivate()
        constraint2?.deactivate()

        loginSegmentedControl.selectedSegmentIndex = 0

        loginButton.snp.updateConstraints { (makes) in
            makes.centerX.equalToSuperview()
            makes.top.equalToSuperview().offset(fullScreenSize.height*0.72)
            makes.width.equalTo(fullScreenSize.width*0.54)
            makes.height.equalTo(fullScreenSize.height*0.06)
        }
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            self?.view.layoutIfNeeded()
            self?.backButton.isHidden = true
            self?.compatibilityButton.alpha = 0
            self?.compatibilityButton.isHidden = true
            self?.addAccountButton.alpha = 0
            self?.addAccountButton.isHidden = true
            self?.wifiLabel.alpha = 1
            self?.loginLabel.alpha = 0
            self?.backButton.alpha = 0
            self?.bkgImageView.alpha = 1
            self?.bkgImageView2.alpha = 0
            self?.WizeLinkLOGO.alpha = 1
            //            self?.general_Login.alpha = 0
            //            self?.general_Login.isHidden = true
            //            self?.cloud_Login.alpha = 0
            //            self?.cloud_Login.isHidden = true
            self?.accountLabel.alpha = 0
            self?.passwordLabel.alpha = 0
            self?.accountTextField.alpha = 0
            self?.accountTextField.isHidden = true
            self?.passwordTextField.alpha = 0
            self?.passwordTextField.isHidden = true
            self?.moduleLabel.alpha = 0
            self?.routerMode.alpha = 0
            self?.routerMode.isHidden = true
            self?.inputSwitch.alpha = 0
            self?.inputSwitch.isHidden = true
            self?.inputLabel.alpha = 0
            self?.emailLabel.alpha = 0
            self?.cloudPasswordLabel.alpha = 0
            self?.emailTextField.alpha = 0
            self?.emailTextField.isHidden = true
            self?.cloudPasswordTextField.alpha = 0
            self?.cloudPasswordTextField.isHidden = true
            self?.loginSegmentedControl.alpha = 0
            self?.loginSegmentedControl.isHidden = true
        })

    }

    func forTest(){
        accountTextField.text = "admin"
        passwordTextField.text = "admin"
    }

    @objc func clickLoginButton(){

        if bkgImageView2.alpha == 1{
            //Mike test
            if accountTextField.text == "admin" && passwordTextField.text == "admin"{
                 UserDefaults.standard.set(accountTextField.text,forKey: "accountText")
                 UserDefaults.standard.set(passwordTextField.text,forKey: "passwordText")

//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuView")
//                vc!.modalPresentationStyle = .fullScreen
//                self.present(vc!, animated: true, completion: nil)
                
                
//                UIView.animate(withDuration: 0.6, animations: { [weak self] in self?.view.layoutIfNeeded()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuView")
                vc!.modalPresentationStyle = .fullScreen
                guard let window = UIApplication.shared.keyWindow else {
                    return
                }
                self.dismiss(animated: true, completion: nil)
                window.rootViewController = vc
//                })
                return
            }

            if loginSegmentedControl.selectedSegmentIndex == 0{
                alertMsg3(title: "連線錯誤", msg: "請確認連線狀態是否正確")
            }else{
                if inputSwitch.isOn{
                    alertMsg3(title: "無效", msg: "電子郵件地址格式不正確")
                }else{
                    alertMsg3(title: "獲取設備IP", msg: "失敗")
                }
            }
        }else{
            //            cloud_Login.setTitleColor(SecondaryColor3, for: .normal) //普通状态下文字的颜色
            //            cloud_Login.backgroundColor = UIColor.clear
            //            general_Login.setTitleColor(UIColor.white, for: .normal) //普通状态下文字的颜色
            //            general_Login.backgroundColor = SecondaryColor3

            emailLabel.alpha = 0
            emailTextField.alpha = 0
            cloudPasswordLabel.alpha = 0
            cloudPasswordTextField.alpha = 0

            inputLabel.alpha = 0
            inputSwitch.alpha = 0
            inputSwitch.isHidden = true

            constraint3?.activate()
            constraint1?.deactivate()
            constraint2?.deactivate()

            accountTextField.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalTo(accountLabel.snp.bottom).offset(spacing2)
                makes.width.equalTo(fullScreenSize.width-(spacing)*2)
                makes.height.equalTo(fullScreenSize.height*0.07)
            }
            passwordLabel.snp.updateConstraints { (makes) in
                makes.top.equalTo(accountTextField.snp.bottom).offset(spacing2)
                makes.width.equalTo(labelWidth)
                makes.height.equalTo(spacing)
                makes.left.equalToSuperview().offset(spacing)
            }
            passwordTextField.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalTo(passwordLabel.snp.bottom).offset(spacing2)
                makes.width.equalTo(fullScreenSize.width-(spacing)*2)
                makes.height.equalTo(fullScreenSize.height*0.07)
            }

            self.loginButton.snp.updateConstraints { (makes) in
                makes.centerX.equalToSuperview()
                makes.top.equalToSuperview().offset(fullScreenSize.height*0.55)
                makes.width.equalTo(fullScreenSize.width*0.72)
                makes.height.equalTo(fullScreenSize.height*0.06)
            }

            UIView.animate(withDuration: 0.6, animations: { [weak self] in self?.view.layoutIfNeeded()
                self?.backButton.isHidden = false
                self?.backButton.alpha = 1
                self?.compatibilityButton.alpha = 1
                self?.compatibilityButton.isHidden = false
                self?.addAccountButton.alpha = 1
                self?.addAccountButton.isHidden = false
                self?.wifiLabel.alpha = 0
                self?.bkgImageView.alpha = 0
                self?.bkgImageView2.alpha = 1
                self?.loginLabel.alpha = 1
                self?.WizeLinkLOGO.alpha = 0
                //                self?.general_Login.alpha = 1
                //                self?.general_Login.isHidden = false
                //                self?.cloud_Login.alpha = 1
                //                self?.cloud_Login.isHidden = false
                self?.accountLabel.alpha = 1
                self?.passwordLabel.alpha = 1
                self?.accountTextField.alpha = 1
                self?.accountTextField.isHidden = false
                self?.passwordTextField.alpha = 1
                self?.passwordTextField.isHidden = false
                self?.moduleLabel.alpha = 1
                self?.routerMode.alpha = 1
                self?.routerMode.isHidden = false
                self?.loginSegmentedControl.alpha = 1
                self?.loginSegmentedControl.isHidden = false
            })

        }


        //            } else {

        //                self.actView.isHidden = false

        //                loginTest(uid: self.accountTextField.text!,pwd: self.passwordTextField.text!){
        //                    //                self.actView.isHidden = true
        //                }
        //            }

    }



    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
